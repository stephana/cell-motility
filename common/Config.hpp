//
// Created by bgirard on 22/03/23.
//

#ifndef ANGIOMODEL_CONFIG_HPP
#define ANGIOMODEL_CONFIG_HPP
#include <boost/program_options/options_description.hpp>
#include "list"
#include <iostream>

namespace bo = boost::program_options;

#define STR(a) #a
#define CC(a,b) a##b

#define PRINT_OPT(type,name) \
    dummy name ## _print = list_print.add(std::string(CLASS_NAME "." STR(name)),&name);\

#define MAKE_OPT_MIN_MAX(type,name,def,minv,maxv,descrption) \
    type name = def;static constexpr type name ## _min = minv;static constexpr type name ## _max = maxv; \
    bo::options_description_easy_init name ## _opts = opts.add_options()( CLASS_NAME "." STR(name),\
            bo::value<type>(&name)->default_value(def)->notifier(val_in(minv,maxv,CLASS_NAME "." STR(name)))\
            ,"In(" STR(minv) "," STR(maxv) ") " descrption); \
    PRINT_OPT(type,name)

#define MAKE_OPT(type,name,def,descrption) \
    type name = def; \
    bo::options_description_easy_init name ## _opts = opts.add_options()( CLASS_NAME "." STR(name),\
            bo::value<type>(&name)->default_value(def)\
            , descrption);                 \
    PRINT_OPT(type,name)

template<typename T,typename P>
auto val_in(T min, P max, char const * const opt_name){
  return [opt_name, min, max](T v){
    if(v < min || v > max){
      throw bo::validation_error
        (bo::validation_error::invalid_option_value,
         opt_name, std::to_string(v));
    }
  };
};

struct dummy
{};

struct printable_a
{
    virtual void show() const = 0;
    virtual ~printable_a(){};
};

//template<typename T>
//struct printable : printable_a
//{
//    printable(const std::string&o,const T*t_) : name(o),t(t_)
//    {}
//    virtual void show() const override
//    {
//
//        std::cout<<name<<"="<<*t<<std::endl;
//    };
//    std::string name;
//    const T * t;
//
//};

struct printable_list
{
    template<class T>
    dummy add(const std::string & o ,const T * ptr) {
        //_list.emplace_back(std::make_unique<printable<T>>(o,ptr));
        return dummy();
    }
    std::list<std::unique_ptr<printable_a>> _list;

    void print()
    {

       // for(auto& f : _list)
        //    f.get()->show();
    }
};

/**
 *
 * Inherit form this class to add configurable member :
 *
 *  Sample :
 *
 *
 Class MyParaList : Configurable
 {
    #define CLASS_NAME section_name_in_ini
    MAKE_OPT(int,myint,0,"help to show to use myint")

    ...
 }


 */
struct Configurable
{
    bo::options_description opts;
    printable_list list_print;
    // = bo::options_description("Configurable Option");

    Configurable()
    {}
    void show_option()
    {
        list_print.print();
    }
//    Configurable& operator=(Configurable other)
//    {
//        std::cout << "copy assignment of Configurable\n"<<std::endl;
//
//
//        return *this;
//    }
//    Configurable(Configurable const& other)
//    {
//        std::cout << "copy value of Configurable\n"<<std::endl;
//
//    }
};

#endif //ANGIOMODEL_CONFIG_HPP
