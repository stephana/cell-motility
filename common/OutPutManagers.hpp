//
// Created by bgirard on 13/03/23.
//

#ifndef ANGIOMODEL_OUTPUTMANAGERS_HPP
#define ANGIOMODEL_OUTPUTMANAGERS_HPP

#include <string>
#include <functional>
#include "list"
#include "Real.hpp"
#include "FastString.hpp"

class World;
class Substrate;
class N1;
class N2;
struct ModelParam;

class OutPutManager
{
public:
    virtual void start_model(const unsigned int& simulationNumber, ModelParam& param,World&) = 0;
    virtual void substrate_model(Substrate& substrate,const unsigned int& simulationNumber) = 0;
    virtual void step_model(const RealType& t , World &w) = 0;

    virtual void node_event(const N1& n, const FastStaticString& eventType, const FastStaticString& event) {};
    virtual void node_event(const N2& n, const FastStaticString& eventType, const FastStaticString& event) {} ;

    virtual void end_model() =0;
};

#define DECL_FUNCTION(fct) \
template<class... Types>   \
void   fct(Types&&... args)\
{\
        for(auto& m : _managers)\
        {\
            m.get().fct(args...);\
        }\
    }\

/*
 * Class that will notify plugin(OutPutManager) when event occurs
 * */
class OutPutManagers {
public:
    template<class T>
    void add(T & om){_managers.emplace_back(om);};

    DECL_FUNCTION(start_model)
    DECL_FUNCTION(end_model)
    DECL_FUNCTION(step_model)
    DECL_FUNCTION(node_event)
    DECL_FUNCTION(substrate_model)

private:
    std::list<std::reference_wrapper<OutPutManager>> _managers;

};


#endif //ANGIOMODEL_OUTPUTMANAGERS_HPP
