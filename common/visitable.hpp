//
// Created by bgirard on 14/03/23.
//

#ifndef ANGIOMODEL_VISITABLE_HPP
#define ANGIOMODEL_VISITABLE_HPP



#define VISITABLE() \
    virtual void visit(Visitor& v) override\
    {\
    v.visit(*this);  \
    }\

class Visitor;
class Visitable
{
private:
    virtual void visit(Visitor& v) = 0;
};
#include "Visitor.hpp"

#endif //ANGIOMODEL_VISITABLE_HPP
