//
// Created by bgirard on 30/03/23.
//

#ifndef ANGIOMODEL_FASTSTRING_HPP
#define ANGIOMODEL_FASTSTRING_HPP

#include <boost/beast/core/static_string.hpp>

class FastStaticString : public boost::beast::static_string<32>
{
public:
  FastStaticString(const std::string & a) : boost::beast::static_string<32>(a.c_str())
  {}
  FastStaticString(const char* a) : boost::beast::static_string<32>(a)
 {}
  FastStaticString() : boost::beast::static_string<32>()
  {}
};

#endif //ANGIOMODEL_FASTSTRING_HPP
