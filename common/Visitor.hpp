//
// Created by bgirard on 14/03/23.
//

#ifndef ANGIOMODEL_VISITOR_HPP
#define ANGIOMODEL_VISITOR_HPP

#define LIST_OF_VISITABLE \
    V(Substrate)          \
    V(SubstratePattern) \
    V(SubstratePattern_Circle) \
    V(SubstratePattern_Triangle)\
    V(SubstratePattern_Rectangle) \
                          \
    V(World)              \
    V(Cell)               \
    V(N0)               \
    V(N1)               \
    V(N2)               \



#define V(name) class name;
LIST_OF_VISITABLE
#undef V

class Visitor
{
public:
    #define V(name)  virtual void visit(name & v){};
    LIST_OF_VISITABLE
    #undef V


};

#endif //ANGIOMODEL_VISITOR_HPP
