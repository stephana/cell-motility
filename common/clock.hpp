//
// Created by bgirard on 08/03/23.
//

#ifndef ANGIOMODEL_CLOCK_HPP
#define ANGIOMODEL_CLOCK_HPP

#include <string>
#include <iomanip>
#include <chrono>
#include <ostream>
#include <sstream>



typedef  std::chrono::steady_clock MostAccurateClock;

template<typename T>
inline std::string format(T timeunit) {
  std::chrono::nanoseconds ns = duration_cast<std::chrono::nanoseconds>(timeunit);
  std::ostringstream os;
  bool foundNonZero  = false;
  os.fill('0');
  typedef std::chrono::duration<int, std::ratio<86400*365>> years;
  const auto y = duration_cast<years>(ns);
  if (y.count()) {
    foundNonZero = true;
    os << y.count() << "y:";
    ns -= y;
  }
  typedef std::chrono::duration<int, std::ratio<86400>> days;
  const auto d = duration_cast<days>(ns);
  if (d.count()) {
    foundNonZero = true;
    os << d.count() << "d:";
    ns -= d;
  }
  const auto h = duration_cast<std::chrono::hours>(ns);
  if (h.count() || foundNonZero) {
    foundNonZero = true;
    os << h.count() << "h:";
    ns -= h;
  }
  const auto m = duration_cast<std::chrono::minutes>(ns);
  if (m.count() || foundNonZero) {
    foundNonZero = true;
    os << m.count() << "m:";
    ns -= m;
  }
  const auto s = duration_cast<std::chrono::seconds>(ns);
  if (s.count() || foundNonZero) {
    foundNonZero = true;
    os << s.count() << "s:";
    ns -= s;
  }
  const auto ms = duration_cast<std::chrono::milliseconds>(ns);
  if (ms.count() || foundNonZero) {
    if (foundNonZero) {
      os << std::setw(3);
    }
    os << ms.count() << "ms";
    ns -= ms;
    foundNonZero = true;
  }
  const auto us = duration_cast<std::chrono::microseconds>(ns);
  if (us.count() || foundNonZero) {
    if (foundNonZero) {
      os << std::setw(3);
    }
    os << us.count() << ".";
    ns -= us;
  }
  os << std::setw(3) << ns.count() << "ns" ;
  return os.str();
}

inline std::ostream& operator<<(std::ostream&o,const MostAccurateClock::duration &d)
{
    o<<format(d);
    return o;
}


#endif //ANGIOMODEL_CLOCK_HPP
