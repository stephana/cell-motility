//
// Created by bgirard on 17/03/23.
//


#define FMT_USE_FULL_CACHE_DRAGONBOX 1
#include <fmt/format-inl.h>
#include <fmt/core.h>
#include <fmt/ostream.h>
#include "Real.hpp"
#include "random"
#include "iostream"
#include "clock.hpp"
void bench_fmt(const std::vector<RealType>& numbers)
{
   fmt::memory_buffer buf;
   buf.reserve(1000000*12);
    for(auto& nb : numbers) {
        fmt::format_to(std::back_inserter(buf),"{}",nb);
    }
}
void bench_to_string(const std::vector<RealType>& numbers)
{
    std::string bug;
    for(auto& nb : numbers) {
        bug+=std::to_string(nb);
    }
}

void bench()
{    // source : https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
    std::mt19937 gen;
    std::uniform_real_distribution<RealType> dis(-100.0, 100.0);
    std::vector<RealType> numbers;
    numbers.resize(1000000);
    for(auto& nb : numbers)
    {
        nb = dis(gen);
    }

    MostAccurateClock clk;
    {
        auto s = clk.now();
        bench_fmt(numbers);
        auto e = clk.now();
        std::cout << " FMT " << (e - s) << std::endl;
    }
        {
        auto s = clk.now();
        bench_to_string(numbers);
        auto e = clk.now();
        std::cout << " TOSTRING  " << (e - s) << std::endl;
    }
}