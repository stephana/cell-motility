//
// Created by bgirard on 21/03/23.
//

#include <gtest/gtest.h>
#include "Node/N2.hpp"
#include "Node/N1.hpp"
#include "Node/N0.hpp"
#include "UiOutput.hpp"
#include "ModelParam.hpp"


TEST(TestNode, TestForces)
{
    ModelParam dp;
    dp.l1_Generator = constant_B1_length_generator;
    dp.l2_Generator = constant_B2_length_generator;

    Eigen::Vector2<RealType> position_n0(0,0);

    N0 n0(dp,position_n0);

    Eigen::Vector2<RealType> position_n1(n0.generateLengthOfNewN1(),0);
    N1& n1 = n0._nodes.emplace_back(dp,n0,position_n1);

    Eigen::Vector2<RealType> position_n2(position_n1.x()+n1.generateLengthOfNewN2(),0);
    N2& n2 = n1._nodes.emplace_back(dp,n1,position_n2);




    Eigen::Vector2<RealType> forces_n2 = n2.compute_forces();

    EXPECT_NEAR(forces_n2.x(),-4*4,0.000001);
    EXPECT_NEAR(forces_n2.y(),0,0.000001);

    Eigen::Vector2<RealType> forces_n1 = n1.compute_forces();

    float age = 0;
    float n1_dis=0;

    EXPECT_NEAR(forces_n1.x(),4*4+(-((dp.k11 - dp.k10) / dp.tau_t * age+dp.k10)*(n1_dis)),0.000001);
    EXPECT_NEAR(forces_n1.y(),0,0.000001);

    Eigen::Vector2<RealType> forces_n0 = n0.compute_forces();


    EXPECT_NEAR(forces_n0.x(),0,0.000001);
    EXPECT_NEAR(forces_n0.y(),0,0.000001);

    n0.apply_forces();

    Eigen::Vector2<RealType> pos_n2 = n2.pos();

    //-16/8*dp.dt
    EXPECT_NEAR(pos_n2.x(),position_n2.x(),0.000001);
    EXPECT_NEAR(pos_n2.y(),0,0.000001);

    Eigen::Vector2<RealType> pos_n1 = n1.pos();
    EXPECT_NEAR(pos_n1.x(),position_n1.x()+16.0/50.0*dp.dt,0.000001);
    EXPECT_NEAR(pos_n1.y(),0,0.000001);

    Eigen::Vector2<RealType> pos_n0 = n0.pos();
    EXPECT_NEAR(pos_n0.x(),position_n0.x(),0.000001);
    EXPECT_NEAR(pos_n0.y(),0,0.000001);

    n0.step_and_kill();

    EXPECT_NEAR(n0.age,dp.dt,0.00000001);
    EXPECT_NEAR(n1.age,dp.dt,0.00000001);
    EXPECT_NEAR(n2.age,dp.dt,0.00000001);


    SUCCEED();

}

TEST(TestNode, TestForcesLong)
{
    ModelParam dp;
    dp.l1_Generator = constant_B1_length_generator;
    dp.l2_Generator = constant_B2_length_generator;

    Eigen::Vector2<RealType> position_n0(0,0);

    N0 n0(dp,position_n0);

    Eigen::Vector2<RealType> position_n1(n0.generateLengthOfNewN1(),0);
    N1& n1 = n0._nodes.emplace_back(dp,n0,position_n1);

    Eigen::Vector2<RealType> position_n2(position_n1.x()+n1.generateLengthOfNewN2(),0);
    //N2& n2 = n1._nodes.emplace_back(dp,n1,position_n2);


    for(unsigned i=0;i<300;i++) {
         n0.compute_forces();
        n0.apply_forces();
        n0.step_and_kill();

    }
        std::cout<<n0.pos()<<std::endl;
//    UiOutput ui(dp);
//    ui.draw_nodes(n0);
//    ui.wait_end();



    SUCCEED();

}