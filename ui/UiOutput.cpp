//
// Created by bgirard on 13/03/23.
//


#include "UiOutput.hpp"

#include <iostream>
#include "World.hpp"
#include "clock.hpp"
#include "Substrate.hpp"
#include "ModelParam.hpp"
#include "FastSahape.hpp"

using namespace std::chrono_literals;
UiOutput::UiOutput()
{

}
UiOutput::UiOutput(ModelParam &param) : _param(&param) {

}

void UiOutput::init()
{
    #if __APPLE__

    #else
        _event_manager = std::thread(&UiOutput::events,this);
    #endif
}

class DrawVisitor : public Visitor
{
public:
    unsigned int draw_order = 0;
    UiOutput& out;
    DrawVisitor(UiOutput& o) :out(o)
    {}
};

class DrawSubstrateVisitor : public DrawVisitor
{

public:
    DrawSubstrateVisitor(UiOutput& output) : DrawVisitor(output) {

    }

    void visit(Substrate& substrate)
    {
        substrate.visit_child(*this);
    }
    void visit(SubstratePattern_Circle& substrate)
    {
        // create an empty shape
        float radius_pixel = substrate.radius * out.PixelsPerMeter;
        std::unique_ptr<sf::CircleShape> circle  = std::make_unique<sf::CircleShape>(radius_pixel);
        // Applying values to SFML methods
        circle->setOrigin(radius_pixel, radius_pixel);
        circle->setRadius(radius_pixel);
        circle->setPosition(substrate.pos().x()*out.PixelsPerMeter,substrate.pos().y()*out.PixelsPerMeter);
        circle->setFillColor(out.colorSubstratePattern);
        out.draw_list_substrate.emplace_back(std::move(circle));
    }
    void visit(SubstratePattern_Triangle& substrate)
    {
        // create an empty shape
        std::unique_ptr<sf::ConvexShape > convex  = std::make_unique<sf::ConvexShape >();
        convex->setPointCount(3);
        Eigen::Vector2<RealType> na = substrate.nodeA*out.PixelsPerMeter;
        Eigen::Vector2<RealType> nb = substrate.nodeB*out.PixelsPerMeter;
        Eigen::Vector2<RealType> nc = substrate.nodeC*out.PixelsPerMeter;

        // Applying values to SFML methods
        convex->setPoint(0,sf::Vector2f(na.x(),na.y()));
        convex->setPoint(1,sf::Vector2f( nb.x(),nb.y()));
        convex->setPoint(2, sf::Vector2f( nc.x(),nc.y()));
        convex->setFillColor(out.colorSubstratePattern);
                out.draw_list_substrate.emplace_back(std::move(convex));
    }
        void visit(SubstratePattern_Rectangle& substrate)
    {
        // create an empty shape
        std::unique_ptr<sf::ConvexShape > convex  = std::make_unique<sf::ConvexShape >();
        convex->setPointCount(4);
        Eigen::Vector2<RealType> na = substrate.nodeA*out.PixelsPerMeter;
        Eigen::Vector2<RealType> nb = substrate.nodeB*out.PixelsPerMeter;
        Eigen::Vector2<RealType> nc = substrate.nodeC*out.PixelsPerMeter;
        Eigen::Vector2<RealType> nd = substrate.nodeD*out.PixelsPerMeter;

        // Applying values to SFML methods
        convex->setPoint(0,sf::Vector2f(na.x(),na.y()));
        convex->setPoint(1,sf::Vector2f( nb.x(),nb.y()));
        convex->setPoint(2, sf::Vector2f( nc.x(),nc.y()));
        convex->setPoint(3, sf::Vector2f( nd.x(),nd.y()));
        convex->setFillColor(out.colorSubstratePattern);
                        out.draw_list_substrate.emplace_back(std::move(convex));
    }

};

void UiOutput::substrate_model(Substrate& substrate,const unsigned int& simulationNumber)
{
    draw_list_substrate.clear();
    std::scoped_lock lock(mutex);
    DrawSubstrateVisitor v(*this);
    substrate.visit(v);

}

class DrawNodeVisitor : public DrawVisitor
{

public:
    DrawNodeVisitor(UiOutput& output) : DrawVisitor(output) {

    }



    void drawnode(unsigned int id,Node& n,float rad2,const sf::Color& color,double fill_ratio = 1.0,const sf::Color& outlineColor=sf::Color::Black)
    {
            float rad =  rad2*out.PixelsPerMeter;

            std::scoped_lock lock(out.mutex);
            update_or_create<CircleFastShape>(out.draw_list,id,draw_order++,rad,n.pos().x()*out.PixelsPerMeter,n.pos().y()*out.PixelsPerMeter,color,fill_ratio,outlineColor);

    }
    void drawTrapezoid(unsigned int id,const Node& n1,const Node& n2, float radius_nodeA, float radius_nodeB, sf::Color& color)
    {

        std::scoped_lock lock(out.mutex);
        update_or_create<Convex4FastShape>(out.draw_list,id,draw_order++,n1.pos().x()*out.PixelsPerMeter,n1.pos().y()*out.PixelsPerMeter
                                           ,n2.pos().x()*out.PixelsPerMeter,n2.pos().y()*out.PixelsPerMeter,
                                           color,radius_nodeA*out.PixelsPerMeter,radius_nodeB*out.PixelsPerMeter);

    }

    void drawLine(unsigned int id,const Node& n1,const Node& n2,float thickness, sf::Color& color)
    {


                      std::scoped_lock lock(out.mutex);
          update_or_create<LIneFastShape>(out.draw_list,id,draw_order++,n1.pos().x()*out.PixelsPerMeter,n1.pos().y()*out.PixelsPerMeter,
                                          n2.pos().x()*out.PixelsPerMeter,n2.pos().y()*out.PixelsPerMeter,
                                          color,thickness);

    }
    void draw_text_force_branch(unsigned int id,const Node& n1,const Node& n2)

    {
        std::scoped_lock lock(out.mutex);
        std::string text = std::to_string(n2.force());
        Eigen::Vector2<RealType> pos = (n1.pos()+n2.pos())/2*out.PixelsPerMeter;

        update_or_create<FastText>(out.draw_list,id,draw_order++,text,pos.x(),pos.y(),15,
                                          sf::Color(255,255,255,255));

    }
    void draw_text_force_branch(unsigned int id,const N0& n0,const N1& n1)

    {
        std::scoped_lock lock(out.mutex);
      ///  std::string text = std::to_string((n1.pos()-n0.pos()).norm());
        std::string text = std::to_string((n1._get_forces_from_n0).norm());
        Eigen::Vector2<RealType> pos = (n1.pos()+n0.pos())/2*out.PixelsPerMeter;

        update_or_create<FastText>(out.draw_list,id,draw_order++,text,pos.x(),pos.y(),15,
                                          sf::Color(255,255,255,255));

    }

    bool _visit_cylo=false;

    void visit(World& w)
    {


        w.visit_child(*this);
    }
    void visit(Cell& c)
    {
        c.visit_child(*this);
    }
    void visit(N0& n)
    {
        unsigned int id = ((unsigned int)(size_t)&n)*10;
        _visit_cylo = true;
        drawnode(id,n,out._param->radiusN0*out._param->cytoplasmScaleFactor,out.colorCytoplasm);

        n.visit_child(*this);
        _visit_cylo = false;
        n.visit_child(*this);

        drawnode(id+1,n,out._param->radiusN0,out.colorN0_fill,0.7,out.colorN0_outline);


    }

    void visit(N1& n)
    {
        unsigned int id = ((unsigned int)(size_t)&n)*10;

        if(_visit_cylo) {
            drawnode(id, n, out._param->radiusN1 * out._param->cytoplasmScaleFactor, out.colorCytoplasm);
            drawTrapezoid(id + 1, n.n0, n, out._param->radiusN0 * out._param->cytoplasmScaleFactor,
                          out._param->radiusN1 * out._param->cytoplasmScaleFactor, out.colorCytoplasm);

            n.visit_child(*this);
        }
        else {
            n.visit_child(*this);

            drawLine(id + 2, n.n0, n, out.thicknessB1, out.colorB1);

            if (n.isImmature()) {
                drawnode(id + 3, n, out._param->radiusN1, out.colorN1_immature_fill, 0.7, out.colorN1_immature_outline);
            }
            if (n.isMature()) {
                drawnode(id + 4, n, out._param->radiusN1, out.colorN1_mature_fill, 0.7, out.colorN1_mature_outline);
            }
            if (n.isIntermediate()) {
                drawnode(id + 4, n, out._param->radiusN1, out.colorN1_intermediate_fill, 0.7,
                         out.colorN1_intermediate_outline);
            }

            draw_text_force_branch(id + 5, n.n0, n);
        }
    }
    void visit(N2& n)
    {
                unsigned int id = ((unsigned int)(size_t)&n)*10;

        if(_visit_cylo) {

            drawnode(id, n, out._param->radiusN2 * out._param->cytoplasmScaleFactor, out.colorCytoplasm);
            drawTrapezoid(id+1, n.n1, n, out._param->radiusN1 * out._param->cytoplasmScaleFactor,
                          out._param->radiusN2 * out._param->cytoplasmScaleFactor, out.colorCytoplasm);
        }
        else {

            drawLine(id+2, n.n1, n, out.thicknessB2, out.colorB2);

            drawnode(id+3,n, out._param->radiusN2, out.colorN2_fill, 0.7, out.colorN2_outline);
            draw_text_force_branch(id+4, n.n1, n);
        }


    }
};


void UiOutput::start_model(const unsigned int &sn, ModelParam &p,  World &)
{
        _param = &p;
        _simulationNumber = &sn;
}

void UiOutput::step_model(const RealType& t ,World &w)
{
            _t = &t;
    {
        {

            std::scoped_lock lock(mutex);
            set_non_exist(draw_list);
        }
        DrawNodeVisitor nv(*this);
        nv.visit(w);
        {

            std::scoped_lock lock(mutex);
            clear(draw_list);
        }
    }
    std::this_thread::yield();
    using namespace std::chrono_literals;
    if(_param)
    {
      std::this_thread::sleep_for(std::chrono::duration<RealType>(_param->dt/simulation_speed));
    }
    //std::this_thread::sleep_for(std::chrono::round<std::chrono::milliseconds>(std::chrono::duration<float>(_param->dt/100)));
    //std::this_thread::sleep_for(50us);
    //std::this_thread::sleep_for(1ms);

/*    if(_param->displayCytoplasm)
    {*/

  //  }

}
#ifdef TESTS
void UiOutput::draw_nodes(N0 &n0)
{
    {
        {

            std::scoped_lock lock(mutex);
            set_non_exist(draw_list);
        }
        DrawNodeVisitor nv(*this);
        nv.visit(n0);
        {

            std::scoped_lock lock(mutex);
            clear(draw_list);
        }
    }
    std::this_thread::yield();
//    using namespace std::chrono_literals;
//    //std::this_thread::sleep_for(std::chrono::round<std::chrono::milliseconds>(std::chrono::duration<float>(_param->dt/100)));
//    std::this_thread::sleep_for(1ms);
}
    #endif



struct DragingPanZoom
{

    DragingPanZoom(sf::RenderTarget& tr) :target(tr)
    {

    }
        sf::RenderTarget& target;
    bool dragging = false;
    sf::Vector2i previous_mouse_position;
    void event(const sf::Event& event)
    {
        switch (event.type) {
    case sf::Event::MouseButtonPressed:
        dragging = true;
        break;
    case sf::Event::MouseButtonReleased:
            dragging = false;
            break;// if dragging mouse

    case sf::Event::MouseWheelScrolled: {
        auto dd = target.getView();
        dd.zoom(std::exp(event.mouseWheelScroll.delta/2.0));
        target.setView(dd);
    }
    break;
    case sf::Event::MouseMoved:
        // get mouse position
        const sf::Vector2i mouse_position{
            event.mouseMove.x, event.mouseMove.y
        };
        // if dragging, move view
        if (dragging) {
            auto diff = target.mapPixelToCoords(mouse_position)- target.mapPixelToCoords(previous_mouse_position);

            auto view = target.getView();
            view.move(-diff);
            target.setView(view);
        }
        // update previous mouse position
        previous_mouse_position = mouse_position;
        break;
        }
    }
};

void UiOutput::events() {

    sf::RenderWindow windows(sf::VideoMode::getDesktopMode(), "Angio Model V0");

        windows.setFramerateLimit(60);
        windows.setActive(true);
        auto view = windows.getView();
        view.setCenter(0,0);
        windows.setView(view);

        DragingPanZoom d(windows);
        while(_run && windows.isOpen()) {

            {

                sf::Event event;
                while (windows.pollEvent(event)) {

                    d.event(event);
                    // "close requested" event: we close the window
                    if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code==sf::Keyboard::Escape)) {

                        windows.close();
                        _run = false;
                        _run.notify_all();
                    }

                    if (event.type == sf::Event::Resized) {

                        // update the view to the new size of the window
                        auto view = windows.getView();
                        view.setSize(event.size.width, event.size.height);
                        windows.setView(view);

                    }

                }



                    windows.setActive(true);
                    windows.clear(sf::Color(255,255,255));


                    if(FastText::font && _t && _param && _simulationNumber ) {
                        sf::Text text(" ", *FastText::font, 20);

                        auto view = windows.getView();

                        text.setPosition(sf::Vector2f(view.getCenter().x - 0.48 * view.getSize().x,
                                                     view.getCenter().y - 0.5 * view.getSize().y  + view.getSize().y/25));

                        text.setString("Simulation nb: " + std::to_string(*_simulationNumber) + "/" +
                                       std::to_string(_param->nbOfSimulations) + "\nDuration = " +  format(std::chrono::duration<RealType>((*_t))));
                        text.setFillColor(sf::Color(0, 0, 0, 255));
                        windows.draw(text);
                    }



                    std::scoped_lock lock(mutex);
                    for(auto& shape : draw_list_substrate) {
                        windows.draw(*shape.get());
                    }
                    build(draw_list);

                    std::vector<FastShape*> draw_sorted;
                    draw_sorted.reserve(draw_list.size());
                    for(auto& shape : draw_list) {
                        draw_sorted.emplace_back(shape.second.get());
                    }

                    std::sort(draw_sorted.begin(),draw_sorted.end(),[](auto& a,auto &b){return a->draw_order<b->draw_order;});

                    for(auto& shape : draw_sorted) {
                        if(shape->shape()!= nullptr)
                        {
                            windows.draw(*shape->shape());
                        }
                    }




            }
                    windows.display();
            std::this_thread::yield();

    }


}
#ifdef TESTS
void UiOutput::wait_end()
{
    _run.wait(true);

}
#endif
UiOutput::~UiOutput()
{
    _run=false;
    _run.notify_all();
    if(_event_manager.joinable())
        _event_manager.join();
}