//
// Created by bgirard on 15/03/23.
//

#ifndef ANGIOMODEL_FASTSAHAPE_HPP
#define ANGIOMODEL_FASTSAHAPE_HPP
#include "memory"
#include "SFML/Graphics.hpp"
#include "list"
#include "unordered_map"
#include "limits"
#include "numbers"
#include "Node/Node.hpp"

class FastShape {
public:
        FastShape (unsigned int id_) : id(id_)
        {};

    FastShape (const FastShape &) = delete;
    FastShape & operator = (const FastShape &) = delete;
    virtual ~FastShape (){};
  protected:
    FastShape () = default;
public:
    unsigned int id;
    bool exist=false;
    bool to_update=true;

    unsigned int draw_order = 0;


    virtual void build() = 0;
    virtual sf::Drawable* shape() =0 ;
};

void set_non_exist(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fh);

void build(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fh);
void clear(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fs);

template<class T ,typename ...Types>
void update_or_create(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fh,unsigned int id,unsigned int order,Types&&... args)
{
    auto it = fh.find(id);
    if(it == fh.end())
    {
        fh.emplace(id,std::make_unique<T>(id,order,std::forward<Types>(args)...));
    }
    else
    {
        static_cast<T*>(it->second.get())->update(id,order,std::forward<Types>(args)...);
    }

}



#define ITEM_AS_ARG(type,name) , const type& name##_
#define ITEM_AS_BUILD(type,name) ,name( name##_ )



#define MAKECLASS(name,CLASSITEM) \
    name(unsigned int id ,unsigned int order CLASSITEM(ITEM_AS_ARG)) :FastShape(id) CLASSITEM(ITEM_AS_BUILD)    \
    {                   \
       exist =true;to_update = true; draw_order = order;                  \
    }\


template<typename T>
inline bool need_update(const T& a,const T&b)
{
    return a!=b;
}

template<>
inline bool need_update(const float& a,const float&b)
{
    return std::abs(a-b)> 5 * std::numeric_limits<double>::epsilon() ;
}

#define ITEM_AS_SET(type,name) if(need_update(name##_,name)){name = name##_;to_update = true;}

#define MAKEUPDATE(CLASSITEM)\
    void update(unsigned int id, unsigned int order CLASSITEM(ITEM_AS_ARG))\
    {  exist =true;          \
       draw_order = order;                      \
       CLASSITEM(ITEM_AS_SET)             \
    }               \
                    \

#define ITEM_AS_PP(type,name) type name;

#define MAKEPP(CLASSITEM)\
       CLASSITEM(ITEM_AS_PP)



class CircleFastShape : public FastShape
{
#define CIRCLEITEM(ITEM)\
    ITEM(float,rad)\
    ITEM(float,x)\
    ITEM(float,y)  \
    ITEM(sf::Color,color) \
    ITEM(float,fill_ratio)\
    ITEM(sf::Color,color_outline) \

public:
    MAKECLASS(CircleFastShape,CIRCLEITEM)
    //MAKEUPDATE(CIRCLEITEM)
void update(unsigned int id, unsigned int order, const float &rad_, const float &x_, const float &y_,
            const sf::Color &color_, const float &fill_ratio_, const sf::Color &color_outline_) {
    exist = true;
    draw_order = order;
    if (need_update(rad_, rad)) {
        rad = rad_;
        to_update = true;
    }
    if (need_update(x_, x)) {
        x = x_;
        to_update = true;
    }
    if (need_update(y_, y)) {
        y = y_;
        to_update = true;
    }
    if (need_update(color_, color)) {
        color = color_;
        to_update = true;
    }
    if (need_update(fill_ratio_, fill_ratio)) {
        fill_ratio = fill_ratio_;
        to_update = true;
    }
    if (need_update(color_outline_, color_outline)) {
        color_outline = color_outline_;
        to_update = true;
    }
}

    MAKEPP(CIRCLEITEM)

    std::unique_ptr<sf::CircleShape> _shape =0 ;

    void build()
    {
        if(exist && to_update)
        {
            if(_shape == nullptr)
                _shape = std::make_unique<sf::CircleShape>();

           _shape->setOrigin(rad*fill_ratio, rad*fill_ratio);
           _shape->setRadius(rad*fill_ratio);
           _shape->setPosition(x,y);
           _shape->setFillColor(color);
           _shape->setOutlineThickness( (1-fill_ratio) * rad);
           _shape->setOutlineColor(color_outline);
           to_update = false;
        }

    }
    virtual sf::Drawable* shape()
    {
        return _shape.get();
    };

};

class Convex4FastShape : public FastShape
{
#define CONVEXITEM(ITEM)\
    ITEM(float,x)\
    ITEM(float,y)       \
    ITEM(float,x2)\
    ITEM(float,y2)       \
    ITEM(sf::Color,color) \
    ITEM(float,radius_nodeA) \
    ITEM(float,radius_nodeB) \


public:
    MAKECLASS(Convex4FastShape,CONVEXITEM)
    MAKEUPDATE(CONVEXITEM)
    MAKEPP(CONVEXITEM)

    std::unique_ptr<sf::ConvexShape> _shape =0 ;

    void build()
    {
        if(exist && to_update)
        {
            if(_shape == nullptr) {
                _shape = std::make_unique<sf::ConvexShape>();
                _shape->setPointCount(4);
            }

        float length = std::sqrt((x2-x)*(x2-x)+(y2-y)*(y2-y));


        float angle = angle_bettween(1.0, 0.0,x2-x,y2-y)/(2*std::numbers::pi)*360;

        _shape->setPoint(0, sf::Vector2f(0, radius_nodeA)); // point A1
        _shape->setPoint(1, sf::Vector2f(0, -radius_nodeA)); // point A2
        _shape->setPoint(2, sf::Vector2f(length, -radius_nodeB)); // point B2
        _shape->setPoint(3, sf::Vector2f(length, radius_nodeB)); // point B1

        _shape->setPosition(x,y);
        _shape->setRotation(angle);
        _shape->setFillColor(color);

        }

    }
    virtual sf::Drawable* shape()
    {
        return _shape.get();
    };

};
#undef LINEITEM

class LIneFastShape : public FastShape
{
#define LINEITEM(ITEM)\
    ITEM(float,x)\
    ITEM(float,y) \
    ITEM(float,x2)\
    ITEM(float,y2)  \
    ITEM(sf::Color,color) \
    ITEM(float,thickness) \

public:
    MAKECLASS(LIneFastShape,LINEITEM)
    MAKEUPDATE(LINEITEM)
    MAKEPP(LINEITEM)

    std::unique_ptr<sf::RectangleShape> _shape =0 ;

    void build()
    {
        if(exist && to_update)
        {
            if(_shape == nullptr) {
                _shape = std::make_unique<sf::RectangleShape>();
            }

            float length = std::sqrt((x2-x)*(x2-x)+(y2-y)*(y2-y));;
          auto angle = angle_bettween(1, 0, x2-x,y2-y)/(2*std::numbers::pi)*360;

        _shape->setSize(sf::Vector2f(length, thickness));
        _shape->setOrigin(0,thickness/2);
        _shape->setPosition(x,y);
        _shape->setRotation(angle);
        _shape->setFillColor(color);

        }

    }
    virtual sf::Drawable* shape()
    {
        return _shape.get();
    };

};
#undef LINEITEM

class FastText : public FastShape
{
#define LINEITEM(ITEM)\
    ITEM(std::string,text)\
    ITEM(float,x) \
    ITEM(float,y)\
    ITEM(unsigned int,size)  \
    ITEM(sf::Color,color) \

public:
    MAKECLASS(FastText,LINEITEM)
    MAKEUPDATE(LINEITEM)
    MAKEPP(LINEITEM)

    std::unique_ptr<sf::Text> _shape =0 ;
    static std::unique_ptr<sf::Font> font;
    void build()
    {
        if(exist && to_update)
        {

                if (!font)
                {
                    font = std::make_unique<sf::Font>();
                    font->loadFromFile( "../arial.ttf" );
                }

            if(_shape == nullptr) {
                _shape = std::make_unique<sf::Text>();
            }

        _shape->setFont(*font);
        _shape->setCharacterSize(size);
        _shape->setString(text);
        _shape->setPosition(x,y);
        _shape->setFillColor(color);

        }

    }
    virtual sf::Drawable* shape()
    {
        return _shape.get();
    };

};

#endif //ANGIOMODEL_FASTSAHAPE_HPP
