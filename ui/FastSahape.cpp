//
// Created by bgirard on 15/03/23.
//

#include "FastSahape.hpp"
std::unique_ptr<sf::Font> FastText::font = nullptr;
void clear(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fs)
{
    std::erase_if(
    fs,
    [](auto& shape) { return !shape.second->exist;});
}

void build(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fs)
{
     for(auto& shape : fs) {
            shape.second->build();
      }

}

void set_non_exist(std::unordered_map<unsigned int,std::unique_ptr<FastShape>>& fs)
{
       for(auto& shape : fs) {
            shape.second->exist = false;
      }

}