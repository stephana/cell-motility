//
// Created by bgirard on 13/03/23.
//

#ifndef ANGIOMODEL_UIOUTPUT_HPP
#define ANGIOMODEL_UIOUTPUT_HPP

#include <mutex>
#include <thread>
#include <SFML/Graphics.hpp>
#include "Config.hpp"
#include "../common/OutPutManagers.hpp"
#include "unordered_map"




inline sf::Color predfColorBlue4() { return sf::Color(8, 42, 106, 255); }
inline sf::Color predfColorBlue1() { return sf::Color(87, 125, 195, 255); }

inline sf::Color predfColorGrayDark() { return sf::Color(30, 30, 30, 255); }
inline sf::Color predfColorGrayLight() { return sf::Color(180, 180, 180, 255); }


inline sf::Color predfColorBlack() { return sf::Color(0, 0, 0, 255); }

inline sf::Color predfColorGreen2() { return sf::Color(78, 218, 48, 255); }
inline sf::Color predfColorGreen3() { return sf::Color(29, 169, 0, 255); }
inline sf::Color predfColorGreen4() { return sf::Color(23, 133, 0, 255); }


inline sf::Color predfColorRed4() { return sf::Color(154, 0, 15, 255); }

inline sf::Color predfColorYellow2() { return sf::Color(255, 185, 57, 255); }

class FastShape;
class N0;
class UiOutput : public OutPutManager, public Configurable {
public:
    UiOutput();
    UiOutput(ModelParam& param);
    void init();

    virtual void start_model(const unsigned int&, ModelParam& p,World&);
    virtual void substrate_model(Substrate& substrate,const unsigned int& simulationNumber);
    virtual void step_model(const RealType& t ,World &w);
    virtual void end_model() {};

    #ifdef TESTS
        virtual void draw_nodes(N0 &n0);
        virtual void wait_end();

    #endif

    void events();
    ~UiOutput();

public:

       ModelParam* _param;

       std::mutex mutex;

       std::thread _event_manager;
       std::atomic_bool _run=true;

       const RealType *_t;
       const unsigned int *_simulationNumber;

       std::list<std::unique_ptr<sf::Drawable>> draw_list_substrate;

       std::unordered_map<unsigned int, std::unique_ptr<FastShape>> draw_list;

       #define CLASS_NAME "UI"

       MAKE_OPT_MIN_MAX(RealType,simulation_speed,100,1,100000,"Vitesse d'execution de la simulationj pour l'interface UI")


       float PixelsPerMeter = 13; float PixelsPerMeter_min = 1; float PixelsPerMeter_max = 100;
       float MetersPerPixel() { return 1 / PixelsPerMeter; }

       float thicknessB1 = 5;  // in pixels
       float thicknessB2 = 3;  // in pixels

       bool displayNodes = true;


       sf::Color colorN0_fill = predfColorBlue4();
       sf::Color colorCytoplasm = predfColorBlue1();

       sf::Color colorB1 = predfColorGrayDark();
       sf::Color colorB2 = predfColorGrayDark();

       sf::Color colorN0_outline = predfColorBlack();
       sf::Color colorN1_immature_fill = predfColorGreen2();
       sf::Color colorN1_immature_outline = predfColorBlue4();
       sf::Color colorN1_intermediate_fill = predfColorYellow2();
       sf::Color colorN1_intermediate_outline = predfColorBlue4();
       sf::Color colorN1_mature_fill = predfColorRed4();
       sf::Color colorN1_mature_outline = predfColorBlue4();
       sf::Color colorN2_fill = predfColorGreen3();
       sf::Color colorN2_outline = predfColorGreen4();
       sf::Color colorSubstratePattern = predfColorGrayLight();

        #undef CLASS_NAME
    };


#endif //ANGIOMODEL_UIOUTPUT_HPP
