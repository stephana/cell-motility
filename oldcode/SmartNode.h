#ifndef SMARTNODE_H
#define SMARTNODE_H

//#define PI 3.14159265
#include <SFML/Graphics.hpp>
#include <math.h>

#include <iostream>
#include <string>
#include <set>
#include <memory>
#include <mutex>

#include "properties.h"
#include "Vector2D.h"
#include "SmartSubstrate.h"
#include "RecordsOfNodeTransitionPeriods.h"


/** SMART NODE
SmartNode is the parent class of cell and extremities
**/
class SmartNode : public GraphicProperties, public SmartNodeWrapper
{
    //mutable std::mutex mutex_SN;

    public:
        SmartNode(ParametersObj& param, unsigned int cellRefNb);
        SmartNode(ParametersObj& param, Vector2D xy, unsigned int cellRefNb);
        SmartNode(ParametersObj& param, Vector2D xy, int nodeRefNb, unsigned int cellRefNb);
        virtual ~SmartNode();
        void setLifeSpan(ParametersObj& param);
        void setParentCellRefNb(unsigned int cellRefNb);
        unsigned int getPatentCellRefNb();
        // mechanics
        void setPosition(Vector2D xy);
        Vector2D getPosition();
        //void setRadius(float r);
        float getRadius(ParametersObj& param);
        float getRadiusPixel(ParametersObj& param);
        void applyForce(Vector2D& Fxy);
        float stepNode(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, SmartSubstrate& substrate, bool onPattern);
        void shouldLive(ParametersObj& param, float& dt);
        float get_k_substrate();

        // types
        bool isN0();
        bool isN1();
        bool isN2();
        int getOrder();

        // set Order
        void setOrderToN0(ParametersObj& param);
        void setOrderToN1(ParametersObj& param);
        void setOrderToN2(ParametersObj& param);
        void setOrderAllToFalse();

        bool isImmature();
        bool isIntermediate();
        bool isMature();
        void setIntermediate();
        void setMature();

        //bool isSlipping = false;

        bool isAlive();
        void setAliveToFalse(std::string causeOfDeath);
        Vector2D getAdhesionForce() { return netForce_previous_step; }
        float getAlpha() { return alpha; }
        bool isInhibited() { return is_inhibited; }
        void setInhibited() { is_inhibited = true; }
        void setUninhibited() { is_inhibited = false; }

    public:
        bool bound = false;
        bool hasParentN0Slipped = false;
        bool is_immature = false;
        bool is_intermediate = false;
        bool is_mature = false;
        // order
        bool is_N0 = false;
        bool is_N1 = false;
        bool is_N2 = false;
        bool is_connected_to_parent = false;
        float lifespan_2 = 0;
        float durationIntermediateState_Max = 0;
        float durationMatureState_Max = 0;
        unsigned int parentCellRefNb;

    protected:
        float Fmax;
        //float radius;
        Vector2D position;
        Vector2D netForce = Vector2D(0,0);
        Vector2D netForce_previous_step = Vector2D(0, 0);
        float magnitudeProtrusionForce_last = 0;
        float alpha;
        bool is_alive = true;

    private:
        bool is_inhibited = false;
        

};

void drawNode(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition, float radius, sf::Color nodeColor);
void drawNode(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition, float radius, sf::Color nodeColor, sf::Color outlineColor);
void drawN0(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition);
void drawN1(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition, double maturation_ratio);
void drawN1_immature(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition);
void drawN1_intermediate(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition);
void drawN1_mature(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition);
void drawN2(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition);

#endif