﻿#pragma once
//#ifndef PROPERTIES_H
//#define PROPERTIES_H
#define _USE_MATH_DEFINES
#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <cstdlib>
#include "memory"
#include <random> // poisson_distribution
#include <armadillo> // matrix library
 
#define NEW_CHANGES 0

#define min_f(a, b, c)  (fminf(a, fminf(b, c)))
#define max_f(a, b, c)  (fmaxf(a, fmaxf(b, c)))

//double PI  = 3.141592653589793238463;
//float  PI_F = 3.14159265358979f;
//double DEGTORAD = 0.0174532925199432957f;
//double RADTODEG = 57.295779513082320876f;

enum patternType { triangle_pattern, rectangle_pattern, circle_pattern, homogeneous_pattern};
enum B1_length_Generator { constant_B1_length_generator, uniforme_B1_length_generator };
enum B2_length_Generator { constant_B2_length_generator, uniforme_B2_length_generator };


std::shared_ptr<sf::Font> loadFont(std::string fontName);

float randomFloat(float	min, float max);
float perlinNoise(float previousValue, float varLow, float varHigh);
bool probabilityToCreate_Sigmoid(int N_current, int N_target, float t_halfLife, float dt, float stippness = 1);
std::poisson_distribution<int> CreatePoissonDistribution(double nu, double dt);
float probabilityOfDyingDuringTimeStep(float t_halfLife, float dt); //probability of dying during the interval dt according to the half-life t_halfLife (in seconds).

sf::Color predfColorWhite();
sf::Color predfColorBlack();
sf::Color predfColorGrayLight();
sf::Color predfColorGrayMedium();
sf::Color predfColorGrayDark();

sf::Color predfColorRed0();
sf::Color predfColorRed1();
sf::Color predfColorRed2();
sf::Color predfColorRed3();
sf::Color predfColorRed4();

sf::Color predfColorYellow0();
sf::Color predfColorYellow1();
sf::Color predfColorYellow2();
sf::Color predfColorYellow3();
sf::Color predfColorYellow4();

sf::Color predfColorBlue0();
sf::Color predfColorBlue1();
sf::Color predfColorBlue2();
sf::Color predfColorBlue3();
sf::Color predfColorBlue4();

sf::Color predfColorGreen0();
sf::Color predfColorGreen1();
sf::Color predfColorGreen2();
sf::Color predfColorGreen3();
sf::Color predfColorGreen4();

struct HSVcolor
{
    float H = 0; //0-360
    float S = 0; //0-1
    float V = 0; //0-1
    float alpha = 1; //0-1
};

class ColorObj
{
    public:
        float R = 0; // 0-255
        float G = 0; // 0-255
        float B = 0; // 0-255
        float alpha = 1; //0-1
        
        ColorObj() {};
        ColorObj(sf::Color newColor) { setColor(newColor); };
        ~ColorObj() {};

        void setColor(sf::Color newColor) {
            R = newColor.r;
            G = newColor.g;
            B = newColor.b;
            alpha = newColor.a;
        }
        sf::Color getSFMLColor() {
            return sf::Color(R, G, B, alpha);
        }
        HSVcolor getHSV()
        {
            float r = R / 255.0f;
            float g = G / 255.0f;
            float b = B / 255.0f;

            float h, s, v; // h:0-360.0, s:0.0-1.0, v:0.0-1.0

            float max = max_f(r, g, b);
            float min = min_f(r, g, b);

            v = max;

            if (max == 0.0f) {
                s = 0;
                h = 0;
            }
            else if (max - min == 0.0f) {
                s = 0;
                h = 0;
            }
            else {
                s = (max - min) / max;

                if (max == r) {
                    h = 60 * ((g - b) / (max - min)) + 0;
                }
                else if (max == g) {
                    h = 60 * ((b - r) / (max - min)) + 120;
                }
                else {
                    h = 60 * ((r - g) / (max - min)) + 240;
                }
            }

            if (h < 0) h += 360.0f;

            HSVcolor HSV;
            HSV.H = h; // 0-360
            HSV.S = s; // 0-1
            HSV.V = v; // 0-1
            HSV.alpha = alpha;
            return HSV;
        }
        sf::Color hsv2rgb(HSVcolor HSV)
        {
            float h = HSV.H; // 0-360
            float s = HSV.S; // 0.0-1.0
            float v = HSV.V; // 0.0-1.0

            float r = 0; // 0.0-1.0
            float g = 0; // 0.0-1.0
            float b = 0; // 0.0-1.0

            int hi = (int)(h / 60.0f) % 6;
            float f = (h / 60.0f) - hi;
            float p = v * (1.0f - s);
            float q = v * (1.0f - s * f);
            float t = v * (1.0f - s * (1.0f - f));

            switch (hi) {
            case 0: r = v, g = t, b = p; break;
            case 1: r = q, g = v, b = p; break;
            case 2: r = p, g = v, b = t; break;
            case 3: r = p, g = q, b = v; break;
            case 4: r = t, g = p, b = v; break;
            case 5: r = v, g = p, b = q; break;
            }

            R = (unsigned int)(r * 255); // r : 0-255
            G = (unsigned int)(g * 255); // r : 0-255
            B = (unsigned int)(b * 255); // r : 0-255
            alpha = HSV.alpha;
            return getSFMLColor();
        }
};

class DomainRange
{
    public:
        DomainRange(float min, float max) : min(min), max(max) {};
        DomainRange(){};
        ~DomainRange() {};
        void set(float minimum, float maximum) { min = minimum; max = maximum; }
        float min;
        float max;
};

class TimeObj
{
    public:
        TimeObj() { t = 0; };
        TimeObj(double time) { t = time; };
        ~TimeObj() {};
        float t; // time in seconds
        unsigned int hours() { return t / 3600; };
        unsigned int minutes() { return t / 60 - hours() * 60;};
        unsigned int seconds() { return t - 60 * minutes() - 3600 * hours(); };
        std::string tellTime() {
            if (t <= 1)
            {
                return std::to_string(t) + " secondes";
            }
            else{ return std::to_string(hours()) + "h " + std::to_string(minutes()) + "min " + std::to_string(seconds()) + "s"; }
        };
};

class ParametersObj
{
public:
    ParametersObj() {
        font_ptr = loadFont(fontName);
        setPoissonDistributions();
    };
    ~ParametersObj() {}; // To do : must inform nodes that SmartInteraction is being destroyed;
    void setPoissonDistributions() {
        Poisson_nu1 = CreatePoissonDistribution(nu1, dt);
        Poisson_nu2_im = CreatePoissonDistribution(nu2_im, dt);
        Poisson_nu2_int = CreatePoissonDistribution(nu2_int, dt);
        Poisson_nu2_m = CreatePoissonDistribution(nu2_m, dt);
    }
    
    int numberEventsDuringStep(double probability, double dt);
    

    // General simulation parameters
    float duration = 2 * 60 * 60; // equivalent to 2h = 2 *60 * 60 seconds
    float dt = 0.1; // secondes
    int nbOfSimulations = 1;
    bool displaySimulation = true;
    int displaySimulationNbStepBetweenFrames = 10;
    bool recordDetails = true;
    std::string simlabel = "not_defined";
    std::string simlabel_x = "not_defined";
    std::string simlabel_y = "not_defined";
    std::string simdetailed_label = "not_defined";
    int simulationRefNb = 0;
    int labelRefNb = 0;
    int numberSimulationsWithThisParameters = -1;

    float maxInteractionDistanceBetweenCells = 150; // this is not a physical distance. It is used as an optimization parameter. It must be greater than twice the max length of a B1 branch
    float F_RCC = 300; // Rupture force cell cell adhesion

    float delta_theta_2 = M_PI / 6;    float delta_theta2_min = 0;               float delta_theta2_max = M_PI;        float delta_theta2_interval = 0.05*M_PI;

    // Cell and node parameters
    float tau_int = 15 * 60;    float tau_int_min() { return tau2; };   float tau_int_max = 45 * 60;// Durées de vie de  tau_int pour le nœud N1 intermédiaire
    float tau_m = 35 * 60;      float tau_m_min() { return tau_int; };  float tau_m_max = 10 * 60 * 60;// Durées de vie de pour le nœud N1 mature
    float tau2 = 200;           float tau2_min = 1;                     float tau2_max = 50 * 60;// Durées de vie de pour le noeud N2
    float l1initial = 7;        float l1initial_min = 1;                float l1initial_max = 25;// Longueurs de création des branches : l1initial pour la branche primaire avec la valeur l1initial tirée aléatoirement entre l1min et l1max
    float l2initial = 4;        float l2initial_min = 1;                float l2initial_max = 15;// Longueurs de création des branches : l2initial pour la branche secondaire avec l2initial longueur constante à fixer
    
    B1_length_Generator l1_Generator = constant_B1_length_generator;
    B2_length_Generator l2_Generator = constant_B2_length_generator;


    //float l1eq; //l1eq = l1initial initial

    float epsilon_m = 0.5;      float epsilon_m_min = 0.0;         float epsilon_m_max = 0.2;  float epsilon_m_interval = 0.01;
    float epsilon10 = 0;
    float epsilon20 = 0.5;      float epsilon20_min = 0.2;         float epsilon20_max = 1.0;  float epsilon20_interval = 0.01;
    float l2rest(){ return l2initial/(1+epsilon20); };

    float nu1 = 0.01;             float nu1_min = 0.001;              float nu1_max = 0.20;          float nu1_interval = 0.001;
    float nu2_im = 0.015;         float nu2_im_min = 0.001;           float nu2_im_max = .5;         float nu2_im_interval = 0.001;// probability of creating an N2 from a intermediate N1
    float nu2_int = 0.015;        float nu2_int_min = 0.001;          float nu2_int_max = .5;        float nu2_int_interval = 0.001;// probability of creating an N2 from a intermediate N1
    float nu2_m = 0.00001;        float nu2_m_min = 0.00001;          float nu2_m_max = .01;         float nu2_m_interval = 0.001;// probability of creating an N2 from a mature N1

    // we assign a these are variables to save computation time
    std::poisson_distribution<int> Poisson_nu1;
    std::poisson_distribution<int> Poisson_nu2_im;
    std::poisson_distribution<int> Poisson_nu2_int;
    std::poisson_distribution<int> Poisson_nu2_m;

    float kic = 10; // intercell stiffness
    float k10 = 0.1;            float k10_min = 0.1;                float k10_max = 100;            float k10_interval = 0.1;// Coefficients de raideurs élastiques des branches : k10=0 pour la branche primaire immature
    float k11 = 3.5;            float k1_min = 0.1;                 float k1_max = 100;             float k1_interval = 0.1;// Coefficients de raideurs élastiques des branches : k11 pour la branche primaire intermédiaire
    float k2 = 12;              float k2_min = 0.1;                 float k2_max = 100;// Coefficients de raideurs élastiques des branches : k2 pour la branche secondaire
    float gamma_max__ = 30;     float gamma_max__min = 30;         float gamma_max__max = 60;      float gamma_max__interval = 0.1;// Contractilité de la branche primaire !!???

    float F_gamma = 1.0;    float F_gamma_min = 0.2;    float F_gamma_max = 5.0;

    float gamma1(float F_a) { return gamma_max__* (1 - exp(- F_a / F_gamma)); }

    bool B1_tension_only = false;
    bool B2_tension_only = true;



    // Delta inhibition domain
    float delta_theta_1 = float(1.0) * M_PI / float(2.0);
    float delta_theta_1_min = float(1.0) * M_PI / float(6.0);
    float delta_theta_1_max = float(1.0) * M_PI / float(1.0);
    float delta_theta_1_interval = M_PI / float(12.0);

    float alpha0 = 400;          float alpha0_min = 1;         float alpha0_max = 1000;// Coefficients de frottement α0 pour le nœud N0
    float alpha10 = 50.0;        float alpha10_min = 1;        float alpha10_max = 1000;// Coefficients de frottement α10 = 0 pour le nœud N1 immature (pas d’adhésion)
    //
    float tau_t = 5 * 60;                           float tau_t_min = 1 * 60;                               float tau_t_max = 20 * 60;// Temps caractéristique pour qu'un N1 arrive à l'état intermédiare
    float alpha_int = 150;                          float alpha_int_min() { return alpha10; };              float alpha_int_max = 1000;
    //float alpha1i_slope_coefficient = 1.5;         float alpha1i_slope_coefficient_min = 0.1;      float alpha1i_slope_coefficient_max = 10;  float alpha1i_slope_coefficient_interval = 0.1;
    float alpha_im(float t) {
        float thisalpha = (alpha_int - alpha10) / tau_t * t + alpha10;
        if (thisalpha > alpha_int) { return alpha_int; }
        else { return thisalpha; }
    }; // Coefficients de frottement • α1i(t) pour le nœud N1 intermédiaire est une fonction sigmoïde qui dépend du temps et atteint une saturation dont le seuil αsat est à définir
    float coef_alpha_int_to_alpha_m = 1.1;      float coef_alpha_int_to_alpha_m_min = 1;        float coef_alpha_int_to_alpha_m_max = 2;
    float alpha1_m() { return coef_alpha_int_to_alpha_m * alpha_int; };                         float alpha1_m_min() { return alpha_int; };             float alpha1_m_max() { return coef_alpha_int_to_alpha_m * alpha_int_max; };// Coefficients de frottement α1m pour le nœud N1 mature avec α1m > αsat
    float alpha2 = 8;                           float alpha2_min = 1;                           float alpha2_max() { return alpha1_m();};// Coefficients de frottement α2 pour le nœud N2 avec α2 << αsat
    
    float adhesion_coeficient_FR012 = 1.0;
    float F_R0 = 30;                            float F_R0_min = 10;                            float F_R0_max = 100;               float F_R0_interval = 0.1;
    float F_R1 = 34;                            float F_R1_min = 10;                            float F_R1_max = 100;
    float F_R2 = 17;                            float F_R2_min = 0;                             float F_R2_max() { return F_R1; };

    // result output
    std::string resultsOutputFilename = "results.csv";
    std::string parametersOutputFilename = "parameters.csv";
    std::string nodeLifeOutputFilename = "nodeLife.csv";
    std::string nodeInfoOutputFilename = "nodeInfo_regTimeStep.csv";
    std::string branchInfoOutputFilename = "branchInfo_regTimeStep.csv";
    std::string patternOutputFilename = "pattern.csv";
    std::string cellSnapShotOutputFilename = "cellSnapShot.csv";
    float saveResultTimeIntervals = 1*60;
    float saveInfoTimeIntervals = 0.1;
    bool recordInfoTimeIntervals = false;
    bool saveSnapShot = false;

    // visual parameters
    float radiusN0 = 2.8; /** in µm **/          float radiusN0_min = 0.1;        float radiusN0_max = 15;     float radiusN0_interval = 0.1;
    float radiusN1 = 1.2; /** in µm **/          float radiusN1_min = 0.1;        float radiusN1_max = 10;     float radiusN1_interval = 0.1;
    float radiusN2 = .2; /** in µm **/          float radiusN2_min = 0.1;        float radiusN2_max = 5;      float radiusN2_interval = 0.1;
    float thicknessB1 = 5;  // in pixels
    float thicknessB2 = 3;  // in pixels
    float cytoplasmScaleFactor = 2.5;
    float scaleBarLength = 5;

    sf::Color colorN0_fill = predfColorBlue4();
    sf::Color colorN0_outline = predfColorBlack();
    sf::Color colorN1_immature_fill = predfColorGreen2();
    sf::Color colorN1_immature_outline = predfColorBlue4();
    sf::Color colorN1_intermediate_fill = predfColorYellow2();
    sf::Color colorN1_intermediate_outline = predfColorBlue4();
    sf::Color colorN1_mature_fill = predfColorRed4();
    sf::Color colorN1_mature_outline = predfColorBlue4();
    sf::Color colorN2_fill = predfColorGreen3();
    sf::Color colorN2_outline = predfColorGreen4();
    sf::Color colorB1 = predfColorGrayDark();
    sf::Color colorB2 = predfColorGrayDark();
    sf::Color colorCytoplasm = predfColorBlue1();
    sf::Color colorBackground = predfColorWhite();
    sf::Color colorScaleBar = predfColorBlack();
    sf::Color colorSubstratePattern = predfColorGrayLight();

    // show node type
    bool displayLegend = true;
    bool displayNodeType = false;
    bool displayNodeID = false;
    bool displayNodeMaturationState = false;
    bool displayBranchID = false;
    bool displayBranchType = false;
    bool displayCytoplasm = true;
    bool displayInteractions = true;
    bool displayNodes = true;


    // Display ---------------

    // Screen & scale parameters
    int ScreenHeight = 720;  int ScreenHeight_min = 400;  int ScreenHeight_max = 1280;
    int ScreenWidth = 1280; int ScreenWidth_min = 600; int ScreenWidth_max = 1920;
    int ScreenOffSet_x = 0;
    int ScreenOffSet_y = 0;
    float zoom = 1;
    float PixelsPerMeter = 13; float PixelsPerMeter_min = 1; float PixelsPerMeter_max = 100;
    float MetersPerPixel() { return 1 / PixelsPerMeter; }

    // Legend parameters
    float legend_offsetX_coefficient = 0; float legend_offsetX_coefficient_min = -.5; float legend_offsetX_coefficient_max = .5; float legend_offsetX_coefficient_interval = 0.01;
    float legend_offsetY_coefficient = 0; float legend_offsetY_coefficient_min = -.5; float legend_offsetY_coefficient_max = .5; float legend_offsetY_coefficient_interval = 0.01;
    float legend_offsetX() { return ( - 0.5 * ScreenOffSet_x + legend_offsetX_coefficient * ScreenWidth ) * MetersPerPixel(); }
    float legend_offsetY() { return ( - 0.4 * ScreenOffSet_y + legend_offsetY_coefficient * ScreenHeight ) * MetersPerPixel(); }
    float legend_spacingX = 4.6; float legend_spacingX_min = 1; float legend_spacingX_max = 10; float legend_spacingX_interval = 0.1;
    float legend_spacingY = 5; float legend_spacingY_min = 1; float legend_spacingY_max = 10; float legend_spacingY_interval = 0.1;


    std::string fontName = "arial";
    std::shared_ptr<sf::Font> font_ptr;

    bool refresh = false;

    //
    bool adhereEveryWhere = true;
    patternType pattern_type = homogeneous_pattern;
    float pattern_height = 10; float pattern_height_min = 2; float pattern_height_max = 30;
    float pattern_width = 20; float pattern_width_min = 5; float pattern_width_max = 35;
    float pattern_spacing = 0; float pattern_spacing_min = -16 /**-8**/; float pattern_spacing_max = 4;
    float pattern_radius = pattern_width / 2.0; float pattern_radius_min = pattern_width_min / 2.0; float pattern_radius_max = pattern_width_max / 2.0;
    unsigned int pattern_nb_motifs = 1;

    bool verify(bool cout_info = false, bool correct_it = false)
    {
        bool acceptable = true;
        if ((tau2 < tau_int) && (tau_int < tau_m)) { acceptable = acceptable * true; }
        else {
            acceptable = acceptable * false;
            if (cout_info) { std::cout << "Error in parameter settings : condition '(tau2 < tau_int) && (tau_int < tau_m)' not respected" << std::endl; }

            if (correct_it && (tau2 > tau_int)) { tau_int = tau_int_min();}
            if (correct_it && (tau_int > tau_m)) { tau_m = tau_m_min(); }
        }

        if ((l2rest() < l2initial)) { acceptable = acceptable * true; }
        else {
            acceptable = acceptable * false;
            if (cout_info) { std::cout << "Error in parameter settings : condition 'l2rest < l2initial' not respected" << std::endl; }
            }
        
        if ((alpha1_m() > alpha_int)) { acceptable = acceptable * true; }
        else {
            acceptable = acceptable * false;
            if (cout_info) { std::cout << "Error in parameter settings : condition 'alpha1_m > alpha_int' not respected" << std::endl; }
            }

        return acceptable;
    };

    const char* paramlist[29] = { "tau_int", "tau_m", "tau2", "tau_t",
                                "l1initial", "l2initial",
                                "epsilon_m", "epsilon20",
                                "nu1", "nu2_im", "nu2_int", "nu2_m",
                                "k10", "k11", "k2", "gamma_max__", "delta_theta_1", "delta_theta_2",
                                "alpha0", "alpha10", "alpha_int", // coef_alpha_int_to_alpha_m or alpha1_m
                                "F_R0", "F_R1", "F_R2", "F_gamma",
                                "pattern_height", "pattern_width", "pattern_spacing", "pattern_radius" };

    void setValueOf(std::string varname, float newvalue)
    {
        if      (varname == "tau_int") { tau_int = newvalue; }
        else if (varname == "tau_m") { tau_m = newvalue; }
        else if (varname == "tau2") { tau2 = newvalue; }
        else if (varname == "tau_t") { tau_t = newvalue; }
        else if (varname == "l1initial") { l1initial = newvalue; }
        else if (varname == "l2initial") { l2initial = newvalue; }
        else if (varname == "epsilon_m") { epsilon_m = newvalue; }
        else if (varname == "epsilon10") { epsilon10 = newvalue; }
        else if (varname == "epsilon20") { epsilon20 = newvalue; }
        else if (varname == "nu1") { nu1 = newvalue; Poisson_nu1 = CreatePoissonDistribution(nu1, dt);}
        else if (varname == "nu2_im") { nu2_im = newvalue; Poisson_nu2_im = CreatePoissonDistribution(nu2_im, dt);}
        else if (varname == "nu2_int") { nu2_int = newvalue; Poisson_nu2_int = CreatePoissonDistribution(nu2_int, dt);}
        else if (varname == "nu2_m") { nu2_m = newvalue; Poisson_nu2_m = CreatePoissonDistribution(nu2_m, dt);}
        else if (varname == "k10") { k10 = newvalue; }
        else if (varname == "k11") { k11 = newvalue; }
        else if (varname == "k2") { k2 = newvalue; }
        else if (varname == "gamma_max__") { gamma_max__ = newvalue; }
        else if (varname == "delta_theta_1") { delta_theta_1 = newvalue; }
        else if (varname == "delta_theta_2") { delta_theta_2 = newvalue; }
        else if (varname == "alpha0") { alpha0 = newvalue; }
        else if (varname == "alpha10") { alpha10 = newvalue; }
        else if (varname == "alpha_int") { alpha_int = newvalue; }
        else if (varname == "coef_alpha_int_to_alpha_m") { coef_alpha_int_to_alpha_m = newvalue; }
        //else if (varname == "alpha1_m") { alpha1_m() = newvalue; }
        else if (varname == "alpha2") { alpha2 = newvalue; }
        else if (varname == "F_R0") { F_R0 = newvalue; }
        else if (varname == "F_R1") { F_R1 = newvalue; }
        else if (varname == "F_R2") { F_R2 = newvalue; }
        else if (varname == "F_gamma") { F_gamma = newvalue; }
        else if (varname == "pattern_height") { pattern_height = newvalue; }
        else if (varname == "pattern_width") { pattern_width = newvalue; }
        else if (varname == "pattern_spacing") { pattern_spacing = newvalue; }
        else { std::cout << " Warning: Unreferenced variable (" << varname << ") in 'setValueOf()': the value remains unchanged !" << std::endl; }
    }

    float getValueOf(std::string varname)
    {
        float value = 0;

        if      (varname == "tau_int") { value = tau_int; }
        else if (varname == "tau_m") { value = tau_m; }
        else if (varname == "tau2") { value = tau2; }
        else if (varname == "tau_t") { value = tau_t; }
        else if (varname == "l1initial") { value = l1initial; }
        else if (varname == "l2initial") { value = l2initial; }
        else if (varname == "epsilon_m") { value = epsilon_m; }
        else if (varname == "epsilon20") { value = epsilon10; }
        else if (varname == "epsilon20") { value = epsilon20; }
        else if (varname == "nu1") { value = nu1; }
        else if (varname == "nu2_im") { value = nu2_im; }
        else if (varname == "nu2_int") { value = nu2_int; }
        else if (varname == "nu2_m") { value = nu2_m; }
        else if (varname == "k10") { value = k10; }
        else if (varname == "k11") { value = k11; }
        else if (varname == "k2") { value = k2; }
        else if (varname == "gamma_max__") { value = gamma_max__; }
        else if (varname == "delta_theta_1") { value = delta_theta_1; }
        else if (varname == "delta_theta_2") { value = delta_theta_2; }
        else if (varname == "alpha0") { value = alpha0; }
        else if (varname == "alpha10") { value = alpha10; }
        else if (varname == "alpha_int") { value = alpha_int; }
        else if (varname == "coef_alpha_int_to_alpha_m") { value = coef_alpha_int_to_alpha_m; }
        else if (varname == "alpha1_m") { value = alpha1_m(); }
        else if (varname == "alpha2") { value = alpha2; }
        else if (varname == "F_R0") { value = F_R0; }
        else if (varname == "F_R1") { value = F_R1; }
        else if (varname == "F_R2") { value = F_R2; }
        else if (varname == "F_gamma") { value = F_gamma; }
        else if (varname == "pattern_height") { value = pattern_height; }
        else if (varname == "pattern_width")  { value = pattern_width; }
        else if (varname == "pattern_spacing") { value = pattern_spacing; }
        else { std::cout << " Warning: Unreferenced variable (" << varname << ") in methode 'getValueOf()': the value remains unchanged !" << std::endl; }
        return value;
    }

    DomainRange getValueRangeOf(std::string varname)
    {
        DomainRange dr = DomainRange();
        
        if      (varname == "tau_int") { dr.set(tau_int_min(), tau_int_max) ; }
        else if (varname == "tau_m") { dr.set(tau_m_min(), tau_m_max) ; }
        else if (varname == "tau2") { dr.set(tau2_min, tau2_max) ; }
        else if (varname == "l1initial") { dr.set(l1initial_min, l1initial_max) ; }
        else if (varname == "l2initial") { dr.set(l2initial_min, l2initial_max) ; }
        else if (varname == "epsilon20") { dr.set(epsilon20_min, epsilon20_max) ; }
        else if (varname == "nu1") { dr.set(nu1_min, nu1_max) ; }
        else if (varname == "nu2_im") { dr.set(nu2_im_min, nu2_im_max) ; }
        else if (varname == "nu2_int") { dr.set(nu2_int_min, nu2_int_max) ; }
        else if (varname == "nu2_m") { dr.set(nu2_m_min, nu2_m_max) ; }
        else if (varname == "k10") { dr.set(k10_min, k10_max) ; }
        else if (varname == "k11") { dr.set(k1_min, k1_max) ; }
        else if (varname == "k2") { dr.set(k2_min, k2_max) ; }
        else if (varname == "gamma_max__") { dr.set(gamma_max__min, gamma_max__max) ; }
        else if (varname == "epsilon_m") { dr.set(epsilon_m_min, epsilon_m_max) ; }
        else if (varname == "delta_theta_1") { dr.set(delta_theta_1_min, delta_theta_1_max) ; }
        else if (varname == "delta_theta_2") { dr.set(delta_theta2_min, delta_theta2_max); }
        else if (varname == "tau_t") { dr.set(tau_t_min, tau_t_max); }
        else if (varname == "alpha0") { dr.set(alpha0_min, alpha0_max) ; }
        else if (varname == "alpha10") { dr.set(alpha10_min, alpha10_max) ; }
        else if (varname == "alpha_int") { dr.set(alpha_int_min(), alpha_int_max) ; }
        else if (varname == "coef_alpha_int_to_alpha_m") { dr.set(coef_alpha_int_to_alpha_m_min, coef_alpha_int_to_alpha_m_max); }
        else if (varname == "alpha1_m") { dr.set(alpha1_m_min(), alpha1_m_max()) ; }
        else if (varname == "alpha2") { dr.set(alpha2_min, alpha2_max()) ; }
        else if (varname == "F_R0") { dr.set(F_R0_min, F_R0_max); }
        else if (varname == "F_R1") { dr.set(F_R1_min, F_R1_max) ; }
        else if (varname == "F_R2") { dr.set(F_R2_min, F_R2_max()) ; }
        else if (varname == "F_gamma") { dr.set(F_gamma_min, F_gamma_max); }
        else if (varname == "pattern_height") { dr.set(pattern_height_min, pattern_height_max); }
        else if (varname == "pattern_width")  { dr.set(pattern_width_min, pattern_width_max); }
        else if (varname == "pattern_spacing") { dr.set(pattern_spacing_min, pattern_spacing_max); }
        else { std::cout << " Warning: Unreferenced variable  (" << varname << ") in method 'getValueRangeOf()': the value remains unchanged !" << std::endl; }
        return dr;
    }

    int nbIntervals(float v_min, float v_max, float v_step) {
        return (v_max - v_min) / v_step + 1;
    }
    int intervalIndex(float v_min, float v_step, float v)
    {
        return (v - v_min) / v_step;
    }
};

arma::uvec f_block_no_superposition(ParametersObj& param, arma::vec alphas, arma::vec x);


inline double probability2die(float age, double tau) { return 1 - exp(- age / tau); } // method imposed by Arnaud


inline double probability2die_sigmoid(float age, double tau, double a = 1/100.0) {
    //double p = std::max(0.0, tanh(a * (age - tau)) / 2.0 + 1.0 / 2.0);
    //p = std::min(p, 1.0);
    return tanh(a * (age - tau)) / 2.0 + 1.0 / 2.0;
}

/*
inline double probability2die_sigmoid(float age, double tau, double beta = 5) {
    //double p = std::max(0.0, tanh(a * (age - tau)) / 2.0 + 1.0 / 2.0);
    //p = std::min(p, 1.0);
    double a= beta/tau;
    return tanh(a * (age - tau)) / 2.0 + 1.0 / 2.0;
}
//*/

void draw_f_f_block_no_superposition(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, arma::uvec ff);

float event_frequency(int n, int n_target);

void displayMessageDuringStandby(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, std::string message);
void displayMessageDuringSimulation(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, std::string message);

/**  Color Class Definition  **/
class Color
{
    public:
        Color();
        Color(float Red, float Green, float Blue);
        Color(float Red, float Green, float Blue, float alpha);
        ~Color();
        void addColor(float Red, float Green, float Blue, float alpha);
        void setColor(float Red, float Green, float Blue, float alpha);
        sf::Color getColor_SFML();
        float R;
        float G;
        float B;
        float Alpha;
};

/**  GraphicProperties Class Definition  **/

class GraphicProperties
{
    /** The GraphicProperties class will be the parent class of
        - SmartCell
        - SmartNode
        - SmartInteraction
    so that they we only have to define common color methods once
     **/
    public:
        GraphicProperties();
        ~GraphicProperties();
        void setColor(float R, float G, float B, float alpha); // RGB color : Red Green Blue (dark =0; bright = 255);
        void setColor(Color newColor);
        sf::Color getColor_SFML();
        Color getColor();
    //private:
        Color color = Color(255,255,255,150);
};

struct SimulationLabel
{
    std::string label;
    std::string label_x = "label_x_not_defined";
    std::string label_y = "label_y_not_defined";
    int labelRefNb;
};

class SmartNodeWrapper
{
    public:
        int refNb;
        float age = 0;
        float durationImmatureState = 0;
        float durationIntermediateState = 0;
        float durationMatureState = 0;
        std::string causeOfDeath = "undefined";
        float netForce_magnitude_previous_step=0;
};

class CPMwrapperObj
{
    public:
        virtual void nodeLifeEventsValuesToString(ParametersObj& param, SmartNodeWrapper& nodeWrp, std::string eventType, std::string nodeOrder, std::string nodeState = "none") {};
};

//#endif // PROPERTIES_H