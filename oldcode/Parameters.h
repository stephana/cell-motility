#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <iostream>
#include <vector>
#include <math.h>
#include <cstdlib>
#include <random>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif // !M_PI



class ParametersObj
{
public:
    ParametersObj() { };
    ~ParametersObj() {}; // To do : must inform nodes that SmartInteraction is being destroyed;

    float duration = 2 * 60 * 60; // equivalent to 2h = 2 *60 * 60 seconds
    float dt = 0.1; // secondes
    int nbOfSimulations = 3; int nbOfSimulations_max = 10000;
    bool displaySimulation = true;
    bool recordDetails = true;

    // Cell and node parameters
    float DN1i = 10*60; // Durées de vie de  DN1i pour le nœud N1 intermédiaire
    float DN1m = 20*60; // Durées de vie de pour le nœud N1 mature
    float DN2 = 30; // Durées de vie de pour le noeud N2


    float l2 = 3; // Longueurs de création des branches : l2 pour la branche secondaire avec l2 longueur constante à fixer
    //float l1eq; //l1eq = l1 initial
    float l2_prestress = 0.5; float l2_prestress_interval = 0.01;

    float P_N1 = 0.02 * dt; float P_N1_interval = 0.001;
    float P_N20 = 0.07 * dt; float P_N20_interval = 0.001;// probability of creating an N2 from a immature N1
    float P_N2i = 0.06 * dt; float P_N2i_interval = 0.001;// probability of creating an N2 from a intermediate N1
    float P_N2m = 0.03 * dt; float P_N2m_interval = 0.001;// probability of creating an N2 from a mature N1

    float k10 = 2; // Coefficients de raideurs élastiques des branches : k10=0 pour la branche primaire immature
    float k1 = 2; // Coefficients de raideurs élastiques des branches : k1 pour la branche primaire intermédiaire
    float k2 = 2; // Coefficients de raideurs élastiques des branches : k2 pour la branche secondaire
    float gamma1 = 2; // Contractilité de la branche primaire !!???
    bool B1_tension_only = false;
    bool B2_tension_only = false;
    float B1_stretch_maturation_threshold = 0.20; float B1_stretch_maturation_threshold_interval = 0.01;

    float delta = float(1.0) * M_PI / float(2.0); float delta_interval = M_PI / float(12.0); // inhibition domain

    float alpha0 = 50; // Coefficients de frottement α0 pour le nœud N0
    float alpha10 = 10.0; // Coefficients de frottement α10 = 0 pour le nœud N1 immature (pas d’adhésion)
    float alpha1i_constant_coefficient() { return alpha10; };
    float alpha1i_slope_coefficient = 1.25; float alpha1i_slope_coefficient_interval = 0.1;
    float alpha1i(float t) { return alpha1i_slope_coefficient * t + alpha1i_constant_coefficient(); }; // Coefficients de frottement • α1i(t) pour le nœud N1 intermédiaire est une fonction sigmoïde qui dépend du temps et atteint une saturation dont le seuil αsat est à définir
    float alphasat = 100;
    float alpha1m = alphasat; // Coefficients de frottement α1m pour le nœud N1 mature avec α1m > αsat
    float alpha2 = 100; // Coefficients de frottement α2 pour le nœud N2 avec α2 << αsat


    float ruptureForce_N0 = 0; // not used !!!
    float ruptureForce_N1 = 200;
    float ruptureForce_N2 = 15;


    // Minimums
    float DN1i_min = 1;
    float DN1m_min = 1;
    float DN2_min = 1;
    float l1min = 1; // Longueurs de création des branches : l1 pour la branche primaire avec la valeur l1 tirée aléatoirement entre l1min et l1max

    float l2_min = 1; // Longueurs de création des branches : l2 pour la branche secondaire avec l2 longueur constante à fixer
    //float l1eq; //l1eq = l1 initial
    float l2_prestress_min = 0.2;

    float P_N1_min  = 0.01 * dt;
    float P_N20_min = 0.01 * dt; // probability of creating an N2 from a immature N1
    float P_N2i_min = 0.01 * dt; // probability of creating an N2 from a intermediate N1
    float P_N2m_min = 0.01 * dt; // probability of creating an N2 from a mature N1

    float k10_min = 0.1; // Coefficients de raideurs élastiques des branches : k10=0 pour la branche primaire immature
    float k1_min = 0.1; // Coefficients de raideurs élastiques des branches : k1 pour la branche primaire intermédiaire
    float k2_min = 0.1; // Coefficients de raideurs élastiques des branches : k2 pour la branche secondaire
    float gamma1_min = 0.0; // Contractilité de la branche primaire !!???

    float B1_stretch_maturation_threshold_min = 0.0;

    float delta_min = float(1.0) * M_PI / float(12.0); // inhibition domain

    float alpha0_min = 0.1; // Coefficients de frottement α0 pour le nœud N0
    float alpha10_min = 0.1; // Coefficients de frottement α10 = 0 pour le nœud N1 immature (pas d’adhésion)
    float alpha1i_constant_coefficient_min() { return alpha10_min; };
    float alpha1i_slope_coefficient_min = 0.1;
    float alphasat_min = 0.1;
    float alpha1m_min = alphasat_min; // Coefficients de frottement α1m pour le nœud N1 mature avec α1m > αsat
    float alpha2_min = 0.1; // Coefficients de frottement α2 pour le nœud N2 avec α2 << αsat


    float ruptureForce_N1_min = 0;
    float ruptureForce_N2_min = 0;


    // Maximums
    float DN1i_max = 100*60;
    float DN1m_max = 100*60;
    float DN2_max = 10*60;
    float l1max = 6;

    float l2_max = 15; // Longueurs de création des branches : l2 pour la branche secondaire avec l2 longueur constante à fixer
    //float l1eq; //l1eq = l1 initial
    float l2_prestress_max = 1.0;

    float P_N1_max = 0.5 * dt;
    float P_N20_max = 0.5 * dt; // probability of creating an N2 from a immature N1
    float P_N2i_max = 0.5 * dt; // probability of creating an N2 from a intermediate N1
    float P_N2m_max = 0.5 * dt; // probability of creating an N2 from a mature N1

    float k10_max = 100; // Coefficients de raideurs élastiques des branches : k10=0 pour la branche primaire immature
    float k1_max = 100; // Coefficients de raideurs élastiques des branches : k1 pour la branche primaire intermédiaire
    float k2_max = 100; // Coefficients de raideurs élastiques des branches : k2 pour la branche secondaire
    float gamma1_max = 500; // Contractilité de la branche primaire !!???

    float B1_stretch_maturation_threshold_max = 5.0;

    float delta_max = float(1.0) * M_PI / float(1.0); //float(3.0)/float(4.0) * M_PI; // inhibition domain

    float alpha0_max = 1000; // Coefficients de frottement α0 pour le nœud N0
    float alpha10_max = 1000; // Coefficients de frottement α10 = 0 pour le nœud N1 immature (pas d’adhésion)
    float alpha1i_constant_coefficient_max() { return alpha10_max; };
    float alpha1i_slope_coefficient_max = 10;
    float alphasat_max = 1000;
    float alpha1m_max = alphasat_max; // Coefficients de frottement α1m pour le nœud N1 mature avec α1m > αsat
    float alpha2_max = 1000; // Coefficients de frottement α2 pour le nœud N2 avec α2 << αsat

    float ruptureForce_N1_max = 400;
    float ruptureForce_N2_max = 100;


    // Display ---------------

    // Screen & scale parameters
    int ScreenHeight = 720;  int ScreenHeight_min = 400;  int ScreenHeight_max = 1208;
    int ScreenWidth = 1280; int ScreenWidth_min = 600; int ScreenWidth_max = 1920;
    float PixelsPerMeter = 20; float PixelsPerMeter_min = 1; float PixelsPerMeter_max = 100;
    float MetersPerPixel() { return 1 / PixelsPerMeter; }
    // Legend parameters
    float legend_offsetX_coefficient = 0.75; float legend_offsetX_coefficient_min = 0.10; float legend_offsetX_coefficient_max = 5; float legend_offsetX_coefficient_interval = 0.01;
    float legend_offsetY_coefficient = 0.3; float legend_offsetY_coefficient_min = 0.10; float legend_offsetY_coefficient_max = 5; float legend_offsetY_coefficient_interval = 0.01;
    float legend_offsetX() { return legend_offsetX_coefficient * ScreenWidth * MetersPerPixel(); }
    float legend_offsetY() { return legend_offsetY_coefficient * ScreenHeight * MetersPerPixel(); }
    float legend_spacingX = 2; float legend_spacingX_min = 1; float legend_spacingX_max = 10; float legend_spacingX_interval = 0.1;
    float legend_spacingY = 5; float legend_spacingY_min = 1; float legend_spacingY_max = 10; float legend_spacingY_interval = 0.1;

    // show node type
    bool displayLegend = true;
    bool displayNodeType = false;
    bool displayNodeID = false;
    bool displayNodeMaturationState = false;
    bool displayBranchID = false;
    bool displayBranchType = false;


    int nbIntervals(float v_min, float v_max, float v_step){
        return (v_max-v_min)/v_step;
    }
    int intervalIndex(float v_min, float v_step, float v)
    {
        return (v-v_min)/v_step;
    }
};

#endif // PARAMETERS_H
