#ifndef SMARTSUBSTRATE_H
#define SMARTSUBSTRATE_H

#include <iostream>
#include <memory>
#include <map>

#include "properties.h"
#include "Vector2D.h"


enum _patternShape : unsigned int
{
    patternShape_undefined,
    patternShape_triangle,
    patternShape_rectangle,
    patternShape_circle,
    patternShape_unconstrained
};


float k_subsrate_rigidity(Vector2D xy);


class SubstratePattern
{
    public:
        virtual void draw(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {}
        virtual std::string patternToSingleLineText(ParametersObj& param, int i) { std::string lineTextPattern = ""; return lineTextPattern; }
        virtual bool isIn(Vector2D& pt) { return false; }
        _patternShape shapeType = patternShape_undefined;
        unsigned int partternRefNb = 0;
        virtual void setAdhesionStrength(float newAdhesionStrength) { adhesionStrength = newAdhesionStrength; };
        virtual void setAdhesionStrength(ParametersObj& param, float newAdhesionStrength) { adhesionStrength = newAdhesionStrength; calculateColor(param); };
        virtual float getAdhesionStrength() { return adhesionStrength; }
        virtual sf::Color getPatternColor() { return patternColor; }
        virtual void setPatternColor(sf::Color newPatternColor) { patternColor = newPatternColor; }
        sf::Color calculateColor(ParametersObj& param)
        {
            ColorObj newColor =  ColorObj(param.colorSubstratePattern);
            HSVcolor HSV = newColor.getHSV();
            HSV.V = getAdhesionStrength() * HSV.V;
            sf::Color color_sfml = newColor.hsv2rgb(HSV);
            setPatternColor(color_sfml);
            return getPatternColor();
        }
    private:
        float adhesionStrength = 1.0;
        sf::Color patternColor = sf::Color(255, 0, 0, 1);
};


class SubstratePattern_Circle : public SubstratePattern
{
public:
    SubstratePattern_Circle(ParametersObj& param, Vector2D position, float radius): position(position), radius(radius){
        _patternShape shapeType = patternShape_circle;
        // set pattern color
        calculateColor(param);
    };
    ~SubstratePattern_Circle() {};
    void draw(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {
        // create an empty shape
        float radius_pixel = radius * param.PixelsPerMeter;
        sf::CircleShape circle(radius_pixel);
        // Applying values to SFML methods
        circle.setOrigin(radius_pixel, radius_pixel);
        circle.setRadius(radius_pixel);
        circle.setPosition(position.getPixelCoord(param));
        circle.setFillColor(param.colorSubstratePattern);
        // Drawing
        window_ptr->draw(circle);
    };
    virtual std::string patternToSingleLineText(ParametersObj& param, int simulationNumber) {
        if (param.adhereEveryWhere) { return std::string(""); }

        std::string lineTextPattern = std::to_string(simulationNumber) + ",";
        lineTextPattern += std::to_string(param.labelRefNb) + ",";
        lineTextPattern += std::string("circle") + ",";
        lineTextPattern += std::to_string(radius) + ",";
        lineTextPattern += std::to_string(getAdhesionStrength()) + ",";
        lineTextPattern += std::to_string(position.x) + "," + std::to_string(position.y)  + "\n";
        return lineTextPattern;
    }
    bool isIn(Vector2D& pt)
    {
        if (position.distance(pt) < radius){ return true;}
        return false; 
    };
    Vector2D position;
    float radius;
};

///////////////////////////////////

class SubstratePattern_Rectangle : public SubstratePattern
{
public:
    SubstratePattern_Rectangle() {};
    SubstratePattern_Rectangle(ParametersObj& param, Vector2D position, float height, float width, float rotation = 0) {
        _patternShape shapeType = patternShape_rectangle;
        nodeA = position;
        nodeB = position;
        nodeC = position;
        nodeD = position;

        nodeA.add(Vector2D( width/2.0, height/2.0));
        nodeB.add(Vector2D( width/2.0,-height/2.0));
        nodeC.add(Vector2D(-width/2.0,-height/2.0));
        nodeD.add(Vector2D(-width/2.0, height/2.0));

        // set pattern color
        calculateColor(param);

    };
    ~SubstratePattern_Rectangle() {};
    void draw(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {
        // create an empty shape
        sf::ConvexShape convex;

        // resize it to 5 points
        convex.setPointCount(4);

        // define the points
        convex.setPoint(0, nodeA.getPixelCoord(param));
        convex.setPoint(1, nodeB.getPixelCoord(param));
        convex.setPoint(2, nodeC.getPixelCoord(param));
        convex.setPoint(3, nodeD.getPixelCoord(param));
        convex.setFillColor(calculateColor(param));
        // Drawing
        window_ptr->draw(convex);
    };
    virtual std::string patternToSingleLineText(ParametersObj& param, int simulationNumber) {
        if (param.adhereEveryWhere) { return std::string(""); }

        std::string lineTextPattern = std::to_string(simulationNumber) + ",";
        lineTextPattern += std::to_string(param.labelRefNb) + ",";
        lineTextPattern += std::string("rectangle") + ",";
        lineTextPattern += std::string("no radius") + ",";
        lineTextPattern += std::to_string(getAdhesionStrength()) + ",";
        lineTextPattern += std::to_string(nodeA.x) + "," + std::to_string(nodeA.y) + ",";
        lineTextPattern += std::to_string(nodeB.x) + "," + std::to_string(nodeB.y) + ",";
        lineTextPattern += std::to_string(nodeC.x) + "," + std::to_string(nodeC.y) + ",";
        lineTextPattern += std::to_string(nodeD.x) + "," + std::to_string(nodeD.y);
        lineTextPattern += "\n";
        return lineTextPattern;
    }

    bool isIn(Vector2D& pt)
    {
        return (pt.x < nodeA.x) && (pt.x > nodeD.x) && (pt.y < nodeA.y) && (pt.y > nodeB.y);
    };
    Vector2D nodeA;
    Vector2D nodeB;
    Vector2D nodeC;
    Vector2D nodeD;
};


///////////////////////////////////

class SubstratePattern_Triangle : public SubstratePattern
{
    public:
        SubstratePattern_Triangle() {};
        SubstratePattern_Triangle(ParametersObj& param, Vector2D position, float height, float width, float rotation) {
            _patternShape shapeType = patternShape_triangle;
            nodeA = position;
            nodeB = position;
            nodeC = position;

            nodeA.add(Vector2D(0, height/2.0));
            nodeB.add(Vector2D(0,-height/2.0));
            nodeC.add(Vector2D(width, 0));
        };
        ~SubstratePattern_Triangle() {};
        void draw(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {
            // create an empty shape
            sf::ConvexShape convex;

            // resize it to 5 points
            convex.setPointCount(3);

            // define the points
            convex.setPoint(0, nodeA.getPixelCoord(param));
            convex.setPoint(1, nodeB.getPixelCoord(param));
            convex.setPoint(2, nodeC.getPixelCoord(param));
            convex.setFillColor(param.colorSubstratePattern);
            // Drawing
            window_ptr->draw(convex);
        };
        virtual std::string patternToSingleLineText(ParametersObj& param, int simulationNumber) {
            if (param.adhereEveryWhere) { return std::string(""); }

            std::string lineTextPattern = std::to_string(simulationNumber) + ",";
            lineTextPattern += std::to_string(param.labelRefNb) + ",";
            lineTextPattern += std::string("triangle") + ",";
            lineTextPattern += std::string("no radius") + ",";
            lineTextPattern += std::to_string(getAdhesionStrength()) + ",";
            lineTextPattern += std::to_string(nodeA.x) + "," + std::to_string(nodeA.y) + ",";
            lineTextPattern += std::to_string(nodeB.x) + "," + std::to_string(nodeB.y) + ",";
            lineTextPattern += std::to_string(nodeC.x) + "," + std::to_string(nodeC.y);
            lineTextPattern += "\n";
            return lineTextPattern;
        }
        bool isIn(Vector2D& pt) { return PointInTriangle(pt); };
        Vector2D nodeA;
        Vector2D nodeB;
        Vector2D nodeC;

    private:
        float sign(Vector2D p1, Vector2D  p2, Vector2D p3)
        {
            return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
        }

        bool PointInTriangle(Vector2D pt)
        {
            float d1, d2, d3;
            bool has_neg, has_pos;

            d1 = sign(pt, nodeA, nodeB);
            d2 = sign(pt, nodeB, nodeC);
            d3 = sign(pt, nodeC, nodeA);

            has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
            has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

            return !(has_neg && has_pos);
        }
};


class SmartSubstrate
{
    public:
        SmartSubstrate() {};
        SmartSubstrate(ParametersObj& param) {};
        virtual ~SmartSubstrate() {};
        bool adhereEveryWhere = false;
        unsigned int nbPatterns = 0;
        float defaultAdhesionStrengthOutsidePattern = 1.0; // adhesion strength outside pattern
        std::map<unsigned int, std::shared_ptr<SubstratePattern>> substratePattern_Map;
        void addPattern(std::shared_ptr<SubstratePattern> pattern) {
            pattern->partternRefNb = nbPatterns;
            substratePattern_Map.insert(std::make_pair(pattern->partternRefNb, pattern));
            nbPatterns++;
        };
        bool isIn(Vector2D pt) {
            if (adhereEveryWhere) { return true; }
            std::map<unsigned int, std::shared_ptr<SubstratePattern>>::iterator it_pattern = substratePattern_Map.begin();
            while (it_pattern != substratePattern_Map.end())
            {
                if (it_pattern->second->isIn(pt)) { return true; }
                it_pattern++;
            }
            return false;
        }
        float getAdhesionStrength(Vector2D pt)
        {
            if (adhereEveryWhere) { return 1.0; }
            std::map<unsigned int, std::shared_ptr<SubstratePattern>>::iterator it_pattern = substratePattern_Map.begin();
            while (it_pattern != substratePattern_Map.end())
            {
                if (it_pattern->second->isIn(pt)) { return it_pattern->second->getAdhesionStrength(); }
                it_pattern++;
            }
            return defaultAdhesionStrengthOutsidePattern;
        }

        void draw(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {
            if (!param.adhereEveryWhere)
            {
                std::map<unsigned int, std::shared_ptr<SubstratePattern>>::iterator it_pattern = substratePattern_Map.begin();
                while (it_pattern != substratePattern_Map.end())
                {
                    it_pattern->second->draw(param, window_ptr);
                    it_pattern++;
                }      
            }
        }
        static std::string patternToHeaderLineText()
        {
            std::string lineTextPattern = "";
            lineTextPattern += "simulationNumber,labelRefNb,patternType,radius,adhesionStrengthFactor,nodeA.x,nodeA.y,nodeB.x,nodeB.y,nodeC.x,nodeC.y,nodeD.x,nodeD.y";
            lineTextPattern += "\n";
            return lineTextPattern;
        }
        std::string exportPatternsToCSV(ParametersObj& param, int simulationNumber)
        {
            std::string linesPatterns = "";
            std::map<unsigned int, std::shared_ptr<SubstratePattern>>::iterator it_pattern = substratePattern_Map.begin();
            while (it_pattern != substratePattern_Map.end())
            {
                linesPatterns += it_pattern->second->patternToSingleLineText(param, simulationNumber);
                it_pattern++;
            }
            return linesPatterns;
        }
};

#endif