#include "RecordsOfNodeTransitionPeriods.h"

void RecordsOfNodeTransitionPeriods::addImmatureN1TransitionPeriodToList(float age) { list_immatureNodeTransitionPeriod.push_back(age); }
void RecordsOfNodeTransitionPeriods::addIntermediateN1TransitionPeriodToList(float age) { list_intermediateNodeTransitionPeriod.push_back(age); }
void RecordsOfNodeTransitionPeriods::addMatureN1TransitionPeriodToList(float age) { list_matureNodeTransitionPeriod.push_back(age); }
void RecordsOfNodeTransitionPeriods::addImmatureN1Death(float age) { nbImmatureN1Deaths++;  addN1AgeAtDeathToList(age);};
void RecordsOfNodeTransitionPeriods::addIntermediateN1Death(float age) { nbIntermediateN1Deaths++;  addN1AgeAtDeathToList(age);};
void RecordsOfNodeTransitionPeriods::addMatureN1Death(float age) { nbMatureN1Deaths++; addN1AgeAtDeathToList(age);};
int RecordsOfNodeTransitionPeriods::nbImmatureN1Transitions() { return list_immatureNodeTransitionPeriod.size(); }
int RecordsOfNodeTransitionPeriods::nbIntermediateN1Transitions() { return list_intermediateNodeTransitionPeriod.size(); };
int RecordsOfNodeTransitionPeriods::nbMatureImmatureN1Transitions() { return list_matureNodeTransitionPeriod.size(); };

float RecordsOfNodeTransitionPeriods::immatureN1TransitionPeriod_minimum(){ return vector_minimum(list_immatureNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::immatureN1TransitionPeriod_max(){ return vector_maximum(list_immatureNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::immatureN1TransitionPeriod_mean(){ return vector_mean(list_immatureNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::intermediateN1TransitionPeriod_minimum(){ return vector_minimum(list_intermediateNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::intermediateN1TransitionPeriod_maximum(){ return vector_maximum(list_intermediateNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::intermediateN1TransitionPeriod_mean(){ return vector_mean(list_intermediateNodeTransitionPeriod); }
float RecordsOfNodeTransitionPeriods::matureN1TransitionPeriod_minimum() { return vector_minimum(list_matureNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::matureN1TransitionPeriod_maximum(){ return vector_maximum(list_matureNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::matureN1TransitionPeriod_mean(){ return vector_mean(list_matureNodeTransitionPeriod);}
float RecordsOfNodeTransitionPeriods::matureN1TransitionPeriod_std() { return vector_standardDeviation(list_matureNodeTransitionPeriod); }
float RecordsOfNodeTransitionPeriods::intermediateN1TransitionPeriod_std() { return vector_standardDeviation(list_intermediateNodeTransitionPeriod); }

///////////// N1 nodes
void RecordsOfNodeTransitionPeriods::addN1AgeAtDeathToList(float age) { list_N1AgesAtDeath.push_back(age); }
float RecordsOfNodeTransitionPeriods::N1AgeAtDeath_mean() { return vector_mean(list_N1AgesAtDeath);}
float RecordsOfNodeTransitionPeriods::N1AgeAtDeath_standardDeviation(){ return vector_standardDeviation(list_N1AgesAtDeath); }
float RecordsOfNodeTransitionPeriods::N1AgeAtDeath_maximum(){ return vector_maximum(list_N1AgesAtDeath);}

///////////// N2 nodes
void RecordsOfNodeTransitionPeriods::addN2AgeAtDeathToList(float age) { list_N2AgesAtDeath.push_back(age); }
float RecordsOfNodeTransitionPeriods::N2AgeAtDeath_mean(){ return vector_mean(list_N2AgesAtDeath);}
float RecordsOfNodeTransitionPeriods::N2AgeAtDeath_standardDeviation() { return vector_standardDeviation(list_N2AgesAtDeath);}
float RecordsOfNodeTransitionPeriods::N2AgeAtDeath_maximum(){ return vector_maximum(list_N2AgesAtDeath); }