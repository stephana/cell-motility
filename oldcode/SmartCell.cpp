#include "SmartCell.h"

SmartCell::SmartCell(){}

SmartCell::SmartCell(ParametersObj& param, unsigned int cellRefNb) {
    colorCytoplasm = param.colorCytoplasm; refNb = cellRefNb;
    std::cout << "coucou new cell created" << std::endl;
}

SmartCell::~SmartCell(){}
std::shared_ptr<SmartNode> SmartCell::addNode(ParametersObj& param, Vector2D xy){
    // This method is used to add an extremity
    int nodeRefNb = nodeRefNb_iterator;
    std::shared_ptr<SmartNode> node_shared_ptr = std::make_shared<SmartNode>(param, xy, nodeRefNb);
    node_Map.insert(std::make_pair(nodeRefNb, node_shared_ptr));
    nodeRefNb_iterator = nodeRefNb_iterator + 1; // we update this value so that the next node does not get the same ref number.
    return node_shared_ptr;
}

void SmartCell::addInteraction(std::shared_ptr<SmartNode> nodeA_shared_ptr, std::shared_ptr<SmartNode> nodeB_shared_ptr)// adds interaction between two nodes
{
    std::shared_ptr<SmartInteraction> interaction = std::make_shared<SmartInteraction>(nodeA_shared_ptr, nodeB_shared_ptr); // we create the interaction and give it a pair of node references.
    interaction_List.emplace_back(interaction);//interaction_List.push_back(interaction);
}

void SmartCell::applyInteractionForce(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, int interactionNb, float dt)
{
    if (interaction_List[interactionNb]->isAlive()){
        // apply the force on both nodes
        interaction_List[interactionNb]->applyForceToNodePair(param, nodeTransitionRecords, CPM, dt);
    }
}

void SmartCell::morphologyManager(float& t_halfLife, float& dt){}

int SmartCell::getNbNodes() {return node_Map.size();}
int SmartCell::getNbInteractions(){ return interaction_List.size();}
std::shared_ptr<SmartInteraction> SmartCell::getInteraction_ptr(std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction) { return *it_interaction;}
std::shared_ptr<SmartNode> SmartCell::getNode_ptr(std::map<int, std::shared_ptr<SmartNode>>::iterator it_node) { return it_node->second; }
void SmartCell::protrusionManager(){}
void SmartCell::drawCytoplasm(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr){}
void SmartCell::drawNodes(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        // Drawing background nodes (cytoplasm)
        // Collecting values
        Vector2D nodePosition = getNode_ptr(it_node)->getPosition();

        // Applying values to SFML methods

        if (getNode_ptr(it_node)->isN0())
        {
            drawN0(param, window_ptr, nodePosition);
        }
        else if (getNode_ptr(it_node)->isN1())
        {
            if (getNode_ptr(it_node)->isIntermediate()){ drawN1_intermediate(param, window_ptr, nodePosition); }
            else if (getNode_ptr(it_node)->isMature()) { drawN1_mature(param, window_ptr, nodePosition);}
            else { drawN1_immature(param, window_ptr, nodePosition); }

            if (param.displayNodeMaturationState)
            {
                sf::Text text("", *param.font_ptr, 20);
                if (getNode_ptr(it_node)->isMature()) { text.setString("Mature"); }
                else if (getNode_ptr(it_node)->isIntermediate()) { text.setString("Intermediate"); }
                else { text.setString("Immature"); }
                text.setPosition(nodePosition.getPixelCoord(param));
                window_ptr->draw(text);
            }  
        }
        else if (getNode_ptr(it_node)->isN2())
        {
            drawN2(param, window_ptr, nodePosition);
        }
        else { std::cout << "Node not displayed! Its type is not properly defined" << std::endl; }


        if (param.displayNodeID || param.displayNodeType)
        {
            sf::Text text("Node", *param.font_ptr, 20);
            if (param.displayNodeID) { text.setString("N" + std::to_string(getNode_ptr(it_node)->refNb)); }
            else if (param.displayNodeType) {  text.setString("N" + std::to_string(getNode_ptr(it_node)->getOrder()) ); }
            text.setPosition(nodePosition.getPixelCoord(param));
            window_ptr->draw(text);
        }
        it_node++;
    }
}
void SmartCell::drawInteractions(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
        /** Display Interactions **/
        for (int i=int(interaction_List.size()-1); i>=0; i--)
        {
            interaction_List[i]->draw(param, window_ptr, i);
        }
}
void SmartCell::shouldLive(ParametersObj& param, float& dt)
{
    std::vector< std::shared_ptr<SmartInteraction> >::iterator it_interaction = interaction_List.begin();
    it_interaction = interaction_List.begin();
    while (it_interaction != interaction_List.end())// || !isConnectedToParentNode)
    {   
        getInteraction_ptr(it_interaction)->nodeA_shared_ptr->is_connected_to_parent = true;
        getInteraction_ptr(it_interaction)->nodeB_shared_ptr->is_connected_to_parent = true;
        ++it_interaction;
    }

    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    it_node++;
    while (it_node != node_Map.end())
    {
        if (!getNode_ptr(it_node)->is_connected_to_parent)
        {
            getNode_ptr(it_node)->setAliveToFalse("no_parent"); // kill nodes not connected to parents
        }
        else {
            getNode_ptr(it_node)->shouldLive(param, dt);
            // initialize value for next time
            getNode_ptr(it_node)->is_connected_to_parent = false; // initialize value for next time
        }
        it_node++;
    }
}
void SmartCell::killTheDead(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM)
{
    std::vector< std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin();
    while (it_interaction != interaction_List.end())
    {
        if (!getInteraction_ptr(it_interaction)->isAlive())
        {
            // the interaction is non valid, it will be deleted
            // erase() invalidates the iterator, use returned iterator
            it_interaction = interaction_List.erase(it_interaction);
        }
        else { ++it_interaction; }
    }

    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        // remove odd numbers
        if (!getNode_ptr(it_node)->isAlive())
        {
            // erase() invalidates the iterator, use returned iterator
            if (param.recordDetails)
            {
                float age = getNode_ptr(it_node)->age;
                if (getNode_ptr(it_node)->isN1())
                {
                    if (getNode_ptr(it_node)->isMature())
                    {
                        nodeTransitionRecords.addMatureN1Death(age);
                        CPM.nodeLifeEventsValuesToString(param, *getNode_ptr(it_node), "death", "N1", "mature");
                    }
                    else if (getNode_ptr(it_node)->isIntermediate())
                    {
                        nodeTransitionRecords.addMatureN1Death(age);
                        CPM.nodeLifeEventsValuesToString(param, *getNode_ptr(it_node), "death", "N1", "intermediate");
                        nodeTransitionRecords.addIntermediateN1Death(age);
                    }
                    else
                    {
                        CPM.nodeLifeEventsValuesToString(param, *getNode_ptr(it_node), "death", "N1", "immature");
                        nodeTransitionRecords.addImmatureN1Death(age);
                    }
                }
                else if (getNode_ptr(it_node)->isN2())
                {
                    CPM.nodeLifeEventsValuesToString(param, *getNode_ptr(it_node), "death", "N2");
                    nodeTransitionRecords.addN2AgeAtDeathToList(age);
                }
            }
            it_node = node_Map.erase(it_node);
        }
        // Notice that iterator is incremented only on the else part (why?)
        else { ++it_node; }
    }
}

void SmartCell::addNewSubstrateAdhesionStrengthFactorToList(float newSubstrateAdhesionStrengthFactor) {
    listOfSubstrateAdhesionStrengthFactorsInContactWithCell.insert(newSubstrateAdhesionStrengthFactor);
};

void SmartCell::clearListOfSubstrateAdhesionStrengthFactors() {
    listOfSubstrateAdhesionStrengthFactorsInContactWithCell.clear();
    };

int SmartCell::nbSubstrateAdhesionStrengthFactors() {
    return listOfSubstrateAdhesionStrengthFactorsInContactWithCell.size();
};

std::string SmartCell::snapshot(ParametersObj& param)
{
    return std::string("");
};

unsigned int SmartCell::getRefNb() { return refNb; };

void SmartCell::setRefNb(unsigned int cellRefNb) { refNb = cellRefNb; };

/** #########################################################################################################
    ########################################  SPYDER CELL  ##################################################
    ######################################################################################################### **/
SpyderCell::SpyderCell(ParametersObj& param, unsigned int cellRefNb, Vector2D xy)
{
    colorCytoplasm = param.colorCytoplasm;
    //colorCytoplasm.a = 100;
    /** this class creates extremities collectively when the cell is being created,
    so that the cell starts out in a circular configuration. **/

    // Giving the cell color
    //Create central node at the beginning of the cell life
    N0_ptr = addNode(param, xy );
    N0_ptr->setOrderToN0(param);
    // Create Extremities at the start of the cell life
    // The extremities are nodes arranged in a circular fashion

    setRefNb(cellRefNb);
}


SpyderCell::~SpyderCell()
{
}


void SpyderCell::forces(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt)
{
    clearListOfSubstrateAdhesionStrengthFactors();

#if ASYNC_SMARTCELL_APPLY_INTERACTION_FORCE
    // scan all interactions
    for (int i = 0; i<int(interaction_List.size()); i++)
    {
        std::async(std::launch::async, &SmartCell::applyInteractionForce, this, param, nodeTransitionRecords, i, dt);
    }
#else
    // scan all interactions
    for (int i = 0; i<int(interaction_List.size()); i++)
    {
        applyInteractionForce(param, nodeTransitionRecords, CPM, i, dt);
    }
#endif
}

void SpyderCell::step(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt)
{

    int nodeRef = 0;
    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.find(nodeRef);
    Vector2D positionCentralNode = getNode_ptr(it_node)->getPosition();

    // step the simulation
    it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        bool onPattern = substrate.isIn(getNode_ptr(it_node)->getPosition());
        if (getNode_ptr(it_node)->isN1() && !getNode_ptr(it_node)->isMature() && !onPattern)
        {
            std::vector<std::shared_ptr<SmartNode>> childNodePtrs = getChildNodePtrsOnThisNode( *getNode_ptr(it_node) );
            std::vector<std::shared_ptr<SmartNode>>::iterator it_childnode = childNodePtrs.begin();
            while (it_childnode != childNodePtrs.end())
            {
                std::shared_ptr<SmartNode> childnode_ptr = *it_childnode;
                if (substrate.isIn(childnode_ptr->getPosition()))
                {
                    onPattern = true;
                    break;
                }
                it_childnode++;
            }

            float substrateAdhesionStrengthFactor = getNode_ptr(it_node)->stepNode(param, nodeTransitionRecords, CPM, dt, substrate, onPattern);
            addNewSubstrateAdhesionStrengthFactorToList(substrateAdhesionStrengthFactor);
        }
        else
        {
            float substrateAdhesionStrengthFactor = getNode_ptr(it_node)->stepNode(param, nodeTransitionRecords, CPM, dt, substrate, onPattern);
            addNewSubstrateAdhesionStrengthFactorToList(substrateAdhesionStrengthFactor);
        }
        it_node++;
    }
}
Vector2D SpyderCell::getPosition() {
    return N0_ptr->getPosition();
}


// Managers
void SpyderCell::morphologyManager(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, float& dt)
{
    N1Manager(param, nodeTransitionRecords, CPM, interCell_interaction_List, dt);
    N2Manager(param, interCell_interaction_List, dt);
}
void SpyderCell::N1Manager(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, float& dt)
{
    // Manage Extremities
    //if ( shouldProtrudeBasedOnLength() && shouldAddExtremity() ) // 
    int lambda1 = param.numberEventsDuringStep(nu1(param), param.dt);
    while (lambda1>0)
    {
        addN1(param, interCell_interaction_List);
        lambda1--;
    }

#if false //NEW_CHANGES
    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        if (getNode_ptr(it_node)->isN1())
        {
            if (countNbN2OnN1(getNode_ptr(it_node)->refNb, true) >= 4)
            {
                getNode_ptr(it_node)->setMature();
                nodeTransitionRecords.addMatureN1TransitionPeriodToList(getNode_ptr(it_node)->age);
                CPM.nodeLifeEventsValuesToString(param, *getNode_ptr(it_node), "transitionMature", "N1");
            }
        }
        it_node++;
    }
#endif
}


void SpyderCell::N2Manager(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, float& dt)
{
    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        if (getNode_ptr(it_node)->isN1())
        {
            int lambda2 = param.numberEventsDuringStep(nu2(param, getNode_ptr(it_node)), param.dt);
            while (lambda2 > 0) // we decide if we create N2 A
            {
                addN2(param, interCell_interaction_List, getNode_ptr(it_node)); // we create N2 A
                lambda2--;
            }
        }
        it_node++;
    }
}

//

void SpyderCell::applyInteractionForce(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, int interactionNb, float dt)
{
    if (interaction_List[interactionNb]->isAlive())
    {
        bool lengthen = false;
        // apply the force on both nodes
        if (interaction_List[interactionNb]->nodeB_shared_ptr->isImmature())
        {
            if (countNbN2OnN1(interaction_List[interactionNb]->nodeB_shared_ptr->refNb) == 0)
            {
                lengthen = true;
            }
        }
        interaction_List[interactionNb]->applyForceToNodePair(param, nodeTransitionRecords, CPM, dt, lengthen);
    }
}


// Counters
int SpyderCell::countNbN2OnN1(int nodeRefNb, bool nodesAreBound)
{
    int nbOfN2s = 0;
    int nbOfN2s_bound = 0;
 
    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction != interaction_List.end(); ++it_interaction)
    {
        int refNode_A = getInteraction_ptr(it_interaction)->getRefNbNodeA();
        int refNode_B = getInteraction_ptr(it_interaction)->getRefNbNodeB();
        
        if(nodeRefNb == refNode_A)
        {
            std::map<int, std::shared_ptr<SmartNode>>::iterator it_NB;
            it_NB = node_Map.find(refNode_B);
            if ( it_NB != node_Map.end() && getNode_ptr(it_NB)->isN2() )
            {
                nbOfN2s++;
                if (getNode_ptr(it_NB)->bound)
                {
                    nbOfN2s_bound++;
                }
            }
        }
        else if (nodeRefNb == refNode_B)
        {
            std::map<int, std::shared_ptr<SmartNode>>::iterator it_NA;
            it_NA = node_Map.find(refNode_A);
            if ( it_NA != node_Map.end() && getNode_ptr(it_NA)->isN2() )
            {
                nbOfN2s++;
                if (getNode_ptr(it_NA)->bound)
                {
                    nbOfN2s_bound++;
                }
            }
        }
    }

    if (nodesAreBound)
    {
        return nbOfN2s_bound;
    }
    return nbOfN2s;
}

int SpyderCell::countNbOfChildNodesOnThisNode(SmartNode& node)
{
    if (node.isN0()) { return countN1s(); }
    else if (node.isN1()) { return countNbN2OnN1(node.refNb); }
    else{ return 0; }
}


std::shared_ptr<SmartNode> SpyderCell::getParentNodePtrsOnThisNode(SmartNode& node)
{
    SmartNode& childNode = node;

    std::shared_ptr<SmartNode> parentNodePtrs = NULL;

    std::map<int, std::shared_ptr<SmartNode>>::iterator it_Node_other;

    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction != interaction_List.end(); ++it_interaction)
    {
        int refNode_A = getInteraction_ptr(it_interaction)->getRefNbNodeA();
        int refNode_B = getInteraction_ptr(it_interaction)->getRefNbNodeB();

        if (childNode.refNb == refNode_A || childNode.refNb == refNode_B) // if the node is part of the interaction pair
        {
            int refNb_Node_other = NULL;
            if (childNode.refNb == refNode_B) // if the node is in the second position it means that it is the child node, in other words node A is the parent node
            {
                return getNode_ptr(node_Map.find(refNode_A));
            }
        }
    }
    return parentNodePtrs;
}

std::vector<std::shared_ptr<SmartNode>> SpyderCell::getChildNodePtrsOnThisNode(SmartNode& node)
{
    SmartNode& nodeA = node;
    std::vector<std::shared_ptr<SmartNode>> ChildNodePtrs;

    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction != interaction_List.end(); ++it_interaction)
    {
        int refNode_A = getInteraction_ptr(it_interaction)->getRefNbNodeA();
        int refNode_B = getInteraction_ptr(it_interaction)->getRefNbNodeB();

        if (nodeA.refNb == refNode_A)
        {
            std::map<int, std::shared_ptr<SmartNode>>::iterator it_NB;
            it_NB = node_Map.find(refNode_B);
            if (it_NB != node_Map.end() && getNode_ptr(it_NB)->isN2())
            {
                ChildNodePtrs.push_back(getNode_ptr(it_NB));
            }
        }
        else if (nodeA.refNb == refNode_B)
        {
            std::map<int, std::shared_ptr<SmartNode>>::iterator it_NA;
            it_NA = node_Map.find(refNode_A);
            if (it_NA != node_Map.end() && getNode_ptr(it_NA)->isN2())
            {
                ChildNodePtrs.push_back(getNode_ptr(it_NA));
            }
        }
    }
    return ChildNodePtrs;
}


int SpyderCell::countN1s()
{
    int nbN1s = 0;

    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {        
        if(getNode_ptr(it_node)->isN1() )
        {
            nbN1s = nbN1s + 1;
        }
        it_node++;
    }
    return nbN1s;
}


int SpyderCell::countN2s()
{
    int nbN2s = 0;

    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        if (getNode_ptr(it_node)->isN2())
        {
            nbN2s = nbN2s + 1;
        }
        it_node++;
    }
    return nbN2s;
}

arma::vec SpyderCell::getAnglesInterCellInteractions(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List)
{
    std::vector<float> alphas; // initialize the std::vector list to that will contain all the angles
    std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interCell_interaction = interCell_interaction_List.begin();

    while( it_interCell_interaction != interCell_interaction_List.end() )
    {
        if ( (*it_interCell_interaction)->isAlive() && ( (*it_interCell_interaction)->nodeA_shared_ptr->getPatentCellRefNb()==getRefNb() || (*it_interCell_interaction)->nodeB_shared_ptr->getPatentCellRefNb()==getRefNb()) )
        {
            Vector2D positionNodeA = (*it_interCell_interaction)->nodeA_shared_ptr->getPosition();
            Vector2D positionNodeB = (*it_interCell_interaction)->nodeB_shared_ptr->getPosition();

            alphas.push_back(angleRandiants(Vector2D(1, 0), sub(positionNodeB, positionNodeA)));
        }
        ++it_interCell_interaction;
    }

    // we convert the std::vector to an arma::vec
    arma::vec alphas_arma = arma::conv_to<arma::vec>::from(alphas);  //arma::vec alphas = arma::vec(countN1s(), arma::fill::ones);
    return alphas_arma;
}


arma::vec SpyderCell::getAnglesChildOfN0()
{
    arma::vec alphas = arma::vec( countN1s(), arma::fill::ones);
    int j = 0;
    for (int i = int(interaction_List.size() - 1); i >= 0; i--)
    {
        if ( interaction_List[i]->isAlive() && interaction_List[i]->nodeA_shared_ptr->isN0() )
        {
            Vector2D positionNodeA = interaction_List[i]->nodeA_shared_ptr->getPosition();
            Vector2D positionNodeB = interaction_List[i]->nodeB_shared_ptr->getPosition();
            alphas[j] = angleRandiants(Vector2D(1, 0), sub(positionNodeB, positionNodeA));
            j = j + 1;
        }
    }
    return alphas;
}

arma::vec SpyderCell::getAnglesInterCellConnections(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List)
{
    // find if one of the cell has the same reference nb
    for (auto it_inteCell_interaction = begin(interCell_interaction_List); it_inteCell_interaction != end(interCell_interaction_List); ++it_inteCell_interaction)
    {
        (*it_inteCell_interaction)->nodeA_shared_ptr->getPatentCellRefNb();
    }

    unsigned int nbInterCellConnectionsWithThisCell = 0;
    arma::vec alphas = arma::vec( nbInterCellConnectionsWithThisCell, arma::fill::ones);
    return alphas;
}

// Decision Makers

float SpyderCell::nu1(ParametersObj& param){return param.nu1;}


float SpyderCell::nu2(ParametersObj& param, std::shared_ptr<SmartNode> N1_ptr)
{
    if (N1_ptr->isN1()) {
        if (N1_ptr->isImmature()) { return param.nu2_im; }
        else if (N1_ptr->isIntermediate()) { return param.nu2_int; }
        else if (N1_ptr->isMature()) { return param.nu2_m; }
        else { throw("Error in SpyderCell::nu2 the state of the N1 is neither immature, intermediate nor mature."); }
    }
    else {
        std::cout << "Warning: in SpyderCell::nu2 no node found using nodRefNb_N1, this means the N2 must not have a parent, probability to die is 1. -> (return nu2 = 1)" << std::endl;
        return 0;
    }
}

// Action taker (make it happen)
void SpyderCell::addN1(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List)
{
    // Extremity
    float xi, yi;
    NewAngle newangle = generateAngleOfNewN1(param, interCell_interaction_List);
    float length = generateLengthOfNewN1(param, newangle.angle);
    if (newangle.create)
    {
        // Here we will first calculate the coordinate of the node and then create it.
        xi = getPosition().x + length * cos(newangle.angle); // we calculate the x coordinate of the node
        yi = getPosition().y + length * sin(newangle.angle); // we calculate the x coordinate of the node

        // We add a new node
        std::shared_ptr<SmartNode> N1_ptr = addNode(param, Vector2D(xi, yi)); // we call the method addNode to create the i th node located at the coordinates (xi,yi)
        N1_ptr->setOrderToN1(param); // Node assignment to order 1
        Color nodeColor = getColor();
        nodeColor.Alpha = 255;
        nodeColor.R = 0.75 * nodeColor.R;
        N1_ptr->setColor(nodeColor);
        // We create an new interaction between the new node and the central cell node i.e. node [0]
        addInteraction(N0_ptr, N1_ptr); // Here we only pass the pointers of the nodes we want to connect.
    }
}
void SpyderCell::addN2(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, std::shared_ptr<SmartNode> N1_ptr)
{
    /**
     The protrusion method only creates a protrusion node
    **/
    if ( N1_ptr->isN1() && !N1_ptr->isInhibited() )
    {
        // define the position of the future protrusion
        Vector2D Vector_position_N1 = N1_ptr->getPosition();
        Vector2D Vector_CellCenter_N1 = sub( Vector_position_N1, getPosition() );

        // angle of vector from center to extremity
        float angle_CellCenter_N1 = angleRandiants(Vector2D(1, 0), Vector_CellCenter_N1);
        NewAngle angle = generateAngleOfNewN2(param, interCell_interaction_List, N1_ptr);
        
        if (angle.create)
        {
            angle.angle += angle_CellCenter_N1;
            float r = generateLengthOfNewN2(param);

            Vector2D Vector_positionProtrusion = Vector2D(Vector_position_N1.x + r * cos(angle.angle), Vector_position_N1.y + r * sin(angle.angle));

            // create a new smart node (protrusion)
            std::shared_ptr<SmartNode> N2_ptr = addNode(param, Vector_positionProtrusion);
            N2_ptr->setOrderToN2(param);
            // connect the new smart node
            addInteraction(N1_ptr, N2_ptr);
        }
    }
    else if (N1_ptr->isInhibited())
    {
        std::cout << "NB: Inhibited node cannot protrude !!!" << std::endl;
    }
    else
    {
        std::cout << "Warning: You are trying to create an N2, the parent node is not an N1 it therefore cannot protrude !!!" << std::endl;
    }
}
// Generators (length and angles)
float SpyderCell::generateLengthOfNewN1(ParametersObj& param, float& angle)
{
    if (param.l1_Generator == constant_B1_length_generator)
    {
        return param.l1initial;
    }
    else if (param.l1_Generator == uniforme_B1_length_generator)
    {
        return randomFloat(param.l1initial_min, param.l1initial_max);
    }

    return param.l1initial;
}
float SpyderCell::generateLengthOfNewN2(ParametersObj& param)
{
    if (param.l2_Generator == constant_B2_length_generator)
    {
        return param.l2initial;
    }
    else if (param.l2_Generator == uniforme_B2_length_generator)
    {
        return randomFloat(param.l2initial_min, param.l2initial_max);
    }

    return param.l2initial;



    //return randomFloat(3,param.l2initial); // <--------
}
NewAngle SpyderCell::generateAngleOfNewN1(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List)
{

    /* GROS PROBLEM ICI ?? APPROXIMATION DE PI/120 suffisante ?????
     * on a 1.5° Pres ????
     *
     * */
    NewAngle newangle;
    float dx = PI / 120;
    arma::Mat<double> x = arma::regspace(-PI, dx, PI);

    // We check where there is are existing B1/N1 to inhibit
    arma::vec alphas = getAnglesChildOfN0();

    // find intercell connections here to inhibit
    alphas.insert_rows(alphas.n_rows, getAnglesInterCellInteractions(param, interCell_interaction_List)); // source: https://stackoverflow.com/questions/28247490/push-back-append-or-appending-a-vector-with-a-loop-in-c-armadillo

    arma::uvec f = f_block_no_superposition(param, alphas, x);

    if (arma::sum(f) > 0)
    {
        arma::vec f_s = arma::vec(f.size(), arma::fill::ones);
        arma::vec f_s1 = arma::vec(f.size(), arma::fill::ones);
        f_s = f_s % f * dx;
        float area_f = arma::sum(f_s);
        arma::vec f_norm = f_s / area_f;
        float p = randomFloat(0, 1);
        arma::vec f_s2 = arma::cumsum(f_s1) % (arma::cumsum(f_norm) <= p);
        int index = round(arma::max(f_s2));
        newangle.angle = x[index];
        newangle.create = true;
    }
    //std::cout << "       newangle = " << newangle.angle << std::endl;
    return newangle;
}


NewAngle SpyderCell::generateAngleOfNewN2(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, std::shared_ptr<SmartNode> N1_ptr)
{
    NewAngle newangle;

    if ( !N1_ptr->isInhibited() )
    {
        newangle.create = true;
        newangle.angle = randomFloat(-param.delta_theta_2, param.delta_theta_2);
    }
    return newangle;

}


// Calculate (measure)
float SpyderCell::calculateTheLengthOfAllInteractions()
{
    float L = 0;

    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction < interaction_List.end(); ++it_interaction)
    {

        if (getInteraction_ptr(it_interaction)->isAlive())
        {
            L = L + getInteraction_ptr(it_interaction)->distanceBetweenNodes();
        }
    }
    return L;
}

float SpyderCell::calculateTheLengthOfAllB1s()
{
    float L = 0;

    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction < interaction_List.end(); ++it_interaction)
    {   
        if (getInteraction_ptr(it_interaction)->isAlive() && getInteraction_ptr(it_interaction)->nodeB_shared_ptr->isN1() )
        {
            L = L + getInteraction_ptr(it_interaction)->distanceBetweenNodes();
        }
    }
    return L;
}

float SpyderCell::calculateTheLengthOfAllB2s()
{
    float L = 0;

    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction < interaction_List.end(); ++it_interaction)
    {
        if (getInteraction_ptr(it_interaction)->isAlive() && getInteraction_ptr(it_interaction)->nodeB_shared_ptr->isN2())
        {
            L = L + getInteraction_ptr(it_interaction)->distanceBetweenNodes();
        }
    }
    return L;
}

void SpyderCell::drawCytoplasm(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
    sf::CircleShape circle(0);
    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        // Drawing background nodes (cytoplasm)
        // Collecting values
        Vector2D nodePosition = getNode_ptr(it_node)->getPosition();
        float nodeRadius = param.cytoplasmScaleFactor * getNode_ptr(it_node)->getRadiusPixel(param);
        sf::Color nodeColor = getNode_ptr(it_node)->getColor_SFML();

        // Applying values to SFML methods
        circle.setOrigin(nodeRadius, nodeRadius);
        circle.setRadius(nodeRadius);
        circle.setPosition(nodePosition.getPixelCoord(param));
        circle.setFillColor(colorCytoplasm);

        // Drawing
        window_ptr->draw(circle);
        it_node++;
    }

    for (int i = int(interaction_List.size() - 1); i >= 0; i--)
    {
        if (interaction_List[i]->isAlive())
        {
            Vector2D positionNodeA = interaction_List[i]->nodeA_shared_ptr->getPosition();
            Vector2D positionNodeB = interaction_List[i]->nodeB_shared_ptr->getPosition();
            float RadiusPixel_NodeA = interaction_List[i]->nodeA_shared_ptr->getRadiusPixel(param);
            float RadiusPixel_NodeB = interaction_List[i]->nodeB_shared_ptr->getRadiusPixel(param);
            drawTrapezoid(param, positionNodeA, positionNodeB, param.cytoplasmScaleFactor * RadiusPixel_NodeA, param.cytoplasmScaleFactor * RadiusPixel_NodeB, colorCytoplasm, window_ptr);
        }
    }

    if (!param.displayNodes) {
        Vector2D nodePosition = getPosition();
        float nodeRadius = param.PixelsPerMeter * param.radiusN0;

        // Applying values to SFML methods
        circle.setOrigin(nodeRadius, nodeRadius);
        circle.setRadius(nodeRadius);
        circle.setPosition(nodePosition.getPixelCoord(param));
        circle.setFillColor(param.colorN0_fill);

        // Drawing
        window_ptr->draw(circle);
    }
}

void SpyderCell::addInfoHeader(std::string& nodeInfo, std::string& branchInfo)
{
    nodeInfo = nodeInfo + nodeInfo_RegularTimeInterval_HeadersToString();
    branchInfo = branchInfo + branchInfo_RegularTimeInteval_HeadersToString();
}

void SpyderCell::addInfoData(ParametersObj& param, float t, std::string& nodeInfo, std::string& branchInfo)
{
    std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
    it_node = node_Map.begin();
    while (it_node != node_Map.end())
    {
        nodeInfo += nodeInfo_RegularTimeInterval_DataToString(param, *(it_node->second), t);
        it_node++;
    }

    for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interaction = interaction_List.begin(); it_interaction < interaction_List.end(); ++it_interaction)
    {
        if (getInteraction_ptr(it_interaction)->isAlive())
        {
            branchInfo += branchInfo_RegularTimeInteval_DataToString(param, **it_interaction, t);
        }
    }
}

std::string SpyderCell::nodeInfo_RegularTimeInterval_HeadersToString()
{
    std::string lineNodeInfo;
    lineNodeInfo += std::string("t") + ",";
    lineNodeInfo += std::string("nodeRefNb") + ",";
    lineNodeInfo += std::string("simulationRefNb") + ",";
    lineNodeInfo += std::string("simlabel") + ",";
    lineNodeInfo += std::string("simlabel_x") + ",";
    lineNodeInfo += std::string("simlabel_y") + ",";
    lineNodeInfo += std::string("simdetailed_label") + ",";
    lineNodeInfo += std::string("age") + ",";
    lineNodeInfo += std::string("netForce") + ",";
    lineNodeInfo += std::string("alpha") + ",";

    lineNodeInfo += std::string("isN0") + ",";
    lineNodeInfo += std::string("isN1") + ",";
    lineNodeInfo += std::string("isN2") + ",";
    lineNodeInfo += std::string("isImmature") + ",";
    lineNodeInfo += std::string("isIntermediate") + ",";
    lineNodeInfo += std::string("isMature") + ",";

    lineNodeInfo += std::string("countNbOfChildNodesOnThisNode") + ",";

    lineNodeInfo += std::string("x") + ",";
    lineNodeInfo += std::string("y");

    lineNodeInfo += std::string("\n"); // 
    return lineNodeInfo;
}
std::string SpyderCell::nodeInfo_RegularTimeInterval_DataToString(ParametersObj& param, SmartNode& node, float t)
{
    std::string lineNodeInfo;
    lineNodeInfo += std::to_string(t) + ",";
    lineNodeInfo += std::to_string(node.refNb) + ",";
    lineNodeInfo += std::to_string(param.simulationRefNb) + ",";
    lineNodeInfo += param.simlabel + ",";
    lineNodeInfo += param.simlabel_x + ",";
    lineNodeInfo += param.simlabel_y + ",";
    lineNodeInfo += param.simdetailed_label + ",";
    lineNodeInfo += std::to_string(node.age) + ",";
    lineNodeInfo += std::to_string(node.netForce_magnitude_previous_step) + ",";
    lineNodeInfo += std::to_string(node.getAlpha()) + ",";

    lineNodeInfo += std::to_string(node.isN0()) + ",";
    lineNodeInfo += std::to_string(node.isN1()) + ",";
    lineNodeInfo += std::to_string(node.isN2()) + ",";
    lineNodeInfo += std::to_string(node.isImmature()) + ",";
    lineNodeInfo += std::to_string(node.isIntermediate()) + ",";
    lineNodeInfo += std::to_string(node.isMature()) + ",";

    lineNodeInfo += std::to_string(countNbOfChildNodesOnThisNode(node)) + ",";

    lineNodeInfo += std::to_string(node.getPosition().x) + ",";
    lineNodeInfo += std::to_string(node.getPosition().y);

    lineNodeInfo += std::string("\n"); // 
    return lineNodeInfo;
}

std::string SpyderCell::branchInfo_RegularTimeInteval_HeadersToString()
{
    std::string lineBranchInfo;
    lineBranchInfo += std::string("t") + ",";
    lineBranchInfo += std::string("branchRefNb") + ",";
    lineBranchInfo += std::string("simulationRefNb") + ",";
    lineBranchInfo += std::string("simlabel") + ",";
    lineBranchInfo += std::string("simlabel_x") + ",";
    lineBranchInfo += std::string("simlabel_y") + ",";
    lineBranchInfo += std::string("simdetailed_label") + ",";

    lineBranchInfo += std::string("branchOrder") + ",";
    lineBranchInfo += std::string("parentNodeRefNb") + ",";
    lineBranchInfo += std::string("childNodeRefNb") + ",";
    lineBranchInfo += std::string("parentNodeNetForce") + ",";
    lineBranchInfo += std::string("childNodeNetForce") + ",";

    lineBranchInfo += std::string("branchAge") + ",";
    lineBranchInfo += std::string("parentNodeAge") + ",";
    lineBranchInfo += std::string("childNodeAge") + ",";

    lineBranchInfo += std::string("F") + ",";
    lineBranchInfo += std::string("F_el") + ",";
    lineBranchInfo += std::string("F_c") + ",";

    lineBranchInfo += std::string("childNodeIsImmature") + ",";
    lineBranchInfo += std::string("childNodeIsIntermediate") + ",";
    lineBranchInfo += std::string("childNodeIsMature") + ",";

    lineBranchInfo += std::string("L0_spring") + ",";
    lineBranchInfo += std::string("distanceNodesAB") + ",";
    lineBranchInfo += std::string("epsilon");

    lineBranchInfo += std::string("\n"); // 
    return lineBranchInfo;
}

std::string SpyderCell::branchInfo_RegularTimeInteval_DataToString(ParametersObj& param, SmartInteraction& branch, float t)
{
    std::string lineBranchInfo;
    lineBranchInfo += std::to_string(t) + ",";
    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->refNb) + ","; // branchRefNb
    lineBranchInfo += std::to_string(param.simulationRefNb) + ",";
    lineBranchInfo += param.simlabel + ",";
    lineBranchInfo += param.simlabel_x + ",";
    lineBranchInfo += param.simlabel_y + ",";
    lineBranchInfo += param.simdetailed_label + ",";

    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->getOrder()) + ",";
    lineBranchInfo += std::to_string(branch.nodeA_shared_ptr->refNb) + ",";
    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->refNb) + ",";
    lineBranchInfo += std::to_string(branch.nodeA_shared_ptr->netForce_magnitude_previous_step) + ",";
    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->netForce_magnitude_previous_step) + ",";

    lineBranchInfo += std::to_string(branch.age) + ",";
    lineBranchInfo += std::to_string(branch.nodeA_shared_ptr->age) + ",";
    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->age) + ",";

    lineBranchInfo += std::to_string(branch.F) + ",";
    lineBranchInfo += std::to_string(branch.F_el) + ",";
    lineBranchInfo += std::to_string(branch.F_c) + ",";

    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->isImmature()) + ",";
    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->isIntermediate()) + ",";
    lineBranchInfo += std::to_string(branch.nodeB_shared_ptr->isMature()) + ",";

    lineBranchInfo += std::to_string(branch.L0_spring) + ",";
    lineBranchInfo += std::to_string(branch.distanceNodesAB) + ",";
    lineBranchInfo += std::to_string(branch.epsilon);

    lineBranchInfo += std::string("\n"); // 
    return lineBranchInfo;
}


std::shared_ptr<SpyderCell> SpyderCell_List::getCell_ptr(std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell) { return *it_cell; }

void SpyderCell_List::addNewCell(ParametersObj& param, const Vector2D& position) {
    cellRefNbCounter++;
    std::shared_ptr<SpyderCell> cell_ptr = std::make_shared<SpyderCell>(param, cellRefNbCounter, position); // we create the interaction and give it a pair of node references.
    cell_List.emplace_back(cell_ptr);
}
void SpyderCell_List::addInfoHeader(std::string& nodeInfo, std::string& branchInfo) {
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())// || !isConnectedToParentNode)
    {
        getCell_ptr(it_cell)->addInfoHeader(nodeInfo, branchInfo);
        ++it_cell;
    }
}
void SpyderCell_List::drawCytoplasm(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())// || !isConnectedToParentNode)
    {
        getCell_ptr(it_cell)->drawCytoplasm(param, window_ptr);
        ++it_cell;
    }
}
void SpyderCell_List::drawInteractions(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())// || !isConnectedToParentNode)
    {
        getCell_ptr(it_cell)->drawInteractions(param, window_ptr);
        ++it_cell;
    }
    std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interCell_interaction = interCell_interaction_List.begin();
    while (it_interCell_interaction != interCell_interaction_List.end())// || !isConnectedToParentNode)
    {
        (*it_interCell_interaction)->draw(param, window_ptr, 0);
        ++it_interCell_interaction;
    }
}
void SpyderCell_List::drawNodes(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr) {
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())// || !isConnectedToParentNode)
    {
        getCell_ptr(it_cell)->drawNodes(param, window_ptr);
        ++it_cell;
    }
}
void SpyderCell_List::morphologyManager(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float& dt)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())// || !isConnectedToParentNode)
    {
        getCell_ptr(it_cell)->morphologyManager(param, nodeTransitionRecords, CPM, interCell_interaction_List, dt);
        ++it_cell;
    }
}
void SpyderCell_List::protrusionManager() {
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->protrusionManager();
        ++it_cell;
    }
}
void SpyderCell_List::forces(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->forces(param, nodeTransitionRecords, CPM, substrate, dt);
        ++it_cell;
    }

    InterCell_forces( param, nodeTransitionRecords, CPM, dt);

}

void SpyderCell_List::step(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->step(param, nodeTransitionRecords, CPM, substrate, dt);
        ++it_cell;
    }
}

void SpyderCell_List::shouldLive(ParametersObj& param, float& dt) {
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->shouldLive(param, dt);
        ++it_cell;
    }
}
void SpyderCell_List::killTheDead(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->killTheDead(param, nodeTransitionRecords, CPM);
        ++it_cell;
    }
}

void SpyderCell_List::addInfoData(ParametersObj& param, float t, std::string& nodeInfo, std::string& branchInfo)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->addInfoData(param, t, nodeInfo, branchInfo);
        ++it_cell;
    }
}

void SpyderCell_List::snapshot(ParametersObj& param, bool& cellSnapShotFileHasFeader, int& simulationNumber, float& t, std::string& snaptxt)
{
    std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell = cell_List.begin();
    while (it_cell != cell_List.end())
    {
        getCell_ptr(it_cell)->snapshot(param, cellSnapShotFileHasFeader, simulationNumber, t, snaptxt);
        ++it_cell;
    }
}

int SpyderCell_List::getNbCells() { return cell_List.size(); }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void drawLegend(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
    Vector2D rectangle_xy = Vector2D(param.legend_offsetX(), param.legend_offsetY() + 4.5*param.legend_spacingY);
    float rectangle_width = 14 * param.zoom;
    float rectangle_height = 5*param.legend_spacingY * param.zoom;

    //drawRectangle(param, rectangle_xy, rectangle_width, rectangle_height, param.colorBackground, window_ptr, true);

    sf::Text text("", *param.font_ptr, 20);
    text.setOrigin(0, 10); // this alignes the text with the nodes
    text.setFillColor(sf::Color(0, 0, 0, 255));
    
    Vector2D positionN0 = Vector2D(param.legend_offsetX() + param.legend_spacingX, param.legend_offsetY() + 4*param.legend_spacingY);
    drawN0(param, window_ptr, positionN0);
    text.setString("N0");
    Vector2D positionText = positionN0;
    positionText.x = positionN0.x + param.legend_spacingX;
    text.setPosition(positionText.getPixelCoord(param));
    window_ptr->draw(text);
    
    Vector2D positionN1 = Vector2D(param.legend_offsetX() + param.legend_spacingX, param.legend_offsetY() + 3*param.legend_spacingY);
    drawN1_immature(param, window_ptr, positionN1);
    text.setString("N1 immature");
    positionText = positionN1;
    positionText.x = positionN1.x + param.legend_spacingX;
    text.setPosition(positionText.getPixelCoord(param));
    window_ptr->draw(text);

    Vector2D positionN1_intermediate = Vector2D(param.legend_offsetX() + param.legend_spacingX, param.legend_offsetY() + 2 * param.legend_spacingY);
    drawN1_intermediate(param, window_ptr, positionN1_intermediate);
    text.setString("N1 intermediate");
    positionText = positionN1_intermediate;
    positionText.x = positionN1_intermediate.x + param.legend_spacingX;
    text.setPosition(positionText.getPixelCoord(param));
    window_ptr->draw(text);

    Vector2D positionN1_mature = Vector2D(param.legend_offsetX() + param.legend_spacingX, param.legend_offsetY() + 1 * param.legend_spacingY);
    drawN1_mature(param, window_ptr, positionN1_mature);
    text.setString("N1 mature");
    positionText = positionN1_mature;
    positionText.x = positionN1_mature.x + param.legend_spacingX;
    text.setPosition(positionText.getPixelCoord(param));
    window_ptr->draw(text);
    
    Vector2D positionN2 = Vector2D(param.legend_offsetX() + param.legend_spacingX, param.legend_offsetY() + 0*param.legend_spacingY);
    drawN2(param, window_ptr, positionN2);
    text.setString("N2");
    positionText = positionN2;
    positionText.x = positionN2.x + param.legend_spacingX;
    text.setPosition(positionText.getPixelCoord(param));
    window_ptr->draw(text);


    Vector2D positionScaleBar = Vector2D(param.legend_offsetX() + param.legend_spacingX, param.legend_offsetY() + -1.5 * param.legend_spacingY);
    drawRectangle(param, positionScaleBar, param.scaleBarLength, .5, param.colorScaleBar, window_ptr, false);

    std::ostringstream scaleBarLength_str_value;
    scaleBarLength_str_value.precision(2);
    scaleBarLength_str_value << param.scaleBarLength;

    text.setString(scaleBarLength_str_value.str() +" �m");
    positionText = positionScaleBar;
    positionText.x = positionScaleBar.x + param.scaleBarLength/2 -1;
    positionText.y = positionScaleBar.y + 1;
    text.setPosition(positionText.getPixelCoord(param));
    window_ptr->draw(text);
};