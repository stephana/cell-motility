#ifndef COMPUTATIONMANAGER_H
#define COMPUTATIONMANAGER_H

#include <SFML/Graphics.hpp>
#include <iostream>
//#include <io.h>

#include <fstream>
#include <ctime>
//#include <omp.h>
#include <stdio.h>
#include <sstream>

#include "properties.h"
#include "Vector2D.h"
#include "RecordsOfNodeTransitionPeriods.h"
#include "SmartNode.h"
#include "SmartInteraction.h"
#include "SmartCell.h"
#include "SmartSubstrate.h"
#include "createSubstrateMotifs.h"

class ComputationManager: public CPMwrapperObj
{
    /** 
    The computation manager is able to launch several simulations under various conditions.
    **/
    
    public:
        ComputationManager(ParametersObj& param) { duration = param.duration; dt = param.dt;
            resultsOutputFilename=param.resultsOutputFilename;
            parametersOutputFilename=param.parametersOutputFilename;
            nodeLifeOutputFileName = param.nodeLifeOutputFilename;
            nodeInfoOutputFilename = param.nodeInfoOutputFilename;
            branchInfoOutputFilename = param.branchInfoOutputFilename;
            patternOutputFilename = param.patternOutputFilename;
            cellSnapShotOutputFilename = param.cellSnapShotOutputFilename;
            openResultsFile();
            openParamtersFile();
            openNodeLifeFile();
            openNodeInfoFile();
            openBranchInfoFile();
            openPatternFile();
            openCellSnapShotFile();

            parametersHeaderToString(param); // adding all the titles to parameter file
            nodeLifeEventsHeadersToString(param);
            resultsHeaderToString(param);
        };
        ~ComputationManager() {};

        void updateParameters(ParametersObj& param);
        void setWindowPtr(std::shared_ptr<sf::RenderWindow> win_ptr);

        // opening files
        void openResultsFile();
        void openParamtersFile();
        void openNodeLifeFile();
        void openNodeInfoFile();
        void openBranchInfoFile();
        void openPatternFile();
        void openCellSnapShotFile();

        // saving data into the files
        void saveResults();
        void saveParameters();
        void saveNodeLifeEventFile();
        void saveNodeInfo();
        void saveBranchFile();
        void savePatternFile();
        void saveCellSnapShotFile();

        // closing files
        void closeResultFile();
        void closeParamterFile();
        void closeNodeLifeFile();
        void closeNodeInfoFile();
        void closeBranchInfoFile();
        void closePatternFile();
        void closeCellSnapShotFile();

        void displayMultiTaskProgress(ParametersObj& param, time_t tstart, time_t tstart_thiscomputation, int simNb);

        void nodeLifeEventsHeadersToString(ParametersObj& param);
        void nodeLifeEventsValuesToString(ParametersObj& param, SmartNodeWrapper& nodeWrp, std::string eventType, std::string nodeOrder, std::string nodeState);
        void parametersHeaderToString(ParametersObj& param);
        void parametersCurenValuesToString(ParametersObj& param, SimulationLabel& simLabel);
        void resultsHeaderToString(ParametersObj& param);
        void resultsCurentValuesToString(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords,SpyderCell& cell, SimulationLabel& simLabel);
        void launchCellModel(ParametersObj& param, SimulationLabel& simLabel);

    public:
        int simulationNumber = 0;
        float duration; // param.duration seconds
        float dt; // param.dt secondes
        float t = 0;

        // file names
        std::string resultsOutputFilename;
        std::string parametersOutputFilename;
        std::string nodeLifeOutputFileName;
        std::string nodeInfoOutputFilename;
        std::string branchInfoOutputFilename;
        std::string patternOutputFilename;
        std::string cellSnapShotOutputFilename;
        std::string results = "";
        std::string parameters = "";
        std::string nodeLife = "";
        std::string nodeInfo = "";
        std::string branchInfo = "";
        std::string patternsInfo = SmartSubstrate::patternToHeaderLineText();
        std::string cellSnapShot = "";

        bool cellSnapShotFileHasFeader = false;

        // data holder
        std::shared_ptr<sf::RenderWindow> window_ptr;
        std::shared_ptr<std::ofstream> resultsFile_sptr = std::make_shared<std::ofstream>();
        std::shared_ptr<std::ofstream> parametersFile_sptr = std::make_shared<std::ofstream>();
        std::shared_ptr<std::ofstream> nodeLifeFile_sptr = std::make_shared<std::ofstream>();
        std::shared_ptr<std::ofstream> nodeInfoFile_sptr = std::make_shared<std::ofstream>();
        std::shared_ptr<std::ofstream> branchInfoFile_sptr = std::make_shared<std::ofstream>();
        std::shared_ptr<std::ofstream> patternFile_sptr = std::make_shared<std::ofstream>();
        std::shared_ptr<std::ofstream> cellSnapShotFile_sptr = std::make_shared<std::ofstream>();
};

#endif // COMPUTATIONMANAGER_H