#include "ComputationManager.h"



void ComputationManager::updateParameters(ParametersObj& param) {
    duration = param.duration; dt = param.dt;
    //ne sert a rien car deja ouvert resultsOutputFilename = param.resultsOutputFilename;
    //ne sert a rien car deja ouvert parametersOutputFilename = param.parametersOutputFilename;
};
void ComputationManager::setWindowPtr(std::shared_ptr<sf::RenderWindow> win_ptr)
{
    window_ptr = win_ptr;
}
void ComputationManager::openResultsFile()
{
    const char* filename_char = resultsOutputFilename.c_str();
    remove(filename_char);
    resultsFile_sptr->open(resultsOutputFilename, std::ios_base::app); // here we open the result file in append mode
}
void ComputationManager::openParamtersFile()
{
    const char* filename_char = parametersOutputFilename.c_str();
    remove(filename_char);
    parametersFile_sptr->open(parametersOutputFilename, std::ios_base::app);
}
void ComputationManager::openNodeLifeFile()
{
    const char* filename_char = nodeLifeOutputFileName.c_str();
    remove(filename_char);
    nodeLifeFile_sptr->open(nodeLifeOutputFileName, std::ios_base::app); // here we open the result file in append mode
}
void ComputationManager::openNodeInfoFile()
{
    const char* filename_char = nodeInfoOutputFilename.c_str();
    remove(filename_char);
    nodeInfoFile_sptr->open(nodeInfoOutputFilename, std::ios_base::app); // here we open the result file in append mode
}
void ComputationManager::openBranchInfoFile()
{
    const char* filename_char = branchInfoOutputFilename.c_str();
    remove(filename_char);
    branchInfoFile_sptr->open(branchInfoOutputFilename, std::ios_base::app); // here we open the result file in append mode
}
void ComputationManager::openPatternFile()
{
    const char* filename_char = patternOutputFilename.c_str();
    remove(filename_char);
    patternFile_sptr->open(patternOutputFilename, std::ios_base::app); // here we open the result file in append mode
}

void ComputationManager::openCellSnapShotFile()
{
    const char* filename_char = cellSnapShotOutputFilename.c_str();
    remove(filename_char);
    cellSnapShotFile_sptr->open(cellSnapShotOutputFilename, std::ios_base::app); // here we open the result file in append mode
};

void ComputationManager::saveResults()
{
    std::cout << "    saving results...  ";
    *resultsFile_sptr << results; // we append the result to the file
    results = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
}
void ComputationManager::saveParameters()
{
    std::cout << "    saving parameters...  ";
    *parametersFile_sptr << parameters; // we append the result to the file
    parameters = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
}
void ComputationManager::saveNodeLifeEventFile()
{
    std::cout << "    saving node life data...  ";
    *nodeLifeFile_sptr << nodeLife; // we append the result to the file
    nodeLife = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
}
void ComputationManager::saveNodeInfo()
{
    std::cout << "    saving node Info file data...  ";
    *nodeInfoFile_sptr << nodeInfo; // we append the result to the file
    nodeInfo = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
}
void ComputationManager::saveBranchFile()
{
    std::cout << "    saving Branch Info file data...  ";
    *branchInfoFile_sptr << branchInfo; // we append the result to the file
    branchInfo = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
}
void ComputationManager::savePatternFile()
{
    std::cout << "    saving Pattern Info file data...  ";
    *patternFile_sptr << patternsInfo; // we append the result to the file
    patternsInfo = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
}

void ComputationManager::saveCellSnapShotFile()
{
    std::cout << "    saving Cell Snap Shot file data...  ";
    *cellSnapShotFile_sptr << cellSnapShot; // we append the result to the file
    cellSnapShot = ""; // we clear the results from memory
    std::cout << " -> saved " << std::endl;
};

void ComputationManager::closeResultFile()
{
    std::cout << "    --- Closing result file (.csv) --- " << std::endl;
    resultsFile_sptr->close();
    std::cout << "    ---        Result file closed         --- " << std::endl;
}
void ComputationManager::closeParamterFile()
{
    std::cout << "    --- Closing parameter file (.csv) --- " << std::endl;
    parametersFile_sptr->close();
    std::cout << "    ---        Paramter file closed         --- " << std::endl;
}
void ComputationManager::closeNodeLifeFile()
{
    std::cout << "    --- Closing nodeLifeFile file (.csv) --- " << std::endl;
    nodeLifeFile_sptr->close();
    std::cout << "    ---        nodeLifeFile file closed         --- " << std::endl;
}
void ComputationManager::closeNodeInfoFile()
{
    std::cout << "    --- Closing nodeInfoFile file (.csv) --- " << std::endl;
    nodeInfoFile_sptr->close();
    std::cout << "    ---        nodeInfoFile file closed         --- " << std::endl;
}
void ComputationManager::closeBranchInfoFile()
{
    std::cout << "    --- Closing branchInfoFile file (.csv) --- " << std::endl;
    branchInfoFile_sptr->close();
    std::cout << "    ---        branchInfoFile file closed         --- " << std::endl;
}
void ComputationManager::closePatternFile()
{
    std::cout << "    --- Closing patternInfoFile file (.csv) --- " << std::endl;
    patternFile_sptr->close();
    std::cout << "    ---        patternInfoFile file closed         --- " << std::endl;
}
void ComputationManager::closeCellSnapShotFile() {
    std::cout << "    --- Closing cell snapt shot file (.csv) --- " << std::endl;
    cellSnapShotFile_sptr->close();
    std::cout << "    ---        cell snap shot file closed         --- " << std::endl;
};

void ComputationManager::displayMultiTaskProgress(ParametersObj& param, time_t tstart, time_t tstart_thiscomputation, int simNb)
{
    std::string computationMsg = "";
    float percentDone = double(double(100) * simNb) / double(param.nbOfSimulations);
    time_t tend_thiscomputation = time(0);

    //float timeRemaining = difftime(tend_thiscomputation, tstart) * float( float(param.nbOfSimulations) - simNb ) / float(simNb);
    TimeObj timeRemaining(difftime(tend_thiscomputation, tstart) * float(param.nbOfSimulations - simNb) / float(simNb));
    TimeObj timeThisSimulation(difftime(tend_thiscomputation, tstart_thiscomputation));
    computationMsg = std::to_string(simNb) + " computations done out of " + std::to_string(param.nbOfSimulations) + " | ";
    computationMsg += " this computation took " + timeThisSimulation.tellTime() + " | ";
    computationMsg += std::to_string(percentDone) + " % done | ";
    computationMsg += "about " + timeRemaining.tellTime() + " seconds remaining";
    std::cout << computationMsg << std::endl;
}

void ComputationManager::nodeLifeEventsHeadersToString(ParametersObj& param)
{
    std::string lineNodeLife;
    // this methode is called to create a HEADER in the CSV file
    lineNodeLife += std::string("t") + ",";
    lineNodeLife += std::string("nodeRefNB") + ",";
    lineNodeLife += std::string("simulationRefNb") + ",";
    lineNodeLife += std::string("label") + ",";
    lineNodeLife += std::string("label_x") + ",";
    lineNodeLife += std::string("label_y") + ",";
    lineNodeLife += std::string("simdetailed_label") + ",";
    lineNodeLife += std::string("nodeAge") + ",";
    lineNodeLife += std::string("eventType") + ",";
    lineNodeLife += std::string("nodeOrder") + ",";
    lineNodeLife += std::string("nodeState") + ",";
    lineNodeLife += std::string("durationImmatureState") + ",";
    lineNodeLife += std::string("durationIntermediateState") + ",";
    lineNodeLife += std::string("durationMatureState") + ",";
    lineNodeLife += std::string("causeOfDeath") + ",";
    lineNodeLife += std::string("netForce");
    lineNodeLife += std::string("\n"); // 
    nodeLife += lineNodeLife;
}

void ComputationManager::nodeLifeEventsValuesToString(ParametersObj& param, SmartNodeWrapper& nodeWrp, std::string eventType, std::string nodeOrder, std::string nodeState)
{
    std::string lineNodeLife;
    // this methode is called during each node life event (transition & death)
    lineNodeLife += std::to_string(t) + ",";
    lineNodeLife += std::to_string(nodeWrp.refNb) + ",";
    lineNodeLife += std::to_string(param.simulationRefNb) + ",";
    lineNodeLife += param.simlabel + ",";
    lineNodeLife += param.simlabel_x + ",";
    lineNodeLife += param.simlabel_y + ",";
    lineNodeLife += param.simdetailed_label + ",";
    lineNodeLife += std::to_string(nodeWrp.age) + ",";
    lineNodeLife += eventType + ",";
    lineNodeLife += nodeOrder + ",";
    lineNodeLife += nodeState + ",";
    lineNodeLife += std::to_string(nodeWrp.durationImmatureState) + ",";
    lineNodeLife += std::to_string(nodeWrp.durationIntermediateState) + ",";
    lineNodeLife += std::to_string(nodeWrp.durationMatureState) + ",";
    lineNodeLife += nodeWrp.causeOfDeath + ",";
    lineNodeLife += std::to_string(nodeWrp.netForce_magnitude_previous_step);
    lineNodeLife += std::string("\n"); // 
    nodeLife += lineNodeLife;
}

void ComputationManager::parametersHeaderToString(ParametersObj& param)
{
    std::string lineParameters;
    lineParameters += std::string("duration") + ","; // 0
    lineParameters += std::string("dt") + ","; // 1
    lineParameters += std::string("simulation_NB") + ","; // 2
    lineParameters += std::string("tau_int") + ","; // 3
    lineParameters += std::string("tau_m") + ","; // 4
    lineParameters += std::string("tau2") + ","; // 5
    lineParameters += std::string("l1initial") + ","; // 6
    lineParameters += std::string("l2initial") + ","; // 7
    lineParameters += std::string("epsilon20") + ","; // 8
    lineParameters += std::string("k10") + ","; // 9
    lineParameters += std::string("k11") + ","; // 10
    lineParameters += std::string("k2") + ","; // 11
    lineParameters += std::string("gamma_max__") + ","; // 12
    lineParameters += std::string("B1_tension_only") + ","; // 13
    lineParameters += std::string("B2_tension_only") + ","; // 14
    lineParameters += std::string("epsilon_m") + ","; // 15
    lineParameters += std::string("tau_t") + ","; // 16
    lineParameters += std::string("alpha_int") + ","; // 17
    lineParameters += std::string("alpha0") + ","; // 18
    lineParameters += std::string("alpha10") + ","; // 19
    lineParameters += std::string("alpha1_m") + ","; // 20
    lineParameters += std::string("alpha2") + ","; // 21
    lineParameters += std::string("delta_theta_1") + ","; // 22
    lineParameters += std::string("delta_theta_2") + ","; // 23
    lineParameters += std::string("F_R0") + ","; // 24
    lineParameters += std::string("F_R1") + ","; // 25
    lineParameters += std::string("F_R2") + ","; // 26
    lineParameters += std::string("F_gamma") + ","; // 27
    lineParameters += std::string("nu1") + ","; // 28
    lineParameters += std::string("nu2_im") + ","; // 29
    lineParameters += std::string("nu2_int") + ","; // 30
    lineParameters += std::string("nu2_m") + ","; // 31
    lineParameters += std::string("labelRefNb") + ","; // 32
    lineParameters += std::string("label") + ","; // 33
    lineParameters += std::string("label_x") + ","; // 34
    lineParameters += std::string("label_y") + ","; // 35
    lineParameters += std::string("adhesion_coeficient_FR012"); //36
    lineParameters += std::string("\n"); // 
    parameters += lineParameters;
}


void ComputationManager::parametersCurenValuesToString(ParametersObj& param, SimulationLabel& simLabel)
{
    std::string lineParameters;
    lineParameters += std::to_string(param.duration) + ","; // 0
    lineParameters += std::to_string(param.dt) + ","; // 1
    lineParameters += std::to_string(simulationNumber) + ","; // 2
    lineParameters += std::to_string(param.tau_int) + ","; // 3
    lineParameters += std::to_string(param.tau_m) + ","; // 4
    lineParameters += std::to_string(param.tau2) + ","; // 5
    lineParameters += std::to_string(param.l1initial) + ","; // 6
    lineParameters += std::to_string(param.l2initial) + ","; // 7
    lineParameters += std::to_string(param.epsilon20) + ","; // 8
    lineParameters += std::to_string(param.k10) + ","; // 9
    lineParameters += std::to_string(param.k11) + ","; // 10
    lineParameters += std::to_string(param.k2) + ","; // 11
    lineParameters += std::to_string(param.gamma_max__) + ","; // 12
    lineParameters += std::to_string(param.B1_tension_only) + ","; // 13
    lineParameters += std::to_string(param.B2_tension_only) + ","; // 14
    lineParameters += std::to_string(param.epsilon_m) + ","; // 15
    lineParameters += std::to_string(param.tau_t) + ","; // 16
    lineParameters += std::to_string(param.alpha_int) + ","; // 17
    lineParameters += std::to_string(param.alpha0) + ","; // 18
    lineParameters += std::to_string(param.alpha10) + ","; // 19
    lineParameters += std::to_string(param.alpha1_m()) + ","; // 20
    lineParameters += std::to_string(param.alpha2) + ","; // 21
    lineParameters += std::to_string(param.delta_theta_1) + ","; // 22
    lineParameters += std::to_string(param.delta_theta_2) + ","; // 23
    lineParameters += std::to_string(param.F_R0) + ","; // 24
    lineParameters += std::to_string(param.F_R1) + ","; // 25
    lineParameters += std::to_string(param.F_R2) + ","; // 26
    lineParameters += std::to_string(param.F_gamma) + ","; // 27
    lineParameters += std::to_string(param.nu1) + ","; // 28
    lineParameters += std::to_string(param.nu2_im) + ","; // 29
    lineParameters += std::to_string(param.nu2_int) + ","; // 30
    lineParameters += std::to_string(param.nu2_m) + ","; // 31
    lineParameters += std::to_string(simLabel.labelRefNb) + ","; // 32
    lineParameters += simLabel.label + ","; // 33
    lineParameters += simLabel.label_x + ","; // 34
    lineParameters += simLabel.label_y + ","; // 35
    lineParameters += std::to_string(param.adhesion_coeficient_FR012);
    lineParameters += std::string("\n");
    parameters += lineParameters;
}


void ComputationManager::resultsHeaderToString(ParametersObj& param)
{
    if (param.recordDetails)
    {
        std::string lineResult;
        lineResult += std::string("simulationNumber") + ","; // 0
        lineResult += std::string("t") + ","; // 1
        lineResult += std::string("cell_position.x") + ","; // 2
        lineResult += std::string("cell_position.y") + ","; // 3
        lineResult += std::string("labelRefNb") + ","; // 4
        lineResult += std::string("label") + ","; // 5
        lineResult += std::string("NbNodes") + ","; // 6
        lineResult += std::string("countN1s") + ","; // 7
        lineResult += std::string("countN2s") + ","; // 8
        lineResult += std::string("NbInteractions") + ","; // 9
        lineResult += std::string("LengthOfAllInteractions") + ","; // 10
        lineResult += std::string("LengthOfAllB1s") + ","; // 11
        lineResult += std::string("LengthOfAllB2s") + ","; // 12
        lineResult += std::string("N2AgeAtDeath_mean") + ","; // 13
        lineResult += std::string("N2AgeAtDeath_standardDeviation") + ","; // 14
        lineResult += std::string("N1AgeAtDeath_mean") + ","; // 15
        lineResult += std::string("N1AgeAtDeath_standardDeviation") + ","; // 16
        lineResult += std::string("nbImmatureN1Transitions") + ","; // 17
        lineResult += std::string("nbIntermediateN1Transitions") + ","; // 18
        lineResult += std::string("nbMatureImmatureN1Transitions") + ","; // 19
        lineResult += std::string("immatureN1TransitionPeriod_minimum") + ","; // 20
        lineResult += std::string("immatureN1TransitionPeriod_max") + ","; // 21
        lineResult += std::string("immatureN1TransitionPeriod_mean") + ","; // 22
        lineResult += std::string("intermediateN1TransitionPeriod_minimum") + ","; // 23
        lineResult += std::string("intermediateN1TransitionPeriod_maximum") + ","; // 24
        lineResult += std::string("intermediateN1TransitionPeriod_mean") + ","; // 25
        lineResult += std::string("matureN1TransitionPeriod_minimum") + ","; // 26
        lineResult += std::string("matureN1TransitionPeriod_maximum") + ","; // 27
        lineResult += std::string("matureN1TransitionPeriod_mean") + ","; // 28
        lineResult += std::string("matureN1TransitionPeriod_std") + ",";
        lineResult += std::string("intermediateN1TransitionPeriod_std") + ",";
        lineResult += std::string("nbOfSubstratesAdhesionStrengthFactors");
        lineResult += std::string("\n");
        results += lineResult;
    }
}


void ComputationManager::resultsCurentValuesToString(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, SpyderCell& cell, SimulationLabel& simLabel){
    if (param.recordDetails)
    {
        std::string lineResult;
        lineResult += std::to_string(simulationNumber) + ","; // 0
        lineResult += std::to_string(t) + ","; // 1
        lineResult += std::to_string(cell.getPosition().x) + ","; // 2
        lineResult += std::to_string(cell.getPosition().y) + ","; // 3
        lineResult += std::to_string(simLabel.labelRefNb) + ","; // 4
        lineResult += simLabel.label + ","; // 5
        lineResult += std::to_string(cell.getNbNodes()) + ","; // 6
        lineResult += std::to_string(cell.countN1s()) + ","; // 7
        lineResult += std::to_string(cell.countN2s()) + ","; // 8
        lineResult += std::to_string(cell.getNbInteractions()) + ","; // 9
        lineResult += std::to_string(cell.calculateTheLengthOfAllInteractions()) + ","; // 10
        lineResult += std::to_string(cell.calculateTheLengthOfAllB1s()) + ","; // 11
        lineResult += std::to_string(cell.calculateTheLengthOfAllB2s()) + ","; // 12
        lineResult += std::to_string(nodeTransitionRecords.N2AgeAtDeath_mean()) + ","; // 13
        lineResult += std::to_string(nodeTransitionRecords.N2AgeAtDeath_standardDeviation()) + ","; // 14
        lineResult += std::to_string(nodeTransitionRecords.N1AgeAtDeath_mean()) + ","; // 15
        lineResult += std::to_string(nodeTransitionRecords.N1AgeAtDeath_standardDeviation()) + ","; // 16
        lineResult += std::to_string(nodeTransitionRecords.nbImmatureN1Transitions()) + ","; // 17
        lineResult += std::to_string(nodeTransitionRecords.nbIntermediateN1Transitions()) + ","; // 18
        lineResult += std::to_string(nodeTransitionRecords.nbMatureImmatureN1Transitions()) + ","; // 19
        lineResult += std::to_string(nodeTransitionRecords.immatureN1TransitionPeriod_minimum()) + ","; // 20
        lineResult += std::to_string(nodeTransitionRecords.immatureN1TransitionPeriod_max()) + ","; // 21
        lineResult += std::to_string(nodeTransitionRecords.immatureN1TransitionPeriod_mean()) + ","; // 22
        lineResult += std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_minimum()) + ","; // 23
        lineResult += std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_maximum()) + ","; // 24
        lineResult += std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_mean()) + ","; // 25
        lineResult += std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_minimum()) + ","; // 26
        lineResult += std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_maximum()) + ","; // 27
        lineResult += std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_mean()) + ","; // 28
        lineResult += std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_std()) + ",";
        lineResult += std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_std()) + ",";
        lineResult += std::to_string(cell.nbSubstrateAdhesionStrengthFactors());
        lineResult += std::string("\n");
        results += lineResult;
    }
}

void ComputationManager::launchCellModel(ParametersObj& param, SimulationLabel& simLabel)
{
    simulationNumber++; // we itterate the simulation number

    t = 0; // we reinitialize the computation
    float tResult = 0;
    float tInfo = 0;
    float lastSaveDump = 0;
    float saveDumpInterval = 24 * 60 * 60;
    bool calculationIsOver = false;
    int NbStepSinceLastFrame = 0;

    srand(650365); // seedind the random function

    RecordsOfNodeTransitionPeriods nodeTransitionRecords;

    SpyderCell_List cells =  SpyderCell_List();
    cells.addNewCell(param);
    //cells.addNewCell(param, Vector2D(50, 25));
    //cells.addNewCell(param, Vector2D(-20, 0));

    SmartSubstrate substrate = SmartSubstrate(param);
    createSubstrateMotifs(param, substrate);

    patternsInfo += substrate.exportPatternsToCSV(param, simulationNumber);

    cells.addInfoHeader(nodeInfo, branchInfo);
    sf::View view = window_ptr->getDefaultView();
    sf::Vector2u win_size = window_ptr->getSize();
    param.ScreenOffSet_x = -int(win_size.x) / 2.0;
    param.ScreenOffSet_y = int(win_size.y) / 2.0;
    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
    window_ptr->setView(view);


    // Save initial results and position in a string variable
    if (t == 0)
    {
        resultsCurentValuesToString(param, nodeTransitionRecords, *cells.cell_List[0], simLabel);
        parametersCurenValuesToString(param, simLabel);
    }

    // Create a graphical text to display
    while (!calculationIsOver)
    {
        sf::Event event;
        while (window_ptr->pollEvent(event))
        {
            if (event.type == sf::Event::Closed) { window_ptr->close(); }

            if (event.type == sf::Event::Resized)
            {
                param.ScreenWidth = event.size.width;
                param.ScreenHeight = event.size.height;
                // on met � jour la vue, avec la nouvelle taille de la fen�tre
                sf::FloatRect visibleArea(0.f, 0.f, event.size.width, event.size.height);
                window_ptr->setView(sf::View(visibleArea));
            }

            if (event.type == sf::Event::KeyPressed)
            {
                sf::View view = window_ptr->getDefaultView();
                if (event.key.code == sf::Keyboard::Add)
                {
                    param.zoom = param.zoom - 0.1;
                    view.zoom(param.zoom);
                    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
                }
                else if (event.key.code == sf::Keyboard::Subtract)
                {
                    param.zoom = param.zoom + 0.1;
                    view.zoom(param.zoom);
                    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
                }
                else if (event.key.code == sf::Keyboard::Left)
                {
                    param.ScreenOffSet_x = param.ScreenOffSet_x - 10;
                    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
                    view.zoom(param.zoom);
                }
                else if (event.key.code == sf::Keyboard::Right)
                {
                    param.ScreenOffSet_x = param.ScreenOffSet_x + 10;
                    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
                    view.zoom(param.zoom);
                }
                else if (event.key.code == sf::Keyboard::Up)
                {
                    param.ScreenOffSet_y = param.ScreenOffSet_y - 10;
                    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
                    view.zoom(param.zoom);
                }
                else if (event.key.code == sf::Keyboard::Down)
                {
                    param.ScreenOffSet_y = param.ScreenOffSet_y + 10;
                    view.move(param.ScreenOffSet_x, param.ScreenOffSet_y);
                    view.zoom(param.zoom);
                }
                window_ptr->setView(view);
                if (event.key.code == sf::Keyboard::Escape)
                {
                    param.refresh = true;
                }
            }
            //else{ window_ptr->setView(window_ptr->getDefaultView()); }
        }

        if (param.refresh) {
            param.refresh = false;
            calculationIsOver = true;
        }

        if (param.displaySimulation)
        {
            if (NbStepSinceLastFrame >= param.displaySimulationNbStepBetweenFrames)
            {
                window_ptr->clear(param.colorBackground);
                substrate.draw(param, window_ptr);
                if (param.displayCytoplasm) { cells.drawCytoplasm(param, window_ptr); }
                if (param.displayInteractions) { cells.drawInteractions(param, window_ptr); };
                if (param.displayNodes) { cells.drawNodes(param, window_ptr); }


                // Draw the string to display time
                sf::Text text(" ", *param.font_ptr, 20);
                TimeObj tObj(t);
                text.setPosition(sf::Vector2f(param.zoom * (param.ScreenOffSet_x + 0.02 * param.ScreenWidth), param.zoom * param.ScreenOffSet_y + param.ScreenHeight / 25));
                text.setString("Simulation nb: " + std::to_string(simulationNumber) + "/" + std::to_string(param.nbOfSimulations) + "\nDuration = " + tObj.tellTime());
                text.setFillColor(sf::Color(0, 0, 0, 255));
                window_ptr->draw(text);
                if (param.displayLegend)
                {
                    drawLegend(param, window_ptr);
                }

                std::string msg = simLabel.label;

                displayMessageDuringSimulation(param, window_ptr, msg);

                /** Render on window **/
                window_ptr->display();
                NbStepSinceLastFrame = 0;
            }
            else { NbStepSinceLastFrame++; }
        }

        // Here we step the computation
        cells.InterCell_createInteractions(param);
        cells.InterCell_inhibit(param);
        cells.morphologyManager(param, nodeTransitionRecords, *this, dt);
        cells.protrusionManager();
        cells.forces(param, nodeTransitionRecords, *this, substrate, dt);
        cells.step(param, nodeTransitionRecords, *this, substrate, dt);
        cells.shouldLive(param, dt);
        cells.killTheDead(param, nodeTransitionRecords, *this);

        t = t + dt;
      std::this_thread::sleep_for(std::chrono::duration<float>(dt/150));
        tResult += dt;
        tInfo += dt;

        if (tInfo > (param.saveInfoTimeIntervals) && param.recordInfoTimeIntervals)
        {
            cells.addInfoData(param, t, nodeInfo, branchInfo);
            tInfo = 0;
        }
        // Save result in a string variable
        if (tResult > (param.saveResultTimeIntervals))
        {
            resultsCurentValuesToString(param, nodeTransitionRecords, *cells.cell_List[0], simLabel);
            tResult = 0;
            if (param.saveSnapShot)
            {
                cells.snapshot(param, cellSnapShotFileHasFeader, simulationNumber, t, cellSnapShot);
            }
        }

        if (lastSaveDump > saveDumpInterval)
        {
            saveResults(); // Calculation sample terminated (under same condition) -> Saving results to csv file -> keep going with new condition
            saveParameters();
            saveNodeLifeEventFile();
            saveNodeInfo();
            saveBranchFile();
            savePatternFile();
            saveCellSnapShotFile();
            lastSaveDump = 0;
        }
        else { saveDumpInterval += dt; }

        if (t >= duration) { calculationIsOver = true; t = 0; }
    }
};