#ifndef SMARTCELL_H
#define SMARTCELL_H

#include <set>
#include <vector>
#include <memory>
#include <iostream>
#include <string>
#include <random>
#include <random> // poisson_distribution
#include <armadillo> // matrix library
#include <future>
#include <mutex>
#include <map>

#include "Vector2D.h"
#include "properties.h"
#include "RecordsOfNodeTransitionPeriods.h"
#include "SmartNode.h"
#include "SmartInteraction.h"

#define ASYNC_SMARTCELL_APPLY_INTERACTION_FORCE 0 // 2X times faster.

struct NewAngle {
    bool create = false;
    float angle = NULL;
};

/** SmartCell
The SmartCell class is the parent class of all cell objects
 **/

class SmartCell: public GraphicProperties
{
    public:
        SmartCell();
        SmartCell(ParametersObj& param, unsigned int cellRefNb);
        ~SmartCell();
        virtual std::shared_ptr<SmartNode> addNode(ParametersObj& param, Vector2D xy); // This method is used to add an extremity
        virtual void addInteraction(std::shared_ptr<SmartNode> nodeA_shared_ptr, std::shared_ptr<SmartNode> nodeB_shared_ptr);
        virtual void applyInteractionForce(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, int interactionNb, float dt);
        virtual void morphologyManager(float& t_halflife, float& dt);
        //virtual void forces(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, float& dt);
        int getNbNodes();
        int getNbInteractions();
        //int findNextN1(int indexPresentEdgeNode);
        //int findPreviousN1(int indexPresentEdgeNode);
        //virtual Vector2D getPosition();
        std::map<int,std::shared_ptr<SmartNode>> node_Map; // This "c++ vector" stores the node pointers, the node at index 0 is the central node, other the rest are extremities.
        std::vector<std::shared_ptr<SmartInteraction>> interaction_List; // This vector stores the pointers of all interactions.
        std::shared_ptr<SmartInteraction> getInteraction_ptr(std::vector< std::shared_ptr<SmartInteraction> >::iterator it_interaction);
        std::shared_ptr<SmartNode> getNode_ptr(std::map<int, std::shared_ptr<SmartNode>>::iterator it_node);
        void virtual protrusionManager();
        virtual void drawCytoplasm(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);
        virtual void drawNodes(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);
        virtual void drawInteractions(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);;
        void shouldLive(ParametersObj& param, float& dt);
        virtual void killTheDead(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM);
        void addNewSubstrateAdhesionStrengthFactorToList(float newSubstrateAdhesionStrengthFactor);
        void clearListOfSubstrateAdhesionStrengthFactors();
        int nbSubstrateAdhesionStrengthFactors();
        virtual std::string snapshot(ParametersObj& param);
        unsigned int getRefNb();
        void setRefNb(unsigned int cellRefNb);
        bool isAlive() { return is_alive; };
        void kill() { is_alive = false; };
        void setAllNodesToUninhibited() {
            // calculate the force intensity (for all intercell_interactions)
            for (std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = begin(node_Map); it_node != end(node_Map); ++it_node) {
                it_node->second->setUninhibited();
            }
        };

    protected:
        sf::Color colorCytoplasm;
    private:
        unsigned int refNb;
        std::set<float> listOfSubstrateAdhesionStrengthFactorsInContactWithCell;
        int nodeRefNb_iterator = 0; // this is an iterator that is used to generate a unique reference number for each node that will be used to search it in the vector object and link it to the corresponding interactions.
        bool is_alive = true;
};

/**
    #########################################################################################################
    ########################################  SPYDER CELL  ##################################################
    #########################################################################################################
**/

class SpyderCell: public SmartCell
{
    public:
        SpyderCell(ParametersObj& param, unsigned int cellRefNb,Vector2D xy);
        ~SpyderCell();
        void forces(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt);
        void step(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt);
        Vector2D getPosition();
        // Managers
        void morphologyManager(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, float& dt);
        void N1Manager(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, float& dt);
        void N2Manager(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, float& dt);
        //
        void applyInteractionForce(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, int interactionNb, float dt);
        // Counters
        int countN1s();
        int countN2s();
        int countNbN2OnN1(int nodeRefNb, bool nodesAreBound = false);
        int countNbOfChildNodesOnThisNode(SmartNode& node);
        std::shared_ptr<SmartNode> getParentNodePtrsOnThisNode(SmartNode& node);
        std::vector<std::shared_ptr<SmartNode>> getChildNodePtrsOnThisNode(SmartNode& node);
        arma::vec getAnglesInterCellInteractions(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List);
        arma::vec getAnglesChildOfN0();
        arma::vec getAnglesInterCellConnections(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List);
        // Probability distributions
        float nu1(ParametersObj& param);
        float nu2(ParametersObj& param, std::shared_ptr<SmartNode> N1_ptr);
        // Action taker (make it happen)
        void addN1(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List);
        void addN2(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, std::shared_ptr<SmartNode> N1_shared_ptr);
        // Generators (length and angles)
        NewAngle generateAngleOfNewN1(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List);
        NewAngle generateAngleOfNewN2(ParametersObj& param, std::vector<std::shared_ptr<SmartInteraction>>& interCell_interaction_List, std::shared_ptr<SmartNode> N1_ptr);
        float generateLengthOfNewN1(ParametersObj& param, float& angle);
        float generateLengthOfNewN2(ParametersObj& param);
        // Calculate (measure)
        // Calculate (measure)
        float calculateTheLengthOfAllInteractions();
        float calculateTheLengthOfAllB1s();
        float calculateTheLengthOfAllB2s();
        void drawCytoplasm(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);

        void addInfoHeader(std::string& nodeInfo, std::string& branchInfo);
        void addInfoData(ParametersObj& param, float t, std::string& nodeInfo, std::string& branchInfo);

        std::string nodeInfo_RegularTimeInterval_HeadersToString();
        std::string nodeInfo_RegularTimeInterval_DataToString(ParametersObj& param, SmartNode& node, float t);
        std::string branchInfo_RegularTimeInteval_HeadersToString();
        std::string branchInfo_RegularTimeInteval_DataToString(ParametersObj& param, SmartInteraction& branch, float t);
        virtual void snapshot(ParametersObj& param, bool& cellSnapShotFileHasFeader, int& simulationNumber, float& t, std::string& snaptxt)
        {
            if (!cellSnapShotFileHasFeader)
            {
                snaptxt += "simulationNumber,t,nodeRefNb,nodeType,this.x,this.y,this.Radius,color.R,color.G,color.B,hasParent,parent.x,parent.y,parent.Radius\n";
                cellSnapShotFileHasFeader = true;
            }

            std::map<int, std::shared_ptr<SmartNode>>::iterator it_node = node_Map.begin();
            while (it_node != node_Map.end())
            {
                snaptxt += std::to_string(simulationNumber) + ","; // 0
                snaptxt += std::to_string(t) + ","; // 1
                snaptxt += std::to_string(getNode_ptr(it_node)->refNb) + ",";
                snaptxt += std::to_string(getNode_ptr(it_node)->getOrder()) + ",";
                snaptxt += std::to_string(getNode_ptr(it_node)->getPosition().x) + "," + std::to_string(getNode_ptr(it_node)->getPosition().y) + ",";
                snaptxt += std::to_string(getNode_ptr(it_node)->getRadius(param)) + ",";
                snaptxt += std::to_string(getNode_ptr(it_node)->getColor_SFML().r) + ",";
                snaptxt += std::to_string(getNode_ptr(it_node)->getColor_SFML().g) + ",";
                snaptxt += std::to_string(getNode_ptr(it_node)->getColor_SFML().b) + ",";

                std::shared_ptr<SmartNode> parentNode_ptr = getParentNodePtrsOnThisNode(*it_node->second);
                if (parentNode_ptr != NULL){
                    snaptxt += std::string("true,") + std::to_string(parentNode_ptr->getPosition().x) + "," + std::to_string(parentNode_ptr->getPosition().y) + "," + std::to_string(parentNode_ptr->getRadius(param)) + "\n";
                }
                else{
                    snaptxt += std::string("false,0,0,0\n");
                }
                it_node++;
            }
        }

    protected:
    private:
        std::shared_ptr<SmartNode> N0_ptr;
};


class SpyderCell_List
{
public:
    SpyderCell_List() {}
    ~SpyderCell_List() {};

    // Intracell
    std::shared_ptr<SpyderCell> getCell_ptr(std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell);
    void addNewCell(ParametersObj& param, const Vector2D& position = Vector2D(0, 0));
    void addInfoHeader(std::string& nodeInfo, std::string& branchInfo);
    void drawCytoplasm(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);
    void drawInteractions(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);
    void drawNodes(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);
    void morphologyManager(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float& dt);
    void protrusionManager();
    void forces(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt);
    void step(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, SmartSubstrate& substrate, float& dt);
    void shouldLive(ParametersObj& param, float& dt);
    void killTheDead(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM);
    void addInfoData(ParametersObj& param, float t, std::string& nodeInfo, std::string& branchInfo);
    void snapshot(ParametersObj& param, bool& cellSnapShotFileHasFeader, int& simulationNumber, float& t, std::string& snaptxt);
    int getNbCells();


    // Intercell tools
    float InterCell_celldistance(std::shared_ptr<SpyderCell>& cellA, std::shared_ptr<SpyderCell>& cellB) { return (cellB->getPosition()).distance(cellA->getPosition()); }; // distance between cells centers
    float InterCell_nodeDistance(std::shared_ptr<SpyderCell>& cellA, int nodeRefNbCellA, std::shared_ptr<SpyderCell>& cellB, int nodeRefNbCellB) {
        std::map<int, std::shared_ptr<SmartNode>>::iterator it_nodeA = cellA->node_Map.find(nodeRefNbCellA);
        Vector2D positionNode_CellA = cellA->getNode_ptr(it_nodeA)->getPosition();
        std::map<int, std::shared_ptr<SmartNode>>::iterator it_nodeB = cellB->node_Map.find(nodeRefNbCellB);
        Vector2D positionNode_CellB = cellB->getNode_ptr(it_nodeB)->getPosition();
        return positionNode_CellB.distance(positionNode_CellA);
    }
    bool InterCell_IsNodePairConnected(SmartNode& nodeA, SmartNode& nodeB)
    {
        for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interCell_interaction = begin(interCell_interaction_List); it_interCell_interaction != end(interCell_interaction_List); ++it_interCell_interaction) {
            if ((nodeA.refNb == (*it_interCell_interaction)->nodeA_shared_ptr->refNb) && (nodeA.getPatentCellRefNb() == (*it_interCell_interaction)->nodeA_shared_ptr->getPatentCellRefNb()))
            {
                if ( (nodeB.refNb == (*it_interCell_interaction)->nodeB_shared_ptr->refNb) && (nodeB.getPatentCellRefNb() == (*it_interCell_interaction)->nodeB_shared_ptr->getPatentCellRefNb()) )
                {
                    return true;
                }
            }
            else if ( (nodeA.refNb == (*it_interCell_interaction)->nodeB_shared_ptr->refNb) && (nodeA.getPatentCellRefNb() == (*it_interCell_interaction)->nodeB_shared_ptr->getPatentCellRefNb()))
            {
                if ( (nodeB.refNb == (*it_interCell_interaction)->nodeA_shared_ptr->refNb) && (nodeB.getPatentCellRefNb() == (*it_interCell_interaction)->nodeA_shared_ptr->getPatentCellRefNb()) )
                {
                    return true;
                }
            }
        }
        return false;
    }

    void InterCell_createSingleInteraction(ParametersObj& param, std::shared_ptr<SpyderCell>& cellA_ptr, std::shared_ptr<SmartNode>& nodeA_ptr, std::shared_ptr<SpyderCell> cellB_ptr, std::shared_ptr<SmartNode> nodeB_ptr) {
            std::shared_ptr<SmartInteraction> interaction = std::make_shared<SmartInteraction>(nodeA_ptr, nodeB_ptr); // we create the interaction and give it a pair of node references.
            interaction->refNbCellA = cellA_ptr->getRefNb();
            interaction->refNbCellB = cellB_ptr->getRefNb();
            interaction->setConnectionType("intercell");
            interCell_interaction_List.emplace_back(interaction);
    };

    // Intercell Manager
    void InterCell_createInteractions( ParametersObj& param) {

        for(auto it_cellA = begin(cell_List); it_cellA != end(cell_List); ++it_cellA) {
            for (auto it_cellB = it_cellA+1; it_cellB != end(cell_List); ++it_cellB) {

                // test the distance between all cells, if distance is less than a minimal distance, then it is possible for the cells to interact
                if (InterCell_celldistance((*it_cellA), (*it_cellB)) < param.maxInteractionDistanceBetweenCells){

                    // test the distance between all N1 and N0 nodes of the cells the interacting cell pair
                    for (auto it_nodeA = begin((*it_cellA)->node_Map); it_nodeA != end((*it_cellA)->node_Map); ++it_nodeA) // for all nodes in cell A
                    {
                        for (auto it_nodeB = begin((*it_cellB)->node_Map); it_nodeB != end((*it_cellB)->node_Map); ++it_nodeB)//for all nodes in cell B
                        {
                            float contactDistanceAB = param.cytoplasmScaleFactor * ( (*it_cellA)->getNode_ptr(it_nodeA)->getRadius(param) + (*it_cellB)->getNode_ptr(it_nodeB)->getRadius(param) );
                            if ( Vector2D(it_nodeA->second->getPosition()).distance(it_nodeB->second->getPosition()) < contactDistanceAB && it_nodeA->second->isAlive() && it_nodeB->second->isAlive())
                            {
                                if ( !(*it_cellA)->getNode_ptr(it_nodeA)->isN2() && !(*it_cellB)->getNode_ptr(it_nodeB)->isN2() && ( !InterCell_IsNodePairConnected( *(*it_cellA)->getNode_ptr(it_nodeA), *(*it_cellB)->getNode_ptr(it_nodeB))) )
                                {
                                    // if the nodes N1/N0 nodes are too close than an interaction is formed. Bellow a R_node there is compression, otherwise there is tension.
                                    InterCell_createSingleInteraction(param, (*it_cellA), it_nodeA->second, (*it_cellB), it_nodeB->second);
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    void InterCell_UnhibitAllNodes()
    {
        // reinitialise the state of all nodes
        for(auto it_cellA = begin(cell_List); it_cellA != end(cell_List); ++it_cellA)
        {
            (*it_cellA)->setAllNodesToUninhibited();
        }
    }

    void InterCell_inhibit(ParametersObj& param) {
        // if there is an interaction and the distance is less than R_nodeA + R_nodeB than there is an inhibition.
        float Radius_nodeA = 0;
        float Radius_nodeB = 0;
        float node_distance = 0;

        std::shared_ptr<SpyderCell> cellA;
        std::shared_ptr<SpyderCell> cellB;

        InterCell_UnhibitAllNodes();

        // calculate the force intensity (for all intercell_interactions)
        for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interCell_interaction = begin(interCell_interaction_List); it_interCell_interaction != end(interCell_interaction_List); ++it_interCell_interaction) {
            Radius_nodeA = (*it_interCell_interaction)->nodeA_shared_ptr->getRadius(param);
            Radius_nodeB = (*it_interCell_interaction)->nodeB_shared_ptr->getRadius(param);
            node_distance = Vector2D((*it_interCell_interaction)->nodeB_shared_ptr->getPosition()).distance((*it_interCell_interaction)->nodeA_shared_ptr->getPosition());


            if( node_distance < param.cytoplasmScaleFactor * ( Radius_nodeA + Radius_nodeB) )
            {
                (*it_interCell_interaction)->nodeA_shared_ptr->setInhibited();
                (*it_interCell_interaction)->nodeB_shared_ptr->setInhibited();
            }
        }
    };

    void InterCell_forces(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt)
    {
        // calculate the force intensity (for all intercell_interactions)
        for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interCell_interaction = begin(interCell_interaction_List); it_interCell_interaction != end(interCell_interaction_List); ++it_interCell_interaction) {
            (*it_interCell_interaction)->applyForceToNodePair(param, nodeTransitionRecords, CPM, dt, false, true);
        }
    };

    void InterCell_killDeadInteractions(ParametersObj& param) {
        for (std::vector<std::shared_ptr<SmartInteraction>>::iterator it_interCell_interaction = begin(interCell_interaction_List); it_interCell_interaction != end(interCell_interaction_List); ++it_interCell_interaction) {

            // if one of the cell is dead kill the interaction ( not implemented yet )
            bool testCellsAreAlive_is_done = false;
            while (!testCellsAreAlive_is_done)
            {
                std::vector<std::shared_ptr<SpyderCell>>::iterator it_cell_List;
                if ( (*it_cell_List)->getRefNb() == (*it_interCell_interaction)->refNbCellA || (*it_cell_List)->getRefNb() == (*it_interCell_interaction)->refNbCellB)
                {
                    if (!(*it_cell_List)->isAlive()) { (*it_interCell_interaction)->setAliveToFalse(); }
                }
                ++it_cell_List;
            }


            // if one of the nodes is dead kill the interaction
            (*it_interCell_interaction)->isAlive(); // this function will check if the nodes are alive


            // if the cells are too far kill the interaction
            if ((*it_interCell_interaction)->distanceBetweenNodes() > param.maxInteractionDistanceBetweenCells) {
                (*it_interCell_interaction)->setAliveToFalse();
            }


            // if the nodes are too far kill the interaction
            if ((*it_interCell_interaction)->distanceBetweenNodes() > param.maxInteractionDistanceBetweenCells) {
                (*it_interCell_interaction)->setAliveToFalse();
            }
        }
    };


public:
    std::vector<std::shared_ptr<SpyderCell>> cell_List;
    std::vector<std::shared_ptr<SmartInteraction>> interCell_interaction_List; // This vector stores the pointers of all interactions.
    unsigned int cellRefNbCounter = 0;
};

void drawLegend(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr);
#endif // SMARTCELL_H
