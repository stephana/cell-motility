#include "properties.h"

std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::default_random_engine generator;


std::shared_ptr<sf::Font> loadFont(std::string fontName)
{
    std::shared_ptr<sf::Font> font_ptr(new sf::Font);
    if (!font_ptr->loadFromFile( fontName + ".ttf" )) { std::cout << "Could not find font file" << std::endl; }//return EXIT_FAILURE; }
    return font_ptr;
}

float N1_maturation_ratio(ParametersObj& param, float& ageNode)
{
    return param.alpha_im(ageNode) / param.alpha_int;
}

arma::uvec f_block_no_superposition(ParametersObj& param, arma::vec alphas, arma::vec x) {
    arma::uvec ff = arma::uvec(x.size(), arma::fill::ones);
    arma::uvec f = arma::uvec(x.size(), arma::fill::ones);
    //if(alphas.n_elem > 0)
    //{ std::cout << "alphas : " << std::endl;
    //alphas.print();
    //}
    
    for (int i = 0; i < alphas.size(); i++) {

        double alpha = alphas[i]; // position

        arma::uvec f_alpha_left = x < (alpha - param.delta_theta_1);
        arma::uvec f_alpha_right = x > (alpha + param.delta_theta_1);
        f = f % ((f_alpha_left + f_alpha_right) >= 1);

        if ((alpha + param.delta_theta_1) > M_PI)
        {
            arma::uvec uf_alpha_tail_right = (x >= (alpha + param.delta_theta_1 - 2 * M_PI) );
            f = f % uf_alpha_tail_right;
        }

        if ((alpha - param.delta_theta_1) < -M_PI)
        {
            arma::uvec uf_alpha_tail_left = (x <= (alpha - param.delta_theta_1 + 2 * M_PI) );
            f = f % uf_alpha_tail_left;
        }
        
        ff = ff % f;
    }
    return ff;
}

void draw_f_f_block_no_superposition(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, arma::uvec ff)
{
    // create an empty shape
    sf::ConvexShape convex_ff;

    // resize it to 5 points
    convex_ff.setPointCount(5);

    // define the points
    convex_ff.setPoint(0, sf::Vector2f(0, 0));
    convex_ff.setPoint(1, sf::Vector2f(150, 10));
    convex_ff.setPoint(2, sf::Vector2f(120, 90));
    convex_ff.setPoint(3, sf::Vector2f(30, 100));
    convex_ff.setPoint(4, sf::Vector2f(0, 50));
}

float event_frequency(int n, int n_target) {
    if (n < n_target)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void displayMessageDuringStandby(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, std::string message)
{
    while (window_ptr->isOpen())
    {
        sf::Event event;
        while (window_ptr->pollEvent(event))
        {
            if (event.type == sf::Event::Closed) { window_ptr->close(); }
            if (event.type == sf::Event::KeyReleased) { break; }
        }

        window_ptr->clear();
        // Draw the string to display time
        sf::Text text(message, *param.font_ptr, 20);
        window_ptr->draw(text);

        /** Render on window **/
        window_ptr->display();
    }
}

void displayMessageDuringSimulation(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, std::string message)
{
    // Draw the string to display time

    sf::Text text(message, *param.font_ptr, 25);
    text.setFillColor(predfColorBlack());
    text.setPosition(sf::Vector2f(param.zoom * param.ScreenOffSet_x + param.ScreenWidth / 2 - text.getLocalBounds().width /2, param.zoom * param.ScreenOffSet_y + param.ScreenHeight / 25));
    window_ptr->draw(text);
}

float randomFloat(float	min, float max) {
    // source : https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
    std::uniform_real_distribution<> dis(min, max);
    return dis(gen);
}

float perlinNoise(float previousValue, float varLow, float varHigh){
    return previousValue + randomFloat(varLow, varHigh);
}

bool probabilityToCreate_Sigmoid(int N_current, int N_target, float t_halfLife, float dt, float stippness)
{
    // we write the sigmoid like function
    float probability_based_Sigmoid = 1 - (1 / (1 + exp( float(N_target) - stippness * float(N_current) ) ) );
    float probability_event_during_dt_based_on_halfLife = probabilityOfDyingDuringTimeStep(t_halfLife, dt);
    if ( randomFloat(0,1) <= (probability_based_Sigmoid * probability_event_during_dt_based_on_halfLife ))
    {
        return true;
    }
    else
    {
        return false;
    }
}


std::poisson_distribution<int> CreatePoissonDistribution(double nu, double dt)
{
    std::poisson_distribution<int> poisson_distrib(nu * dt);
    return poisson_distrib;
}


int ParametersObj::numberEventsDuringStep(double nu, double dt) {
    int nbEvents = 0;
    if (nu == nu1) { nbEvents = Poisson_nu1(generator); }
    else if (nu == nu2_im) { nbEvents = Poisson_nu2_im(generator); }
    else if (nu == nu2_int) { nbEvents = Poisson_nu2_int(generator); }
    else if (nu == nu2_m) { nbEvents = Poisson_nu2_m(generator); }
    return nbEvents;
}


sf::Color predfColorWhite() {return sf::Color(255, 255, 255, 255); }
sf::Color predfColorBlack() { return sf::Color(0, 0, 0, 255); }
sf::Color predfColorGrayLight() { return sf::Color(180, 180, 180, 255); }
sf::Color predfColorGrayMedium() { return sf::Color(70, 70, 70, 255); }
sf::Color predfColorGrayDark() { return sf::Color(30, 30, 30, 255); }


sf::Color predfColorRed0() { return sf::Color(247, 1, 24, 255); }
sf::Color predfColorRed1() { return sf::Color(249, 97, 111, 255); }
sf::Color predfColorRed2() { return sf::Color(247, 55, 73, 255); }
sf::Color predfColorRed3() { return sf::Color(196, 0, 19, 255); }
sf::Color predfColorRed4() { return sf::Color(154, 0, 15, 255); }

sf::Color predfColorYellow0() { return sf::Color(255, 166, 1, 255); }
sf::Color predfColorYellow1() { return sf::Color(255, 200, 99, 255); }
sf::Color predfColorYellow2() { return sf::Color(255, 185, 57, 255); }
sf::Color predfColorYellow3() { return sf::Color(203, 132, 0, 255); }
sf::Color predfColorYellow4() { return sf::Color(159, 104, 0, 255); }

sf::Color predfColorBlue0() { return sf::Color(17, 72, 174, 255); }
sf::Color predfColorBlue1() { return sf::Color(87, 125, 195, 255); }
sf::Color predfColorBlue2() { return sf::Color(53, 98, 179, 255); }
sf::Color predfColorBlue3() { return sf::Color(12, 55, 135, 255); }
sf::Color predfColorBlue4() { return sf::Color(8, 42, 106, 255); }

sf::Color predfColorGreen0() { return sf::Color(38, 215, 1, 255); }
sf::Color predfColorGreen1() { return sf::Color(112, 225, 88, 255); }
sf::Color predfColorGreen2() { return sf::Color(78, 218, 48, 255); }
sf::Color predfColorGreen3() { return sf::Color(29, 169, 0, 255); }
sf::Color predfColorGreen4() { return sf::Color(23, 133, 0, 255); }



float probabilityOfDyingDuringTimeStep(float t_halfLife, float dt)
{ /** //probability of dying during the interval dt according to the half-life t_halfLife (in seconds).
  The half life is the time needed for the probability of dying to be equal to 0.5.
  **/
    return 1 - pow(0.5, dt/t_halfLife);  //0.5*dt/t_halfLife;

    /*
    Demonstration by recurence for a simpler case dt=t_halfLife => t=n*dt=n*halflife :  (1-p)^n = probability to be alive at time t
    p = 0.5

    p_live = (1-p)^0 = 1 after      1=0*t_.5
    p_live = (1-p)^1 = 0.5 after    t=1*t_.5
    p_live = (1-p)^2 = 0.75 after   t=2*t_.5
    ...
    Initializatioin:
        p_live = (1-p)^0 = 1 is true
    Heredity:
        if is true (1-p)^n is it true at (1-p)^(n+1)
        (1-p)^n at n then at ((1-p)^n)(1-p)=(1-p)^(n+1) => true
    Conclusion:
        The relation is therefore true for all n � N
    */
}

/**  Color Methods  **/

Color::Color()
{
    R = 255;
    G = 255;
    B = 255;
    Alpha = 255;
}

Color::Color(float Red, float Green, float Blue)
{
    R = Red;
    G = Green;
    B = Blue;
    Alpha = 255;
}

Color::Color(float Red, float Green, float Blue, float alpha)
{
    R = Red;
    G = Green;
    B = Blue;
    Alpha = alpha;
 }

Color::~Color()
{
}

void Color::addColor(float Red, float Green, float Blue, float alpha)
{
    /** we can add (ex: Red>0) or subtract (ex Red<0) from each of the current color value **/
    R = R + Red;
    G = G + Green;
    B = B + Blue;
    Alpha = Alpha + alpha;
}

void Color::setColor(float Red, float Green, float Blue, float alpha)
{
    R = Red;
    G = Green;
    B = Blue;
    Alpha = alpha;
}

sf::Color Color::getColor_SFML()
{
    return sf::Color(R,G,B,Alpha);
}

/**  GraphicProperties Methods  **/
GraphicProperties::GraphicProperties()
{
}
GraphicProperties::~GraphicProperties()
{
}
void GraphicProperties::setColor(float R, float G, float B, float alpha)
{
    color.setColor(R, G, B, alpha);
}
void GraphicProperties::setColor(Color newColor)
{
    color = newColor;
}
sf::Color GraphicProperties::getColor_SFML()
{
    //return sf::Color(color.R, color.G, color.B, color.Alpha);
    return color.getColor_SFML();
}
Color GraphicProperties::getColor()
{
    return color;
}