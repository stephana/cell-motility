#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>

#include <string>
#include <iostream>
#include <fstream>
#include <cassert>
#include <exception>
#include <memory>

#include <SFML/Graphics.hpp>

#include "properties.h"
#include "ParametersObj_essential.h"
#include "ui_mainwindow.h"

QT_BEGIN_NAMESPACE
    namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(std::shared_ptr<ParametersObj> param_ptr, std::shared_ptr<sf::RenderWindow> window_ptr, QWidget* parent = Q_NULLPTR);
    ~MainWindow();
    std::shared_ptr<ParametersObj> param_ptr;
    std::shared_ptr<sf::RenderWindow> window_ptr;
    void set_all_gui_values();

private:
    Ui::MainWindow* ui;

private slots:
    // General
    void save_simulation_parameters();
    void load_simulation_parameters();

    void duration_lineEdit_editingFinished();
    void dt_lineEdit_editingFinished();
    void nbOfSimulations_lineEdit_editingFinished();
    void displaySimulation_clicked();
    void recordDetails_clicked();
    void simulationProgressBar_setVisible(bool visible);
    void simulationProgressBar_update(float percentDone);
    void refreshPushButton_released();
    // N0 --------------------
    void delta_theta_1_slider_moved();
    void alpha0_slider_moved();
    void F_R0_slider_moved();
    void radiusN0_slider_moved();
    // N1 --------------------
    void nu1_slider_moved();
    void tau_int_slider_moved();
    void tau_m_slider_moved();
    void tau_t_slider_moved();
    void alpha10_slider_moved();
    void alpha_int_slider_moved();
    //void alpha1m_slider_moved();
    void F_R1_slider_moved();
    void radiusN1_slider_moved();
    // N2 --------------------
    void delta_theta_2_slider_moved();
    void nu2_im_slider_moved();
    void nu2_int_slider_moved();
    void nu2_m_slider_moved();
    void tau_2_slider_moved();
    void alpha2_slider_moved();
    void ruptureForce_N2_slider_moved();
    void radiusN2_slider_moved();
    // B1 --------------------
    void B1_tension_only_clicked();
    void l1i_slider_moved();
    void k10_slider_moved();
    void k11_slider_moved();
    void gamma_max_slider_moved();
    void epsilon_m_slider_moved();
    // B2 --------------------
    void B2_tension_only_clicked();
    void l2i_slider_moved();
    void epsilon20_slider_moved();
    void k2_slider_moved();
    // Display --------------------
    void ScreenHeight_slider_moved();
    void ScreenWidth_slider_moved();
    void PixelsPerMeter_slider_moved();
    void legend_offsetX_coefficient_slider_moved();
    void legend_offsetY_coefficient_slider_moved();
    void legend_spacingX_slider_moved();
    void legend_spacingY_slider_moved();
    // Legend --------------------
    void displayLegend_clicked();
    void displayNodeType_clicked();
    void displayNodeID_clicked();
    void displayNodeMaturationState_clicked();
    void displayBranchID_clicked();
    void displayBranchType_clicked();
    void displayCytoplasm_clicked();
    void displayInteractions_clicked();
    void displayNodes_clicked();
};
#endif // MAINWINDOW_H
