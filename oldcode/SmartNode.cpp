#include "SmartNode.h"

/**
#####################################################################
######################     SMART NODE   #############################
#####################################################################
**/

SmartNode::SmartNode(ParametersObj& param, unsigned int cellRefNb) { position = Vector2D(0, 0); setLifeSpan(param); setParentCellRefNb(cellRefNb);}
SmartNode::SmartNode(ParametersObj& param, Vector2D xy, unsigned int cellRefNb) { position = xy; setLifeSpan(param); setParentCellRefNb(cellRefNb);}
SmartNode::SmartNode(ParametersObj& param, Vector2D xy, int nodeRefNb, unsigned int cellRefNb) { refNb =nodeRefNb;  position = xy; setLifeSpan(param); setParentCellRefNb(cellRefNb);}
SmartNode::~SmartNode(){}

void SmartNode::setLifeSpan(ParametersObj& param)
{
    lifespan_2 = param.tau2 * randomFloat(0.95, 1.05);
    durationIntermediateState_Max = param.tau_int * randomFloat(0.95, 1.05);
    durationMatureState_Max = param.tau_m * randomFloat(0.95, 1.05);
}

void SmartNode::setParentCellRefNb(unsigned int cellRefNb) { parentCellRefNb = cellRefNb; };

unsigned int SmartNode::getPatentCellRefNb() { return parentCellRefNb; };

// mechanics
void SmartNode::setPosition(Vector2D xy) { position = xy;}
Vector2D SmartNode::getPosition() { return position;}
//void SmartNode::setRadius(float r) { radius = r;}
float SmartNode::getRadius(ParametersObj& param) {
    float radius;
    if (isN0()) { return radius = param.radiusN0; }
    else if (isN1()) { return radius = param.radiusN1; }
    else if (isN2()) { return radius = param.radiusN2; }
}

float SmartNode::getRadiusPixel(ParametersObj& param) {
    return getRadius(param)*param.PixelsPerMeter;
}

void SmartNode::applyForce(Vector2D& Fxy) {
    netForce.add(Fxy);
}
float SmartNode::stepNode(ParametersObj& param,RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, SmartSubstrate& substrate, bool onPattern)
{
    bound = false;

    float adhesionStrengthFactor = substrate.getAdhesionStrength(getPosition());

    if (isN0())
    {
        alpha = param.alpha0;  // unused for now, but I keep it if they change their mind

        //if (!onPattern){ alpha = param.alpha0 / 10; } // <----------------
        
        if (netForce.magnitude() < param.F_R0 * adhesionStrengthFactor  && onPattern) { bound = true; }
    }
    else if (isN1())
    {
        if (!onPattern) { setAliveToFalse("rupture"); }

        if (isImmature() && age > param.tau_t)
        {
            setIntermediate();
            if (param.recordDetails)
            {
                nodeTransitionRecords.addIntermediateN1TransitionPeriodToList(age);
                CPM.nodeLifeEventsValuesToString(param, *this, "transitionIntermediate", "N1");
            }

        }

        if (isImmature())
        {
            alpha = param.alpha_im(age);
        }
        else if (isIntermediate())
        {
            alpha = param.alpha_int;
        }
        else if (isMature())
        {
            alpha = param.alpha1_m();
            bound = true;
            if (netForce.magnitude() > param.F_R1 * adhesionStrengthFactor || !onPattern) {

                setAliveToFalse("rupture");

            }
        }

        //if (!onPattern) { alpha = param.alpha_im(0) / 10.0; } // <----------------
    }
    else if (isN2())
    {
        alpha = param.alpha2; // unused for now, but I keep it if they change their mind
#if NEW_CHANGES // used to create splipery N2 nodes (friction coefficient alpha2)

        if (netForce.magnitude() > param.F_R2 * adhesionStrengthFactor && onPattern)
        {
            setAliveToFalse("rupture");
        }
        else if (onPattern)
        {
            bound = true;
        }
        else
        {
            bound = false;
        }

#else
        if( netForce.magnitude() > param.F_R2 * adhesionStrengthFactor || !onPattern)
        {
            setAliveToFalse("rupture");
        }
        else
        {
            bound = true;
        }
#endif
    }

    if (isAlive())
    {
        if (!bound)
        {
            alpha = adhesionStrengthFactor * alpha;
            Vector2D vec = netForce;
            vec.mult(1 / (alpha)); // Quasistatic approximation

            // Time step integration
            vec.mult(dt);// displacement / movement

            Vector2D pos = getPosition(); // getting current position

            pos.add(vec);
            setPosition(pos); // change in position => new position
            // keep a record of net force can be usefull to calculate adhesion force
        }
        netForce_previous_step = netForce;
        netForce_magnitude_previous_step = netForce.magnitude();

        age = age + dt;
    }
    // reinitializing netForce for the next step.
    netForce.mult(0);// reinitializing netForce for the next step.

    // record duration in each state
    if (isN1())
    {
        if (isIntermediate()) { durationIntermediateState = durationIntermediateState + dt; }
        else if (isMature()) { durationMatureState = durationMatureState + dt; }
        else { durationImmatureState = durationImmatureState + dt; }
    }


    return adhesionStrengthFactor;
}


void SmartNode::shouldLive(ParametersObj& param, float& dt)
{
    // testing half life
    if (!isN0() && !isImmature())
    {
        if (isIntermediate() && durationIntermediateState >= durationIntermediateState_Max) { setAliveToFalse("lifeSpan"); }
        else if (isMature() && durationMatureState >= durationMatureState_Max) { setAliveToFalse("lifeSpan"); } // else if (isMature() && age >= durationMatureState_Max) { setAliveToFalse("lifeSpan"); }
        else if( isN2() && age >= lifespan_2) { setAliveToFalse("lifeSpan"); }
    }
}

// Node order?
bool SmartNode::isN0() { return is_N0; }
bool SmartNode::isN1() { return is_N1; }
bool SmartNode::isN2() { return is_N2; }
int SmartNode::getOrder()
{ 
    if (isN0()) {return 0;}
    if (isN1()) { return 1; }
    if (isN2()) { return 2; }
    else { return -1; }
}
// Set order
void SmartNode::setOrderToN0(ParametersObj& param) { setOrderAllToFalse(); is_N0 = true; }
void SmartNode::setOrderToN1(ParametersObj& param) { setOrderAllToFalse(); is_N1 = true; is_immature = true; }
void SmartNode::setOrderToN2(ParametersObj& param) { setOrderAllToFalse(); is_N2 = true; }
void SmartNode::setOrderAllToFalse()
{
    is_N0 = false;
    is_N1 = false;
    is_N2 = false;
}

bool SmartNode::isImmature() { return is_immature;}
bool SmartNode::isIntermediate() { return is_intermediate; }
bool SmartNode::isMature() { return is_mature; }
void SmartNode::setIntermediate() { is_immature = false; is_intermediate = true; is_mature = false; }
void SmartNode::setMature() { is_immature = false; is_intermediate = false; is_mature = true; }
bool SmartNode::isAlive() {
    if (isN0()) { return true; }
    else {
        return is_alive;
    }
}
void SmartNode::setAliveToFalse(std::string cause_of_death)
{
    is_alive = false;
    causeOfDeath = cause_of_death;
}
float SmartNode::get_k_substrate()
{
    return k_subsrate_rigidity( getPosition() );
}

void drawNode(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition, float radius, sf::Color nodeColor)
{
    radius = radius * param.PixelsPerMeter;
    sf::CircleShape circle(radius);
    // Applying values to SFML methods
    circle.setOrigin(radius, radius);
    circle.setRadius(radius);
    circle.setPosition(nodePosition.getPixelCoord(param));
    circle.setFillColor(nodeColor);
    // Drawing
    window_ptr->draw(circle);
}

void drawNode(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition, float radius, sf::Color nodeColor, sf::Color outlineColor)
{
    double fill_ratio = 0.7;

    radius = radius * param.PixelsPerMeter;
    sf::CircleShape circle(radius);
    // Applying values to SFML methods
    circle.setOrigin(fill_ratio *radius, fill_ratio *radius);
    circle.setRadius(fill_ratio *radius);
    circle.setPosition(nodePosition.getPixelCoord(param));
    circle.setFillColor(nodeColor);
    circle.setOutlineThickness( (1-fill_ratio) * radius);
    circle.setOutlineColor(outlineColor);
    // Drawing
    window_ptr->draw(circle);
}

void drawN0(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition)
{
    drawNode(param, window_ptr, nodePosition, param.radiusN0, param.colorN0_fill, param.colorN0_outline);
}

void drawN1(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr,  Vector2D nodePosition, double maturation_ratio)
{
    drawNode(param, window_ptr, nodePosition, param.radiusN1, param.colorN1_immature_outline);
}

void drawN1_immature(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition)
{
    drawNode(param, window_ptr, nodePosition, param.radiusN1, param.colorN1_immature_fill, param.colorN1_immature_outline);
}

void drawN1_intermediate(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition)
{
    drawNode(param, window_ptr, nodePosition, param.radiusN1, param.colorN1_intermediate_fill, param.colorN1_intermediate_outline);
}

void drawN1_mature(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition)
{
    drawNode(param, window_ptr, nodePosition, param.radiusN1, param.colorN1_mature_fill, param.colorN1_mature_outline);
}

void drawN2(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, Vector2D nodePosition)
{
    drawNode(param, window_ptr, nodePosition, param.radiusN2, param.colorN2_fill, param.colorN2_outline);
}