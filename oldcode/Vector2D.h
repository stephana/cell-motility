#ifndef VECTOR2D_H
#define VECTOR2D_H

#define PI 3.14159265

#include <iostream>
#include <SFML/Graphics.hpp>
#include "properties.h"

/*
    This code is used to define 2D vector and associated tools (functions)
*/


// Vector2D is used to create a vector.
class Vector2D
{
    public:
        Vector2D();
        Vector2D(float x, float y);
        void add(Vector2D v);
        void sub(Vector2D v);
        Vector2D offset(float& angle, float& length);
        float magnitude();
        void normalize();
        void mult(float a);
        void divid(float b);
        Vector2D perpendicular();
        void flip180();
        void setFromPixel(ParametersObj& param ,sf::Vector2f xy_pixelCoord);
        sf::Vector2f getPixelCoord(ParametersObj& param);
        float scalarPixelsToMeters(ParametersObj& param, int pixels);
        float scalarMetersToPixels(ParametersObj& param, float meters);
        float distance(Vector2D positionVector);
        float measureAngleRadiants(Vector2D v);
        float scalarProduct(Vector2D v);
        void setNull(); // this will define the vector as null
        bool isNonNull(); // check that the vector is non null
        virtual ~Vector2D();
        float x;
        float y;

    protected:

    private:
};

float angleRandiants(Vector2D u, Vector2D v);
float angleDegrees(Vector2D u, Vector2D v);
float scalarProduct(Vector2D u, Vector2D v);
Vector2D add(Vector2D u, Vector2D v);
Vector2D sub(Vector2D u, Vector2D v);
Vector2D crossProduct(Vector2D u, Vector2D v);
Vector2D normalize(Vector2D v);
void drawRectangle(ParametersObj& param, Vector2D xy, float width, float height, sf::Color& color, std::shared_ptr<sf::RenderWindow>& window_ptr, bool outline);
void drawLine(ParametersObj& param, Vector2D xy1, Vector2D xy2, float thickness,const sf::Color& color, std::shared_ptr<sf::RenderWindow>& window_ptr);
void drawTrapezoid(ParametersObj& param, Vector2D xy1, Vector2D xy2, float radius_nodeA, float radius_nodeB, sf::Color& color, std::shared_ptr<sf::RenderWindow>& window_ptr);

// ##############################################

class Segment
{
    public:
        Segment(float x1, float y1, float x2, float y2) : x1(x1), y1(y1), x2(x2), y2(y2) {};
        ~Segment(){};
        float x1;
        float y1;
        float x2;
        float y2;
};

bool doPointsIncommon(Segment &segment1, Segment &segment2);
bool doIntersect(Segment &segment1, Segment &segment2);

inline float vector_mean(std::vector<float> vec)
{
    float sumValues = 0;
    for (std::vector<float>::iterator it = vec.begin(); it < vec.end(); ++it)
    {
        sumValues = sumValues + *it;
    }

    if (vec.size() > 0)
    {
        return sumValues / float(vec.size());
    }
    else { return 0; }
}

inline float vector_standardDeviation(std::vector<float> vec)
{
    if (vec.size() > 0)
    {
        float vec_mean = vector_mean(vec);
        float Nb = float(vec.size());
        float sum_sqr_diff = 0;

        for (std::vector<float>::iterator it = vec.begin(); it < vec.end(); ++it)
        {
            sum_sqr_diff = sum_sqr_diff + pow(*it - vec_mean, 2);
        }
        float sigma = sqrt(1 / Nb * sum_sqr_diff);
        return sigma;
    }
    else { return 0; }
}

inline float vector_minimum(std::vector<float> vec)
{
    if (vec.size() == 0)
    {
        return 0;
    }
    std::vector<float>::iterator it = vec.begin();
    float minimum_value = *it; // initialization
    for (it = vec.begin(); it < vec.end(); ++it)
    {
        if (*it < minimum_value) { minimum_value = *it; } //
    }
    return minimum_value;
}

inline float vector_maximum(std::vector<float> vec)
{
    float max_value = 0;
    for (std::vector<float>::iterator it = vec.begin(); it < vec.end(); ++it)
    {
        if (*it > max_value) { max_value  = *it; }
    }
    return max_value;
}


#endif // VECTOR2D_H
