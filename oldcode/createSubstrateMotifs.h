#ifndef CREATESUBSTRATEMOTIFS_H
#define CREATESUBSTRATEMOTIFS_H

#include <memory>

#include "properties.h"
#include "SmartSubstrate.h"



inline void createSubstrateMotifs_rectangles(ParametersObj& param, SmartSubstrate& substrate)
{
    float height = param.pattern_height;
    float width = param.pattern_width;
    float pattern_spacing = param.pattern_spacing;
    int nbmotifs = param.pattern_nb_motifs;

    for (int i = 0; i < nbmotifs; i++)
    {
        //std::shared_ptr<SubstratePattern_Rectangle> rectangle = std::make_shared<SubstratePattern_Rectangle>(param, Vector2D((i - nbmotifs / 2.0 - 0.5) * (pattern_spacing + width) + pattern_spacing + width / 2.0, 0), height, width);
        std::shared_ptr<SubstratePattern_Rectangle> rectangle = std::make_shared<SubstratePattern_Rectangle>(param, Vector2D( (i - nbmotifs / 2.0) * (pattern_spacing + width) + pattern_spacing + width / 2.0, 0), height, width);
        std::shared_ptr<SubstratePattern> substrate_rectangle = std::static_pointer_cast<SubstratePattern>(rectangle);
        substrate.addPattern(substrate_rectangle);
    }
}

inline void createSubstrateMotifs_rectangles_multiadhesion(ParametersObj& param, SmartSubstrate& substrate, float& height, float& width, float& pattern_spacing)
{
    int nbmotifs = param.pattern_nb_motifs;
    for (int i = 0; i < nbmotifs; i++)
    {
        std::shared_ptr<SubstratePattern_Rectangle> rectangle = std::make_shared<SubstratePattern_Rectangle>(param, Vector2D(0, 50- 100 * i), 100, 100);
        std::shared_ptr<SubstratePattern> substrate_rectangle = std::static_pointer_cast<SubstratePattern>(rectangle);
        substrate_rectangle->setAdhesionStrength(param, 0.9 + 0.2 * float(i) );
        substrate.addPattern(substrate_rectangle);
    }
}


inline void createSubstrateMotifs_circles(ParametersObj& param, SmartSubstrate& substrate)
{
    float height = param.pattern_height;
    float width = param.pattern_width;
    float pattern_spacing = param.pattern_spacing;
    float radius = param.pattern_radius;
    int nbmotifs = param.pattern_nb_motifs;

    for (int i = 0; i < nbmotifs; i++)
    {
        std::shared_ptr<SubstratePattern_Circle> circle = std::make_shared<SubstratePattern_Circle>(param, Vector2D( (i + 1 - nbmotifs / 2.0) * pattern_spacing - radius / 2.0, 0), radius);
        std::shared_ptr<SubstratePattern> substrate_circle = std::static_pointer_cast<SubstratePattern>(circle);
        substrate.addPattern(substrate_circle);
    }
}


inline void createSubstrateMotifs_triangles(ParametersObj& param, SmartSubstrate& substrate)
{
    float height = param.pattern_height;
    float width = param.pattern_width;
    float pattern_spacing = param.pattern_spacing;
    float radius = param.pattern_radius;
    int nbmotifs = param.pattern_nb_motifs;

    for (int i = 0; i < nbmotifs; i++)
    {
        std::shared_ptr<SubstratePattern_Triangle> traingle = std::make_shared<SubstratePattern_Triangle>(param, Vector2D( (i - nbmotifs / 2.0 - 0.5) * (pattern_spacing + width) + pattern_spacing + width/2.0, 0), height, width, 0);
        std::shared_ptr<SubstratePattern> substrate_triangle = std::static_pointer_cast<SubstratePattern>(traingle);
        substrate.addPattern(substrate_triangle);
    }

}


inline void scalebarMotifs(ParametersObj& param, SmartSubstrate& substrate, float& height, float& width)
{
    float verticalshift = -20;
    std::shared_ptr<SubstratePattern_Rectangle> rectangle = std::make_shared<SubstratePattern_Rectangle>(param, Vector2D( width/2.0, verticalshift), height, width);
    std::shared_ptr<SubstratePattern> substrate_rectangle = std::static_pointer_cast<SubstratePattern>(rectangle);
    substrate.addPattern(substrate_rectangle);

    rectangle = std::make_shared<SubstratePattern_Rectangle>(param, Vector2D(- width / 2.0, verticalshift), height, width);
    substrate_rectangle = std::static_pointer_cast<SubstratePattern>(rectangle);
    substrate.addPattern(substrate_rectangle);
}

inline void createSubstrateMotifs(ParametersObj& param, SmartSubstrate& substrate)
{
    
    if (param.pattern_type == homogeneous_pattern)
    {
        param.adhereEveryWhere = true;
        substrate.adhereEveryWhere = param.adhereEveryWhere;
    } else
    {
        param.adhereEveryWhere = false;
        substrate.adhereEveryWhere = param.adhereEveryWhere;
        if (param.pattern_type == triangle_pattern)
        {
            createSubstrateMotifs_triangles(param, substrate);
        }
        else if (param.pattern_type == rectangle_pattern)
        {
            createSubstrateMotifs_rectangles(param, substrate);
            //createSubstrateMotifs_rectangles_multiadhesion(param, substrate, height, width, pattern_spacing);
        }
        else if (param.pattern_type == circle_pattern)
        {
            createSubstrateMotifs_circles(param, substrate);
        }
    }
    


    float scalebarwidth = 2;
    float scalebarheight = 100;
    //scalebarMotifs( param, substrate, scalebarwidth, scalebarheight);
}

#endif