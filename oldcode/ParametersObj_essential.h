﻿#ifndef PROPERTIES_ESSENTIAL_H
#define PROPERTIES_ESSENTIAL_H
#include <iostream>
#include <fstream>
#include <cassert>
#include "properties.h"


/*
    This code is used to save the parameter values in binary form with the GUI.
*/

class ParametersObj_essential
{
public:
    float duration;
    float dt;
    int nbOfSimulations;
    bool displaySimulation;
    bool recordDetails;

    // Cell and node parameters
    float tau_int;
    float tau_m;
    float tau2;
    float tau_t;
    float l1initial;
    float l2initial;

    float epsilon_m;
    float epsilon10;
    float epsilon20;

    float nu1;
    float nu2_im;
    float nu2_int;
    float nu2_m;

    float k10;
    float k11;
    float k2;
    float gamma_max__;

    bool B1_tension_only;
    bool B2_tension_only;

    float delta_theta_2;
    float radiusN0;
    float radiusN1;
    float radiusN2;

    // Delta inhibition domain
    float delta_theta_1;

    float alpha0;
    float alpha10;


    float alpha_int;
    
    // Coefficients de frottement • α1i(t) pour le nœud N1 intermédiaire est une fonction sigmoïde qui dépend du temps et atteint une saturation dont le seuil αsat est à définir
    float coef_alpha_int_to_alpha_m;
    float alpha2;

    float F_R0;
    float F_R1;
    float F_R2;
    float F_gamma;

    // result output
    //std::string resultsOutputFilename = "";
    //std::string parametersOutputFilename = "";
    float saveResultTimeIntervals;

    // COPY FUNCTION
    void copy(ParametersObj& param)
    {
        duration = param.duration;
        dt = param.dt;
        nbOfSimulations = param.nbOfSimulations;
        displaySimulation = param.displaySimulation;
        recordDetails = param.recordDetails;

        // Cell and node parameters
        tau_int = param.tau_int;
        tau_m = param.tau_m;
        tau2 = param.tau2;
        tau_t = param.tau_t;

        l1initial = param.l1initial;
        l2initial = param.l2initial;

        epsilon_m = param.epsilon_m;
        epsilon10 = param.epsilon10;
        epsilon20 = param.epsilon20;

        nu1 = param.nu1;
        nu2_im = param.nu2_im;
        nu2_int = param.nu2_int;
        nu2_m = param.nu2_m;

        k10 = param.k10;
        k11 = param.k11;
        k2 = param.k2;
        gamma_max__ = param.gamma_max__;

        B1_tension_only = param.B1_tension_only;
        B2_tension_only = param.B2_tension_only;


        delta_theta_2 = param.delta_theta_2;
        radiusN0 = param.radiusN0;
        radiusN1 = param.radiusN1;
        radiusN2 = param.radiusN2;

        // Delta inhibition domain
        delta_theta_1 = param.delta_theta_1;


        alpha0 = param.alpha0;
        alpha10 = param.alpha10;
        alpha_int = param.alpha_int;

        // Coefficients de frottement • α1i(t) pour le nœud N1 intermédiaire est une fonction sigmoïde qui dépend du temps et atteint une saturation dont le seuil αsat est à définir
        coef_alpha_int_to_alpha_m = param.coef_alpha_int_to_alpha_m;
        alpha2 = param.alpha2;

        F_R0 = param.F_R0;
        F_R1 = param.F_R1;
        F_R2 = param.F_R2;
        F_gamma = param.F_gamma;

        // result output
        //resultsOutputFilename = param.resultsOutputFilename;
        //parametersOutputFilename = param.parametersOutputFilename;
        saveResultTimeIntervals = param.saveResultTimeIntervals;
    }

    ParametersObj paste(ParametersObj& param)
    {
        param.duration = duration;
        param.dt = dt;
        param.nbOfSimulations = nbOfSimulations;
        param.displaySimulation = displaySimulation;
        param.recordDetails = recordDetails;

        // Cell and node parameters
        param.tau_int = tau_int;
        param.tau_m = tau_m;
        param.tau2 = tau2;
        param.tau_t = tau_t;

        param.l1initial = l1initial;
        param.l2initial = l2initial;

        param.epsilon_m = epsilon_m;
        param.epsilon10 = epsilon10;
        param.epsilon20 = epsilon20;

        param.nu1 = nu1;
        param.nu2_im = nu2_im;
        param.nu2_int = nu2_int;
        param.nu2_m = nu2_m;

        param.k10 = k10;
        param.k11 = k11;
        param.k2 = k2;
        param.gamma_max__ = gamma_max__;

        param.B1_tension_only = B1_tension_only;
        param.B2_tension_only = B2_tension_only;

        param.delta_theta_2 =delta_theta_2;
        param.radiusN0 = radiusN0;
        param.radiusN1 = radiusN1;
        param.radiusN2 = radiusN2;

        // Delta inhibition domain
        param.delta_theta_1 = delta_theta_1;


        param.alpha0 = alpha0;
        param.alpha10 = alpha10;
        param.alpha_int = alpha_int;

        // Coefficients de frottement • α1i(t) pour le nœud N1 intermédiaire est une fonction sigmoïde qui dépend du temps et atteint une saturation dont le seuil αsat est à définir
        param.coef_alpha_int_to_alpha_m = coef_alpha_int_to_alpha_m;
        param.alpha2 = alpha2;

        param.F_R0 = F_R0;
        param.F_R1 = F_R1;
        param.F_R2 = F_R2;
        param.F_gamma = F_gamma;


        // result output
        //param.resultsOutputFilename = resultsOutputFilename;
        //param.parametersOutputFilename = parametersOutputFilename;
        param.saveResultTimeIntervals = saveResultTimeIntervals;
        param.setPoissonDistributions();

        return param;
    }

    void save(std::ofstream& of)
    {
        of.write((char*)&duration, sizeof(duration));
        of.write((char*)&dt, sizeof(dt));
        of.write((char*)&nbOfSimulations, sizeof(nbOfSimulations));
        of.write((char*)&displaySimulation, sizeof(displaySimulation));
        of.write((char*)&recordDetails, sizeof(recordDetails));

        of.write((char*)&tau_int, sizeof(tau_int));
        of.write((char*)&tau_m, sizeof(tau_m));
        of.write((char*)&tau2, sizeof(tau2));
        of.write((char*)&tau_t, sizeof(tau_t));

        of.write((char*)&l1initial, sizeof(l2initial));
        of.write((char*)&l2initial, sizeof(l2initial));

        of.write((char*)&epsilon_m, sizeof(epsilon_m));
        of.write((char*)&epsilon10, sizeof(epsilon10));
        of.write((char*)&epsilon20, sizeof(epsilon20));

        of.write((char*)&nu1, sizeof(nu1));
        of.write((char*)&nu2_im, sizeof(nu2_im));
        of.write((char*)&nu2_int, sizeof(nu2_int));
        of.write((char*)&nu2_m, sizeof(nu2_m));

        of.write((char*)&k10, sizeof(k10));
        of.write((char*)&k11, sizeof(k11));
        of.write((char*)&k2, sizeof(k2));
        of.write((char*)&gamma_max__, sizeof(gamma_max__));

        of.write((char*)&B1_tension_only, sizeof(B1_tension_only));
        of.write((char*)&B2_tension_only, sizeof(B2_tension_only));

        of.write((char*)&delta_theta_2, sizeof(delta_theta_2));
        of.write((char*)&radiusN0, sizeof(radiusN0));
        of.write((char*)&radiusN1, sizeof(radiusN1));
        of.write((char*)&radiusN2, sizeof(radiusN2));

        of.write((char*)&delta_theta_1, sizeof(delta_theta_1));
        of.write((char*)&alpha0, sizeof(alpha0));
        of.write((char*)&alpha10, sizeof(alpha10));
        of.write((char*)&alpha_int, sizeof(alpha_int));

        of.write((char*)&coef_alpha_int_to_alpha_m, sizeof(coef_alpha_int_to_alpha_m));
        of.write((char*)&alpha2, sizeof(alpha2));
        of.write((char*)&F_R0, sizeof(F_R0));
        of.write((char*)&F_R1, sizeof(F_R1));
        of.write((char*)&F_R2, sizeof(F_R2));
        of.write((char*)&F_gamma, sizeof(F_gamma));

        //of.write((char*)&resultsOutputFilename, sizeof(resultsOutputFilename));
        //of.write((char*)&parametersOutputFilename, sizeof(parametersOutputFilename));
        of.write((char*)&saveResultTimeIntervals, sizeof(saveResultTimeIntervals));
    }

    void load(std::ifstream& inf)
    {
        //inf.read((char*)&dt, sizeof(dt));
        //inf.read((char*)&name, sizeof(name));

        inf.read((char*)&duration, sizeof(duration));
        inf.read((char*)&dt, sizeof(dt));
        inf.read((char*)&nbOfSimulations, sizeof(nbOfSimulations));
        inf.read((char*)&displaySimulation, sizeof(displaySimulation));
        inf.read((char*)&recordDetails, sizeof(recordDetails));

        inf.read((char*)&tau_int, sizeof(tau_int));
        inf.read((char*)&tau_m, sizeof(tau_m));
        inf.read((char*)&tau2, sizeof(tau2));
        inf.read((char*)&tau_t, sizeof(tau_t));
        inf.read((char*)&l1initial, sizeof(l2initial));
        inf.read((char*)&l2initial, sizeof(l2initial));

        inf.read((char*)&epsilon_m, sizeof(epsilon_m));
        inf.read((char*)&epsilon10, sizeof(epsilon10));
        inf.read((char*)&epsilon20, sizeof(epsilon20));

        inf.read((char*)&nu1, sizeof(nu1));
        inf.read((char*)&nu2_im, sizeof(nu2_im));
        inf.read((char*)&nu2_int, sizeof(nu2_int));
        inf.read((char*)&nu2_m, sizeof(nu2_m));

        inf.read((char*)&k10, sizeof(k10));
        inf.read((char*)&k11, sizeof(k11));
        inf.read((char*)&k2, sizeof(k2));
        inf.read((char*)&gamma_max__, sizeof(gamma_max__));

        inf.read((char*)&B1_tension_only, sizeof(B1_tension_only));
        inf.read((char*)&B2_tension_only, sizeof(B2_tension_only));

        inf.read((char*)&delta_theta_2, sizeof(delta_theta_2));
        inf.read((char*)&radiusN0, sizeof(radiusN0));
        inf.read((char*)&radiusN1, sizeof(radiusN1));
        inf.read((char*)&radiusN2, sizeof(radiusN2));

        inf.read((char*)&delta_theta_1, sizeof(delta_theta_1));

        inf.read((char*)&alpha0, sizeof(alpha0));
        inf.read((char*)&alpha10, sizeof(alpha10));
        inf.read((char*)&alpha_int, sizeof(alpha_int));

        inf.read((char*)&coef_alpha_int_to_alpha_m, sizeof(coef_alpha_int_to_alpha_m));
        inf.read((char*)&alpha2, sizeof(alpha2));
        inf.read((char*)&F_R0, sizeof(F_R0));
        inf.read((char*)&F_R1, sizeof(F_R1));
        inf.read((char*)&F_R2, sizeof(F_R2));
        inf.read((char*)&F_gamma, sizeof(F_gamma));
        //getline(inf, resultsOutputFilename, '\0');
        //getline(inf, parametersOutputFilename, '\0');
        inf.read((char*)&saveResultTimeIntervals, sizeof(saveResultTimeIntervals));
      
    }


};

#endif