#include "Vector2D.h"


Vector2D::Vector2D()
{
    x = 0;
    y = 0;
}
Vector2D::Vector2D(float x, float y): x(x), y(y)
{
}
void Vector2D::add(Vector2D v)
{
    x = x + v.x;
    y = y + v.y;
}

void Vector2D::sub(Vector2D v)
{
    x = x - v.x;
    y = y - v.y;
}
Vector2D Vector2D::offset(float& angle, float& length)
{
    x = x + length * cos(angle);
    y = y + length * sin(angle);
    return Vector2D(x,y);
}
float Vector2D::magnitude()
{
    return sqrt(x*x+y*y);
}

void Vector2D::normalize()
{
    if (magnitude() == 0)
    {
        std::cout << "Warning: the magnitude you are trying to normalize is equal to zero" << std::endl;
    }
    else
    {
        divid(magnitude());
    }
}

void Vector2D::mult(float a)
{
    x = a * x;
    y = a * y;
}
void Vector2D::divid(float a)
{
    x = x/a;
    y = y/a;
}
Vector2D Vector2D::perpendicular()
{
    return Vector2D(y, -x);
}
void Vector2D::flip180()
{
    x = -x;
    y = -y;
}
void Vector2D::setFromPixel(ParametersObj& param, sf::Vector2f xy_pixelCoord)
{
    // x pixel to meters
    x = param.MetersPerPixel() * xy_pixelCoord.x;
    y = param.MetersPerPixel() * xy_pixelCoord.y - param.ScreenHeight;
}
sf::Vector2f Vector2D::getPixelCoord(ParametersObj& param)
{
    // world to pixels
    float i = param.PixelsPerMeter * x;
    float j = param.ScreenHeight - param.PixelsPerMeter * y;
    return sf::Vector2f(round(i),round(j));
}
float Vector2D::scalarPixelsToMeters(ParametersObj& param, int pixels){
    // x pixel to meters
    return param.MetersPerPixel() * pixels;
}
float Vector2D::scalarMetersToPixels(ParametersObj& param, float meters){
    // world to pixels
    return param.PixelsPerMeter * meters;
}
float Vector2D::distance(Vector2D positionVector)
{
    positionVector.sub(Vector2D(x,y));
    return positionVector.magnitude();
}

float Vector2D::scalarProduct(Vector2D v)
{
    return x*v.x + y*v.y;
}

float Vector2D::measureAngleRadiants(Vector2D v)
{
    return angleRandiants( Vector2D(x,y), v );
}

void Vector2D::setNull()
{
    mult(0);
}

bool Vector2D::isNonNull()
{
    if (magnitude() > 0.0)
    {
        return true; 
    }
    else if (magnitude() < 0.0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Vector2D::~Vector2D()
{
}


/**
##############################$
**/

float angleRandiants(Vector2D u, Vector2D v)
{
    float angle = 0;
    if(u.isNonNull() && v.isNonNull())
    {
        float ratio = scalarProduct(u, v) / (u.magnitude() * v.magnitude());
        if (ratio > 1) // this if statement was written because numerical imprecisions create can calculate values above 1 or below -1 which is outside the mathematical domain of the arcoss function
        {
            ratio = 1; // this is a correction when computational imprecision occurs
        }
        else if (ratio < -1)
        {
            ratio = -1; // this is a correction when computational imprecision occurs
        }
        angle = acos( ratio );

        // Is angle between [0,PI] or ]PI,2*PI[
        float direction = (u.x * v.y - u.y * v.x);
        if (direction<0) // testing if the angle is between PI and 2*PI
        {
            angle = -angle;
        }
    }
    return angle;
}

float angleDegrees(Vector2D u, Vector2D v)
{
    double RADTODEG = 57.295779513082320876f;
    return -RADTODEG * double(angleRandiants(u,v));
}

float scalarProduct(Vector2D u, Vector2D v)
{
    return u.x * v.x + u.y * v.y;
}

Vector2D add(Vector2D u, Vector2D v)
{
    return(Vector2D(u.x + v.x, u.y + v.y));
}

Vector2D sub(Vector2D u, Vector2D v)
{
    return(Vector2D(u.x - v.x, u.y - v.y));
}

void drawRectangle(ParametersObj& param, Vector2D xy, float width, float height, sf::Color& color, std::shared_ptr<sf::RenderWindow>& window_ptr, bool outline)
{
    //float length = xy1.distance(xy2) * PixelsPerMeter();
    float width_pixels = width * param.PixelsPerMeter;
    float height_pixels = height * param.PixelsPerMeter;
    // we want to draw a line, but to do that we need to create a rectangle (which we name line), give it a length, width, position and orientation.
    sf::RectangleShape rectangle(sf::Vector2f(width_pixels, height_pixels)); // defines the shape and length of the rectangle

    // Applying values to SFML methods
    rectangle.setOrigin(0, 0);
    rectangle.setPosition(xy.getPixelCoord(param));
    rectangle.setFillColor(color);
    if (outline)
    {
        rectangle.setOutlineThickness(2);
        rectangle.setOutlineColor(sf::Color(0, 0, 0));
    }
    window_ptr->draw(rectangle);
}

void drawLine(ParametersObj& param, Vector2D xy1, Vector2D xy2, float thickness,const  sf::Color& color, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
    float length = xy1.distance(xy2) * param.PixelsPerMeter;
    float width = thickness;
    // we want to draw a line, but to do that we need to create a rectangle (which we name line), give it a length, width, position and orientation.
    sf::RectangleShape line(sf::Vector2f(length, width)); // defines the shape and length of the rectangle

    // Applying values to SFML methods
    line.setOrigin(0, width/2);
    line.setPosition(xy1.getPixelCoord(param));
    line.setRotation(angleDegrees(Vector2D(1,0),sub(xy2,xy1)));
    line.setFillColor(color);
    window_ptr->draw(line);
}

void drawTrapezoid(ParametersObj& param, Vector2D xy1, Vector2D xy2, float radius_nodeA, float radius_nodeB, sf::Color& color, std::shared_ptr<sf::RenderWindow>& window_ptr)
{
    float length = xy1.distance(xy2) * param.PixelsPerMeter;

    // Applying values to SFML methods
    //line.setOrigin(0, width / 2);

    // cr�e une forme vide
    sf::ConvexShape convex;

    // d�finit le nombre de points (5)
    convex.setPointCount(4);

    // d�finit les points
    convex.setPoint(0, sf::Vector2f(0, radius_nodeA)); // point A1
    convex.setPoint(1, sf::Vector2f(0, -radius_nodeA)); // point A2
    convex.setPoint(2, sf::Vector2f(length, -radius_nodeB)); // point B2
    convex.setPoint(3, sf::Vector2f(length, radius_nodeB)); // point B1
    convex.setPosition(xy1.getPixelCoord(param));
    convex.setRotation(angleDegrees(Vector2D(1, 0), sub(xy2, xy1)));
    
    convex.setFillColor(color);
    window_ptr->draw(convex);

}


Vector2D crossProduct(Vector2D u, Vector2D v)
{
    return Vector2D( u.x*v.y-u.y*v.x, u.y*v.x-v.y*u.x);
}

Vector2D normalize(Vector2D v)
{
    if (v.isNonNull() )
    {
        v.divid( v.magnitude() );
        return v;
    }
    else
    {
        std::cout << "warning !!! from 'Vector2D normalize(Vector2D v)'. You are trying to normalize a null Vector2D" << std::endl;
        return Vector2D(0, 0); // warning this is not mathematically correct
    }
}

/**
###################################
**/

// A C++ program to check if two given line segments intersect 

bool doPointsIncommon(Segment &segment1, Segment &segment2)
{
    if (segment1.x1 == segment2.x1 && segment1.y1 == segment2.y1)
    {
        return true;
    }
    else if (segment1.x1 == segment2.x2 && segment1.y1 == segment2.y2)
    {
        return true;
    }
    else if (segment1.x2 == segment2.x1 && segment1.y2 == segment2.y1)
    {
        return true;
    }
    else if (segment1.x2 == segment2.x2 && segment1.y2 == segment2.y2)
    {
        return true;
    }
    else
    {
        return false;
    }
}


bool doIntersect(Segment &segment1, Segment &segment2)
{
    float X1 = segment1.x1;
    float Y1 = segment1.y1;
    float X2 = segment1.x2;
    float Y2 = segment1.y2;

    float X3 = segment2.x1;
    float Y3 = segment2.y1;
    float X4 = segment2.x2;
    float Y4 = segment2.y2;

    // The abcisse Xa of the potential point of intersection(Xa, Ya) must be contained in both interval I1 and I2, defined as follow :

    float Ix_1[2] = { std::min(X1, X2), std::max(X1, X2) };
    float Ix_2[2] = { std::min(X3, X4), std::max(X3, X4) };

    // we define the interval where the two segments could potentially meet. We get Ia = {xa_min, xa_max}

    float xa_min = std::max( Ix_1[0], Ix_2[0] );
    float xa_max = std::min(Ix_1[1], Ix_2[1]);

    float Ia[2] = { xa_min, xa_max };


    float Iy_1[2] = { std::min(Y1, Y2), std::max(Y1, Y2) };
    float Iy_2[2] = { std::min(Y3, Y4), std::max(Y3, Y4) };

    float ya_min = std::max(Iy_1[0], Iy_2[0]);
    float ya_max = std::min(Iy_1[1], Iy_2[1]);

    float Iya[2] = { ya_min, ya_max };


    if (xa_min > xa_max || ya_min > ya_max) // check that X and Y do overlap
    {
        return false; // no domain overlap -> no intersection
    }
    else
    {
        float a1;
        float a2;
        float b1;
        float b2;
        float Xa;
        float Ya;

        if (X1 == X2)  // we check that we do not divide by zero
        {
            // We assume that f1 is vertical
            // The question is does f1(X1)
            if (X3 != X4)
            {
                a2 = (Y3 - Y4) / (X3 - X4);  // Pay attention to not dividing by zero
                b2 = Y3 - a2 * X3;
                float f2_X1 = a2 * X3 + b2;
                if (f2_X1 > Iy_1[0] && f2_X1 < Iy_1[1])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // both are vertical
                return false;
            }
        }
        else if (X3 == X4)
        {
            // We assume that f2 is vertical
            // The question is does f2(X3)
            if (X3 != X4)
            {
                a1 = (Y1 - Y2) / (X1 - X2);  // Pay attention to not dividing by zero
                b1 = Y1 - a1 * X1;
                float f1_X3 = a1 * X1 + b1;
                if (f1_X3 > Iy_2[0] && f1_X3 < Iy_2[1])
                {
                    //return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // both are vertical
                return false;
            }

        }
        else
        {
            a1 = (Y1 - Y2) / (X1 - X2); //Pay attention to not dividing by zero
            a2 = (Y3 - Y4) / (X3 - X4);  // Pay attention to not dividing by zero

            if ((a1 != a2)) //  division by zero
            {
                b1 = Y1 - a1 * X1;
                b2 = Y3 - a2 * X3;

                Xa = (b2 - b1) / (a1 - a2);   // Once again, pay attention to not dividing by zero

                Ya = a1 * Xa + b1;

                
                if ( Xa >= xa_min && Xa <= xa_max && Ya >= ya_min || Ya <= ya_max)
                {
                    return true; // intersection is out of bound
                }
            }
        }
    }
    return false;
    }