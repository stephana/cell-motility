#include "SmartInteraction.h"

SmartInteraction::SmartInteraction(std::shared_ptr<SmartNode> nodeA_shared_ptr, std::shared_ptr<SmartNode> nodeB_shared_ptr): nodeA_shared_ptr(nodeA_shared_ptr), nodeB_shared_ptr(nodeB_shared_ptr)
{
    color.setColor(0, 120, 0, 120);
}
SmartInteraction::~SmartInteraction(){}
void SmartInteraction::setConnectionType(std::string connection_Type){ connectionType = connection_Type;}
int SmartInteraction::getRefNbNodeA() { return nodeA_shared_ptr->refNb; }
int SmartInteraction::getRefNbNodeB() { return nodeB_shared_ptr->refNb; }
Vector2D SmartInteraction::getVector_AB()
{
    Vector2D AB = nodeB_shared_ptr->getPosition(); // we get the position of B
    AB.sub(nodeA_shared_ptr->getPosition()); // we subtract the position of A to that of B
    // NB: the method sub()'s return type is void
    //this is why we do not return it directly and thus return AB instead
    return AB;
}
float SmartInteraction::distanceBetweenNodes()
{
    return getVector_AB().magnitude();
}

float SmartInteraction::forceMagnitude(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, bool lengthen, bool interCell)
{
    if (interCell)
    {
        return forceMagnitude_interCell(param, nodeTransitionRecords, CPM);
    }

    return forceMagnitude_intraCell(param, nodeTransitionRecords, CPM, dt, lengthen);
}

float SmartInteraction::forceMagnitude_interCell(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM)
{
    float gap_between_cells = 0;
    float Force_magnitude = 0;

    gap_between_cells = distanceBetweenNodes() - param.cytoplasmScaleFactor * (nodeA_shared_ptr->getRadius(param) + nodeB_shared_ptr->getRadius(param));

    if (gap_between_cells <= 0)
    {
        Force_magnitude = param.kic * gap_between_cells; // push
        //std::cout << Force_magnitude << std::endl;
    }
    else
    {
        Force_magnitude = param.kic * gap_between_cells; // pull
        //std::cout << Force_magnitude << std::endl;
    }

    if (Force_magnitude >= param.F_RCC)
    {
        Force_magnitude = 0;
        setAliveToFalse();
    }
    return Force_magnitude;
}

float SmartInteraction::forceMagnitude_intraCell(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, bool lengthen)
{
    // we make the transition between immature and intermediate happen between here other wise there is a delay, that seems incoherent in the graphes.
    if (nodeB_shared_ptr->isN1())
    {
        if (nodeB_shared_ptr->isImmature() && nodeB_shared_ptr->age > param.tau_t)
        {
            nodeB_shared_ptr->setIntermediate();
            if (param.recordDetails)
            {
                nodeTransitionRecords.addIntermediateN1TransitionPeriodToList(nodeB_shared_ptr->age);
                CPM.nodeLifeEventsValuesToString(param, *nodeB_shared_ptr, "transitionIntermediate", "N1");
            }
        }
    }

    // Initializing values to default
    F = 0;
    F_el = 0;
    F_c = 0;
    distanceNodesAB = distanceBetweenNodes();

    // Elastic force
    if (nodeB_shared_ptr->isN1())
    {   
        if (!nodeB_shared_ptr->hasParentN0Slipped && !nodeA_shared_ptr->bound)
        {
            nodeB_shared_ptr->hasParentN0Slipped = true;
        }

        if (nodeB_shared_ptr->isImmature())
        {
#if false //NEW_CHANGES
            if(L0_spring==NULL){ L0_spring = param.l1initial; }

            if (lengthen)
            {
                L0_spring += ( 20 - param.l1initial )/ param.tau_t * param.dt;
                param.B1_tension_only = false;
            }
            else
            {
                param.B1_tension_only = true;
            }

            F_el = (param.k10 + (param.k11 - param.k10) / param.tau_t * age) * (distanceNodesAB - L0_spring);
            
#else
            L0_spring = param.l1initial;
            F_el = (param.k10 + 0*(param.k11 - param.k10) / param.tau_t * age) * (distanceNodesAB - param.l1initial);
#endif  
           
            
            L_tau_t = distanceNodesAB;

        }
        else if (nodeB_shared_ptr->isIntermediate())
        {
            L0_spring = L_tau_t;

            F_el = param.k11 * (distanceNodesAB - L_tau_t);

#if true//!NEW_CHANGES
            float epsilon_tau_t = (distanceNodesAB - L_tau_t) / L_tau_t;

            if (epsilon_tau_t > param.epsilon_m && !nodeB_shared_ptr->hasParentN0Slipped)
            { 
                nodeB_shared_ptr->setMature();
                nodeTransitionRecords.addMatureN1TransitionPeriodToList(nodeB_shared_ptr->age);
                CPM.nodeLifeEventsValuesToString(param, *nodeB_shared_ptr, "transitionMature", "N1");
            }
#endif
        }
        else if (nodeB_shared_ptr->isMature()) {

            L0_spring = L_tau_t;

            F_el = param.k11 * (distanceNodesAB - L_tau_t);
            F_c = param.gamma_max__ * (1 - exp(- nodeB_shared_ptr->getAdhesionForce().magnitude() / param.F_gamma));

            if (distanceNodesAB < 3) // <-------------------------
            { // <-------------------------
                //std::cout << "die sucker" << std::endl; // <-------------------------
                nodeB_shared_ptr->setAliveToFalse("B2WasFullyContracted"); // <-------------------------
            } // <-------------------------
        }

        if (param.B1_tension_only) { F_el = std::max(float(0), F_el); }
    }
    else if (nodeB_shared_ptr->isN2())
    {

        L0_spring = param.l2rest();

        F_el = param.k2 * (distanceNodesAB - param.l2rest());  // <-------------
        //if(distanceNodesAB > 1) { F_c = 5; }
        //else { nodeB_shared_ptr->setAliveToFalse("B2WasFullyContracted"); }

        if (param.B2_tension_only) { F_el = std::max(float(0), F_el); }
    }
    /*
    if (distanceNodesAB < L0_spring)
    {
        std::string node_order = "error";
        std::string node_state = "not_defined";
        if (nodeB_shared_ptr->isN1())
        {
            node_order = "N1";
            if (nodeB_shared_ptr->isImmature()) { node_state = "immature"; }
            else if (nodeB_shared_ptr->isIntermediate()) { node_state = "intermdediate"; }
            else if (nodeB_shared_ptr->isMature()) { node_state = "mature"; }
        }
        else if (nodeB_shared_ptr->isN2())
        {
            node_order = "N2";
        }

        nodeB_shared_ptr->setAliveToFalse("slack_interaction");
        CPM.nodeLifeEventsValuesToString(param, *nodeB_shared_ptr, "death", "N1");
    }/**/

    F = F_el + F_c;

    // set color
    if (F<=0)
    {
        color.setColor(0, 0, 120, 120);
    }
    age = age + dt;
    return F;
}
void SmartInteraction::applyForceToNodePair(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, bool lengthen, bool interCell)
{
    if (isAlive())
    {
        float magnitude = forceMagnitude(param, nodeTransitionRecords, CPM, dt, lengthen, interCell);

        Vector2D direction = getVector_AB();
        direction.normalize();
        direction.mult(magnitude);
        Vector2D force = direction;
        // applying force to node A
        nodeA_shared_ptr->applyForce(force);
        // applying force to node B
        force.mult(-1); // we inverse the force
        nodeB_shared_ptr->applyForce(force);
    }
}

std::string SmartInteraction::getConnectionType()
{
    return connectionType;
}
bool SmartInteraction::isAlive() {
    if (!is_alive || !nodeA_shared_ptr->isAlive() || !nodeB_shared_ptr->isAlive()) {
        is_alive = false;
        if(nodeB_shared_ptr->isAlive())
            if (!nodeA_shared_ptr->isAlive())
            {
                nodeB_shared_ptr->setAliveToFalse("dead parent");// "dead_parent_node:" + nodeA_shared_ptr->causeOfDeath);
            }
            else { nodeB_shared_ptr->setAliveToFalse("dead_parent_branch"); }
    }
    return is_alive;
}

void SmartInteraction::setAliveToFalse()
{
    is_alive = false;
}

bool SmartInteraction::isNodeOf(int& nodeRefNb)
{
    if(nodeRefNb== getRefNbNodeA() || nodeRefNb == getRefNbNodeB()){return true;}
    else{return false;}
}
