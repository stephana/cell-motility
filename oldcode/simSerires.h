#pragma once

#include <set>
#include <vector>
#include <memory>
#include <iostream>
#include <string>

#include "properties.h"
#include "SmartNode.h"
#include "SmartInteraction.h"
#include "ComputationManager.h"
#include "mainwindow.h"



inline void simseriesA(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{    
    // This simseries is intended to be used to determine the cause of death. Single cell 72h on homogeneous substrate.

    std::cout << "Running sumlation series : " << "simuSeriesA" << "\n..." << std::endl;

    if (i == 1) {
        param.duration = 72 * 60 * 60;
        param.nbOfSimulations = 1;
        param.displaySimulation = true;
        simLabel.label = "PI/6";
        param.simlabel = simLabel.label;
        param.simdetailed_label = "Visualisation mode";
        param.recordInfoTimeIntervals = false;
        param.saveResultTimeIntervals = 120 * param.dt;
        param.saveInfoTimeIntervals = 120 * param.dt;
        
        param.gamma_max__ = 40;

        param.displayLegend = false;

        param.epsilon_m = 0.5;// 0.2;

        param.tau_m = 35 * 60; // <-------
    }
    param.simulationRefNb = i;
}

inline void simseriesB(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simSeries is used to draw the cell (visual) and record forces on nodes and in branches
    std::cout << "Running sumlation series : " << "simuSeries0" << "\n..." << std::endl;

    if (i == 1) {
        param.duration = 6 * 60 * 60;
        param.nbOfSimulations = 1;
        param.displaySimulation = false;
        simLabel.label = "PI/6";
        param.simlabel = simLabel.label;
        param.simdetailed_label = "Visualisation mode";
        param.recordInfoTimeIntervals = true;
        param.saveResultTimeIntervals = 0.1 * param.dt;
        param.saveInfoTimeIntervals = 0.1 * param.dt;

        param.gamma_max__ = 40;

        param.displayLegend = false;
    }
    param.simulationRefNb = i;
}

inline void simseriesC(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simSeries is used to draw migration patterns with a homogeneous substrate with a given F_R1 value
    std::cout << "Running sumlation series : " << "simuSeriesC" << "\n..." << std::endl;

    if (i == 1) {
        param.duration = 72 * 60 * 60;
        param.nbOfSimulations = 50;
        param.displaySimulation = false;
        simLabel.label = "PI/6";
        param.simlabel = simLabel.label;
        param.simdetailed_label = "Visualisation mode";
        param.recordInfoTimeIntervals = false;
        param.saveResultTimeIntervals = 120 * param.dt;
        param.saveInfoTimeIntervals = 120 * param.dt;

        param.gamma_max__ = 40;
        param.F_R1 = 34;
        param.tau_m = 360 * 60;
        param.displayLegend = false;
    }
    param.simulationRefNb = i;
}

inline void simseriesD(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simu series is intended to be used to recreate the Bell curve by changing F_R1.
    std::cout << "Running sumlation series : " << "simuSeriesD: recreate the Bell curve" << "\n..." << std::endl;
    int nb_simulation_per_condition = 25;
    defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 10;
    int nb_of_parameters = 1;

    defaultparam.B1_tension_only = false;

    defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 4 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    param = defaultparam;
    param.recordInfoTimeIntervals = false;

    param.tau_m = 35 * 60;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    std::string nameVariable = "F_R1";

    DomainRange dr = param.getValueRangeOf(nameVariable);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(nameVariable, newvalue);



    std::cout << " ---> " << nameVariable << "= " << param.getValueOf(nameVariable) << std::endl;

    param.simdetailed_label = nameVariable + "=" + std::to_string(param.getValueOf(nameVariable));
    param.simlabel_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_y = std::to_string(param.getValueOf(nameVariable));
    simLabel.label = nameVariable;
    param.simlabel = simLabel.label;
    param.simulationRefNb = i;
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseriesE(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simseries is intended to be used to recreate single cell migration bias on triangular migration patterns.

    std::cout << "Running sumlation series : " << "simuSeries5" << "\n..." << std::endl;
    int nb_simulation_per_condition = 25;
    //defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 9;
    int nb_of_parameters = 1;

    param.saveResultTimeIntervals = 500 * param.dt;
    param.saveInfoTimeIntervals = 500 * param.dt;

    defaultparam.B1_tension_only = false;

    defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 72 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    param = defaultparam;
    param.recordInfoTimeIntervals = false;
    //
    param.epsilon_m = 0.2;
    param.pattern_spacing = 0;
    param.pattern_height = 11;
    param.pattern_width = 14;
    param.pattern_nb_motifs = 30;
    param.pattern_type = triangle_pattern;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    std::string nameVariable = "pattern_spacing";

    float Surface = 11 * 14;

    DomainRange dr = param.getValueRangeOf(nameVariable);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(nameVariable, newvalue);

    //param.pattern_width = Surface / newvalue;

    std::cout << " ---> " << nameVariable << "= " << param.getValueOf(nameVariable) << std::endl;

    param.simdetailed_label = nameVariable + "=" + std::to_string(param.getValueOf(nameVariable));
    param.simlabel_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_y = std::to_string(param.getValueOf(nameVariable));
    simLabel.label = nameVariable;
    param.simlabel = simLabel.label;
    param.simulationRefNb = i;
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseriesF(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simu series is intended to be used to recreate the Bell curve by changing several parameters F_R1, F_R2 and F_th by changing the adhesion strength of the smartSubstrate.
    std::cout << "Running sumlation series : " << "simuSeriesD: recreate the Bell curve" << "\n..." << std::endl;
    int nb_simulation_per_condition = 10;
    defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 15;
    int nb_of_parameters = 1;

    defaultparam.B1_tension_only = false;

    defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 12 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    param = defaultparam;
    param.recordInfoTimeIntervals = false;

    param.adhereEveryWhere = true;

    param.tau_m = 100 * 60* 60;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    float adhesion_strength_coef_min = 0.25;
    float adhesion_strength_coef_max = 2.0;

    //float adhesion_strength_coef = adhesion_strength_coef_min + float(param.labelRefNb-1) * (adhesion_strength_coef_max - adhesion_strength_coef_min) / float(nb_per_condition_per_parameter-1);

    param.adhesion_coeficient_FR012 = adhesion_strength_coef_min + float(param.labelRefNb) * 1.5 / 12.0;

    param.F_R0 = 30.0 * param.adhesion_coeficient_FR012;
    param.F_R1 = 34.0 * param.adhesion_coeficient_FR012;
    param.F_R2 = 17.0 * param.adhesion_coeficient_FR012;

    std::cout << " ---> " << "adhesion_strength_coef" << "= " << param.adhesion_coeficient_FR012 << std::endl;

    param.simdetailed_label = "adhesion_strength_coef" + std::string("= ") + std::to_string(param.adhesion_coeficient_FR012);
    param.simlabel_x = "adhesion_strength_coef";
    simLabel.label_x = "adhesion_strength_coef";
    simLabel.label_y = "adhesion_strength_coef";
    simLabel.label = "adhesion_strength_coef";
    param.simlabel = simLabel.label;
    param.simulationRefNb = i;
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseriesG(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simSeries is used to draw migration patterns with a homogeneous substrate with a given F_R012 value
    std::cout << "Running sumlation series : " << "simuSeriesC" << "\n..." << std::endl;

    if (i == 1) {
        param.duration = 72 * 60 * 60;
        param.nbOfSimulations = 50;
        param.displaySimulation = false;
        simLabel.label = "PI/6";
        param.simlabel = simLabel.label;
        param.simdetailed_label = "Visualisation mode";
        param.recordInfoTimeIntervals = false;
        param.saveResultTimeIntervals = 120 * param.dt;
        param.saveInfoTimeIntervals = 120 * param.dt;

        param.gamma_max__ = 40;
        param.adhesion_coeficient_FR012 = 1.5;
        param.F_R0 = 30 * param.adhesion_coeficient_FR012;
        param.F_R1 = 34 * param.adhesion_coeficient_FR012;
        param.F_R2 = 17 * param.adhesion_coeficient_FR012;
        param.tau_m = 35 * 60;
        param.displayLegend = false;
        param.adhereEveryWhere = true;
    }
    param.simulationRefNb = i;
}

inline void simseriesH(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simseries is intended to be used to recreate single cell migration bias on triangular migration patterns.

    std::cout << "Running sumlation series : " << "simuSeries5" << "\n..." << std::endl;
    int nb_simulation_per_condition = 25;
    //defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 6; //9
    int nb_of_parameters = 1;

    param.saveResultTimeIntervals = 500 * param.dt;
    param.saveInfoTimeIntervals = 500 * param.dt;

    defaultparam.B1_tension_only = false;

    defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 72 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    param = defaultparam;
    param.recordInfoTimeIntervals = false;
    //
    param.epsilon_m = 0.2;
    param.pattern_spacing = 0;
    param.pattern_height = 11;
    param.pattern_width = 14;
    param.pattern_type = triangle_pattern;
    param.pattern_nb_motifs = 51;


    //param.pattern_radius;
    param.adhereEveryWhere = false;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    std::string nameVariable = "pattern_spacing";

    float Surface = 11 * 14;

    DomainRange dr = param.getValueRangeOf(nameVariable);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(nameVariable, newvalue);

    //param.pattern_width = Surface / newvalue;

    // making a large rectangle pattern
    if (param.labelRefNb == 0)
    {
        param.pattern_height = 11;
        param.pattern_width = 300;
        param.pattern_type = rectangle_pattern;
        param.pattern_nb_motifs = 1;
    }
    else if (param.labelRefNb == 1)
    {
        param.pattern_nb_motifs = 153;
    }


    std::cout << " ---> " << nameVariable << "= " << param.getValueOf(nameVariable) << std::endl;

    param.simdetailed_label = nameVariable + "=" + std::to_string(param.getValueOf(nameVariable));
    param.simlabel_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_y = std::to_string(param.getValueOf(nameVariable));
    simLabel.label = nameVariable;
    param.simlabel = simLabel.label;
    param.simulationRefNb = i;
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseriesI(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    // This simseries is intended to be used to recreate single cell migration bias on triangular migration patterns with random B1 branch length.

    std::cout << "Running sumlation series : " << "simuSeriesI" << "\n..." << std::endl;
    int nb_simulation_per_condition = 10;
    //defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 6; //9
    int nb_of_parameters = 1;

    param.saveResultTimeIntervals = 500 * param.dt;
    param.saveInfoTimeIntervals = 500 * param.dt;

    defaultparam.B1_tension_only = false;

    defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 72 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    param = defaultparam;
    param.recordInfoTimeIntervals = false;
    //
    param.epsilon_m = 0.2;
    param.pattern_spacing = 0;
    param.pattern_height = 11;
    param.pattern_width = 14;
    param.pattern_type = triangle_pattern;
    param.pattern_nb_motifs = 51;
    param.l1_Generator = uniforme_B1_length_generator;


    //param.pattern_radius;
    param.adhereEveryWhere = false;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    std::string nameVariable = "pattern_spacing";

    float Surface = 11 * 14;

    DomainRange dr = param.getValueRangeOf(nameVariable);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(nameVariable, newvalue);

    //param.pattern_width = Surface / newvalue;

    // making a large rectangle pattern
    if (param.labelRefNb == 0)
    {
        param.pattern_height = 11;
        param.pattern_width = 300;
        param.pattern_type = rectangle_pattern;
        param.pattern_nb_motifs = 1;
    }
    else if (param.labelRefNb == 1)
    {
        param.pattern_nb_motifs = 153;
    }


    std::cout << " ---> " << nameVariable << "= " << param.getValueOf(nameVariable) << std::endl;

    param.simdetailed_label = nameVariable + "=" + std::to_string(param.getValueOf(nameVariable));
    param.simlabel_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_y = std::to_string(param.getValueOf(nameVariable));
    simLabel.label = nameVariable;
    param.simlabel = simLabel.label;
    param.simulationRefNb = i;
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

/* -------------------------------------------------------------------------------------------------------------- */

inline void simseries1(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    std::cout << "Running sumlation series : " << "simuSeries1" << "\n..." << std::endl;

    int nb_simulation_per_condition = 5;
    int nb_per_condition_per_parameter = 3;
    int nb_of_parameters = (sizeof(param.paramlist) / sizeof(*param.paramlist));
    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 2 * 60 * 60;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.displaySimulation = false;
    simLabel.label_x = "label_x_not_defined";
    simLabel.label_y = "label_y_not_defined";
    simLabel.label = "Visualisation mode";
    param.simlabel = simLabel.label;
    param.simdetailed_label = "Visualisation mode";
    param.simulationRefNb = i;
    bool random_spaced_interval = true;

    if (random_spaced_interval)
    {
        int i_para = (i - 1) / (nb_per_condition_per_parameter * nb_simulation_per_condition);
        int j = i_para;
        param = defaultparam;
        DomainRange dr = param.getValueRangeOf(param.paramlist[j]);
        float newvalue = randomFloat(dr.min, dr.max);
        param.setValueOf(param.paramlist[j], newvalue);
        simLabel.label_x = param.paramlist[j];
        simLabel.label = param.paramlist[j];
        simLabel.labelRefNb = j;
    }
    else
    {
        int i_para = (i - 1) / (nb_per_condition_per_parameter * nb_simulation_per_condition);
        int i_cond = (i - 1 - (i_para * nb_per_condition_per_parameter * nb_simulation_per_condition)) / nb_simulation_per_condition;
        int i_simurep = (i - 1 - (i_para * nb_per_condition_per_parameter * nb_simulation_per_condition)) % nb_simulation_per_condition;

        int j = i_para;
        param = defaultparam;
        DomainRange dr = param.getValueRangeOf(param.paramlist[j]);

        float newvalue = dr.min + float(i_cond) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
        param.setValueOf(param.paramlist[j], newvalue);
        simLabel.label_x = param.paramlist[j];
        simLabel.label = param.paramlist[j];
        simLabel.labelRefNb = j;
    }
}

inline void simseries2(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    int nb_simulation_per_condition = 25;
    int nb_per_condition_per_parameter = 20;
    int nb_of_parameters = 1;

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 8 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;
    param = defaultparam;


    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    simLabel.label_y = std::to_string(param.F_R1);
    simLabel.label = "F_R1";
    param.simlabel = simLabel.label;

    param.simulationRefNb = i;
    bool random_spaced_interval = true;

    DomainRange dr = param.getValueRangeOf(simLabel.label);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(simLabel.label, newvalue);

    simLabel.label_x = std::string(simLabel.label) + "=" + std::to_string(param.F_R1);
    param.simdetailed_label = std::string(simLabel.label) + "=" + std::to_string(param.F_R1);
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;

}

inline void simseries3(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    int nb_simulation_per_condition = 200;
    int nb_per_condition_per_parameter = 50;
    int nb_of_parameters = 1;

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 4 * 60 * 60;
    defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;
    param = defaultparam;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    simLabel.label_y = std::to_string(param.F_R1);
    simLabel.label = "F_R1";
    param.simlabel = simLabel.label;

    param.simulationRefNb = i;
    bool random_spaced_interval = true;

    DomainRange dr = param.getValueRangeOf(simLabel.label);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(simLabel.label, newvalue);

    simLabel.label_x = std::string(simLabel.label) + "=" + std::to_string(param.F_R1);
    param.simdetailed_label = std::string(simLabel.label) + "=" + std::to_string(param.F_R1);
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseries4(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    std::cout << "Running sumlation series : " << "simuSeries4" << "\n..." << std::endl;

    int nb_simulation_per_condition = 25;
    defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 20;
    int nb_of_parameters = 1;

    defaultparam.B1_tension_only = false;

    //defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 48 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    defaultparam.tau_m = 9 * 60 * 60;

    param = defaultparam;

    param.adhereEveryWhere = true;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    simLabel.label_y = std::to_string(param.F_R1);
    simLabel.label = "F_R1";
    param.simlabel = simLabel.label;

    param.simulationRefNb = i;

    DomainRange dr = param.getValueRangeOf(simLabel.label);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    //param.setValueOf("F_R0", newvalue);
    param.setValueOf(simLabel.label, newvalue);
    //param.setValueOf("F_R2", newvalue / 2.0);

    simLabel.label_x = std::string(simLabel.label) + "=" + std::to_string(param.F_R1);
    param.simdetailed_label = std::string(simLabel.label) + "=" + std::to_string(param.F_R1);
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseries5(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{
    std::cout << "Running sumlation series : " << "simuSeries5" << "\n..." << std::endl;
    int nb_simulation_per_condition = 25;
    defaultparam.saveInfoTimeIntervals = 2 * defaultparam.duration;
    int nb_per_condition_per_parameter = 11;
    int nb_of_parameters = 1;

    defaultparam.B1_tension_only = false;

    defaultparam.gamma_max__ = 40; // <------------------------

    // Here we can change all the default parameters so that they may apply for all computations
    defaultparam.duration = 72 * 60 * 60;
    defaultparam.displaySimulation = false;

    defaultparam.labelRefNb = param.labelRefNb;
    defaultparam.nbOfSimulations = nb_per_condition_per_parameter * nb_of_parameters * nb_simulation_per_condition;
    defaultparam.numberSimulationsWithThisParameters = param.numberSimulationsWithThisParameters;

    //defaultparam.tau_m = 9 * 60 * 60;

    param = defaultparam;
    param.recordInfoTimeIntervals = false;

    //
    param.epsilon_m = 0.2;
    param.pattern_spacing = 0;
    param.pattern_height = 11;
    param.pattern_width = 14;
    //param.pattern_radius;
    param.adhereEveryWhere = false;

    if (param.numberSimulationsWithThisParameters < nb_simulation_per_condition - 1 && i > 0)
    {
        param.numberSimulationsWithThisParameters += 1;
    }
    else
    {
        param.labelRefNb += 1;
        param.numberSimulationsWithThisParameters = 0;
    }

    param.tau_m = 360 * 60;
    std::string nameVariable = "pattern_height";

    float Surface = 11 * 14;

    DomainRange dr = param.getValueRangeOf(nameVariable);

    float newvalue = dr.min + float(param.labelRefNb) * (dr.max - dr.min) / float(nb_per_condition_per_parameter - 1);
    param.setValueOf(nameVariable, newvalue);

    param.pattern_width = Surface/ newvalue;


    std::cout << " ---> "<< nameVariable <<"= " << param.getValueOf(nameVariable) << std::endl;

    param.simdetailed_label = nameVariable + "=" + std::to_string(param.getValueOf(nameVariable));
    param.simlabel_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_x = std::to_string(param.getValueOf(nameVariable));
    simLabel.label_y = std::to_string(param.getValueOf(nameVariable));
    simLabel.label = nameVariable;
    param.simlabel = simLabel.label;
    param.simulationRefNb = i;
    simLabel.labelRefNb = param.labelRefNb;
    std::cout << "labelRefNb = " << param.labelRefNb << std::endl;
}

inline void simseries6(ParametersObj& param, ParametersObj& defaultparam, SimulationLabel& simLabel, int i)
{        // Here we can change all the default parameters so that they may apply for all computations

    std::cout << "Running sumlation series : " << "simuSeries6 durotaxis" << "\n..." << std::endl;

    if (i == 1) {
        param.duration = 72 * 60 * 60;
        param.nbOfSimulations = 1;
        param.displaySimulation = true;
        simLabel.label = "PI/6";
        param.simlabel = simLabel.label;
        param.simdetailed_label = "Free adhesion";
        param.recordInfoTimeIntervals = false;
        param.saveResultTimeIntervals = 600;// 0.5 * param.dt;
        param.saveInfoTimeIntervals = 600; // 0.5 * param.dt; // <-----------------

        param.gamma_max__ = 40; //60;

        param.displayLegend = false;
        param.adhereEveryWhere = true;

        //param.epsilon_m = 0.2;
        //param.pattern_spacing = 2;
        //param.pattern_height = 11;
        //param.pattern_width = 14;
    }
    param.simulationRefNb = i;
}