#ifndef PREDEFINEDSIMULATIONS_H
#define PREDEFINEDSIMULATIONS_H

#include <set>
#include <vector>
#include <memory>
#include <iostream>
#include <string>

#include "properties.h"
#include "SmartNode.h"
#include "SmartInteraction.h"
#include "ComputationManager.h"
#include "mainwindow.h"
#include "simSerires.h"

SimulationLabel changeParameters(ParametersObj& param, ParametersObj& defaultparam, int i)
{
    SimulationLabel simLabel;
    simseriesA(param, defaultparam, simLabel, i);
    return simLabel;
}
void multipleSimulationsManager(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, MainWindow& winQt)
{
    ParametersObj defaultparam = param; // default parameter is used to reset parametric conditions between all versions.
    time_t tstart, tend, tstart_thiscomputation;

    //bool display = false;
    ComputationManager CPM = ComputationManager(param);
    CPM.setWindowPtr(window_ptr);

    //displayMessageDuringStandby(" COMPUTATION RUNNING ");

    tstart = time(0);
    tstart_thiscomputation = time(0);
    double percentDone = 0;
    std::string computationMsg = "";

    int simNb = 1; // initialization
    for (int i = 1; i <= param.nbOfSimulations; i++) // nb of cases k_central variations
    {
        SimulationLabel simLabel = changeParameters(param, defaultparam, i);
        winQt.set_all_gui_values();
        CPM.updateParameters(param);
        tstart_thiscomputation = time(0);
        sf::Event event;
        while (window_ptr->pollEvent(event))
        {
            if (event.type == sf::Event::Closed) { window_ptr->close(); }
        }
        CPM.launchCellModel(param, simLabel);
        CPM.displayMultiTaskProgress(param, tstart, tstart_thiscomputation, simNb);
        //displayMessageDuringStandby(computationMsg);
        simNb++;
        CPM.saveResults(); // Calculation sample terminated (under same condition) -> Saving results to csv file -> keep going with new condition
        CPM.saveParameters();
        CPM.saveNodeLifeEventFile();
        CPM.saveNodeInfo();
        CPM.saveBranchFile();
        CPM.savePatternFile();
        CPM.saveCellSnapShotFile();
    }
    //Closing result file (.csv)
    CPM.closeResultFile();
    CPM.closeParamterFile();
    CPM.closeNodeLifeFile();
    CPM.closeNodeInfoFile();
    CPM.closeBranchInfoFile();
    CPM.closePatternFile();
    CPM.closeCellSnapShotFile();
    tend = time(0);
    std::cout << " ALL COMPUTATION TERMINATED after " << difftime(tend, tstart) << " second(s)." << std::endl;
    std::cout << '\a'; // make a beep sound
    // total time of the calculation
    displayMessageDuringStandby(param, CPM.window_ptr, " ALL COMPUTATION TERMINATED ");
}
#endif // PREDEFINEDSIMULATIONS_H

