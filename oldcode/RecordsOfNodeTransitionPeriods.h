#ifndef RECORDSOFNODETRANSITIONPERIODS_H
#define RECORDSOFNODETRANSITIONPERIODS_H
#define _USE_MATH_DEFINES
#include <iostream>
#include <vector>
#include <cmath>

#include "Vector2D.h"
class RecordsOfNodeTransitionPeriods
{
public:
    // Record age and durations
    std::vector<float> list_immatureNodeTransitionPeriod;
    std::vector<float> list_intermediateNodeTransitionPeriod;
    std::vector<float> list_matureNodeTransitionPeriod;
    int nbImmatureN1Deaths = 0;
    int nbIntermediateN1Deaths = 0;
    int nbMatureN1Deaths = 0;
    void addImmatureN1TransitionPeriodToList(float age);
    void addIntermediateN1TransitionPeriodToList(float age);
    void addMatureN1TransitionPeriodToList(float age);
    void addImmatureN1Death(float age);
    void addIntermediateN1Death( float age);
    void addMatureN1Death( float age);
    int nbImmatureN1Transitions();
    int nbIntermediateN1Transitions();
    int nbMatureImmatureN1Transitions();
    float immatureN1TransitionPeriod_minimum();
    float immatureN1TransitionPeriod_max();
    float immatureN1TransitionPeriod_mean();
    float intermediateN1TransitionPeriod_minimum();
    float intermediateN1TransitionPeriod_maximum();
    float intermediateN1TransitionPeriod_mean();
    float matureN1TransitionPeriod_minimum();
    float matureN1TransitionPeriod_maximum();
    float matureN1TransitionPeriod_mean();
    float matureN1TransitionPeriod_std();
    float intermediateN1TransitionPeriod_std();

    // N1 age variables and classes
    std::vector<float> list_N1AgesAtDeath;
    void addN1AgeAtDeathToList(float age);
    float N1AgeAtDeath_mean();
    float N1AgeAtDeath_standardDeviation();
    float N1AgeAtDeath_maximum();

    // N2 age variables and classes
    std::vector<float> list_N2AgesAtDeath;
    void addN2AgeAtDeathToList(float age);
    float N2AgeAtDeath_mean();
    float N2AgeAtDeath_standardDeviation();
    float N2AgeAtDeath_maximum();
};

#endif // RECORDSOFNODETRANSITIONPERIODS_H