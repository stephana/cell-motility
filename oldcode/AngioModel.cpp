// AngioModel.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
#include <QtWidgets/QApplication>
#include <SFML/Graphics.hpp>
#include <memory>
#include <iostream>
#include <stdio.h>

#include "properties.h"
#include "Vector2D.h"
#include "SmartNode.h"
#include "SmartInteraction.h"
#include "SmartCell.h"
#include "predefinedsimulations.h"
#include "mainwindow.h"

//sf::RenderWindow window(sf::VideoMode(ScreenWidth(), ScreenHeight()), "Angio Model V0");
sf::CircleShape circle(20.f);

/**
Coding note: Reference and Dereference operators
    The reference and dereference operators are thus complementary:
        & is the address-of operator, and can be read simply as "address of"
        * is the reference operator, and can be read as "value pointed to by"
**/

int main(int argc, char* argv[])
{
    //call_unitest();

    std::shared_ptr<ParametersObj> param_ptr = std::make_shared<ParametersObj>();
    std::shared_ptr<sf::RenderWindow> window_ptr = std::make_shared<sf::RenderWindow>(sf::VideoMode(param_ptr->ScreenWidth, param_ptr->ScreenHeight), "Angio Model V0");
    QApplication applicationQT(argc, argv);
    std::shared_ptr <MainWindow> winQt_ptr = std::make_shared<MainWindow>(param_ptr, window_ptr);
    winQt_ptr->show();
    multipleSimulationsManager(*param_ptr, window_ptr, *winQt_ptr);
    applicationQT.exec();
    return EXIT_SUCCESS;
};