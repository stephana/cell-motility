#pragma once

// DiffusionUnitest.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include "gtest/gtest.h"
#include "properties.h"


namespace AngioModelUnitest_sp
{
    // The fixture for testing class Foo.
    class AngioModelUnitest : public ::testing::Test
    {
    protected:
        // You can remove any or all of the following functions if its body
        // is empty.
        AngioModelUnitest()
        {
            // You can do set-up work for each test here.
        }
        virtual ~AngioModelUnitest()
        {
            // You can do clean-up work that doesn't throw exceptions here.
        }

        // If the constructor and destructor are not enough for setting up
        // and cleaning up each test, you can define the following methods:

        virtual void SetUp()
        {
            // Code here will be called immediately after the constructor (right
            // before each test).
        }

        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        // Objects declared here can be used by all tests in the test case for Foo.
    };

    // Tests that the Foo::Bar() method does Abc.
    TEST(AngioModelUnitest, reMapValue)
    {
        EXPECT_EQ(predfColorBlack(), predfColorBlack());
    }
}  // namespace

int call_unitest()
{
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}