#ifndef SMARTINTERACTION_H
#define SMARTINTERACTION_H

#include <iostream>

#include <vector>   // std::cout
#include <string>
#include <memory>
#include <algorithm>    // std::max
#include "properties.h"
#include "SmartNode.h"


class SmartInteraction: public GraphicProperties
{
    public:
        SmartInteraction(std::shared_ptr<SmartNode> nodeA_shared_ptr, std::shared_ptr<SmartNode> nodeB_shared_ptr);
        virtual ~SmartInteraction(); // To do : must inform nodes that SmartInteraction is being destroyed;
        void setConnectionType(std::string connection_Type);
        int getRefNbNodeA(); // parent node
        int getRefNbNodeB(); // child node
        Vector2D getVector_AB();
        float distanceBetweenNodes();
        float forceMagnitude(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, bool lengthen = false, bool interCell = false);
        float forceMagnitude_interCell(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM);
        float forceMagnitude_intraCell(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, bool lengthen = false);
        void applyForceToNodePair(ParametersObj& param, RecordsOfNodeTransitionPeriods& nodeTransitionRecords, CPMwrapperObj& CPM, float dt, bool lengthen = false, bool interCell = false);
        std::string getConnectionType();
        bool isAlive();
        void setAliveToFalse();
        bool isNodeOf(int& nodeRefNb);
        void draw(ParametersObj& param, std::shared_ptr<sf::RenderWindow>& window_ptr, int branchIDNb = NULL) {

            if (isAlive())
            {
                Vector2D positionNodeA = nodeA_shared_ptr->getPosition();
                Vector2D positionNodeB = nodeB_shared_ptr->getPosition();

                sf::Vertex line1[] = { sf::Vertex(positionNodeA.getPixelCoord(param)) , sf::Vertex(positionNodeB.getPixelCoord(param)) };
                window_ptr->draw(line1, 2, sf::Lines);
                //Color lineColor = interaction_List[i].getColor();

                if ( connectionType == "intercell" )
                {
                    drawLine(param, positionNodeA, positionNodeB, param.thicknessB1, predfColorGrayMedium(), window_ptr);
                }
                else if (nodeB_shared_ptr->isN1())
                {
                    drawLine(param, positionNodeA, positionNodeB, param.thicknessB1, param.colorB1, window_ptr);
                }
                else if (nodeB_shared_ptr->isN2())
                {
                    drawLine(param, positionNodeA, positionNodeB, param.thicknessB2, param.colorB2, window_ptr);
                }


                if (param.displayBranchID && branchIDNb != NULL)
                {
                    Vector2D position_middle = Vector2D((positionNodeA.x + positionNodeB.x) / 2, (positionNodeA.y + positionNodeB.y) / 2);
                    sf::Text text("B ID: " + std::to_string(branchIDNb), *param.font_ptr, 20);
                    text.setFillColor(sf::Color(0, 255, 0, 255));//(lineColor_SFML);
                    text.setPosition(position_middle.getPixelCoord(param));
                    window_ptr->draw(text);
                }
                else if (param.displayBranchType)
                {
                    Vector2D position_middle = Vector2D((positionNodeA.x + positionNodeB.x) / 2, (positionNodeA.y + positionNodeB.y) / 2);
                    sf::Text text("B", *param.font_ptr, 20);

                    if (nodeB_shared_ptr->isN1()) { text.setString("B1"); }
                    else if (nodeB_shared_ptr->isN2()) { text.setString("B2"); }

                    text.setFillColor(sf::Color(0, 255, 0, 255));//(lineColor_SFML);
                    text.setPosition(position_middle.getPixelCoord(param));
                    window_ptr->draw(text);
                }
            }
        };
        float L0_spring = NULL;
        float L_tau_t;
        float age = 0; // in seconds

        float F = 0;
        float epsilon = 0;
        float F_el = 0;
        float F_c = 0;
        float distanceNodesAB;

        std::shared_ptr<SmartNode> nodeA_shared_ptr;
        std::shared_ptr<SmartNode> nodeB_shared_ptr;
        unsigned int refNbCellA = NULL;
        unsigned int refNbCellB = NULL;
    protected:
    private:
        std::string connectionType = "default";
        
        bool is_alive = true;
};



#endif // SMARTINTERACTION_H
