#include "mainwindow.h"

MainWindow::MainWindow(std::shared_ptr<ParametersObj> param_ptr, std::shared_ptr<sf::RenderWindow> window_ptr, QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , param_ptr(param_ptr)
    , window_ptr(window_ptr)
{
    ui->setupUi(this);
    set_all_gui_values();
    connect(ui->save_simulation_parameters_menubutton, SIGNAL(triggered()), this, SLOT(save_simulation_parameters()) );
    connect(ui->load_simulation_parameters_menubutton, SIGNAL(triggered()), this, SLOT(load_simulation_parameters()));
    connect(ui->duration_lineEdit, SIGNAL(editingFinished()), this, SLOT( duration_lineEdit_editingFinished()));
    connect(ui->dt_lineEdit, SIGNAL(editingFinished()), this, SLOT( dt_lineEdit_editingFinished()));
    connect(ui->nbOfSimulations_lineEdit, SIGNAL(editingFinished()), this, SLOT( nbOfSimulations_lineEdit_editingFinished()));
    connect(ui->displaySimulation_checkBox,SIGNAL(clicked(bool)),this, SLOT(displaySimulation_clicked()));
    connect(ui->recordDetails_checkBox,SIGNAL(clicked(bool)),this, SLOT(recordDetails_clicked()));
    connect(ui->delta_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( delta_theta_1_slider_moved()));
    connect(ui->alpha0_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( alpha0_slider_moved()));
    connect(ui->F_R0_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(F_R0_slider_moved()));
    connect(ui->radiusN0_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(radiusN0_slider_moved()));
    
    connect(ui->P_N1_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( nu1_slider_moved()));
    connect(ui->DN1i_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( tau_int_slider_moved()));
    connect(ui->DN1m_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( tau_m_slider_moved()));
    connect(ui->t_c_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(tau_t_slider_moved()));
    connect(ui->alpha10_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( alpha10_slider_moved()));
    connect(ui->alphasat_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( alpha_int_slider_moved()));
    connect(ui->ruptureForce_N1_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( F_R1_slider_moved()));
    connect(ui->radiusN1_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(radiusN1_slider_moved()));
    connect(ui->theta2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(delta_theta_2_slider_moved()));
    connect(ui->P_N20_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( nu2_im_slider_moved()));
    connect(ui->P_N2i_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( nu2_int_slider_moved()));
    connect(ui->P_N2m_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( nu2_m_slider_moved()));
    connect(ui->DN2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( tau_2_slider_moved()));
    connect(ui->alpha2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( alpha2_slider_moved()));
    connect(ui->ruptureForce_N2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( ruptureForce_N2_slider_moved()));
    connect(ui->radiusN2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(radiusN2_slider_moved()));

    connect(ui->B1_tension_only_checkBox,SIGNAL(clicked(bool)),this, SLOT(B1_tension_only_clicked()));
    connect(ui->l1_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( l1i_slider_moved()));
    connect(ui->k10_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( k10_slider_moved()));
    connect(ui->k1_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( k11_slider_moved()));
    connect(ui->gamma1_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( gamma_max_slider_moved()));
    connect(ui->lambda1m_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( epsilon_m_slider_moved()));

    connect(ui->B2_tension_only_checkBox,SIGNAL(clicked(bool)),this, SLOT(B2_tension_only_clicked()));
    connect(ui->l2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( l2i_slider_moved()));
    connect(ui->l2_prestress_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( epsilon20_slider_moved()));
    connect(ui->k2_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( k2_slider_moved()));

    connect(ui->ScreenHeight_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( ScreenHeight_slider_moved()));
    connect(ui->ScreenWidth_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( ScreenWidth_slider_moved()));
    connect(ui->PixelsPerMeter_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( PixelsPerMeter_slider_moved()));
    connect(ui->legend_offsetX_coefficient_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( legend_offsetX_coefficient_slider_moved()));
    connect(ui->legend_offsetY_coefficient_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( legend_offsetY_coefficient_slider_moved()));
    connect(ui->legend_spacingX_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( legend_spacingX_slider_moved()));
    connect(ui->legend_spacingY_horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT( legend_spacingY_slider_moved()));

    connect(ui->displayLegend_checkBox,SIGNAL(clicked(bool)),this, SLOT(displayLegend_clicked()));
    connect(ui->displayNodeType_checkBox,SIGNAL(clicked(bool)),this, SLOT(displayNodeType_clicked()));
    connect(ui->displayNodeID_checkBox,SIGNAL(clicked(bool)),this, SLOT(displayNodeID_clicked()));
    connect(ui->displayNodeMaturationState_checkBox,SIGNAL(clicked(bool)),this, SLOT(displayNodeMaturationState_clicked()));
    connect(ui->displayBranchID_checkBox,SIGNAL(clicked(bool)),this, SLOT(displayBranchID_clicked()));
    connect(ui->displayBranchType_checkBox,SIGNAL(clicked(bool)),this, SLOT(displayBranchType_clicked()));
    connect(ui->displayCytoplasm_checkBox, SIGNAL(clicked(bool)), this, SLOT(displayCytoplasm_clicked()));
    connect(ui->displayInteractions_checkBox, SIGNAL(clicked(bool)), this, SLOT(displayInteractions_clicked()));
    connect(ui->displayNodes_checkBox, SIGNAL(clicked(bool)), this, SLOT(displayNodes_clicked()));
    connect(ui->nextSimu_pushButton, SIGNAL(released()), this, SLOT(refreshPushButton_released()));

    simulationProgressBar_update(0);
    simulationProgressBar_setVisible(false);
}

void MainWindow::set_all_gui_values() {
    int hSld_max;
    int hSld_v;
    // Duration
    ui->duration_lineEdit->setText(QString::number(param_ptr->duration));

    // dt
    ui->dt_lineEdit->setText(QString::number(param_ptr->dt));

    // Number of simulatons
    ui->nbOfSimulations_lineEdit->setText(QString::number(param_ptr->nbOfSimulations));

    // displaySimulation
    ui->displaySimulation_checkBox->setChecked(param_ptr->displaySimulation);

    // recordDetails
    ui->recordDetails_checkBox->setChecked(param_ptr->recordDetails);

    // N0 --------------------------------------------
    // delta_theta_1
    hSld_max = param_ptr->nbIntervals(param_ptr->delta_theta_1_min, param_ptr->delta_theta_1_max, param_ptr->delta_theta_1_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->delta_theta_1_min, param_ptr->delta_theta_1_interval, param_ptr->delta_theta_1);
    ui->delta_horizontalSlider->setMinimum(0);
    ui->delta_horizontalSlider->setMaximum(hSld_max);
    ui->delta_horizontalSlider->setValue(hSld_v);
    ui->delta_value->setText(QString::fromStdString(std::to_string(param_ptr->delta_theta_1 / M_PI) + " Pi"));

    // alpha10
    ui->alpha0_horizontalSlider->setMinimum(param_ptr->alpha0_min);
    ui->alpha0_horizontalSlider->setMaximum(param_ptr->alpha0_max);
    ui->alpha0_horizontalSlider->setValue(param_ptr->alpha0);
    ui->alpha0_value->setNum(param_ptr->alpha0);

    // F_R0
    hSld_max = param_ptr->nbIntervals(param_ptr->F_R0_min, param_ptr->F_R0_max, param_ptr->F_R0_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->F_R0_min, param_ptr->F_R0_interval, param_ptr->F_R0);
    ui->F_R0_horizontalSlider->setMinimum(0);
    ui->F_R0_horizontalSlider->setMaximum(hSld_max);
    ui->F_R0_horizontalSlider->setValue(hSld_v);
    ui->F_R0_value->setNum(param_ptr->F_R0);


    //radiusN0
    hSld_max = param_ptr->nbIntervals(param_ptr->radiusN0_min, param_ptr->radiusN0_max, param_ptr->radiusN0_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->radiusN0_min, param_ptr->radiusN0_interval, param_ptr->radiusN0);
    ui->radiusN0_horizontalSlider->setMinimum(0);
    ui->radiusN0_horizontalSlider->setMaximum(hSld_max);
    ui->radiusN0_horizontalSlider->setValue(hSld_v);
    ui->radiusN0_value->setNum(param_ptr->radiusN0);


    // N1 --------------------------------------------
    // nu1
    hSld_max = param_ptr->nbIntervals(param_ptr->nu1_min, param_ptr->nu1_max, param_ptr->nu1_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->nu1_min, param_ptr->nu1_interval, param_ptr->nu1);
    ui->P_N1_horizontalSlider->setMinimum(0);
    ui->P_N1_horizontalSlider->setMaximum(hSld_max);
    ui->P_N1_horizontalSlider->setValue(hSld_v);
    ui->P_N1_value->setNum(param_ptr->nu1);

    // tau_int
    ui->DN1i_horizontalSlider->setMinimum(param_ptr->tau_int_min());
    ui->DN1i_horizontalSlider->setMaximum(param_ptr->tau_int_max);
    ui->DN1i_horizontalSlider->setValue(param_ptr->tau_int);
    ui->DN1i_value->setNum(param_ptr->tau_int);

    // tau_m
    ui->DN1m_horizontalSlider->setMinimum(param_ptr->tau_m_min());
    ui->DN1m_horizontalSlider->setMaximum(param_ptr->tau_m_max);
    ui->DN1m_horizontalSlider->setValue(param_ptr->tau_m);
    ui->DN1m_value->setNum(param_ptr->tau_m);

    // tau_t
    ui->t_c_horizontalSlider->setMinimum(param_ptr->tau_t_min);
    ui->t_c_horizontalSlider->setMaximum(param_ptr->tau_t_max);
    ui->t_c_horizontalSlider->setValue(param_ptr->tau_t);
    ui->t_c_value->setNum(param_ptr->tau_t);

    // alpha10
    ui->alpha10_horizontalSlider->setMinimum(param_ptr->alpha10_min);
    ui->alpha10_horizontalSlider->setMaximum(param_ptr->alpha10_max);
    ui->alpha10_horizontalSlider->setValue(param_ptr->alpha10);
    ui->alpha10_value->setNum(param_ptr->alpha10);

    // alpha_int
    ui->alphasat_horizontalSlider->setMinimum(param_ptr->alpha_int_min());
    ui->alphasat_horizontalSlider->setMaximum(param_ptr->alpha_int_max);
    ui->alphasat_horizontalSlider->setValue(param_ptr->alpha_int);
    ui->alphasat_value->setNum(param_ptr->alpha_int);

    //alpha1_m
    ui->alpha1m_horizontalSlider->setMinimum(param_ptr->alpha1_m_min());
    ui->alpha1m_horizontalSlider->setMaximum(param_ptr->alpha1_m_max());
    ui->alpha1m_horizontalSlider->setValue(param_ptr->alpha1_m());
    ui->alpha1m_value->setNum(param_ptr->alpha1_m());

    // F_R1
    ui->ruptureForce_N1_horizontalSlider->setMinimum(param_ptr->F_R1_min);
    ui->ruptureForce_N1_horizontalSlider->setMaximum(param_ptr->F_R1_max);
    ui->ruptureForce_N1_horizontalSlider->setValue(param_ptr->F_R1);
    ui->ruptureForce_N1_value->setNum(param_ptr->F_R1);

    //radiusN1
    hSld_max = param_ptr->nbIntervals(param_ptr->radiusN1_min, param_ptr->radiusN1_max, param_ptr->radiusN1_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->radiusN1_min, param_ptr->radiusN1_interval, param_ptr->radiusN1);
    ui->radiusN1_horizontalSlider->setMinimum(0);
    ui->radiusN1_horizontalSlider->setMaximum(hSld_max);
    ui->radiusN1_horizontalSlider->setValue(hSld_v);
    ui->radiusN1_value->setNum(param_ptr->radiusN1);

    // N2 --------------------------------------------
    // delta_theta_2
    hSld_max = param_ptr->nbIntervals(param_ptr->delta_theta2_min, param_ptr->delta_theta2_max, param_ptr->delta_theta2_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->delta_theta2_min, param_ptr->delta_theta2_interval, param_ptr->delta_theta_2);
    ui->theta2_horizontalSlider->setMinimum(0);
    ui->theta2_horizontalSlider->setMaximum(hSld_max);
    ui->theta2_horizontalSlider->setValue(hSld_v);
    ui->theta2_value->setNum(param_ptr->delta_theta_2);

    // nu2_im
    hSld_max = param_ptr->nbIntervals(param_ptr->nu2_im_min, param_ptr->nu2_im_max, param_ptr->nu2_im_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->nu2_im_min, param_ptr->nu2_im_interval, param_ptr->nu2_im);
    ui->P_N20_horizontalSlider->setMinimum(0);
    ui->P_N20_horizontalSlider->setMaximum(hSld_max);
    ui->P_N20_horizontalSlider->setValue(hSld_v);
    ui->P_N20_value->setNum(param_ptr->nu2_im);

    // nu2_int
    hSld_max = param_ptr->nbIntervals(param_ptr->nu2_int_min, param_ptr->nu2_int_max, param_ptr->nu2_int_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->nu2_int_min, param_ptr->nu2_int_interval, param_ptr->nu2_int);
    ui->P_N2i_horizontalSlider->setMinimum(0);
    ui->P_N2i_horizontalSlider->setMaximum(hSld_max);
    ui->P_N2i_horizontalSlider->setValue(hSld_v);
    ui->P_N2i_value->setNum(param_ptr->nu2_int);

    // nu2_m
    hSld_max = param_ptr->nbIntervals(param_ptr->nu2_m_min, param_ptr->nu2_m_max, param_ptr->nu2_m_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->nu2_m_min, param_ptr->nu2_m_interval, param_ptr->nu2_m);
    ui->P_N2m_horizontalSlider->setMinimum(0);
    ui->P_N2m_horizontalSlider->setMaximum(hSld_max);
    ui->P_N2m_horizontalSlider->setValue(hSld_v);
    ui->P_N2m_value->setNum(param_ptr->nu2_m);

    // tau2
    ui->DN2_horizontalSlider->setMinimum(param_ptr->tau2_min);
    ui->DN2_horizontalSlider->setMaximum(param_ptr->tau2_max);
    ui->DN2_horizontalSlider->setValue(param_ptr->tau2);
    ui->DN2_value->setNum(param_ptr->tau2);

    // alpha2
    ui->alpha2_horizontalSlider->setMinimum(param_ptr->alpha2_min);
    ui->alpha2_horizontalSlider->setMaximum(param_ptr->alpha2_max());
    ui->alpha2_horizontalSlider->setValue(param_ptr->alpha2);
    ui->alpha2_value->setNum(param_ptr->alpha2);
#if !NEW_CHANGES
    ui->alpha2_horizontalSlider->setDisabled(true);
#endif

    // F_R2
    ui->ruptureForce_N2_horizontalSlider->setMinimum(param_ptr->F_R2_min);
    ui->ruptureForce_N2_horizontalSlider->setMaximum(param_ptr->F_R2_max());
    ui->ruptureForce_N2_horizontalSlider->setValue(param_ptr->F_R2);
    ui->ruptureForce_N2_value->setNum(param_ptr->F_R2);

    //radiusN2
    hSld_max = param_ptr->nbIntervals(param_ptr->radiusN2_min, param_ptr->radiusN2_max, param_ptr->radiusN2_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->radiusN2_min, param_ptr->radiusN2_interval, param_ptr->radiusN2);
    ui->radiusN2_horizontalSlider->setMinimum(0);
    ui->radiusN2_horizontalSlider->setMaximum(hSld_max);
    ui->radiusN2_horizontalSlider->setValue(hSld_v);
    ui->radiusN2_value->setNum(param_ptr->radiusN2);

    //B1------------------------------------------------
    // B1_tension_only
    ui->B1_tension_only_checkBox->setChecked(param_ptr->B1_tension_only);

    // l1initial
    ui->l1_horizontalSlider->setMinimum(param_ptr->l1initial_min);
    ui->l1_horizontalSlider->setMaximum(param_ptr->l1initial_max);
    ui->l1_horizontalSlider->setValue(param_ptr->l1initial);
    ui->l1_value->setNum(param_ptr->l1initial);

    // k10
     // legend_offsetY_coefficient
    hSld_max = param_ptr->nbIntervals(param_ptr->k10_min, param_ptr->k10_max, param_ptr->k10_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->k10_min, param_ptr->k10_interval, param_ptr->k10);
    ui->k10_horizontalSlider->setMinimum(0);
    ui->k10_horizontalSlider->setMaximum(hSld_max);
    ui->k10_horizontalSlider->setValue(hSld_v);
    ui->k10_value->setNum(param_ptr->k10);

    // k11
    hSld_max = param_ptr->nbIntervals(param_ptr->k1_min, param_ptr->k1_max, param_ptr->k1_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->k1_min, param_ptr->k1_interval, param_ptr->k11);
    ui->k1_horizontalSlider->setMinimum(0);
    ui->k1_horizontalSlider->setMaximum(hSld_max);
    ui->k1_horizontalSlider->setValue(hSld_v);
    ui->k1_value->setNum(param_ptr->k11);

    // gamma_max__
    hSld_max = param_ptr->nbIntervals(param_ptr->gamma_max__min, param_ptr->gamma_max__max, param_ptr->gamma_max__interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->gamma_max__min, param_ptr->gamma_max__interval, param_ptr->gamma_max__);
    ui->gamma1_horizontalSlider->setMinimum(0);
    ui->gamma1_horizontalSlider->setMaximum(hSld_max);
    ui->gamma1_horizontalSlider->setValue(hSld_v);
    ui->gamma1_value->setNum(param_ptr->gamma_max__);


    // B1_stretch_maturation_theshold
    hSld_max = param_ptr->nbIntervals(param_ptr->epsilon_m_min, param_ptr->epsilon_m_max, param_ptr->epsilon_m_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->epsilon_m_min, param_ptr->epsilon_m_interval, param_ptr->epsilon_m);
    ui->lambda1m_horizontalSlider->setMinimum(0);
    ui->lambda1m_horizontalSlider->setMaximum(hSld_max);
    ui->lambda1m_horizontalSlider->setValue(hSld_v);
    ui->lambda1m_value->setNum(param_ptr->epsilon_m);


    //B2------------------------------------------------
    // B2_tension_only
    ui->B2_tension_only_checkBox->setChecked(param_ptr->B2_tension_only);

    // l2initial
    ui->l2_horizontalSlider->setMinimum(param_ptr->l2initial_min);
    ui->l2_horizontalSlider->setMaximum(param_ptr->l2initial_max);
    ui->l2_horizontalSlider->setValue(param_ptr->l2initial);
    ui->l2_value->setNum(param_ptr->l2initial);

    // epsilon20
    hSld_max = param_ptr->nbIntervals(param_ptr->epsilon20_min, param_ptr->epsilon20_max, param_ptr->epsilon20_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->epsilon20_min, param_ptr->epsilon20_interval, param_ptr->epsilon20);
    ui->l2_prestress_horizontalSlider->setMinimum(0);
    ui->l2_prestress_horizontalSlider->setMaximum(hSld_max);
    ui->l2_prestress_horizontalSlider->setValue(hSld_v);
    ui->l2_prestress_value->setNum(param_ptr->epsilon20);

    // k2
    ui->k2_horizontalSlider->setMinimum(param_ptr->k2_min);
    ui->k2_horizontalSlider->setMaximum(param_ptr->k2_max);
    ui->k2_horizontalSlider->setValue(param_ptr->k2);
    ui->k2_value->setNum(param_ptr->k2);

    // ScreenHeight
    ui->ScreenHeight_horizontalSlider->setMinimum(param_ptr->ScreenHeight_min);
    ui->ScreenHeight_horizontalSlider->setMaximum(param_ptr->ScreenHeight_max);
    ui->ScreenHeight_horizontalSlider->setValue(param_ptr->ScreenHeight);
    ui->ScreenHeight_value->setNum(param_ptr->ScreenHeight);

    // ScreenWidth
    ui->ScreenWidth_horizontalSlider->setMinimum(param_ptr->ScreenWidth_min);
    ui->ScreenWidth_horizontalSlider->setMaximum(param_ptr->ScreenWidth_max);
    ui->ScreenWidth_horizontalSlider->setValue(param_ptr->ScreenWidth);
    ui->ScreenWidth_value->setNum(param_ptr->ScreenWidth);

    // PixelsPerMeter
    ui->PixelsPerMeter_horizontalSlider->setMinimum(param_ptr->PixelsPerMeter_min);
    ui->PixelsPerMeter_horizontalSlider->setMaximum(param_ptr->PixelsPerMeter_max);
    ui->PixelsPerMeter_horizontalSlider->setValue(param_ptr->PixelsPerMeter);
    ui->PixelsPerMeter_value->setNum(param_ptr->PixelsPerMeter);

    // legend_offsetX_coefficient
    hSld_max = param_ptr->nbIntervals(param_ptr->legend_offsetX_coefficient_min, param_ptr->legend_offsetX_coefficient_max, param_ptr->legend_offsetX_coefficient_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->legend_offsetX_coefficient_min, param_ptr->legend_offsetX_coefficient_interval, param_ptr->legend_offsetX_coefficient);
    ui->legend_offsetX_coefficient_horizontalSlider->setMinimum(0);
    ui->legend_offsetX_coefficient_horizontalSlider->setMaximum(hSld_max);
    ui->legend_offsetX_coefficient_horizontalSlider->setValue(hSld_v);
    ui->legend_offsetX_coefficient_value->setNum(param_ptr->legend_offsetX_coefficient);

    // legend_offsetY_coefficient
    hSld_max = param_ptr->nbIntervals(param_ptr->legend_offsetY_coefficient_min, param_ptr->legend_offsetY_coefficient_max, param_ptr->legend_offsetY_coefficient_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->legend_offsetY_coefficient_min, param_ptr->legend_offsetY_coefficient_interval, param_ptr->legend_offsetY_coefficient);
    ui->legend_offsetY_coefficient_horizontalSlider->setMinimum(0);
    ui->legend_offsetY_coefficient_horizontalSlider->setMaximum(hSld_max);
    ui->legend_offsetY_coefficient_horizontalSlider->setValue(hSld_v);
    ui->legend_offsetY_coefficient_value->setNum(param_ptr->legend_offsetY_coefficient);


    // legend_spacingX
    hSld_max = param_ptr->nbIntervals(param_ptr->legend_spacingX_min, param_ptr->legend_spacingX_max, param_ptr->legend_spacingX_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->legend_spacingX_min, param_ptr->legend_spacingX_interval, param_ptr->legend_spacingX);
    ui->legend_spacingX_horizontalSlider->setMinimum(0);
    ui->legend_spacingX_horizontalSlider->setMaximum(hSld_max);
    ui->legend_spacingX_horizontalSlider->setValue(hSld_v);
    ui->legend_spacingX_value->setNum(param_ptr->legend_spacingX);

    // legend_spacingY
    hSld_max = param_ptr->nbIntervals(param_ptr->legend_spacingY_min, param_ptr->legend_spacingY_max, param_ptr->legend_spacingY_interval);
    hSld_v = param_ptr->intervalIndex(param_ptr->legend_spacingY_min, param_ptr->legend_spacingY_interval, param_ptr->legend_spacingY);
    ui->legend_spacingY_horizontalSlider->setMinimum(0);
    ui->legend_spacingY_horizontalSlider->setMaximum(hSld_max);
    ui->legend_spacingY_horizontalSlider->setValue(hSld_v);
    ui->legend_spacingY_value->setNum(param_ptr->legend_spacingY);

    // displayLegend
    ui->displayLegend_checkBox->setChecked(param_ptr->displayLegend);

    // displayNodeType
    ui->displayNodeType_checkBox->setChecked(param_ptr->displayNodeType);

    // displayNodeID
    ui->displayNodeID_checkBox->setChecked(param_ptr->displayNodeID);

    // displayNodeMaturationState
    ui->displayNodeMaturationState_checkBox->setChecked(param_ptr->displayNodeMaturationState);

    // displayBranchID
    ui->displayBranchID_checkBox->setChecked(param_ptr->displayBranchID);

    // displayBranchType
    ui->displayBranchType_checkBox->setChecked(param_ptr->displayBranchType);

    // displayCytoplasm
    ui->displayCytoplasm_checkBox->setChecked(param_ptr->displayCytoplasm);

    // displayInteractions
    ui->displayInteractions_checkBox->setChecked(param_ptr->displayInteractions);

    // displayNodes
    ui->displayNodes_checkBox->setChecked(param_ptr->displayNodes);
}
MainWindow::~MainWindow()
{
    delete ui;
    this->close();
}

// General----------------------
void  MainWindow::save_simulation_parameters()
{
    // This function saves the parameters values to a txt file.
    std::string lineParameters;
    lineParameters += "-----------------------------------------------------\n";
    lineParameters += "duration = " + std::to_string(param_ptr->duration) + "\n";
    lineParameters += "dt = " + std::to_string(param_ptr->dt) + "\n";
    lineParameters += "tau_int = " + std::to_string(param_ptr->tau_int) + "\n";
    lineParameters += "tau_m = " + std::to_string(param_ptr->tau_m) + "\n";
    lineParameters += "tau2 = " + std::to_string(param_ptr->tau2) + "\n";
    lineParameters += "tau_t = " + std::to_string(param_ptr->tau_t) + "\n";
    lineParameters += "l1initial = " + std::to_string(param_ptr->l1initial) + "\n";
    lineParameters += "l2initial = " + std::to_string(param_ptr->l2initial) + "\n";
    lineParameters += "epsilon_m = " + std::to_string(param_ptr->epsilon_m) + "\n";
    lineParameters += "epsilon10 = " + std::to_string(param_ptr->epsilon10) + "\n";
    lineParameters += "epsilon20 = " + std::to_string(param_ptr->epsilon20) + "\n";
    lineParameters += "k10 = " + std::to_string(param_ptr->k10) + "\n";
    lineParameters += "k11 = " + std::to_string(param_ptr->k11) + "\n";
    lineParameters += "k2 = " + std::to_string(param_ptr->k2) + "\n";
    lineParameters += "gamma_max__ = " + std::to_string(param_ptr->gamma_max__) + "\n";
    lineParameters += "B1_tension_only = " + std::to_string(param_ptr->B1_tension_only) + "\n";
    lineParameters += "B2_tension_only = " + std::to_string(param_ptr->B2_tension_only) + "\n";
    lineParameters += "alpha_int = " + std::to_string(param_ptr->alpha_int) + "\n";
    lineParameters += "alpha0 = " + std::to_string(param_ptr->alpha0) + "\n";
    lineParameters += "alpha10 = " + std::to_string(param_ptr->alpha10) + "\n";
    lineParameters += "coef_alpha_int_to_alpha_m = " + std::to_string(param_ptr->coef_alpha_int_to_alpha_m) + "\n";
    lineParameters += "alpha1_m = " + std::to_string(param_ptr->alpha1_m()) + "\n";
    lineParameters += "alpha2 = " + std::to_string(param_ptr->alpha2) + "\n";
    lineParameters += "delta_theta_1 = " + std::to_string(param_ptr->delta_theta_1) + "\n";
    lineParameters += "delta_theta_2 = " + std::to_string(param_ptr->delta_theta_2) + "\n";
    lineParameters += "F_R0 = " + std::to_string(param_ptr->F_R0) + "\n";
    lineParameters += "F_R1 = " + std::to_string(param_ptr->F_R1) + "\n";
    lineParameters += "F_R2 = " + std::to_string(param_ptr->F_R2) + "\n";
    lineParameters += "F_gamma = " + std::to_string(param_ptr->F_gamma) + "\n";
    lineParameters += "nu1 = " + std::to_string(param_ptr->nu1) + "\n";
    lineParameters += "nu2_im = " + std::to_string(param_ptr->nu2_im) + "\n";
    lineParameters += "nu2_int = " + std::to_string(param_ptr->nu2_int) + "\n";
    lineParameters += "nu2_m = " + std::to_string(param_ptr->nu2_m) + "\n";
    lineParameters += "radiusN0 = " + std::to_string(param_ptr->radiusN0) + "\n";
    lineParameters += "radiusN1 = " + std::to_string(param_ptr->radiusN1) + "\n";
    lineParameters += "radiusN2 = " + std::to_string(param_ptr->radiusN2) + "\n";
    lineParameters += std::string("\n");

    std::string gui_parametersOutputFilename = "gui_parameter_save.txt";
    std::ofstream gui_parametersFile;
    gui_parametersFile.open(gui_parametersOutputFilename, std::ios_base::app);
    gui_parametersFile << lineParameters; // we append the result to the file
    gui_parametersFile.close();


    ParametersObj_essential param_ess;
    param_ess.copy(*param_ptr);
    std::ofstream file;
    file.open("param.bin", std::ios::binary); //, std::ios::out);
    if (!file) {
        std::cout << "Error in creating file.." << std::endl;
    }
    else {
        param_ess.save(file);
        file.close();
        std::cout << "File loaded successfully.." << std::endl;
    }
}
void MainWindow::load_simulation_parameters() {
    std::cout << "Loading parameter data from file" << std::endl;
    ParametersObj_essential param_ess;

        std::ifstream file;
        file.open("param.bin", std::ios::in | std::ios::binary);
        if (file.is_open())
        {
            std::cout << "File opened." << std::endl;
            std::cout << "Reading file ";
            param_ess.load(file);
            //std::cout << param_ess.dt << std::endl;
            
            
            if (file.fail())
            {
                switch (errno)
                {
                case EACCES:
                    // this is set if the drive is not ready in DOS
                    std::cout << "Drive not ready or permission denied" << std::endl;
                    break;
                case ENOENT:
                    std::cout << "Could not find this file" << std::endl;
                    break;
                default:
                    perror("opening data file");
                }
                exit(EXIT_FAILURE);
                // a real program would then loop back and ask the user to try again.
            }
            file.close();
            
        }else
        {
            std::cout << "Error in opening file.." << std::endl;
        }

        *param_ptr = param_ess.paste(*param_ptr);
        refreshPushButton_released();
        std::cout << " -> File loaded successfully!" << std::endl;
        
}

void MainWindow::duration_lineEdit_editingFinished(){
    QString value_str = ui->duration_lineEdit->text();
    param_ptr->duration = value_str.toFloat();
    ui->duration_lineEdit->setText( QString::number(param_ptr->duration) );
}

void MainWindow::dt_lineEdit_editingFinished(){
    QString value_str = ui->dt_lineEdit->text();
    param_ptr->dt = value_str.toFloat();
    ui->dt_lineEdit->setText( QString::number(param_ptr->dt) );
}

void MainWindow::nbOfSimulations_lineEdit_editingFinished(){
    QString value_str = ui->nbOfSimulations_lineEdit->text();
    int nbOfSimulations = value_str.toInt();
    if(nbOfSimulations >= 1)
    {
        param_ptr->nbOfSimulations = nbOfSimulations;
    }
    ui->nbOfSimulations_lineEdit->setText( QString::number(param_ptr->nbOfSimulations) );
}

void MainWindow::displaySimulation_clicked(){
    param_ptr->displaySimulation = ui->displaySimulation_checkBox->isChecked();
    if(param_ptr->displaySimulation)
    {ui->displaySimulation_checkBox->setStyleSheet("QCheckBox { color: black }");}
    else
    {
        ui->displaySimulation_checkBox->setStyleSheet("QCheckBox { color: red }");
        window_ptr->clear(predfColorBlack());
        // Draw the string to display time
        sf::Text text("Simulation display is OFF", *param_ptr->font_ptr, 25);
        text.setFillColor(predfColorWhite());
        text.setPosition(sf::Vector2f(param_ptr->zoom * param_ptr->ScreenOffSet_x + param_ptr->ScreenWidth / 2 - text.getLocalBounds().width / 2, param_ptr->zoom * param_ptr->ScreenOffSet_y + param_ptr->ScreenHeight / 25));
        window_ptr->draw(text);
        window_ptr->display();
    }
}

void MainWindow::recordDetails_clicked(){
    param_ptr->recordDetails = ui->recordDetails_checkBox->isChecked();
    if(param_ptr->recordDetails)
    {ui->recordDetails_checkBox->setStyleSheet("QCheckBox { color: black }");}
    else{ui->recordDetails_checkBox->setStyleSheet("QCheckBox { color: red }");}
}

void MainWindow::simulationProgressBar_setVisible(bool visible){
    ui->simulationProgressBar->setVisible(visible);
}

void MainWindow::simulationProgressBar_update(float percentDone){
    ui->simulationProgressBar->setValue(percentDone);
}

void MainWindow::refreshPushButton_released() {
    param_ptr->refresh = true;
    set_all_gui_values();
}

// N0----------------------
void MainWindow::delta_theta_1_slider_moved(){
    int n = ui->delta_horizontalSlider->value();
    param_ptr->delta_theta_1 = param_ptr->delta_theta_1_min + n * param_ptr->delta_theta_1_interval;
    ui->delta_value->setText( QString::fromStdString( std::to_string(param_ptr->delta_theta_1 / M_PI) + " Pi" ));
}

void MainWindow::alpha0_slider_moved(){
    param_ptr->alpha0 = ui->alpha0_horizontalSlider->value();
    ui->alpha0_value->setNum(param_ptr->alpha0);
}

void MainWindow::F_R0_slider_moved() {
    int n = ui->F_R0_horizontalSlider->value();
    param_ptr->F_R0 = param_ptr->F_R0_min + n * param_ptr->F_R0_interval;
    ui->F_R0_value->setNum(param_ptr->F_R0);
}

void MainWindow::radiusN0_slider_moved() {
    int n = ui->radiusN0_horizontalSlider->value();
    param_ptr->radiusN0 = param_ptr->radiusN0_min + n * param_ptr->radiusN0_interval;
    ui->radiusN0_value->setNum(param_ptr->radiusN0);
}


// N1----------------------
void MainWindow::nu1_slider_moved(){
    int n = ui->P_N1_horizontalSlider->value();
    param_ptr->nu1 = param_ptr->nu1_min + n * param_ptr->nu1_interval;
    ui->P_N1_value->setNum(param_ptr->nu1);
    param_ptr->Poisson_nu1 = CreatePoissonDistribution( param_ptr->nu1, param_ptr->dt);
}

void MainWindow::tau_int_slider_moved(){
    param_ptr->tau_int = ui->DN1i_horizontalSlider->value();
    ui->DN1i_value->setNum(param_ptr->tau_int);
    param_ptr->verify(false, true);
    ui->DN1m_horizontalSlider->setMinimum(param_ptr->tau_m_min());
    ui->DN1m_horizontalSlider->setValue(param_ptr->tau_m);
    ui->DN1m_value->setNum(param_ptr->tau_m);
}

void MainWindow::tau_m_slider_moved(){
    param_ptr->tau_m = ui->DN1m_horizontalSlider->value();
    ui->DN1m_value->setNum(param_ptr->tau_m);
}

void MainWindow::tau_t_slider_moved() {
    param_ptr->tau_t = ui->t_c_horizontalSlider->value();
    ui->t_c_value->setNum(param_ptr->tau_t);
}

void MainWindow::alpha10_slider_moved(){
    param_ptr->alpha10 = ui->alpha10_horizontalSlider->value();
    ui->alpha10_value->setNum(param_ptr->alpha10);
}

void MainWindow::alpha_int_slider_moved(){
    param_ptr->alpha_int = ui->alphasat_horizontalSlider->value();
    ui->alphasat_value->setNum(param_ptr->alpha_int);

    ui->alpha1m_horizontalSlider->setMinimum(param_ptr->alpha1_m_min());
    ui->alpha1m_horizontalSlider->setMaximum(param_ptr->alpha1_m_max());
    ui->alpha1m_horizontalSlider->setValue( param_ptr->alpha1_m());
    ui->alpha1m_value->setNum(param_ptr->alpha1_m());

    ui->alpha2_horizontalSlider->setMaximum(param_ptr->alpha2_max());
}

void MainWindow::F_R1_slider_moved(){
    param_ptr->F_R1 = ui->ruptureForce_N1_horizontalSlider->value();
    ui->ruptureForce_N1_value->setNum(param_ptr->F_R1);
}

void MainWindow::radiusN1_slider_moved() {
    int n = ui->radiusN1_horizontalSlider->value();
    param_ptr->radiusN1 = param_ptr->radiusN1_min + n * param_ptr->radiusN1_interval;
    ui->radiusN1_value->setNum(param_ptr->radiusN1);
}


// N2 ----------------------------------------------------

void MainWindow::delta_theta_2_slider_moved() {
    int n = ui->theta2_horizontalSlider->value();
    param_ptr->delta_theta_2 = param_ptr->delta_theta2_min + n * param_ptr->delta_theta2_interval;
    ui->theta2_value->setNum(param_ptr->delta_theta_2);
}

void MainWindow::nu2_im_slider_moved(){
    int n = ui->P_N20_horizontalSlider->value();
    param_ptr->nu2_im = param_ptr->nu2_im_min + n * param_ptr->nu2_im_interval;
    ui->P_N20_value->setNum(param_ptr->nu2_im);
    param_ptr->Poisson_nu2_im = CreatePoissonDistribution(param_ptr->nu2_im, param_ptr->dt);
}
void MainWindow::nu2_int_slider_moved(){
    int n = ui->P_N2i_horizontalSlider->value();
    param_ptr->nu2_int = param_ptr->nu2_int_min + n * param_ptr->nu2_int_interval;
    ui->P_N2i_value->setNum(param_ptr->nu2_int);
    param_ptr->Poisson_nu2_int = CreatePoissonDistribution(param_ptr->nu2_int, param_ptr->dt);
}
void MainWindow::nu2_m_slider_moved(){
    int n = ui->P_N2m_horizontalSlider->value();
    param_ptr->nu2_m = param_ptr->nu2_m_min + n * param_ptr->nu2_m_interval;
    ui->P_N2m_value->setNum(param_ptr->nu2_m);
    param_ptr->Poisson_nu2_m = CreatePoissonDistribution(param_ptr->nu2_m, param_ptr->dt);
}

void MainWindow::tau_2_slider_moved(){
    param_ptr->tau2 = ui->DN2_horizontalSlider->value();
    ui->DN2_value->setNum(param_ptr->tau2);
    param_ptr->verify(false, true);
    ui->DN1i_horizontalSlider->setMinimum(param_ptr->tau_int_min());
    ui->DN1i_horizontalSlider->setValue(param_ptr->tau_int);
    ui->DN1i_value->setNum(param_ptr->tau_m);
    ui->DN1m_horizontalSlider->setMinimum(param_ptr->tau_m_min());
    ui->DN1m_horizontalSlider->setValue(param_ptr->tau_m);
    ui->DN1m_value->setNum(param_ptr->tau_m);
}
void MainWindow::alpha2_slider_moved(){
    param_ptr->alpha2 = ui->alpha2_horizontalSlider->value();
    ui->alpha2_value->setNum(param_ptr->alpha2);
}
void MainWindow::ruptureForce_N2_slider_moved(){
    param_ptr->F_R2 = ui->ruptureForce_N2_horizontalSlider->value();
    ui->ruptureForce_N2_value->setNum(param_ptr->F_R2);
}

void MainWindow::radiusN2_slider_moved(){
    int n = ui->radiusN2_horizontalSlider->value();
    param_ptr->radiusN2 = param_ptr->radiusN2_min + n * param_ptr->radiusN2_interval;
    ui->radiusN2_value->setNum(param_ptr->radiusN2);
}

// B1 --------------------
void MainWindow::B1_tension_only_clicked(){
    param_ptr->B1_tension_only = ui->B1_tension_only_checkBox->isChecked();
    if(param_ptr->B1_tension_only)
    {ui->B1_tension_only_checkBox->setStyleSheet("QCheckBox { color: black }");}
    else{ui->B1_tension_only_checkBox->setStyleSheet("QCheckBox { color: red }");}
}
void MainWindow::l1i_slider_moved(){
    // NOT COMPLETE GUI DOES NOT IMPACT param obj
    param_ptr->l1initial = ui->l1_horizontalSlider->value();
    ui->l1_value->setNum(param_ptr->l1initial);
}
void MainWindow::k10_slider_moved(){
    int n = ui->k10_horizontalSlider->value();
    param_ptr->k10 = param_ptr->k10_min + n * param_ptr->k10_interval;
    ui->k10_value->setNum(param_ptr->k10);
}
void MainWindow::k11_slider_moved(){
    int n = ui->k1_horizontalSlider->value();
    param_ptr->k11 = param_ptr->k1_min + n * param_ptr->k1_interval;
    ui->k1_value->setNum(param_ptr->k11);
}
void MainWindow::gamma_max_slider_moved(){
    int n = ui->gamma1_horizontalSlider->value();
    param_ptr->gamma_max__ = param_ptr->gamma_max__min + n * param_ptr->gamma_max__interval;
    ui->gamma1_value->setNum(param_ptr->gamma_max__);
}
void MainWindow::epsilon_m_slider_moved(){
    int n = ui->lambda1m_horizontalSlider->value();
    param_ptr->epsilon_m = param_ptr->epsilon_m_min + n * param_ptr->epsilon_m_interval;
    ui->lambda1m_value->setNum(param_ptr->epsilon_m);
}
// B2 ----------------------------
void MainWindow::B2_tension_only_clicked(){
    param_ptr->B2_tension_only = ui->B2_tension_only_checkBox->isChecked();
    if(param_ptr->B2_tension_only)
    {ui->B2_tension_only_checkBox->setStyleSheet("QCheckBox { color: black }");}
    else{ui->B2_tension_only_checkBox->setStyleSheet("QCheckBox { color: red }");}
}
void MainWindow::l2i_slider_moved(){
    param_ptr->l2initial = ui->l2_horizontalSlider->value();
    ui->l2_value->setNum(param_ptr->l2initial);
}
void MainWindow::epsilon20_slider_moved(){
    int n = ui->l2_prestress_horizontalSlider->value();
    param_ptr->epsilon20 = param_ptr->epsilon20_min + n * param_ptr->epsilon20_interval;
    ui->l2_prestress_value->setNum(param_ptr->epsilon20);
}
void MainWindow::k2_slider_moved(){
    param_ptr->k2 = ui->k2_horizontalSlider->value();
    ui->k2_value->setNum(param_ptr->k2);
}

// Display --------------------
void MainWindow::ScreenHeight_slider_moved(){
    param_ptr->ScreenHeight = ui->ScreenHeight_horizontalSlider->value();
    ui->ScreenHeight_value->setNum(param_ptr->ScreenHeight);
}
void MainWindow::ScreenWidth_slider_moved(){
    param_ptr->ScreenWidth = ui->ScreenWidth_horizontalSlider->value();
    ui->ScreenWidth_value->setNum(param_ptr->ScreenWidth);
}
void MainWindow::PixelsPerMeter_slider_moved(){
    param_ptr->PixelsPerMeter = ui->PixelsPerMeter_horizontalSlider->value();
    ui->PixelsPerMeter_value->setNum(param_ptr->PixelsPerMeter);
}
void MainWindow::legend_offsetX_coefficient_slider_moved(){
    int n = ui->legend_offsetX_coefficient_horizontalSlider->value();
    param_ptr->legend_offsetX_coefficient = param_ptr->legend_offsetX_coefficient_min + n * param_ptr->legend_offsetX_coefficient_interval;
    ui->legend_offsetX_coefficient_value->setNum(param_ptr->legend_offsetX_coefficient);
}
void MainWindow::legend_offsetY_coefficient_slider_moved(){
    int n = ui->legend_offsetY_coefficient_horizontalSlider->value();
    param_ptr->legend_offsetY_coefficient = param_ptr->legend_offsetY_coefficient_min + n * param_ptr->legend_offsetY_coefficient_interval;
    ui->legend_offsetY_coefficient_value->setNum(param_ptr->legend_offsetY_coefficient);
}
void MainWindow::legend_spacingX_slider_moved(){
    int n = ui->legend_spacingX_horizontalSlider->value();
    param_ptr->legend_spacingX = param_ptr->legend_spacingX_min + n * param_ptr->legend_spacingX_interval;
    ui->legend_spacingX_value->setNum(param_ptr->legend_spacingX);
}
void MainWindow::legend_spacingY_slider_moved(){
    int n = ui->legend_spacingY_horizontalSlider->value();
    param_ptr->legend_spacingY = param_ptr->legend_spacingY_min + n * param_ptr->legend_spacingY_interval;
    ui->legend_spacingY_value->setNum(param_ptr->legend_spacingY);
}

// Legend --------------------
void MainWindow::displayLegend_clicked(){
        param_ptr->displayLegend = ui->displayLegend_checkBox->isChecked();
}
void MainWindow::displayNodeType_clicked(){
    param_ptr->displayNodeType = ui->displayNodeType_checkBox->isChecked();
}
void MainWindow::displayNodeID_clicked(){
    param_ptr->displayNodeID = ui->displayNodeID_checkBox->isChecked();
}
void MainWindow::displayNodeMaturationState_clicked(){
    param_ptr->displayNodeMaturationState = ui->displayNodeMaturationState_checkBox->isChecked();
}
void MainWindow::displayBranchID_clicked(){
    param_ptr->displayBranchID = ui->displayBranchID_checkBox->isChecked();
}
void MainWindow::displayBranchType_clicked(){
    param_ptr->displayBranchType = ui->displayBranchType_checkBox->isChecked();
}
void MainWindow::displayCytoplasm_clicked() {
    param_ptr->displayCytoplasm = ui->displayCytoplasm_checkBox->isChecked();
}
void MainWindow::displayInteractions_clicked() {
    param_ptr->displayInteractions = ui->displayInteractions_checkBox->isChecked();
}
void MainWindow::displayNodes_clicked() {
    param_ptr->displayNodes = ui->displayNodes_checkBox->isChecked();
}