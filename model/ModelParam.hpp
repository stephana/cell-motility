//
// Created by bgirard on 08/03/23.
//

#ifndef MODELPARAM_HPP
#define MODELPARAM_HPP


#include "Real.hpp"
#include "Config.hpp"
#include <numbers> // std::numbers
#include <random>
//#include "cmath"





enum class patternType { triangle_pattern, rectangle_pattern, circle_pattern, homogeneous_pattern};

template<typename T>
T&operator>>(T &in, patternType &pt) {
        std::string token;
        in >> token;
        if (token == "homogeneous_pattern")
            pt = patternType::homogeneous_pattern;
        else if (token == "circle_pattern")
            pt = patternType::circle_pattern;
        else if (token == "rectangle_pattern")
            pt = patternType::rectangle_pattern;
        else if (token == "triangle_pattern")
            pt = patternType::triangle_pattern;
        else
            in.setstate(std::ios_base::failbit);

        return in;
}

template<typename T>
T &operator<<(T &in, const patternType &pt) {
        if (pt == patternType::homogeneous_pattern)
            in << "homogeneous_pattern";
        else if (pt == patternType::circle_pattern)
            in << "circle_pattern";
        else if (pt == patternType::rectangle_pattern)
            in << "rectangle_pattern";
        else if (pt == patternType::triangle_pattern)
            in << "triangle_pattern";
        else
            in.setstate(std::ios_base::failbit);

        return in;
}


enum B1_length_Generator { constant_B1_length_generator, uniforme_B1_length_generator };
enum B2_length_Generator { constant_B2_length_generator, uniforme_B2_length_generator };



struct ModelParam : public Configurable
{
//    ModelParam(ModelParam const&)

//

public:
     #define CLASS_NAME "Model"
MAKE_OPT(std::string ,label,"","Label for simulaiton ");
    MAKE_OPT(std::string ,label_x,"","Label for simulaiton ");
    MAKE_OPT(std::string , label_y,"","Label for simulaiton ");
    MAKE_OPT(std::string ,simdetailed_label,"","Label for simulaiton ");
    MAKE_OPT(unsigned int,labelRefNb,0,"id for label ");

    MAKE_OPT(RealType,duration,2 * 60 * 60,"Duration of simulaiton in seconds")
    //RealType duration = 2 * 60 * 60; // equivalent to 2h = 2 *60 * 60 seconds

    MAKE_OPT_MIN_MAX(RealType,dt,0.1,0.00001,1.0,"Delta T"); // secondes
    MAKE_OPT_MIN_MAX(unsigned int,nbOfSimulations,3,1,10000,"nbOfSimulations");

    bool displaySimulation = true;
    bool recordDetails = true;

    float maxInteractionDistanceBetweenCells = 150; // this is not a physical distance. It is used as an optimization parameter. It must be greater than twice the max length of a B1 branch
    float F_RCC = 300; // Rupture force cell cell adhesion

    MAKE_OPT_MIN_MAX(RealType,delta_theta_2,std::numbers::pi_v<RealType> / 6, 0,std::numbers::pi_v<RealType>,"")


    // Cell and node parameters
    MAKE_OPT_MIN_MAX(RealType,tau_int , 15 * 60,1,45 * 60,"Durées de vie de  tau_int pour le nœud N1 intermédiaire")
    MAKE_OPT_MIN_MAX(RealType, tau_m , (35 * 60),1,10 * 60 * 60," Durées de vie de pour le nœud N1 mature")
    MAKE_OPT_MIN_MAX(RealType,tau2 , 200, 1,50 * 60," Durées de vie de pour le noeud N2");
    MAKE_OPT_MIN_MAX(RealType,l1initial,7,1,25,"Longueurs de création des branches : l1initial pour la branche primaire avec la valeur l1initial tirée aléatoirement entre l1min et l1max")
    MAKE_OPT_MIN_MAX(RealType,l2initial,4,1,15,"Longueurs de création des branches : l2initial pour la branche secondaire avec l2initial longueur constante à fixer")

    B1_length_Generator l1_Generator = constant_B1_length_generator;
    B2_length_Generator l2_Generator = constant_B2_length_generator;



    MAKE_OPT_MIN_MAX(RealType ,epsilon_m,0.2f,0.0f,0.6f,"")//  float epsilon_m_interval = 0.01; ??//hahah
    float epsilon10 = 0;

    MAKE_OPT_MIN_MAX(RealType,epsilon20 ,0.5,0.2,1.0,"")//  float epsilon20_interval = 0.01;
    inline float l2rest() const{ return l2initial/(1+epsilon20); };
    //TODO : Definir l2r et calculer epsilon20 ?

    MAKE_OPT_MIN_MAX(RealType, nu1 , 0.01,0.001, 0.20,"")//          float nu1_interval = 0.001;
    MAKE_OPT_MIN_MAX(RealType, nu2_im , 0.015,0.001, .5," probability of creating an N2 from a intermediate N1")//         float nu2_im_interval = 0.001;//
    MAKE_OPT_MIN_MAX(RealType,nu2_int,0.015,0.001,.5,"probability of creating an N2 from a intermediate N1");                    //float nu2_int_interval = 0.001;//

    MAKE_OPT_MIN_MAX(RealType,nu2_m,0.00001,0.000001,1,"")


    bool _init_poisson = false;
    std::poisson_distribution<int> pnu1;
    std::poisson_distribution<int> pnu2_im;
    std::poisson_distribution<int> pnu2_int;
    std::poisson_distribution<int> pnu2_m;

    void init_posson()
    {
        if(_init_poisson)
            return;
        pnu1  = std::poisson_distribution<int>(nu1*dt);
        pnu2_im  = std::poisson_distribution<int>(nu2_im*dt);
        pnu2_int  = std::poisson_distribution<int>(nu2_int*dt);
        pnu2_m  = std::poisson_distribution<int>(nu2_m*dt);
        _init_poisson= true;
    }
    int Poisson_nu1()
    {
        init_posson();
        return pnu1(random_engine);
    }
    unsigned int Poisson_nu2_im()
    {
        init_posson();
        return pnu2_im(random_engine);
    }
    unsigned int Poisson_nu2_int()
    {
        init_posson();
        return pnu2_int(random_engine);
    }
        unsigned int Poisson_nu2_m()
    {
        init_posson();
        return pnu2_m(random_engine);
    }


    float kic = 10; // intercell stiffness


    MAKE_OPT_MIN_MAX(RealType,k10 , 0.1, 0.1, 100,"Coefficients de raideurs élastiques des branches : k10=0 pour la branche primaire immature")
    MAKE_OPT_MIN_MAX(RealType,k11 , 3.5,0., 100," Coefficients de raideurs élastiques des branches : k11 pour la branche primaire intermédiaire")
   MAKE_OPT_MIN_MAX(RealType,k2 , 12,0.1, 100,"Coefficients de raideurs élastiques des branches : k2 pour la branche secondaire")
    MAKE_OPT_MIN_MAX(RealType,gamma_max__ , 30,30, 60," Contractilité de la branche primaire !!???")


    MAKE_OPT_MIN_MAX(RealType, F_gamma, 1.0, 0.2, 5.0,"")

    float gamma1(float F_a) { return gamma_max__* (1 - std::exp(- F_a / F_gamma)); }

    bool B1_tension_only = false;
    bool B2_tension_only = true;

     // Delta inhibition domain
    MAKE_OPT_MIN_MAX(RealType,delta_theta_1 , std::numbers::pi_v<RealType> / 2.0,0,std::numbers::pi_v<RealType>,"")

    MAKE_OPT_MIN_MAX(RealType,alpha0,400,1,1000,"Coefficients de frottement α0 pour le nœud N0");
    MAKE_OPT_MIN_MAX(RealType,alpha10,50.0,1,1000,"Coefficients de frottement α10 = 0 pour le nœud N1 immature (pas d’adhésion)");
    //

    MAKE_OPT_MIN_MAX(RealType,tau_t ,5 * 60,1 * 60, 20 * 60,"Temps caractéristique pour qu'un N1 arrive à l'état intermédiare")
    MAKE_OPT_MIN_MAX(RealType,alpha_int,150,1.0,1000,"");
    //float alpha1i_slope_coefficient = 1.5;         float alpha1i_slope_coefficient_min = 0.1;      float alpha1i_slope_coefficient_max = 10;  float alpha1i_slope_coefficient_interval = 0.1;
    float alpha_im(double t) const {
        double thisalpha = (alpha_int - alpha10) / tau_t * t + alpha10;
        if (thisalpha > alpha_int) { return alpha_int; }
        else { return thisalpha; }
    }; // Coefficients de frottement • α1i(t) pour le nœud N1 intermédiaire est une fonction sigmoïde qui dépend du temps et atteint une saturation dont le seuil αsat est à définir
    float coef_alpha_int_to_alpha_m = 1.1;      float coef_alpha_int_to_alpha_m_min = 1;        float coef_alpha_int_to_alpha_m_max = 2;
    inline float alpha1_m() const { return coef_alpha_int_to_alpha_m * alpha_int; };                         float alpha1_m_min() { return alpha_int; };             float alpha1_m_max() { return coef_alpha_int_to_alpha_m * alpha_int_max; };// Coefficients de frottement α1m pour le nœud N1 mature avec α1m > αsat
    MAKE_OPT_MIN_MAX(RealType ,alpha2,8,1,1000000,"");
    MAKE_OPT_MIN_MAX(RealType ,alpha_free,1,0.000001,1000000,"");

    float adhesion_coeficient_FR012 = 1.0;
                                /*F_TH */
    MAKE_OPT_MIN_MAX(RealType,F_R0 , 30,10,100,"")
    float F_R0_interval = 0.1;
    MAKE_OPT_MIN_MAX(RealType,F_R1 , 34, 10, 100,"")
    MAKE_OPT_MIN_MAX(RealType,F_R2 , 17,10,100,"")

    float radiusN0 = 2.8; /** in µm **/          float radiusN0_min = 0.1;        float radiusN0_max = 15;     float radiusN0_interval = 0.1;
    float radiusN1 = 1.2; /** in µm **/          float radiusN1_min = 0.1;        float radiusN1_max = 10;     float radiusN1_interval = 0.1;
    float radiusN2 = .2; /** in µm **/          float radiusN2_min = 0.1;        float radiusN2_max = 5;      float radiusN2_interval = 0.1;
    float cytoplasmScaleFactor = 2.5;


    MAKE_OPT(patternType,pattern_type,patternType::homogeneous_pattern,"");
    MAKE_OPT_MIN_MAX(RealType,pattern_height,10,2,30,"");
    MAKE_OPT_MIN_MAX(RealType, pattern_width, 20 ,5, 3000,"");
    MAKE_OPT_MIN_MAX(RealType, pattern_spacing , 0,-16 , 4,"")
    float pattern_radius = pattern_width / 2.0; float pattern_radius_min = pattern_width_min / 2.0; float pattern_radius_max = pattern_width_max / 2.0;
    MAKE_OPT_MIN_MAX(unsigned int ,pattern_nb_motifs ,0,0,100,"");

    std::default_random_engine random_engine = std::default_random_engine (55225);

//    MAKE_OPT(int,seed,1555,"random seed");
    int seed = 1555;
    bo::options_description_easy_init seed_opts = opts.add_options()( CLASS_NAME ".seed",
            bo::value<int>(&seed)->default_value(1555)->notifier([this](auto){

                random_engine = std::default_random_engine (seed);
            })
            , "random seed");
    PRINT_OPT(int,seed)

    float random(float start,float end)
    {
        std::uniform_real_distribution d(start,end);
        return d(random_engine);
    }

#undef CLASS_NAME
};

#endif


