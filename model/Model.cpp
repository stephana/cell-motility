//
// Created by bgirard on 08/03/23.
//

#include "Substrate.hpp"
#include "OutPutManagers.hpp"
#include "World.hpp"
#include "Model.hpp"
#include "ModelParam.hpp"

/// run one simulation
void Model::run(const unsigned int simulationNumber)
{
 RealType t = 0;
 RealType tInfo = 0;


  // create and manage substrate
  Substrate substrate = Substrate(_param);

  // create a world with the substrate
  World wold_cells =  World(_param,substrate,_output);

  // for now add one cellule
  wold_cells.addNewCell();

  //RecordsOfNodeTransitionPeriods nodeTransitionRecords;
  //_output.cell(simulationNumber,nodeTransitionRecords,firstcell,_label);

  // notify outpout manafer that the simulation will start
  _output.start_model(simulationNumber,_param,wold_cells);
    // notify outpout manafer that the susbtrate used
  _output.substrate_model(substrate,simulationNumber);
  while(!_isOver)
  {

        // simulate one delta time

        //cells.InterCell_createInteractions(param);
        //cells.InterCell_inhibit(param);
        // add new node according to paper
        wold_cells.compute_morphology();
        //wold_cells.protrusionManager(); //TODO // not implemented

        // compute and apply force
        wold_cells.forces();

        wold_cells.step_and_kill();

        t += _param.dt;
        tInfo+=_param.dt;

        // notify outpout manafer that the simulation have advenced of delta t
        _output.step_model(t,wold_cells);

        //_output.cells();
//        if(tInfo> ??)
//        {
//         //_output.
//         tInfo = 0;
//        }
        if (t >= _param.duration) {
            _isOver = true; t = 0;
        }
  }
  _output.end_model();
}