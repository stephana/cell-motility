//
// Created by bgirard on 08/03/23.
//

#ifndef ANGIOMODEL_WORLD_HPP
#define ANGIOMODEL_WORLD_HPP
#include "visitable.hpp"
#include "Cell.hpp"
#include <vector>

class Substrate;
class OutPutManagers;
class World : Visitable
{
public:
    World(ModelParam& param,const Substrate& substrate,OutPutManagers& output );
    ~World() {};

    // Intracell
    void addNewCell(const Eigen::Vector2<RealType>& position = Eigen::Vector2<RealType>(0, 0));
    void compute_morphology();
   // void protrusionManager();
    void forces();

    void step_and_kill();


    VISITABLE()
    void visit_child(Visitor& v)
    {
            for(Cell& cell : cell_list)
            {
                cell.visit(v);
            }
    }
    std::vector<Cell>& cells() {return cell_list;};

    const Substrate& substrate() const{return _substrate;};
private:
    ModelParam& _param;
    const Substrate& _substrate;
    std::vector<Cell> cell_list;
public:
    OutPutManagers& _output;
};

#endif //ANGIOMODEL_WORLD_HPP
