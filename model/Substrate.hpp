//
// Created by bgirard on 09/03/23.
//

#ifndef ANGIOMODEL_SUBSTRATE_HPP
#define ANGIOMODEL_SUBSTRATE_HPP
#include "visitable.hpp"
#include "Real.hpp"
#include <eigen3/Eigen/Core>
#include <memory>
#include <vector>

struct ModelParam;
class SubstratePattern : Visitable
{

    public:
        //virtual std::string patternToSingleLineText(ParametersObj& param, int i) { std::string lineTextPattern = ""; return lineTextPattern; }

        virtual bool isIn(const Eigen::Vector2<RealType>& position) const  = 0;

        // For compatibility with old csv only only
        virtual void setAdhesionStrength(float newAdhesionStrength) { adhesionStrength = newAdhesionStrength; };
        virtual float getAdhesionStrength() { return adhesionStrength; }
        virtual void visit(Visitor& v) = 0;

        virtual ~SubstratePattern(){};
    private:
        float adhesionStrength = 1.0;
};


class Substrate  : Visitable {
public:
    VISITABLE()

    Substrate(const ModelParam& param);


    void visit_child(Visitor& v);
    Substrate (const Substrate &) = delete;
    Substrate & operator = (const Substrate &) = delete;
    bool isIn(const Eigen::Vector2<RealType>& position) const;

protected:
    void createSubstrateMotifs();


    const ModelParam& _param;
     bool adhereEveryWhere = false;
     float defaultAdhesionStrengthOutsidePattern = 1.0; // adhesion strength outside pattern
     std::vector<std::unique_ptr<SubstratePattern>> _substratePattern;
};

class SubstratePattern_Triangle : public SubstratePattern
{
    public:
        VISITABLE()
        SubstratePattern_Triangle(Eigen::Vector2<RealType> position, float height, float width, float rotation);;
        ~SubstratePattern_Triangle() {};

         virtual bool isIn(const Eigen::Vector2<RealType>& pt) const override {
             return PointInTriangle(pt);
         };
         Eigen::Vector2<RealType> nodeA;
         Eigen::Vector2<RealType> nodeB;
         Eigen::Vector2<RealType> nodeC;

    private:
        static float sign( Eigen::Vector2<RealType> p1,  Eigen::Vector2<RealType>  p2,  Eigen::Vector2<RealType> p3)
        {
            return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
        }

        bool PointInTriangle( Eigen::Vector2<RealType> pt) const;
};
class SubstratePattern_Rectangle : public SubstratePattern
{
public:
    VISITABLE()
    SubstratePattern_Rectangle(const Eigen::Vector2<RealType>& position, float height, float width, float rotation = 0);;
    ~SubstratePattern_Rectangle() {};



    bool isIn(const Eigen::Vector2<RealType>& pt) const override
    {
        return (pt.x() < nodeA.x()) && (pt.x() > nodeD.x()) && (pt.y() < nodeA.y()) && (pt.y() > nodeB.y());
    };
    Eigen::Vector2<RealType> nodeA;
    Eigen::Vector2<RealType> nodeB;
    Eigen::Vector2<RealType> nodeC;
    Eigen::Vector2<RealType> nodeD;
};


class SubstratePattern_Circle : public SubstratePattern
{
public:
    VISITABLE()
    SubstratePattern_Circle(const Eigen::Vector2<RealType>& position, float radius): position(position), radius(radius){
    };
    ~SubstratePattern_Circle() {};

    bool isIn(const Eigen::Vector2<RealType>& pt) const override;;
    const Eigen::Vector2<RealType>& pos(){
        return position;
    }
    Eigen::Vector2<RealType> position;
    float radius;
};
#endif //ANGIOMODEL_SUBSTRATE_HPP
