//
// Created by bgirard on 08/03/23.
//

#ifndef ANGIOMODEL_MODEL_HPP
#define ANGIOMODEL_MODEL_HPP


struct ModelParam;
class OutPutManagers;
class Model {
public:
    Model(ModelParam& param,
          OutPutManagers& output) : _param(param),
    _output(output)
    {}
    //run one simulation
    void run(const unsigned int simulationNumber);

private:
    ModelParam& _param;
    OutPutManagers& _output;
    bool _isOver = false;
};

#endif //ANGIOMODEL_MODEL_HPP
