//
// Created by bgirard on 09/03/23.
//

#include "Substrate.hpp"
#include "ModelParam.hpp"




Substrate::Substrate(const ModelParam &param)  : _param(param)
{
    createSubstrateMotifs();

}

void Substrate::visit_child(Visitor &v) {

        for(auto& p : _substratePattern)
            p->visit(v);

}

bool Substrate::isIn(const Eigen::Vector2<RealType>& position) const
{
    for(auto& subpat : _substratePattern)
    {
        if(subpat->isIn(position))
        {
            return true;
        }
    }
    return adhereEveryWhere  ;

}

void Substrate::createSubstrateMotifs()
{
    if (_param.pattern_type == patternType::homogeneous_pattern)
    {
        adhereEveryWhere = true;
    } else {
        adhereEveryWhere = false;

            float height = _param.pattern_height;
            float width = _param.pattern_width;
            float pattern_spacing = _param.pattern_spacing;
            float radius = _param.pattern_radius;
            int nbmotifs = _param.pattern_nb_motifs;

        for (unsigned int i = 0; i < _param.pattern_nb_motifs; i++) {
            if (_param.pattern_type == patternType::triangle_pattern) {


                _substratePattern.emplace_back(std::make_unique<SubstratePattern_Triangle>(
                        Eigen::Vector2<RealType>( (i - nbmotifs / 2.0 - 0.5) * (pattern_spacing + width) + pattern_spacing + width/2.0, 0),
                        height,
                        width,
                        0
                        ));
            }
        else if (_param.pattern_type == patternType::rectangle_pattern) {

            _substratePattern.emplace_back(std::make_unique<SubstratePattern_Rectangle>(
                 Eigen::Vector2<RealType>( (i - nbmotifs / 2.0) * (pattern_spacing + width) + pattern_spacing + width / 2.0, 0),
                height,
                width
                        ));

            //createSubstrateMotifs_rectangles_multiadhesion(param, substrate, height, width, pattern_spacing);
        } else if (_param.pattern_type == patternType::circle_pattern) {
            _substratePattern.emplace_back(std::make_unique<SubstratePattern_Circle>(
                Eigen::Vector2<RealType>( (i + 1 - nbmotifs / 2.0) * pattern_spacing - radius / 2.0, 0), radius
                        ));        }
    }
    }
}

SubstratePattern_Triangle::SubstratePattern_Triangle(Eigen::Vector2<RealType> position, float height, float width,
                                                     float rotation) {
    nodeA = position;
    nodeB = position;
    nodeC = position;

    nodeA+=Eigen::Vector2<RealType>(0, height/2.0);
    nodeB+=Eigen::Vector2<RealType>(0,-height/2.0);
    nodeC+= Eigen::Vector2<RealType>(width, 0);
}

bool SubstratePattern_Triangle::PointInTriangle(Eigen::Vector2<RealType> pt) const {
    float d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(pt, nodeA, nodeB);
    d2 = sign(pt, nodeB, nodeC);
    d3 = sign(pt, nodeC, nodeA);

    has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

    return !(has_neg && has_pos);
}

SubstratePattern_Rectangle::SubstratePattern_Rectangle(const Eigen::Vector2<RealType> &position, float height,
                                                       float width, float rotation) {
    nodeA = position;
    nodeB = position;
    nodeC = position;
    nodeD = position;

    nodeA+=(Eigen::Vector2<RealType>( width/2.0, height/2.0));
    nodeB+=(Eigen::Vector2<RealType>( width/2.0,-height/2.0));
    nodeC+=(Eigen::Vector2<RealType>(-width/2.0,-height/2.0));
    nodeD+=(Eigen::Vector2<RealType>(-width/2.0, height/2.0));

}

bool SubstratePattern_Circle::isIn(const Eigen::Vector2<RealType> &pt) const {
    if ((position-pt).norm() < radius){ return true;}
    return false;
}
