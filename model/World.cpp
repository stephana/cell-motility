//
// Created by bgirard on 08/03/23.
//

#include "World.hpp"
#include "OutPutManagers.hpp"

World::World(ModelParam &param, const Substrate &substrate, OutPutManagers &output) : _param(param),_substrate(substrate),_output(output)
{}

void World::addNewCell(const Eigen::Vector2<RealType>& position)
{
    cell_list.emplace_back(_param,position,this);

}

void World::compute_morphology()
{
    for(Cell& cell : cell_list)
    {
        cell.compute_morphology();
    }

}

void World::forces()
{
    for(Cell& cell : cell_list)
    {
        cell.forces();
    }

    //    InterCell_forces( param, nodeTransitionRecords, CPM, dt);
}


void World::step_and_kill()
{
    for(Cell& cell : cell_list)
    {
        cell.step_and_kill();
    }

}