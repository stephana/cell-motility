//
// Created by bgirard on 08/03/23.
//

#include <optional>
#include "Cell.hpp"
#include <iostream>
#include <eigen3/Eigen/Geometry>

Cell::Cell(ModelParam& param,const Eigen::Vector2<RealType>& position,const World* world):_param(param),_n0(param,position,world),_world(world)
{
         static std::atomic<unsigned int> id_generator = 1;
        _id = id_generator++;
}



void Cell::compute_morphology()
{
    _n0.compute_morphology();

}


void Cell::forces()
{
    //listOfSubstrateAdhesionStrengthFactorsInContactWithCell.clear();//what is this thingh ??

    _n0.compute_forces();
    _n0.apply_forces();
}

void Cell::step_and_kill()
{
    _n0.step_and_kill();
}