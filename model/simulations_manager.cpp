//
// Created by bgirard on 08/03/23.
//
#include "ModelParam.hpp"
#include "clock.hpp"
#include "Model.hpp"
#include "iostream"


/// This funciton run one simulation multiple time
void simulations_manager(ModelParam& default_param , OutPutManagers& om)
{

    MostAccurateClock clk;
    auto tstart= clk.now();


    for(unsigned int i =0;i<std::min(default_param.nbOfSimulations,default_param.nbOfSimulations_max);i++)
    {
        std::cout <<"Start simulation "<<i+1<<std::endl;
        auto tstart= clk.now();

        Model m(default_param,om);
        m.run(i);

        std::cout<<""<<i+1<<" computations done out of "<<default_param.nbOfSimulations<<" ";
        std::cout<<" this computation took  "<< (clk.now()-tstart)<<" "<<(i+1)*100/default_param.nbOfSimulations<<" done";

    }
    std::cout << " ALL COMPUTATION TERMINATED after " << clk.now()-tstart << "" << std::endl;
    std::cout << '\a'; // make a beep sound
}
