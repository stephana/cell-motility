//
// Created by bgirard on 09/03/23.
//

#include <numbers>
#include <iostream>
#include "Node.hpp"
#include "Cell.hpp"


RealType angle_bettween(const Eigen::Vector2<RealType>& from,const Eigen::Vector2<RealType>& to )
{
   auto angle =  std::atan2(to.y(),to.x())-std::atan2(from.y(),from.x());

   return std::fmod((angle)+2*std::numbers::pi , (2*std::numbers::pi));
}

RealType angle_bettween(const RealType& from_x,const RealType& from_y,const RealType& to_x,const RealType& to_y)
{
      auto angle =  std::atan2(to_y,to_x)-std::atan2(from_y,from_x);

   return std::fmod((angle)+2*std::numbers::pi , (2*std::numbers::pi));
}


Node::Node(ModelParam& param,const Eigen::Vector2<RealType>& position,const World* world):_param(param),_position(position),_world(world)
{
    static std::atomic<unsigned int> id_generator = 1;
    id = id_generator++;
}

//void Node::setLifeSpan()
//{
//    lifespan_2 = _param.tau2 * randomFloat(0.95, 1.05);
//    durationIntermediateState_Max = _param.tau_int * randomFloat(0.95, 1.05);
//    durationMatureState_Max = _param.tau_m * randomFloat(0.95, 1.05);
//}


Node::~Node() {


}

