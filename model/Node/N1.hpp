//
// Created by bgirard on 13/03/23.
//

#ifndef N1_HPP
#define N1_HPP

#include "N2.hpp"
#include "list"

class N0;
class N1 : public Node {
public:
    N1(ModelParam& param, const N0& n0,const Eigen::Vector2<RealType>& position,const World* world= nullptr);
    virtual ~N1(){};

    virtual bool isAlive()const  override {return is_alive;};


    void addN2();
    void compute_n2();
    RealType generateLengthOfNewN2();


//    void setInhibited() { is_inhibited = true; }
//    void setUninhibited() { is_inhibited = false; }
//    bool isInhibited() { return is_inhibited; }
   // bool is_inhibited = false;

    Eigen::Vector2<RealType> get_forces_from_n0();
    Eigen::Vector2<RealType> _get_forces_from_n0;
    virtual Eigen::Vector2<RealType> compute_forces() override;
    virtual void apply_forces() override;
    virtual void step_and_kill() override;


    inline bool isImmature() const { return state==IMMATRURE;}
    inline bool isIntermediate() const{ return state==INTERMEDIATE; }
    inline bool isMature() const { return state==MATURE; }
    void setIntermediate();
    void setMature();

    RealType durationImmatureState = 0;
    RealType durationIntermediateState = 0.0;
    RealType durationMatureState = 0.0;

    virtual size_t childCount() const override
    {
        return _nodes.size();
    }

    virtual RealType getAlpha() const override;

    virtual RealType el_force() const
    {
        return F_el;
    }
    virtual RealType c_force() const
    {
        return F_c;
    }
    virtual RealType previous_force() const
    {
        return _sum_forces_previous.norm();
    }

    RealType L_tau_t=0;
    
    VISITABLE()
    void visit_child(Visitor& v)
    {
            for(N2& n2 : _nodes)
            {
                n2.visit(v);
            }
    }
#ifndef TESTS
protected:
#endif

    enum State{
        IMMATRURE,
        INTERMEDIATE,
        MATURE,
    };
    State state = IMMATRURE;

    std::list<N2> _nodes;

    bool is_alive= true;

    RealType F_el = 0;
    RealType F_c = 0;
    Eigen::Vector2<RealType> _sum_forces_previous;
public:
    const N0& n0;
};


#endif //ANGIOMODEL_N1_HPP
