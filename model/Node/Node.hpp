//
// Created by bgirard on 09/03/23.
//

#ifndef NODE_HPP
#define NODE_HPP
#include "visitable.hpp"
#include "Real.hpp"
#include <eigen3/Eigen/Core>



RealType angle_bettween(const Eigen::Vector2<RealType>& from,const Eigen::Vector2<RealType>& to );
RealType angle_bettween(const RealType& from_x,const RealType& from_y,const RealType& to_x,const RealType& to_y);

struct ModelParam;
/**
 * Represent an abstrat node with one pos ,age,forces
 * */
class Node : Visitable {
public:
    Node(ModelParam& param,const Eigen::Vector2<RealType>& position,const World* world= nullptr);

    virtual ~Node() = 0;
    virtual Eigen::Vector2<RealType> pos() const{return _position;}
    virtual bool isAlive() const = 0;

    virtual Eigen::Vector2<RealType> compute_forces() = 0 ;
    virtual void apply_forces() = 0;

    virtual void step_and_kill() = 0;

    // get friction coef
    virtual RealType getAlpha() const = 0;

    // Use ful for csv format file output
    virtual size_t childCount() const = 0;

    //  norm of sum force apply to this node
    virtual RealType force() const
    {
        return _sum_forces.norm();
    };
#ifndef TESTS
protected:
#endif
    ModelParam& _param;
    Eigen::Vector2<RealType> _sum_forces;
    Eigen::Vector2<RealType> _position;
    const World* _world;

public:
    RealType age = 0; // in seconds
    unsigned int id;
};







#endif //ANGIOMODEL_NODE_HPP
