//
// Created by bgirard on 13/03/23.
//

#ifndef N2_HPP
#define N2_HPP

#include "Node.hpp"

class N1;
class N2 : public Node {
public:
    N2(ModelParam& param,const N1&n1,const Eigen::Vector2<RealType>& position,const World* world= nullptr): Node(param,position,world), n1(n1)
    {

    }
    virtual ~N2();
    virtual bool isAlive() const override {return is_alive;};

    virtual Eigen::Vector2<RealType> compute_forces()  override;
    virtual void apply_forces()  override;
    virtual void step_and_kill() override;
    virtual RealType getAlpha() const override;

    /*
     *  N2 doesn't have child
     *
     */
    virtual size_t childCount() const override
    {
        return 0;
    }

    // For CSV File
    virtual RealType el_force() const
    {
        return F_el;
    };

    VISITABLE()
protected:
    bool is_alive= true;
    RealType F_el = 0;
public:
        const N1& n1;

};


#endif //ANGIOMODEL_N2_HPP
