//
// Created by bgirard on 13/03/23.
//

#ifndef N0_HPP
#define N0_HPP
#include <list>
#include <optional>
#include "N1.hpp"


/*
 *
 * N0 Node are alway alive and contain multiple N1 node
 */
class N0 : public Node {
public:

    N0(ModelParam& param,const Eigen::Vector2<RealType>& position,const World* world= nullptr);
    virtual ~N0(){};

    virtual bool isAlive() const override {return true;};


    std::optional<RealType> generateAngleOfNewN1();
    RealType generateLengthOfNewN1();
    void addN1();

    /**
     * This function add N1 node following rule on paper
     *
     */
    virtual void compute_morphology();


    virtual Eigen::Vector2<RealType> compute_forces() override;
    virtual void apply_forces() override;

    virtual void step_and_kill() override;

    virtual RealType getAlpha() const override;

    virtual size_t childCount() const override
    {
        return _nodes.size();
    }

    VISITABLE()
    void visit_child(Visitor& v)
    {
            for(N1& n1 : _nodes)
            {
                n1.visit(v);
            }
    }
#ifndef TESTS
protected:
#endif
    std::list<N1> _nodes;

};


#endif //ANGIOMODEL_N0_HPP
