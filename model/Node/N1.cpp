//
// Created by bgirard on 13/03/23.
//
#include <iostream>
#include "N0.hpp"
#include "N1.hpp"
#include "N2.hpp"
#include "World.hpp"
#include "OutPutManagers.hpp"
#include "ModelParam.hpp"
#include "Substrate.hpp"

N1::N1(ModelParam &param, const N0 &n0, const Eigen::Vector2<RealType> &position,const World* world) : Node(param,position,world), n0(n0)
{

}

void N1::setIntermediate()
{
    state=INTERMEDIATE;
   if(_world!= nullptr  )
   {
        // notify output manager
            _world->_output.node_event(*this,"transitionIntermediate","undefined");
   }

}

void N1::setMature() {
      state=MATURE;
         if(_world!= nullptr  )
   {        // notify output manager
            _world->_output.node_event(*this,"transitionMature","undefined");
   }
}


RealType N1::generateLengthOfNewN2()
{
    if (_param.l2_Generator == constant_B2_length_generator)
    {
        return _param.l2initial;
    }
    else if (_param.l2_Generator == uniforme_B2_length_generator)
    {
        return _param.random(_param.l2initial_min, _param.l2initial_max);
    }

    return _param.l2initial;



    //return randomFloat(3,param.l2initial); // <--------
}
void N1::addN2()
{
//    if(isInhibited())
//        return;


    //Get angle of N1
    auto angle_n0n_1 =  angle_bettween(Eigen::Vector2<RealType> (1, 0), pos() - n0.pos());

    //Get compute a random angle around angle_n0n_1
    auto angle_ =  angle_n0n_1 + _param.random(-_param.delta_theta_2,_param.delta_theta_2);
    auto length = generateLengthOfNewN2();

    //compute new pos of new N2 node
    Eigen::Vector2<RealType>  rota(length*std::cos(angle_),length*std::sin(angle_));
    Eigen::Vector2<RealType>  t = pos()+rota;

    // Node 2 can only live and be created on substrat
    if(_world == nullptr || _world->substrate().isIn(t))
        _nodes.emplace_back(_param,*this,t,_world);
}

void N1::compute_n2()
{
     int j2 =0;

     //Compute j2 as paper describe it following N1 state
     if (isImmature()) {
         j2 = _param.Poisson_nu2_im();
     }
     else if (isIntermediate()) {
         j2 = _param.Poisson_nu2_int();
     }
     else if (isMature()) {
         j2 = _param.Poisson_nu2_m();
     }

     while(j2 > 0)
     {
                   addN2();
                   j2--;
     }

}


/**
 * Compute force Of B1 branch
 * @return
 */
Eigen::Vector2<RealType> N1::get_forces_from_n0()
{
    Eigen::Vector2<RealType> N0N1  =  (pos() - n0.pos());


    RealType norm_N0N1 = N0N1.norm();
    Eigen::Vector2<RealType> direction_N0N1 = N0N1.normalized();

    F_el = 0;
    F_c = 0;

    if(isImmature())
    {
        F_el = ((_param.k11 - _param.k10) / _param.tau_t * age+_param.k10) * (norm_N0N1 - _param.l1initial);

        // save norm_N0N1 as L_tau_t see paper
        L_tau_t = norm_N0N1;
    }
    else if(isIntermediate() || isMature())
    {
        F_el = _param.k11 * (norm_N0N1 - L_tau_t);
    }

    if(isMature())
    {

        Eigen::Vector2<RealType> F_a = _sum_forces_previous;
        // F_c depend on F_a that depend on F_c !!
        // as approximation, we use the previous sum_force
        F_c = _param.gamma_max__ * (1 - exp(-(F_a.norm()) / _param.F_gamma));

    }
    // save B2 force for efficient computation
    _get_forces_from_n0 = (F_el+F_c)*(-direction_N0N1);
    return _get_forces_from_n0;
}


Eigen::Vector2<RealType> N1::compute_forces()
{
    //Save previous sum_force
    _sum_forces_previous = _sum_forces;

    // sum Forces from N2 Branch
    _sum_forces =  Eigen::Vector2<RealType>(0,0);
    for(auto& n2: _nodes )
    {
        _sum_forces+=-n2.compute_forces();
    }
    // and add Forces from N0 Branch
    _sum_forces+=get_forces_from_n0();

    return _sum_forces;

}
RealType N1::getAlpha() const
{
        if(_world == nullptr || _world->substrate().isIn(pos())) {
            if (isImmature()) {
                return _param.alpha_im(age);
            } else if (isIntermediate()) {
                return _param.alpha_int;
            } else if (isMature()) {
                return _param.alpha1_m();
            }
            return  _param.alpha_int;
        }
        else
        {
            //out of subsrat alpha
            return _param.alpha_free;
        }


}

void N1::apply_forces() {


    // If mature N1 , break and die if force appply no N1 > F_R1
    if(isMature() &&  _sum_forces.norm()>_param.F_R1)
    {
        is_alive= false;
       _world->_output.node_event(*this,"death","rupture");
    }
    else
    {
        for(auto& n2: _nodes )
        {
            n2.apply_forces();
        }

        if(!isMature()) {
            auto alpha = getAlpha();
            // Apply quasi static approximation
            Eigen::Vector2<RealType> vec = (_param.dt / alpha) * _sum_forces;
            _position += vec;
        }
    }

}




void N1::step_and_kill() {
    for(auto& n2: _nodes )
    {
        n2.step_and_kill();
    }

    // Remove the dead
  std::erase_if(
    _nodes,
    [](const auto& n2) { return !n2.isAlive();});

    age+=_param.dt;

    if(isImmature() && age> _param.tau_t )
    {
        //Maturation process
        setIntermediate();
    }
    else if (isIntermediate()) {
            RealType epsilon_tau_t = ((pos() - n0.pos()).norm() - L_tau_t) / L_tau_t;


            if (epsilon_tau_t > _param.epsilon_m)
            {
                //mature is elongated by epsilon_tau_t
                setMature();
            }
            else {

                //or die by time
                durationIntermediateState = durationIntermediateState + _param.dt;
                if(durationIntermediateState>_param.tau_int)
                {
                    is_alive = false;
                    _world->_output.node_event(*this,"death","lifeSpan");
                }
            }
    }
    else if (isMature())
    {

        durationMatureState = durationMatureState + _param.dt;
                        //or die by time
        if(durationMatureState>_param.tau_m)
        {
                    is_alive = false;
                     _world->_output.node_event(*this,"death","lifeSpan");
        }
    }


}
