//
// Created by bgirard on 13/03/23.
//
#include <iostream>
#include "N2.hpp"
#include "N1.hpp"
#include "World.hpp"
#include "OutPutManagers.hpp"
#include "ModelParam.hpp"

Eigen::Vector2<RealType> N2::compute_forces()
{
    _sum_forces = Eigen::Vector2<RealType>(0,0);
    Eigen::Vector2<RealType> N1N2 = pos() - n1.pos();

    //Elastic force compute as Paper
    F_el = _param.k2 * ((N1N2).norm() - _param.l2rest());

    if (_param.B2_tension_only) {
            F_el = std::max(RealType(0.0), F_el);
    }
    //Fel =  max(K2 * (l-l_rest),0)

    // There are only one force on N2 is B2 F_el force
    _sum_forces =  (-N1N2).normalized()*F_el;
    return _sum_forces;
}

RealType N2::getAlpha() const
{
    return _param.alpha2;

}
void N2::apply_forces()
{

   // N2 node are bound, this is not clear in paper
   // N2 cannot exist outsite substrat ,so they can't move

  //Eigen::Vector2<RealType> vec = (_param.dt / N2::getAlpha()) * _sum_forces;
  //TODO N2 node are bound ???
        //_position += vec ;

}

void N2::step_and_kill() {


    // apply rupture force
    if(_sum_forces.norm() > _param.F_R2)
    {
        // rupture
        is_alive=false;
        if(_world != nullptr)
        {
            // notify output manager cause of death
            _world->_output.node_event(*this,"death","rupture");
        }
    }

    age+=_param.dt;

    //Apply age rule
    if ( age >=  _param.tau2 )
    {
        is_alive=false;
                if(_world!= nullptr) {
                                // notify output manager
                    _world->_output.node_event(*this, "death", "lifeSpan");
                }
    }


}

N2::~N2()
{
    if(is_alive && _world)
    {
         // notify output manager cause of death
        _world->_output.node_event(*this,"death","dead parent");
    }

}