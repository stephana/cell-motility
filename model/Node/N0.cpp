//
// Created by bgirard on 13/03/23.
//

#include "N0.hpp"
#include "N1.hpp"
#include <boost/icl/interval_set.hpp>
#include <iostream>
#include "World.hpp"
#include "Substrate.hpp"
#include "ModelParam.hpp"

N0::N0(ModelParam &param, const Eigen::Vector2<RealType> &position, const World *world) : Node(param,position,world)
{
    static std::atomic<unsigned int> id_generator = 1;
    id = id_generator++;
}

/***
 *
 * randomly select a possible angle for a new N1 node
 *
 */
std::optional<RealType>  N0::generateAngleOfNewN1()
{
  boost::icl::interval_set<RealType> availableAngle;
  availableAngle.insert(boost::icl::continuous_interval<RealType>::closed(0, 2 * std::numbers::pi));
  //first all angle are available 0 -> 2π

  Eigen::Vector2<RealType> u(1,0);
  for(const auto& node : _nodes)
  {
     Eigen::Vector2<RealType> branch_vector =  node.pos()-pos();
     auto angle = angle_bettween(u,branch_vector);

     //then we subtract angle around N1 node with Δθ1
     availableAngle.subtract(boost::icl::continuous_interval<RealType>::closed(angle - _param.delta_theta_1, angle + _param.delta_theta_1));

     // do not forget edge
     if(angle-_param.delta_theta_1<0)
     {
            availableAngle.subtract(boost::icl::continuous_interval<RealType>::closed(2 * std::numbers::pi + (angle - _param.delta_theta_1), 2 * std::numbers::pi));
     }
     if(angle+_param.delta_theta_1>2*std::numbers::pi)
     {
       availableAngle.subtract(boost::icl::continuous_interval<RealType>::closed(0, angle + _param.delta_theta_1 - 2 * std::numbers::pi));
     }
  }
  //now availableAngle is only angle whre there are not N1 node
  if(availableAngle.empty())
      return std::optional<RealType>();


  // Here we dont care about open and close intervalle ...
  RealType total_size = 0;
  for(const auto& intervall : availableAngle)
  {
      total_size += intervall.upper()-intervall.lower();
  }
  /*
   *  Here we map availableAngle to 0 -> total_size and select a ramdom number angle between 0 -> total_size
   *  then we map angle in availableAngle domaine
   *
   */


  auto angle = _param.random(0,total_size);
  for(const auto& intervall : availableAngle)
  {
      if(angle>(intervall.upper()- intervall.lower()))
      {
          angle -= intervall.upper() - intervall.lower();
      }
      else {
            return angle + intervall.lower();
      }
  }
      return std::optional<RealType>();
}


RealType N0::generateLengthOfNewN1()
{
    if (_param.l1_Generator == constant_B1_length_generator)
    {
        return _param.l1initial;
    }
    else if (_param.l1_Generator == uniforme_B1_length_generator)
    {
        // TODO Make modélisable ? Unimplemedted
        return _param.random(_param.l1initial_min, _param.l1initial_max);
    }

    return _param.l1initial;
}

void N0::addN1() {

    auto angle = generateAngleOfNewN1();
    // if we can get an angle for a N1 we can add N1 node
    if(angle)
    {
           auto length  = generateLengthOfNewN1();
           auto angle_ = angle.value();
           //Eigen::Rotation2D<RealType> rot(angle_);
           Eigen::Vector2<RealType>  rota(length*std::cos(angle_),length*std::sin(angle_));
           Eigen::Vector2<RealType>  pos_new_n1 = pos()+rota;

           // Node 1 can only be created on substrat
        if(_world == nullptr || _world->substrate().isIn(pos_new_n1))
           _nodes.emplace_back(_param,*this,pos_new_n1,_world);
    }
}
void N0::compute_morphology()
{

   // number N1 node that can be added
   int j1 =  _param.Poisson_nu1();
   while(j1 > 0)
   {
       //add N1 if possible
       addN1();
       j1--;
   }

    // Comute compute_morphology for newyly add N1 and old N1
   for(auto& n1: _nodes )
   {
       n1.compute_n2();
   }

}


Eigen::Vector2<RealType>  N0::compute_forces()
{
    _sum_forces =  Eigen::Vector2<RealType>(0,0);

    // N0 forces are sum  B1 branch force (_get_forces_from_n0)
    // to compute _get_forces_from_n0 we must call  compute_forces() of n1
    for(auto& n1: _nodes )
    {
        n1.compute_forces();
        _sum_forces+=-n1._get_forces_from_n0;
    }

    return _sum_forces;
}

/**
 *
 * @return alpha coef of N0 node
 *  - return alpha_free is the node is node on substrat , alpha0 si n0 is on subtrat
 */
RealType N0::getAlpha() const
{

    if(_world == nullptr || _world->substrate().isIn(pos())) {
        return _param.alpha0;
    }
    else
   {
            //ou of subsrat alpha
            return _param.alpha_free;
   }
}
void N0::apply_forces()
{
    for(auto& n1: _nodes )
    {
        n1.apply_forces();
    }

    if(force() > _param.F_R0) {
        //Quasi static approximation , only move is force > FR0
        Eigen::Vector2<RealType> vec= _param.dt / getAlpha() * _sum_forces;
        _position += vec;

    }

}

void N0::step_and_kill() {
    for(auto& n1: _nodes )
    {
        n1.step_and_kill();
    }

    //remove dead N1 node
    std::erase_if(
    _nodes,
    [](const auto& n1) { return !n1.isAlive();});
    age+=_param.dt;


}

