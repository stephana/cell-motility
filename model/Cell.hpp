//
// Created by bgirard on 08/03/23.
//

#ifndef ANGIOMODEL_CELL_HPP
#define ANGIOMODEL_CELL_HPP
#include "visitable.hpp"
#include "Node/N0.hpp"
/**
 * Cell is define in model as one N0 node
 *
 */
class Cell : Visitable {
public:
    Cell(ModelParam& _param,const Eigen::Vector2<RealType>& position,const World* _world= nullptr);
    virtual ~Cell(){};
    void compute_morphology();

    void forces();
    void step_and_kill();

    VISITABLE()
    void visit_child(Visitor& v)
    {
        _n0.visit(v);
    }

    virtual Eigen::Vector2<RealType> pos() const{return _n0.pos();}
private:
    const ModelParam& _param;
    unsigned int _id;

   // std::set<float> listOfSubstrateAdhesionStrengthFactorsInContactWithCell;


public:
    N0 _n0;
    const World* _world;
};




#endif //ANGIOMODEL_CELL_HPP
