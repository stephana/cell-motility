//
// Created by bgirard on 31/03/23.
//

#ifndef ANGIOMODEL_CIRCURLARBUFFER_HPP
#define ANGIOMODEL_CIRCURLARBUFFER_HPP

#include "chrono"
#include <boost/circular_buffer.hpp>
#include "thread"

template<typename T,size_t SIZE >
class CircularBuffer
{
protected:
        std::allocator<T> alloc_;
    T* _internal;
public:
    CircularBuffer() : alloc_(),_internal(alloc_.allocate(SIZE)) //:_internal(BUFFER_SIZE)
    {

    }
    ~CircularBuffer()
    {
        alloc_.deallocate(_internal, SIZE);
    }
public:

  static constexpr size_t BUFFER_SIZE = SIZE;
  size_t start = 0;// first item occupied
  size_t end   = 0;// first free item

    bool empty() const
    {
        return end==start;
    }
    size_t size() const
    {
             if(end<start)
            {
               return (end+BUFFER_SIZE-start);
            }
            return (end-start);
    }
    size_t free()
    {
        return BUFFER_SIZE-size();
    }
    size_t capacity()
    {
        return BUFFER_SIZE;
    }

    //template<class ...Args>
    bool push_back(const T& t)// Args&& ...args)
    {
        size_t ne = end + 1;
        if(ne >= BUFFER_SIZE)
                ne-= BUFFER_SIZE;
        if(ne == start) // FULL
        {
            using namespace  std::chrono_literals;
            for(unsigned int i=0;i<500 && ne == start ;i++)
                 std::this_thread::sleep_for(10us);
            if(ne == start) // FULL
                return false;
        }
        _internal[end] = t;//copy  // T(std::forward<Args>(args)...);
        end = ne;
        return true;
    }
    template <typename... Args>
     bool emplace_back(Args&& ... args) {

        size_t ne = end + 1;
        if(ne >= BUFFER_SIZE)
                ne-= BUFFER_SIZE;
        if(ne == start) // FULL
        {
            using namespace  std::chrono_literals;
            for(unsigned int i=0;i<500 && ne == start ;i++)
                 std::this_thread::sleep_for(10us);
            if(ne == start) // FULL
                return false;
        }

        std::allocator_traits<std::allocator<T>>::construct(alloc_, std::addressof( _internal[end]),
                                   std::forward<Args>(args) ...);

        end = ne;
        return true;


       }



    T& front()
    {
        return _internal[start];
    }
    void pop_front()
    {
        start++;
        if(start>BUFFER_SIZE)
            start=0;

    }
};

#endif //ANGIOMODEL_CIRCURLARBUFFER_HPP
