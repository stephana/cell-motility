//
// Created by bgirard on 16/03/23.
//

#ifndef ANGIOMODEL_FILESOUTPUT_HPP
#define ANGIOMODEL_FILESOUTPUT_HPP

#include "Config.hpp"
#include "FastWrite.hpp"
#include "OutPutManagers.hpp"

class Cell;
class N0;
class FilesOutput  : public OutPutManager, public Configurable {

public:


    void parametersHeader();
    void writeparameters(const unsigned int &simulationNumber,const ModelParam& c);


    void nodeLifeEventsHeaders();


    void resultsHeader();
    void writeResult(const unsigned int &simulationNumber,const RealType& t,const Cell& c);

    void patternHeader();


    void nodeInfoHeader();
    void branchInfoHeader();
    void writeNodeAndBranchInfo(World& c);

    void snapShotHeader();
    void writeSnapShot(World& c);
    void writeSnapShot(N1& n);
    void writeSnapShot(N2& n);
    void writeSnapShot(N0& n);

    std::array<std::thread,5> _writes_manager;
    std::atomic_bool _run=true;

public:
    FilesOutput();
    void init();
    ~FilesOutput();
    #define CLASS_NAME "FilesOutput"
    MAKE_OPT(std::string,output_path,"./outputs/"," oputput directory path");


    ResultWriter resultsFile;


    std::ofstream parametersFile ;
    NodeEventWriter nodeLifeFile;

    NodeInfoWriter nodeInfoFile ;
    BranchInfoWriter branchInfoFile;


    std::ofstream patternFile;
    SnapshotWriter cellSnapShotFile;


    MAKE_OPT(std::string,resultsOutputFilename,"results.csv","");
    MAKE_OPT(std::string, parametersOutputFilename , "parameters.csv","");
    MAKE_OPT(std::string, nodeLifeOutputFilename , "nodeLife.csv","");
    MAKE_OPT(std::string, nodeInfoOutputFilename , "nodeInfo_regTimeStep.csv","");
    MAKE_OPT(std::string, branchInfoOutputFilename , "branchInfo_regTimeStep.csv","");
    MAKE_OPT(std::string, patternOutputFilename , "pattern.csv","");
    MAKE_OPT(std::string, cellSnapShotOutputFilename , "cellSnapShot.csv","");

    MAKE_OPT_MIN_MAX(unsigned int,saveResultIntervals , 10,1,10000,"");
    unsigned int  interavlle_save = 0;

    void start_model(const unsigned int &, ModelParam &param,World&) override;

    void step_model(const RealType &t, World &w) override;

    virtual void substrate_model(Substrate& substrate,const unsigned int& simulationNumber) override;


    virtual void node_event(const N1& n, const FastStaticString& eventType, const FastStaticString& event) override;
    virtual void node_event(const N2& n, const FastStaticString& eventType, const FastStaticString& event) override;

    virtual void end_model() override {};

    void writeNodeInfo(const N0 &n);

    void writeNodeInfo(const N1 &n);

    void writeNodeInfo(const N2 &n);

    void writeBranchInfo(const N1 &n);

    void writeBranchInfo(const N2 &n);


    const unsigned int *_simulationNumber = nullptr;
    const RealType *_t = nullptr;
    const ModelParam* _param = nullptr;
#undef CLASS_NAME


};


#endif //ANGIOMODEL_FILESOUTPUT_HPP
