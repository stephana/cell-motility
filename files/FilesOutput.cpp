//
// Created by bgirard on 16/03/23.
//

#include <filesystem>
#include <iostream>
#define FMT_USE_FULL_CACHE_DRAGONBOX 1
//#include <fmt/format-inl.h>
#include <fmt/core.h>
#include <fmt/ostream.h>
#include <thread>

#include "Substrate.hpp"
#include "FilesOutput.hpp"
#include "Cell.hpp"
#include "World.hpp"

#include "ModelParam.hpp"

namespace fs = std::filesystem;

template<class T>
void check_stream(T& stream)
{
        if(stream.fail()) {
           std::cout << std::error_code{errno, std::generic_category()}.message();
            throw std::system_error{errno, std::generic_category()};
        }
}
 const size_t bufsize = 256*1024;
char buf[bufsize];

FilesOutput::FilesOutput()
{


}

void FilesOutput::init()
{
    std::filesystem::path output_dir = std::filesystem::path(output_path);
    fs::create_directories(output_dir);



    resultsFile.open(output_dir/resultsOutputFilename);
    parametersFile.open(output_dir/parametersOutputFilename, std::ios_base::trunc);
    check_stream(parametersFile);

    nodeLifeFile.open(output_dir/nodeLifeOutputFilename);
    nodeInfoFile.open(output_dir/nodeInfoOutputFilename );
    branchInfoFile.open(output_dir/branchInfoOutputFilename);

    patternFile.open(output_dir/patternOutputFilename, std::ios_base::trunc );
    cellSnapShotFile.open(output_dir/cellSnapShotOutputFilename);

    parametersHeader();
    nodeLifeEventsHeaders();
    resultsHeader();
    patternHeader();
    nodeInfoHeader();
    branchInfoHeader();
    snapShotHeader();
    // header musth be write first
    _run= true;
    _writes_manager[0] = std::thread(&decltype(nodeInfoFile)::write,&nodeInfoFile,std::ref(_run));
    _writes_manager[1] = std::thread(&decltype(branchInfoFile)::write,&branchInfoFile,std::ref(_run));
    _writes_manager[2] = std::thread(&decltype(nodeLifeFile)::write,&nodeLifeFile,std::ref(_run));
    _writes_manager[3] = std::thread(&decltype(cellSnapShotFile)::write,&cellSnapShotFile,std::ref(_run));
    _writes_manager[4] = std::thread(&decltype(resultsFile)::write,&resultsFile,std::ref(_run));
}

FilesOutput::~FilesOutput()
{
    _run = false;
    for (auto& thread : _writes_manager)
      if (thread.joinable())
        thread.join();

    std::filesystem::path output_dir = std::filesystem::path(output_path);
    if(parametersFile.is_open())
        std::cout<<"Log File are save:"<<absolute((output_dir/resultsOutputFilename));
}


void FilesOutput::resultsHeader()
{
resultsFile.write_header(
            "simulationNumber",
            "t",
            "cell_position.x",
            "cell_position.y",
            "labelRefNb",
            "label");
         // resultsFile<<std::string("NbNodes") << ","; // 6
         // resultsFile<<std::string("countN1s") << ","; // 7
         // resultsFile<<std::string("countN2s") << ","; // 8
         //resultsFile<<std::string("NbInteractions") << ","; // 9
         //resultsFile<<std::string("LengthOfAllInteractions") << ","; // 10
         //resultsFile<<std::string("LengthOfAllB1s") << ","; // 11
         // resultsFile<<std::string("LengthOfAllB2s") << ","; // 12
         //resultsFile<<std::string("N2AgeAtDeath_mean") << ","; // 13
         //resultsFile<<std::string("N2AgeAtDeath_standardDeviation") << ","; // 14
         //resultsFile<<std::string("N1AgeAtDeath_mean") << ","; // 15
         //resultsFile<<std::string("N1AgeAtDeath_standardDeviation") << ","; // 16
         //resultsFile<<std::string("nbImmatureN1Transitions") << ","; // 17
         //resultsFile<<std::string("nbIntermediateN1Transitions") << ","; // 18
         //resultsFile<<std::string("nbMatureImmatureN1Transitions") << ","; // 19
         //resultsFile<<std::string("immatureN1TransitionPeriod_minimum") << ","; // 20
         //resultsFile<<std::string("immatureN1TransitionPeriod_max") << ","; // 21
         //resultsFile<<std::string("immatureN1TransitionPeriod_mean") << ","; // 22
         //resultsFile<<std::string("intermediateN1TransitionPeriod_minimum") << ","; // 23
         //resultsFile<<std::string("intermediateN1TransitionPeriod_maximum") << ","; // 24
         //resultsFile<<std::string("intermediateN1TransitionPeriod_mean") << ","; // 25
         //resultsFile<<std::string("matureN1TransitionPeriod_minimum") << ","; // 26
         //resultsFile<<std::string("matureN1TransitionPeriod_maximum") << ","; // 27
         //resultsFile<<std::string("matureN1TransitionPeriod_mean") << ","; // 28
         //resultsFile<<std::string("matureN1TransitionPeriod_std") << ","; //29
         //resultsFile<<std::string("intermediateN1TransitionPeriod_std") << ","; //30
         //resultsFile<<std::string("nbOfSubstratesAdhesionStrengthFactors"); //31
}
void FilesOutput::writeResult(const unsigned int &simulationNumber,const RealType& t ,const Cell& cell)
{
    resultsFile.emplace(
            simulationNumber,
            t,
            cell.pos().x(),
            cell.pos().y(),
            _param->labelRefNb,
            _param->label
            );
//        resultsFile<< std::to_string(simulationNumber) + ","; // 0
//        resultsFile<< std::to_string(t) + ","; // 1
//        resultsFile<< std::to_string(cell.pos().x()) + ","; // 2
//        resultsFile<< std::to_string(cell.pos().y()) + ","; // 3
//        resultsFile<< std::to_string(_param->labelRefNb) + ","; // 4
//        resultsFile<< _param->label; // 5
       // resultsFile<< std::to_string(cell.getNbNodes()) + ","; // 6
        //resultsFile<< std::to_string(cell.countN1s()) + ","; // 7
        //resultsFile<< std::to_string(cell.countN2s()) + ","; // 8
        //resultsFile<< std::to_string(cell.getNbInteractions()) + ","; // 9
        //resultsFile<< std::to_string(cell.calculateTheLengthOfAllInteractions()) + ","; // 10
        //resultsFile<< std::to_string(cell.calculateTheLengthOfAllB1s()) + ","; // 11
        //resultsFile<< std::to_string(cell.calculateTheLengthOfAllB2s()) + ","; // 12
        /*resultsFile<< std::to_string(nodeTransitionRecords.N2AgeAtDeath_mean()) + ","; // 13
        resultsFile<< std::to_string(nodeTransitionRecords.N2AgeAtDeath_standardDeviation()) + ","; // 14
        resultsFile<< std::to_string(nodeTransitionRecords.N1AgeAtDeath_mean()) + ","; // 15
        resultsFile<< std::to_string(nodeTransitionRecords.N1AgeAtDeath_standardDeviation()) + ","; // 16
        resultsFile<< std::to_string(nodeTransitionRecords.nbImmatureN1Transitions()) + ","; // 17
        resultsFile<< std::to_string(nodeTransitionRecords.nbIntermediateN1Transitions()) + ","; // 18
        resultsFile<< std::to_string(nodeTransitionRecords.nbMatureImmatureN1Transitions()) + ","; // 19
        resultsFile<< std::to_string(nodeTransitionRecords.immatureN1TransitionPeriod_minimum()) + ","; // 20
        resultsFile<< std::to_string(nodeTransitionRecords.immatureN1TransitionPeriod_max()) + ","; // 21
        resultsFile<< std::to_string(nodeTransitionRecords.immatureN1TransitionPeriod_mean()) + ","; // 22
        resultsFile<< std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_minimum()) + ","; // 23
        resultsFile<< std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_maximum()) + ","; // 24
        resultsFile<< std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_mean()) + ","; // 25
        resultsFile<< std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_minimum()) + ","; // 26
        resultsFile<< std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_maximum()) + ","; // 27
        resultsFile<< std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_mean()) + ","; // 28
        resultsFile<< std::to_string(nodeTransitionRecords.matureN1TransitionPeriod_std()) + ","; // 29
        resultsFile<< std::to_string(nodeTransitionRecords.intermediateN1TransitionPeriod_std()) + ","; //30*/
        //resultsFile<< std::to_string(cell.nbSubstrateAdhesionStrengthFactors()); //31

}




void FilesOutput::parametersHeader() {
    parametersFile<<std::string("duration") << ","; // 0
    parametersFile<<std::string("dt") << ","; // 1
    parametersFile<<std::string("simulation_NB") << ","; // 2
    parametersFile<<std::string("tau_int") << ","; // 3
    parametersFile<<std::string("tau_m") << ","; // 4
    parametersFile<<std::string("tau2") << ","; // 5
    parametersFile<<std::string("l1initial") << ","; // 6
    parametersFile<<std::string("l2initial") << ","; // 7
    parametersFile<<std::string("epsilon20") << ","; // 8
    parametersFile<<std::string("k10") << ","; // 9
    parametersFile<<std::string("k11") << ","; // 10
    parametersFile<<std::string("k2") << ","; // 11
    parametersFile<<std::string("gamma_max__") << ","; // 12
    parametersFile<<std::string("B1_tension_only") << ","; // 13
    parametersFile<<std::string("B2_tension_only") << ","; // 14
    parametersFile<<std::string("epsilon_m") << ","; // 15
    parametersFile<<std::string("tau_t") << ","; // 16
    parametersFile<<std::string("alpha_int") << ","; // 17
    parametersFile<<std::string("alpha0") << ","; // 18
    parametersFile<<std::string("alpha10") << ","; // 19
    parametersFile<<std::string("alpha1_m") << ","; // 20
    parametersFile<<std::string("alpha2") << ","; // 21
    parametersFile<<std::string("delta_theta_1") << ","; // 22
    parametersFile<<std::string("delta_theta_2") << ","; // 23
    parametersFile<<std::string("F_R0") << ","; // 24
    parametersFile<<std::string("F_R1") << ","; // 25
    parametersFile<<std::string("F_R2") << ","; // 26
    parametersFile<<std::string("F_gamma") << ","; // 27
    parametersFile<<std::string("nu1") << ","; // 28
    parametersFile<<std::string("nu2_im") << ","; // 29
    parametersFile<<std::string("nu2_int") << ","; // 30
    parametersFile<<std::string("nu2_m") << ","; // 31
    parametersFile<<std::string("labelRefNb") << ","; // 32
    parametersFile<<std::string("label") << ","; // 33
    parametersFile<<std::string("label_x") << ","; // 34
    parametersFile<<std::string("label_y") << ","; // 35
    parametersFile<<std::string("adhesion_coeficient_FR012"); //36
    parametersFile<<std::string("\n"); //

}
void FilesOutput::writeparameters(const unsigned int &simulationNumber,const ModelParam& param)
{

    parametersFile<< std::to_string(param.duration) + ","; // 0
    parametersFile<< std::to_string(param.dt) + ","; // 1
    parametersFile<< std::to_string(simulationNumber) + ","; // 2
    parametersFile<< std::to_string(param.tau_int) + ","; // 3
    parametersFile<< std::to_string(param.tau_m) + ","; // 4
    parametersFile<< std::to_string(param.tau2) + ","; // 5
    parametersFile<< std::to_string(param.l1initial) + ","; // 6
    parametersFile<< std::to_string(param.l2initial) + ","; // 7
    parametersFile<< std::to_string(param.epsilon20) + ","; // 8
    parametersFile<< std::to_string(param.k10) + ","; // 9
    parametersFile<< std::to_string(param.k11) + ","; // 10
    parametersFile<< std::to_string(param.k2) + ","; // 11
    parametersFile<< std::to_string(param.gamma_max__) + ","; // 12
    parametersFile<< std::to_string(param.B1_tension_only) + ","; // 13
    parametersFile<< std::to_string(param.B2_tension_only) + ","; // 14
    parametersFile<< std::to_string(param.epsilon_m) + ","; // 15
    parametersFile<< std::to_string(param.tau_t) + ","; // 16
    parametersFile<< std::to_string(param.alpha_int) + ","; // 17
    parametersFile<< std::to_string(param.alpha0) + ","; // 18
    parametersFile<< std::to_string(param.alpha10) + ","; // 19
    parametersFile<< std::to_string(param.alpha1_m()) + ","; // 20
    parametersFile<< std::to_string(param.alpha2) + ","; // 21
    parametersFile<< std::to_string(param.delta_theta_1) + ","; // 22
    parametersFile<< std::to_string(param.delta_theta_2) + ","; // 23
    parametersFile<< std::to_string(param.F_R0) + ","; // 24
    parametersFile<< std::to_string(param.F_R1) + ","; // 25
    parametersFile<< std::to_string(param.F_R2) + ","; // 26
    parametersFile<< std::to_string(param.F_gamma) + ","; // 27
    parametersFile<< std::to_string(param.nu1) + ","; // 28
    parametersFile<< std::to_string(param.nu2_im) + ","; // 29
    parametersFile<< std::to_string(param.nu2_int) + ","; // 30
    parametersFile<< std::to_string(param.nu2_m) + ","; // 31
    parametersFile<< std::to_string(param.labelRefNb) + ","; // 32
    parametersFile<< param.label + ","; // 33
    parametersFile<< param.label_x + ","; // 34
    parametersFile<< param.label_y + ","; // 35
    parametersFile<< std::to_string(param.adhesion_coeficient_FR012);
    parametersFile<< std::string("\n");

}
void FilesOutput::nodeLifeEventsHeaders()
{
    nodeLifeFile.write_header(
    "t",
    "nodeRefNB",
    "simulationRefNb",
    "label",
    "label_x",
    "label_y",
    "simdetailed_label",
    "nodeAge",
    "eventType",
    "nodeOrder",
    "nodeState",
    "durationImmatureState",
    "durationIntermediateState",
    "durationMatureState",
    "causeOfDeath",
    "netForce");
}

void FilesOutput::node_event(const N1& n, const FastStaticString& eventType, const FastStaticString& event)
{

    if(_t== nullptr)
        return;

    nodeLifeFile.emplace(
      *_t,
      n.id,
       *_simulationNumber,
      _param->label ,
     _param->label_x ,
    _param->label_y ,
    _param->simdetailed_label ,
    n.age,
    eventType,
    "N1",
    (n.isMature()?"mature":(n.isIntermediate()?"intermediate":(n.isImmature()?"immature":""))) ,
    n.durationImmatureState,
    n.durationIntermediateState,
    n.durationMatureState,
    event,
    n.previous_force());
}
void FilesOutput::node_event(const N2& n, const FastStaticString& eventType, const FastStaticString& event)
{

    if(_t== nullptr)
        return;

    nodeLifeFile.emplace(
      *_t,
      n.id,
       *_simulationNumber,
     _param->label ,
     _param->label_x ,
    _param->label_y ,
    _param->simdetailed_label ,
    n.age,
    eventType,
    "N2",
    "none",
    0,
    0,
    0,
    event,
    n.force());
}







void FilesOutput::nodeInfoHeader()
{
    nodeInfoFile.write_header(
    "t",
    "nodeRefNb",
    "simulationRefNb",
    "simlabel",
    "simlabel_x",
    "simlabel_y",
    "simdetailed_label",
    "age",
    "netForce",
    "alpha",
    "isN0",
    "isN1",
    "isN2",
    "isImmature",
    "isIntermediate",
    "isMature",
    "countNbOfChildNodesOnThisNode",
    "x",
    "y");
    
}

void FilesOutput::writeNodeInfo(const N0& n)
{
    nodeInfoFile.emplace(
   *_t,
   n.id,
   *_simulationNumber,
     _param->label ,
     _param->label_x ,
    _param->label_y ,
    _param->simdetailed_label ,
   n.age,
   n.force(),
   n.getAlpha(),

   true,
   false,
   false,
   false,
   false,
   false,

   n.childCount(),

   n.pos().x(),
   n.pos().x());

}
void FilesOutput::writeNodeInfo(const N1& n)
{
    nodeInfoFile.emplace(
    *_t,
   n.id,
   *_simulationNumber,
   _param->label ,
    _param->label_x ,
   _param->label_y ,
    _param->simdetailed_label ,
   n.age,
   n.force(),
   n.getAlpha(),

   false,
   true,
   false,
   n.isImmature(),
   n.isIntermediate(),
   n.isMature(),

   n.childCount(),

   n.pos().x(),
   n.pos().x());

}
void FilesOutput::writeNodeInfo(const N2& n)
{
        nodeInfoFile.emplace(
    *_t,
   n.id,
   *_simulationNumber,
    _param->label ,
    _param->label_x ,
     _param->label_y ,
    _param->simdetailed_label ,
   n.age,
   n.force(),
   n.getAlpha(),

   false,
   false,
   true,
   false,
   false,
   false,

   n.childCount(),

   n.pos().x(),
   n.pos().x());

}


struct InfoVisitor : public Visitor
{
    FilesOutput& out;
    InfoVisitor(FilesOutput& _out) : out(_out){}
    virtual void visit(Cell &v) override
    {
        v.visit_child(*this);
    }

    virtual void visit(N0 &v) override
    {
        out.writeNodeInfo(v);

        v.visit_child(*this);
    }
    virtual void visit(N1 &v) override
    {
        out.writeNodeInfo(v);
        out.writeBranchInfo(v);
        v.visit_child(*this);
    }
    virtual void visit(N2 &v) override
    {
        out.writeNodeInfo(v);
        out.writeBranchInfo(v);
    }
};


void FilesOutput::writeNodeAndBranchInfo(World& w)
{
    InfoVisitor v(*this);
    for(auto& c : w.cells())
    {
        v.visit(c);
    }

}



void FilesOutput::branchInfoHeader()
{

    branchInfoFile.write_header(
            "t",
            "branchRefNb",
            "simulationRefNb",
               "simlabel",
   "simlabel_x",
   "simlabel_y",
   "simdetailed_label",

   "branchOrder",
   "parentNodeRefNb",
   "childNodeRefNb",
   "parentNodeNetForce",
   "childNodeNetForce",
   "branchAge",
   "parentNodeAge",
   "childNodeAge",
   "F",
   "F_el",
   "F_c",
   "childNodeIsImmature",
   "childNodeIsIntermediate",
   "childNodeIsMature",

     "L0_spring",
    "distanceNodesAB"
            );

}



void FilesOutput::writeBranchInfo(const N1& n)
{
   branchInfoFile.emplace(
    *_t,
    n.id , // branchRefNb
    *_simulationNumber ,
    _param->label.c_str() ,
    _param->label_x ,
    _param->label_y ,
    _param->simdetailed_label,
    1 ,
    n.n0.id ,
    n.id ,
    n.n0.force() ,
    n.force() ,

   n.age ,
    n.n0.age ,
    n.age ,

    n._get_forces_from_n0.norm() ,
    n.el_force() ,
    n.c_force(),
   // std::to_string(branch.F_c) ,

    n.isImmature() ,
    n.isIntermediate() ,
    n.isMature(),

    n.L_tau_t,
    (n.n0.pos() - n.pos()).norm()
    );

   //fmt::format_to(fmt::appender(buf),"{},", std::to_string(branch.L0_spring) );
    //branchInfoFile<< std::to_string(branch.distanceNodesAB) );
    //branchInfoFile<< std::to_string(branch.epsilon);

    //branchInfoFile<<buf.data() << "\n"; //
}
void FilesOutput::writeBranchInfo(const N2& n)
{
//   fmt::format_to(fmt::appender(buf),"{},",*_t );
//   fmt::format_to(fmt::appender(buf),"{},", n.id ); // branchRefNb
//   fmt::format_to(fmt::appender(buf),"{},", *_simulationNumber );
//   fmt::format_to(fmt::appender(buf),"{},", _simlabel->label );
//   fmt::format_to(fmt::appender(buf),"{},", _simlabel->label_x );
//   fmt::format_to(fmt::appender(buf),"{},", _simlabel->label_y );
//   fmt::format_to(fmt::appender(buf),"{},", _simlabel->simdetailed_label );
//
//   fmt::format_to(fmt::appender(buf),"{},", 1 );
//   fmt::format_to(fmt::appender(buf),"{},", n._n1.id );
//   fmt::format_to(fmt::appender(buf),"{},", n.id );
//   fmt::format_to(fmt::appender(buf),"{},", n._n1.force() );
//   fmt::format_to(fmt::appender(buf),"{},", n.force() );
//
//   fmt::format_to(fmt::appender(buf),"{},",n.age );
//   fmt::format_to(fmt::appender(buf),"{},", n._n1.age );
//   fmt::format_to(fmt::appender(buf),"{},", n.age );
//
//   fmt::format_to(fmt::appender(buf),"{},", n.force() );
//   //fmt::format_to(fmt::appender(buf),"{},", std::to_string(branch.F_el) );
//   //fmt::format_to(fmt::appender(buf),"{},", std::to_string(branch.F_c) );
//
//   fmt::format_to(fmt::appender(buf),"{},", std::to_string(false) );
//   fmt::format_to(fmt::appender(buf),"{},", std::to_string(false) );
//   fmt::format_to(fmt::appender(buf),"{},", std::to_string(false) );



  branchInfoFile.emplace(
    *_t
    ,n.id
    ,*_simulationNumber
    ,_param->label
    , _param->label_x
    , _param->label_y
    , _param->simdetailed_label
    , 2
    , n.n1.id
    , n.id
    , n.n1.force()
    , n.force()
  ,n.age
  , n.n1.age
  , n.age

  , n.force()
   ,n.el_force()
   ,0.0

  , false
  , false
  , false

  ,   _param->l2rest()
  ,  (n.n1.pos() - n.pos()).norm()
  );
   //fmt::detail::vformat_to();

   //fmt::print(branchInfoFile, "{},", std::to_string(branch.L0_spring) + ",";
    //branchInfoFile<< std::to_string(branch.distanceNodesAB) + ",";
    //branchInfoFile<< std::to_string(branch.epsilon
//
//    //branchInfoFile<<buf.data() << "\n"; //
}
void FilesOutput::snapShotHeader()
{
      cellSnapShotFile.write_header("simulationNumber","t","nodeRefNb"
                                    ,
                                    "nodeType",
                                    "this.x",
                                    "this.y","this.Radius"
                                             ,"color.R","color.G","color.B",
                                             "hasParent","parent.x","parent.y","parent.Radius");

}

struct SnapShotVisitor : public Visitor
{
    FilesOutput& out;
    SnapShotVisitor(FilesOutput& _out) : out(_out){}
    virtual void visit(World &v) override
    {
        v.visit_child(*this);
    }
    virtual void visit(Cell &v) override
    {
        v.visit_child(*this);
    }

    virtual void visit(N0 &v) override
    {
        out.writeSnapShot(v);
        v.visit_child(*this);
    }
    virtual void visit(N1 &v) override
    {
        out.writeSnapShot(v);
        v.visit_child(*this);
    }
    virtual void visit(N2 &v) override
    {
        out.writeSnapShot(v);
    }
};

void FilesOutput::writeSnapShot(World& w)
{
    SnapShotVisitor v(*this);
    v.visit(w);
}
void FilesOutput::writeSnapShot(N0 &n) {

cellSnapShotFile.emplace(*_simulationNumber,*_t,
            n.id,
            0,
            n.pos().x(),
            n.pos().y(),
            _param->radiusN0,
            0,
            0,
            0,
            false,
            0,
            0,
            0
        );
}
void FilesOutput::writeSnapShot(N1 &n) {

cellSnapShotFile.emplace(*_simulationNumber,*_t,
            n.id,
            1,
            n.pos().x(),
            n.pos().y(),
            _param->radiusN1,
            0,
            0,
            0,
            true,
            n.n0.pos().x(),
            n.n0.pos().y(),
            _param->radiusN0
        );
}
void FilesOutput::writeSnapShot(N2 &n) {

cellSnapShotFile.emplace(*_simulationNumber,*_t,
            n.id,
            2,
            n.pos().x(),
            n.pos().y(),
            _param->radiusN2,
            0,
            0,
            0,
            true,
            n.n1.pos().x(),
            n.n1.pos().y(),
            _param->radiusN1
        );
}


void FilesOutput::patternHeader()
{
    patternFile<<"simulationNumber,labelRefNb,patternType,radius,adhesionStrengthFactor,nodeA.x,nodeA.y,nodeB.x,nodeB.y,nodeC.x,nodeC.y,nodeD.x,nodeD.y\n";
}

class VisitPatern : public Visitor
{
public:
    VisitPatern( FilesOutput& out_) : out(out_)
    {}
    FilesOutput& out;
    virtual void visit(Substrate & v){
        v.visit_child(*this);
    }
    virtual void visit(SubstratePattern_Circle & v){

   out.patternFile<< std::to_string(*out._simulationNumber) + ",";
   out.patternFile<< std::to_string(out._param->labelRefNb) + ",";
   out.patternFile<< std::string("circle") + ",";
   out.patternFile<< std::to_string(v.radius) + ",";
   out.patternFile<< std::to_string(v.getAdhesionStrength()) + ",";
   out.patternFile<< std::to_string(v.pos().x()) + "," + std::to_string(v.pos().y())  + "\n";

    }

        virtual void visit(SubstratePattern_Rectangle & v){

   out.patternFile<< std::to_string(*out._simulationNumber) + ",";
   out.patternFile<< std::to_string(out._param->labelRefNb) + ",";
   out.patternFile<< std::string("rectangle") + ",";
   out.patternFile<< std::string ("no radius") + ",";
   out.patternFile<< std::to_string(v.getAdhesionStrength()) + ",";
   out.patternFile<<std::to_string(v.nodeA.x()) + "," + std::to_string(v.nodeA.y()) + ",";
   out.patternFile<< std::to_string(v.nodeB.x()) + "," + std::to_string(v.nodeB.y()) + ",";
   out.patternFile<< std::to_string(v.nodeC.x()) + "," + std::to_string(v.nodeC.y());
   out.patternFile<< std::to_string(v.nodeD.x()) + "," + std::to_string(v.nodeD.y());
   out.patternFile<<"\n";

    }


    virtual void visit(SubstratePattern_Triangle & v){

   out.patternFile<< std::to_string(*out._simulationNumber) + ",";
   out.patternFile<< std::to_string(out._param->labelRefNb) + ",";
   out.patternFile<< std::string("triangle") + ",";
   out.patternFile<< std::string ("no radius") + ",";
   out.patternFile<< std::to_string(v.getAdhesionStrength()) + ",";
   out.patternFile<<std::to_string(v.nodeA.x()) + "," + std::to_string(v.nodeA.y()) + ",";
   out.patternFile<< std::to_string(v.nodeB.x()) + "," + std::to_string(v.nodeB.y()) + ",";
   out.patternFile<< std::to_string(v.nodeC.x()) + "," + std::to_string(v.nodeC.y());
   out.patternFile<<"\n";

    }
};

void FilesOutput::substrate_model(Substrate &substrate,const unsigned int& simulationNumber) {

    VisitPatern p(*this);
    p.visit(substrate);

}

void FilesOutput::start_model(const unsigned int &sn, ModelParam &param,World& w) {

    _simulationNumber = &sn;
    _param = & param;
    interavlle_save = 0;
    writeResult(sn,0.0,w.cells()[0]);
    writeparameters(sn,param);

}

void FilesOutput::step_model(const RealType &t, World &w) {

        _t = &t;

        if(interavlle_save>= saveResultIntervals) {
            writeResult(*_simulationNumber, t,  w.cells()[0]);
            writeNodeAndBranchInfo(w);
            writeSnapShot(w);

            interavlle_save=0;
        }
        interavlle_save++;
}