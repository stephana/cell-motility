//
// Created by bgirard on 17/03/23.
//

#ifndef ANGIOMODEL_FASTWRITE_HPP
#define ANGIOMODEL_FASTWRITE_HPP
#define FMT_USE_FULL_CACHE_DRAGONBOX 1

#include "Real.hpp"

#include "tuple"
#include "vector"
#include "mutex"
#include "fstream"
#include "iostream"
#include "fmt/ostream.h"
#include "thread"
#include "fmt/ranges.h"
#include <filesystem>
#include "FastString.hpp"
#include "fmt/compile.h"
#include "CircurlarBuffer.hpp"

template<> struct fmt::formatter<FastStaticString>
{
    template <typename FormatContext>
    auto format(const FastStaticString& role, FormatContext &ctx) const
    {
        return fmt::format_to(ctx.out(), "{}", role.c_str());
    }

  constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {

      return ctx.begin();
  }
};



/***
 * This Structure is optimieze to write data to a CSV file.
 * the data are stored in a Fast CircularBuffer
 * And write in funciton write
 *
 * Usage: first write header with write_header
 * thatn lauch write in separate thread
 * then write data with emplace_back
 * */
template<size_t SIZE ,typename ...Types>
class FastWrite{

    /** FastStructure
     *  Is the internal struct to keep data in buffer
     * */

    template<typename ... T>
    struct FastStructure {
                template<class W>
                inline void write(W& buffer,bool f = false)
                {
                       fmt::format_to(std::back_inserter(buffer), "\n");
                }
    };

    template<typename T, typename ... Rest>
    struct FastStructure<T, Rest ...>
    {
        FastStructure(const T& first, const Rest& ... rest)
            : first(first)
            , rest(rest...)
        {}
        FastStructure()
        {}
        T first = T();
        FastStructure<Rest ... > rest;
        template<class W>
        inline void write(W& buffer,bool f = true)
        {
                if(f)
                    fmt::format_to(std::back_inserter(buffer), FMT_COMPILE("{}"), first);
                else
                    fmt::format_to(std::back_inserter(buffer), FMT_COMPILE(",{}"), first);
                rest.write(buffer,false);

        };
    };
    /**
     * Specilisation for RealType(double or float) to optimize conversion
     * from Real to string as this problems is very hard
     * */
    template<typename ... Rest>
    struct FastStructure<RealType , Rest ...>
    {
        FastStructure(const RealType& first, const Rest& ... rest)
            : first(first)
            , rest(rest...)
        {}
        FastStructure()
        {}
        RealType first = RealType();
        FastStructure<Rest ... > rest;
        template<class W>
        inline void write(W& buffer,bool f = true)
        {
                if(f)
                    fmt::format_to(std::back_inserter(buffer), FMT_COMPILE("{:.5f}"), first);
                else
                   fmt::format_to(std::back_inserter(buffer), FMT_COMPILE(",{:.5f}"), first);
                rest.write(buffer,false);

        };
    };

    /**
     * Specilisation for FastStaticString
     *
     * @tparam Rest
     */
    template<typename ... Rest>
    struct FastStructure<FastStaticString, Rest ...>
    {
        FastStructure(const FastStaticString& first, const Rest& ... rest)
            : first(first)
            , rest(rest...)
        {}
        FastStructure()
        {}
        FastStaticString first = FastStaticString();
        FastStructure<Rest ... > rest;
        template<class W>
        inline void write(W& buffer,bool f = true)
        {
                if(f)
                      buffer.append(first);
                else {
                    buffer.append(",");
                    buffer.append(first);
                }


                rest.write(buffer,false);

        };
    };
      //std::deque<std::tuple<Types...>> q;
  CircularBuffer<FastStructure<Types...>,SIZE> arr_q;
    
public:
    FastWrite()
    {
    }
    void open(const std::filesystem::path& path)
    {

            _stream.rdbuf()->pubsetbuf(buf, bufsize);
            _stream.exceptions(~std::ios::goodbit);
            _stream.open(path, std::ios_base::trunc);
                if(_stream.fail()) {
                        std::cout << std::error_code{errno, std::generic_category()}.message();
                          throw std::system_error{errno, std::generic_category()};
                     }
    }

    template<class ...Args>
    void write_header(const Args&...hearders)
    {
            static_assert( sizeof...(Args)==sizeof...(Types), "wrong number of header" );
            fmt::print(_stream,"{}\n", fmt::join(std::make_tuple(hearders...), ","));
    }

    template<class ...Args>
    void emplace(Args&& ...args)
    {
        {
            {
                //std::scoped_lock lock(m);
                /**
                 * Here emplace back will wait a little amount of time in arr_q is full
                 * but if arr_q is full too long time emplace_back fail and return false
                 */
                if(!arr_q.emplace_back(std::forward<Args>(args)...))
                {
                     throw std::runtime_error("BUFFER_SIZE EXXECEDE");
                }
            }


//            if(arr_q.free()*1.0/arr_q.capacity()<0.25)
//            {
//             std::cout<<arr_q.free()*1.0/arr_q.capacity()<<std::endl;
//            }

            //q.emplace_back(std::forward<Args>(args)...);
        }
    }
    void write(std::atomic_bool& _run)
    {
        // on ecrit tant que le programme tourne(_run) et que il y aquelque chose a ecrire
        // buffer_size !=0
            unsigned int yield = 0;
            while(_run || !arr_q.empty())
            {
                if(arr_q.empty())//  si il n'y a rien a ecrire
                {
                    std::this_thread::yield();
                    yield++;
                    using namespace std::chrono_literals;
                    if(yield>3)
                        std::this_thread::sleep_for(1us);
                    continue;
                }
                yield = 0;


            //auto& t = q.front();
            auto &t = arr_q.front();
            boost::beast::static_string<512> buffer;
            t.write(buffer);
            _stream.write(buffer.data(),buffer.size());
            //fmt::print(_stream, {buffer.data(), buffer.size()});
            //fmt::print(_stream,"{}\n", fmt::join(t, ","));
            //std::scoped_lock lock(m);
            arr_q.pop_front();
            //std::this_thread::yield();
        }
    }



  //Buf size  for oftream
  static constexpr size_t bufsize = 256*1024;
  char buf[bufsize];
  mutable std::mutex m;

  std::ofstream _stream;
};


constexpr auto SIZE_RING = 500000;//

//Define Type per CSV File

using  BranchInfoWriter =  FastWrite<SIZE_RING,RealType,unsigned int,unsigned int,
   FastStaticString ,FastStaticString,FastStaticString ,FastStaticString ,
    unsigned int,//branchOrder
    unsigned int,//parentNodeRefNb
    unsigned int,//childNodeRefNb

    RealType,//parentNodeNetForce
    RealType,//childNodeNetForce
    RealType,//branchAge

    RealType,//parentNodeAge
    RealType,//childNodeAge
    RealType,//F
    RealType,//F_el
    RealType,//F_c

    bool,
    bool,
    bool,

    RealType,
    RealType
    >;


using  NodeInfoWriter =  FastWrite<SIZE_RING,RealType,unsigned int,unsigned int,
    FastStaticString,FastStaticString,FastStaticString,FastStaticString,
    unsigned int,
    unsigned int,
    RealType ,

    bool,
    bool,
    bool,
    bool,
    bool,
    bool,

    unsigned int,
    RealType ,
    RealType
    >;


using  NodeEventWriter =  FastWrite<SIZE_RING,RealType,unsigned int,unsigned int,
    FastStaticString,FastStaticString,FastStaticString,FastStaticString,
    RealType ,

    FastStaticString,
    FastStaticString,
    FastStaticString,
    RealType,
    RealType,
    RealType,

    FastStaticString,
    RealType
    >;

using  SnapshotWriter =  FastWrite<SIZE_RING,unsigned int,RealType,
unsigned int,
unsigned int,

    RealType,
    RealType,
    RealType,
    RealType,
    RealType,
    RealType,
    bool,
    RealType,
    RealType,
    RealType
    >;


using  ResultWriter =  FastWrite<100000,unsigned int,RealType,

    RealType,
    RealType,

    unsigned int,
    FastStaticString
    >;


#endif //ANGIOMODEL_FASTWRITE_HPP
