#include <iostream>
#include "simulations_manager.hpp"
#include "FilesOutput.hpp"
#include "UiOutput.hpp"
#include "ModelParam.hpp"

#include <boost/program_options.hpp>

#ifdef TESTS
#include <gtest/gtest.h>
#endif





#if __linux__
extern "C"
{
    #include <X11/Xlib.h> // for XInitThreads
};
#endif

#include <fenv.h>
int main(int argc, char* argv[])
{

std::ios_base::sync_with_stdio(false);//fast output
#if __APPLE__

#elif __linux__

    // detect overflow on linux
    feenableexcept(FE_INVALID   |
                   FE_DIVBYZERO |
                   FE_OVERFLOW  |
                   FE_UNDERFLOW);

    //use mulitthred with sfml on linux
    XInitThreads();
#endif

    //List of option
    bo::options_description global_opts;
    global_opts.add_options()
    ("help",bo::bool_switch()->default_value(false)->notifier([&global_opts](bool v){
        if(v)
            std::cout<<global_opts<<std::endl;
    } ),"")
    ("file",bo::value<std::string>()->default_value("paper.ini"), "configuration file");


    // Plugin list
    auto ui_ptr =  UiOutput();
    // UI option
    global_opts.add(ui_ptr.opts);


    auto file_ptr =  FilesOutput();
    // csv File  option
    global_opts.add(file_ptr.opts);

    OutPutManagers mng;
    global_opts.add_options()("ui",bo::bool_switch()->default_value(false)->notifier([&mng,&ui_ptr](bool v){
        if(v)
        {
            ui_ptr.init();
            mng.add(ui_ptr);
        }
    } ),"Run with ui output");

    global_opts.add_options()("files",bo::bool_switch()->default_value(false)->notifier([&mng,&file_ptr](bool v){
        if(v)
        {
            file_ptr.init();
            mng.add(file_ptr);
        }
    } ),"Run with files output");

    global_opts.add_options()("test",bo::bool_switch()->default_value(false) ,"Run TEST ");

    ModelParam mp;
    // Model param
    global_opts.add(mp.opts);


    // Parse command line // Priority on command line
    bo::parsed_options po = bo::command_line_parser(argc, argv).options(global_opts).allow_unregistered().run();
    bo::variables_map vm;
    store(po, vm);
    auto config_file = vm.at("file").as<std::string>();
    std::ifstream ifs(config_file);
    if (ifs) {
        // Parse File
        store(parse_config_file(ifs, global_opts, true), vm);
    }
    else
    {
        // warnin if File is miising
        std::cerr <<"Config file not found:"<<config_file<<std::endl;
        std::cerr<<std::error_code{errno, std::generic_category()}.message();
    }
    notify(vm);

    if(vm.contains("help") && vm["help"].as<bool>())
    {
        return 0;
    }

   #ifdef TESTS
    if(vm.contains("test") && vm["test"].as<bool>())
    {
       ::testing::InitGoogleTest(&argc, argv);
       return RUN_ALL_TESTS();
    }
   #endif


    #if __APPLE__
    // On appel windowui ui musit be the main thread
    if(vm.contains("ui") && vm["ui"].as<bool>())
    {
        std::thread a(simulations_manager,std::ref(mp),std::ref(mng));
        ui_ptr.events();
        a.join();
    }
    else
    {
        simulations_manager(mp,mng);
    }
    #else
        simulations_manager(mp,mng);
    #endif
    //call_unitest();

//    std::shared_ptr<ParametersObj> param_ptr = std::make_shared<ParametersObj>();
//    std::shared_ptr<sf::RenderWindow> window_ptr = std::make_shared<sf::RenderWindow>(sf::VideoMode(param_ptr->ScreenWidth, param_ptr->ScreenHeight), "Angio Model V0");
//    QApplication applicationQT(argc, argv);
//    std::shared_ptr <MainWindow> winQt_ptr = std::make_shared<MainWindow>(param_ptr, window_ptr);
//    winQt_ptr->show();
//    multipleSimulationsManager(*param_ptr, window_ptr, *winQt_ptr);
//    applicationQT.exec();
//    return EXIT_SUCCESS;
};